#!/bin/bash

#for i in $@ ; do

#FILES=*.svg
FILES=$@

for f in $FILES
do
  echo "Converting svg file $f to pdf file ${f%.svg}.pdf"
  convert $f ${f%.svg}.pdf
done
