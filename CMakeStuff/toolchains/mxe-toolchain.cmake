# File:///home/langella/developpement/git/pappsomspp/CMakeStuff/toolchains/mxe-toolchain.cmake# 
# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE= 1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)


find_package(ZLIB REQUIRED)

find_package(SQLite3 REQUIRED)


#install MXE qtsvg package
#langella@piccolo:/media/langella/pappso/mxe$ make qtsvg


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIR "${HOME_DEVEL_DIR}/qcustomplot/development")
set(QCustomPlot_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mxe/libqcustomplot.dll")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


if(MAKE_TEST)

	set(Quazip5_FOUND 1)
	set(Quazip5_INCLUDE_DIRS "${HOME_DEVEL_DIR}/quazip5/development")
	set(Quazip5_LIBRARY "${HOME_DEVEL_DIR}/quazip5/build-area/mxe/libquazip5.dll") 
	if(NOT TARGET Quazip5::Quazip5)
		add_library(Quazip5::Quazip5 UNKNOWN IMPORTED)
		set_target_properties(Quazip5::Quazip5 PROPERTIES
			IMPORTED_LOCATION             "${Quazip5_LIBRARY}"
			INTERFACE_INCLUDE_DIRECTORIES "${Quazip5_INCLUDE_DIRS}")
	endif()

endif()


set(OdsStream_QT5_FOUND 1)
set(OdsStream_INCLUDE_DIR "c:/msys64/home/mydar/devel/odsstream/src")
set(OdsStream_QT5_LIBRARY "c:/msys64/home/mydar/devel/odsstream/build-area/mxe/src/libodsstream-qt5.dll")
if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION             "${OdsStream_QT5_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIR}"
		)
endif()


set(Alglib_FOUND 1)
set(Alglib_INCLUDE_DIR "${HOME_DEVEL_DIR}/alglib/development/src")
set(Alglib_LIBRARY "${HOME_DEVEL_DIR}/alglib/build-area/mxe/libalglib.dll") 
if(NOT TARGET Alglib::Alglib)
	add_library(Alglib::Alglib UNKNOWN IMPORTED)
	set_target_properties(Alglib::Alglib PROPERTIES
		IMPORTED_LOCATION             "${Alglib_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIR}")
endif()

set(RData_FOUND 1)
set(RData_INCLUDE_DIR "/win64/librdata/src")
set(RData_LIBRARY "/win64/librdata/.libs/librdata-0.dll") 
if(NOT TARGET RData::Core)
	add_library(RData::Core UNKNOWN IMPORTED)
	set_target_properties(RData::Core PROPERTIES
		IMPORTED_LOCATION             "${RData_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${RData_INCLUDE_DIR}")
endif()

# All this belly dance does not seem necessary. Just perform like for the other
# libraries...
# Look for the necessary header
set(Zstd_INCLUDE_DIR ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
mark_as_advanced(Zstd_INCLUDE_DIR)
set(Zstd_INCLUDE_DIRS ${Zstd_INCLUDE_DIR})
# Look for the necessary library
set(Zstd_LIBRARY ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/libzstd.dll)
mark_as_advanced(Zstd_LIBRARY)
# Mark the lib as found
set(Zstd_FOUND 1)
set(Zstd_LIBRARIES ${Zstd_LIBRARY})
if(NOT TARGET Zstd::Zstd)
	add_library(Zstd::Zstd UNKNOWN IMPORTED)
	set_target_properties(Zstd::Zstd PROPERTIES
		IMPORTED_LOCATION             "${Zstd_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIR}")
endif()


