message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If building of the user manual is required, add -DBUILD_USER_MANUAL=1")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.
# Here we want it to be lowercase, UNIX-culture,
# and it is needed for the user manual build.
SET(TARGET masschroq)
SET(CAP_TARGET MassChroQ)

message("-----> In file unix-toolchain.cmake")


find_package(Qt6 COMPONENTS Widgets Xml Svg Sql PrintSupport Core5Compat Concurrent REQUIRED)


find_package(QCustomPlotQt6 REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
#message(STATUS "QCustomPlotQt6_LIBRARIES: ${QCustomPlotQt6_LIBRARIES}")
#message(STATUS "QCustomPlotQt6_INCLUDE_DIR: ${QCustomPlotQt6_INCLUDE_DIR}")

find_package(ZLIB REQUIRED)
find_package(QuaZip-Qt6 REQUIRED)

find_package(OdsStream REQUIRED)


find_package(PappsoMSpp COMPONENTS Core Widget REQUIRED)

if(PappsoMSpp_FOUND)

  message(STATUS "PappsoMSpp and PappsoMSppWidget were found at: ${PappsoMSpp_LIBRARY}")
  message(STATUS "Include dir for PappsoMSpp is at: ${PappsoMSpp_INCLUDE_DIRS}")

endif()


## Install directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()


