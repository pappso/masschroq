* MassChroQ FAQ

** What kind of isotopic labellings can I perform with MassChroQ?
** What isotopic labellings are not handled in MassChroQ?
** Does MassChroQ perform protein identification?
** I have LC-MS/MS raw files that I want to process with MassChroQ. How do I proceed?
   The classical workflow of LC-MS/MS data processing can be resumed
   in the following steps:

   1) Conversion of data from RAW format to mzXML or mzML file format.
      The RAW format is a proprietary closed binary format which
      cannot be decoded without the constructory libraries. On the
      contrary, mzXML and mzML formats are open standard proteomics
      formats, the information they contain is fully
      transparent. MassChroQ accepts only mzXML and mzML data
      formats. To convert your raw data to one of these formats,
      several free and open source tools exist. We recommend the
      msconvert or the readW tools from the TransProteomicPipeline 
      (link???). Depending on the type of your spectrometer, you
      should use msconvert or readW. For example the readW tool is
      well suited for Orbitrap Veloz spectrometers data, ...
      
   2) Protein identification and validation 
      MassChroQ does not perform protein identification and
      validation. You have to perform this (en amont). In our
      laboratory, we use the X!Tandem software via the X!Tandem
      pipeline (link) which performs both identification, filtering
      and validation, and offers a direct export of the results into a 
      complete, ready-to-use, masschroqML file. 
      If you use another identification engine (like Mascot, or
      other), you will have to export your results into spreadsheet
      files then rearrange the columns in these files in the MassChroQ
      order (see Question X for the column order). MassChroQ will
      automatically parse these files and extract the information they
      contain during quantification.

   3) 
