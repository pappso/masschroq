#import "template.typ": *

#show: doc => conf(
  subtitle: "Free and Open Source Mass Chromatogram Quantification Software",
  authors: (
    (
      name: "Filippo Rusconi",
    ),
    (
      name: "Benoît Valot",
    ),
    ( 
      name: "Edlira Nano",
    ),
    (
      name: "Olivier Langella",
    ),
    (
      name: "Michel Zivy",
    ),
  ),
  toc: true,
  lang: "en",
  font: "EB Garamond",
  date: "05/09/2023",
  years: (2024, 2025),
  version: "2.4.30",
  "Simple MassChroQ User Manual",
  doc
)


#let simplemcq = "SimpleMassChroQ"

= Introduction

#simplemcq is designed to extract ion current of a list of peptide and measure the are under the curve on a single run. No match between run.

= Input format

#code-block(read("input.json"), "json", title: "input file format")
#code-block(
"peptide proformat, charge, retention time
SLTNDWEDHLAVK, 2, 853.78
SLTNDWEDHLAVK, 3, 853.78
", "csv", title: "CSV input file format")

= Output format

Wouldn't be nice to try #link("https://en.wikipedia.org/wiki/Comparison_of_data-serialization_formats")[CBOR] ?
CBOR is supported in #link("https://doc.qt.io/qt-6/cbor.html")[QT6]

It could be something liket that :
#let code = read("output.json")
#code-block(code, "json", title: "output file format")

== QrDataBlock element

QrDataBlock stands for *quantification run data block* it handles quantification data for a single MSrun.

#let code = read("qr_data_block.json")
#code-block(code, "json", title: "qr_data_block element format")


#let code = read("output.json")
#code-block(code, "json", title: "output file format")



= MSrun retention time alignment

#code-block("std::shared_ptr<pappso::MsRunRetentionTime<QString>> & mcql::MsRunPeptideList::buildMsRunRetentionTimeSp(const mcql::AlignmentMethodSp &alignment_method)", "cpp", title: "MS run retention time function")


= Peak quality code

/ aa: best quality : many MS2 fragmentation event, only one peak directly detected
/ zaa: same as aa, but this charge state was not directly observed in MS2 fragmentation events in this MSrun
/ a: good quality, single MS2 fragmentation event, one peak detected
/ za: same as a, but this charge state was not directly observed in MS2 fragmentation events in this MSrun
/ ab: many MS2 fragmentation event, but more than one peak detected, the greater peak (area) is chosen, it is obviously fragmented... perhaps a hint to check for peak detection parameters
/ zab: same as ab, but this charge state was not directly observed in MS2 fragmentation events in this MSrun
/ b: peak obtained by "match between run" on the mean aligned observed retention times in MS2 fragmentation events *and* also matching with the retention time given by other detected and quantifified MS1 apex peaks in other MS runs
/ c: peak obtained by "match between run" only on the mean of aligned observed retention times in MS2 fragmentation events
/ d: peak obtained by "match between run" only matching with the retention time given by other detected and quantifified MS1 apex peaks in other MS runs

/ missed: no peak detected, no quantification 


= Match between run difference with legacy MassChroQ

The match between run process behaviour is slightly different between leagacy MassChroQ and MassChroQlite. The new process is more conservative as it will not try to find different peptide charge state if the peptide was observed for an MSrun in an other charge state.

An important difference is that MassChroQlite reports missed peaks : this lead to a natural data cleaning when used with MCQR. Indeed, if the most theoretical abundant isotope is not found in *any* msruns, then it will be discarded.
This lead to less quantified peptides but with a better quality.
