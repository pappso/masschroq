<?xml version="1.0" encoding="UTF-8"?>


<!--Purpose:-->
<!--Configure the style of the book's title recto/verso pages.-->

<!--Inspiration: work by Stefan Knorr and Thomas Schraitle-->

<!--Author(s): Stefan Knorr <sknorr@suse.de>, Thomas Schraitle <toms@opensuse.org>-->
		<!--Copyright:  2013, Stefan Knorr, Thomas Schraitle-->
		<!--Copyright: 2019, Filippo Rusconi-->


		<!DOCTYPE xsl:stylesheet
		[
		<!ENTITY % fonts SYSTEM "fonts.ent">
		<!ENTITY % colors SYSTEM "colors.ent">
		<!ENTITY % metrics SYSTEM "metrics.ent">
		%fonts;
		%colors;
		%metrics;
		]>
		<xsl:stylesheet exclude-result-prefixes="d"
			version="1.0"
			xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
			xmlns:d="http://docbook.org/ns/docbook"
			xmlns:exsl="http://exslt.org/common" 
			xmlns:fo="http://www.w3.org/1999/XSL/Format">

			<!-- This stylesheet was created by template/titlepage.xsl-->

			<!--===========================================================================-->
			<!--BOOK-->
			<!--===========================================================================-->


			<!--===========================================================================-->
			<!--RECTO -->
			<!--===========================================================================-->

			<xsl:template name="book.titlepage.recto">

				<xsl:variable name="height">
					<xsl:call-template name="get.value.from.unit">
						<xsl:with-param name="string" select="$page.height"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="unit">
					<xsl:call-template name="get.unit.from.unit">
						<xsl:with-param name="string" select="$page.height"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:message>height is now: <xsl:value-of select="$height"/></xsl:message>
				<xsl:message>unit is now: <xsl:value-of select="$unit"/></xsl:message>
				<xsl:message>goldenratio: &goldenratio;/></xsl:message>

			<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:mediaobject"/>
			<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:mediaobject"/>

			<!--Here we want to write the productname-specific sentence
			mentioning that productname is part of the X!TandemPipeline software
			project.-->

			<xsl:call-template name="book.info.productname" mode="book.titlepage.productname.info">
				<xsl:with-param name="productname" select="/d:book/d:info/d:productname"/>
			</xsl:call-template>

			<!-- Title and product -->

			<xsl:variable name="blockHeight" select="$height * (2 - &goldenratio;)"/>
			<xsl:message>blockHeight is: <xsl:value-of select="$blockHeight"/></xsl:message>
			<fo:block-container top="0" left="0" absolute-position="fixed"
				height="{$blockHeight}{$unit}">

				<fo:block>

					<xsl:variable name="tableWidth" select="(&column; * 7) + (&gutter; * 5)"/>
					<xsl:message>tableWidth is: <xsl:value-of select="$tableWidth"/></xsl:message>

					<fo:table width="{$tableWidth}mm" table-layout="fixed"
						block-progression-dimension="auto">

						<fo:table-column column-number="1" column-width="100%"/>

						<fo:table-body>

							<fo:table-row>

								<xsl:variable name="cellHeight" select="$height div 2.4"/>
								<xsl:message>cellHeight is: <xsl:value-of select="$cellHeight"/></xsl:message>
								<fo:table-cell display-align="after" height="{$cellHeight}{$unit}">
									<!--background-color="blue">-->

									<!--Divide the big table cell that starts at left page border
									and top page border into two horizontal regions. We use a new
									table into that cell.-->

									<fo:table width="{$tableWidth}mm" table-layout="fixed"
										block-progression-dimension="auto">

										<!--Small column that will make the left margin of the page.-->
										<fo:table-column column-number="1" column-width="{&column;}mm"/>
										<!--background-color="pink"/>-->

									<!--Big column that will cover the width of the page less the-->
									<!--left margin above and a matching-size margin on the right.-->

									<fo:table-column column-number="2"
										column-width="{(&column; * 6) + (&gutter; * 5)}mm"/>
									<!--background-color="violet"/>-->

								<fo:table-body>

									<!--This is the row where the book/info/title is going to be output.-->
									<fo:table-row>

										<!--Empty cell that makes the left indent-->
										<fo:table-cell>
											<fo:block></fo:block>
										</fo:table-cell>

										<!--The cell where the title is going to be output-->
										<fo:table-cell>
											<fo:block padding-after="&gutterfragment;mm">
												<xsl:choose>
													<xsl:when test="d:bookinfo/d:title">
														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:bookinfo/d:title"/>
													</xsl:when>
													<xsl:when test="d:info/d:title">

														<!--This is the statement that outputs the title at-->
														<!--roughly the bottom of the first third of the-->
														<!--page height starting from the top. We have put-->
														<!--the title in the book/info element.-->

														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:info/d:title"/>
													</xsl:when>
													<xsl:when test="d:title">
														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:title"/>
													</xsl:when>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<!--This is the row where the book/info/subtitle is going to be output.-->
									<fo:table-row>
										<fo:table-cell>
											<fo:block></fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block padding-after="&gutterfragment;mm">
												<xsl:choose>
													<xsl:when test="d:bookinfo/d:subtitle">
														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:bookinfo/d:subtitle"/>
													</xsl:when>
													<xsl:when test="d:info/d:subtitle">

														<!--This is the statement that outputs the title at-->
														<!--roughly the bottom of the first third of the-->
														<!--page height starting from the top. We have put-->
														<!--the title in the book/info element.-->

														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:info/d:subtitle"/>
													</xsl:when>
													<xsl:when test="d:subtitle">
														<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
															select="d:subtitle"/>
													</xsl:when>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<!--Finally, the rule line.-->
									<fo:table-row>
										<fo:table-cell>

											<!--This is the first cell that makes the left margin.
											The rule line will start from the left page border.-->

											<xsl:attribute name="border-top">
												<xsl:value-of select="concat(&mediumline;,'mm solid ',$mid-brown)"/>
											</xsl:attribute>

											<fo:block> </fo:block>
										</fo:table-cell>

										<fo:table-cell>

											<!--This is the second cell that makes the wide center
											of the page.
											The rule line will stop short of reaching the right
											page border by a distance roughly like the margin of
											the page.-->

											<xsl:attribute name="border-top">
												<xsl:value-of select="concat(&mediumline;,'mm solid ',$mid-brown)"/>
											</xsl:attribute>

											<fo:block> </fo:block>
										</fo:table-cell>

									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<!--Here we finish the big upper cell that contains the title, subtitle and horizontal rule.-->

					<!--At this point, start a new cell in which we'll output the productname and the version.-->

					<fo:table-row>

						<fo:table-cell display-align="after" height="30mm">
							<!--background-color="yellow">-->

							<fo:table width="{$tableWidth}mm" table-layout="fixed"
								block-progression-dimension="auto">

								<!--Med-sized column that will make the left margin of the page.-->
								<fo:table-column column-number="1" column-width="{&column;}mm"/>
								<!--background-color="pink"/>-->

							<!--Big column that will cover the width of the page less the-->
							<!--left margin above and a matching-size margin on the right.-->

							<fo:table-column column-number="2"
								column-width="{(&column; * 6) + (&gutter; * 5)}mm"/>
							<!--background-color="violet"/>-->

						<fo:table-body>

							<!--This is the row where the productname and version are going to be output.-->
							<fo:table-row>

								<!--Empty cell that makes the left indent-->
								<fo:table-cell>
									<fo:block></fo:block>
								</fo:table-cell>

								<!--The cell where the productname and version are going to be ouput.-->
								<fo:table-cell>
									<fo:block padding-after="&gutterfragment;mm">

										<!-- We use the full productname here: -->
										<xsl:apply-templates mode="book.titlepage.recto.auto.mode"
											select="d:bookinfo/d:productname[not(@role='abbrev')]|d:info/d:productname[not(@role='abbrev')]"/>
									</fo:block>
								</fo:table-cell>

							</fo:table-row>
						</fo:table-body>

					</fo:table>

				</fo:table-cell>

			</fo:table-row>

		</fo:table-body>

	</fo:table>

</fo:block>
	</fo:block-container>

	<!-- XEP needs at least one block following the normal flow, else it won't
	create a page break. This is not elegant, but works. -->
	<fo:block><fo:leader/></fo:block>

	<!--We do not want authors to output on the recto page. Only on the verso page.-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:corpauthor"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:corpauthor"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:authorgroup"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:authorgroup/d:author"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:author"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:author"/>-->

	<!--I do not have index right now-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:itermset"/>-->
	<!--<xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:itermset"/>-->

	</xsl:template><!-- End of <xsl:template name="book.titlepage.recto">-->

	<!--This is the template that formats the title page into a very large font size!-->
	<xsl:template match="d:title" mode="book.titlepage.recto.auto.mode">

		<fo:block 
			text-align="start" line-height="1.2" hyphenate="false"
			xsl:use-attribute-sets="title.font title.name.color sans.bold.noreplacement"
			font-weight="normal"
			font-size="{&ultra-large;}pt">
			<xsl:apply-templates select="." mode="book.titlepage.recto.mode"/>
		</fo:block>

	</xsl:template>

	<xsl:template match="d:subtitle" mode="book.titlepage.recto.auto.mode">

		<fo:block
			xsl:use-attribute-sets="title.font title.name.color" 
			font-size="{&x-large; * $sans-fontsize-adjust}pt"
			font-weight="bold"
			space-before="&gutterfragment;mm">
			<xsl:apply-templates select="." mode="book.titlepage.recto.mode"/>
		</fo:block>

	</xsl:template>

	<xsl:template match="d:productname[not(@role='abbrev')]"
		mode="book.titlepage.recto.auto.mode">
		<fo:block text-align="start" hyphenate="false"
			line-height="{$base-lineheight * 0.85}em"
			font-weight="normal" font-size="{&super-large; * $sans-fontsize-adjust}pt"
			space-after="&gutterfragment;mm"
			xsl:use-attribute-sets="title.font sans.bold.noreplacement mid-brown">
			<xsl:apply-templates select="." mode="book.titlepage.recto.mode"/>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="../d:productnumber[not(@role='abbrev')]"
				mode="book.titlepage.recto.mode"/>
		</fo:block>
	</xsl:template>

	<!--This is the template that formats the title page into a very large font size!-->
	<xsl:template match="d:info/d:authorgroup/d:author" mode="book.titlepage.recto.auto.mode">

		<fo:block 
			text-align="center" line-height="1.2" hyphenate="false"
			xsl:use-attribute-sets="title.font title.name.color sans.bold.noreplacement"
			font-weight="normal"
			font-size="{&xx-large;}pt">
			<xsl:apply-templates select="." mode="book.titlepage.recto.mode"/>
		</fo:block>

	</xsl:template>

	<!--===========================================================================-->
	<!--VERSO -->
	<!--===========================================================================-->

	<xsl:template name="book.titlepage.verso">
		<xsl:variable name="height">
			<xsl:call-template name="get.value.from.unit">
				<xsl:with-param name="string" select="$page.height"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="page-height">
			<xsl:call-template name="get.value.from.unit">
				<xsl:with-param name="string" select="$page.height"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="margin-top">
			<xsl:call-template name="get.value.from.unit">
				<xsl:with-param name="string" select="$page.margin.top"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="margin-bottom">
			<xsl:call-template name="get.value.from.unit">
				<xsl:with-param name="string" select="$page.margin.bottom"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="margin-bottom-body">
			<xsl:call-template name="get.value.from.unit">
				<xsl:with-param name="string" select="$body.margin.bottom"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="table.height"
			select="$page-height - ($margin-top + $margin-bottom + $margin-bottom-body)"/>
		<xsl:variable name="unit">
			<xsl:call-template name="get.unit.from.unit">
				<xsl:with-param name="string" select="$page.height"/>
			</xsl:call-template>
		</xsl:variable>

		<fo:table table-layout="fixed" block-progression-dimension="auto"
			height="{$table.height}{$unit}"
			width="{(6 * &column;) + (5 * &gutter;)}mm">
			<fo:table-column column-number="1" column-width="100%"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell height="{0.1 * $table.height}{$unit}">
						<!--background-color="blue">-->
						<xsl:apply-templates
							select="(d:bookinfo/d:title | d:info/d:title | d:title)[1]"
							mode="book.titlepage.verso.auto.mode"/>
						<xsl:apply-templates
							select="(d:bookinfo/d:productname | d:info/d:productname)[not(@role='abbrev')]"
							mode="book.titlepage.verso.auto.mode"/>

						<xsl:apply-templates
							select="(d:bookinfo/d:authorgroup | d:info/d:authorgroup)[1]"
							mode="book.titlepage.verso.auto.mode"/>
						<xsl:apply-templates
							select="(d:bookinfo/d:author | d:info/d:author)[1]"
							mode="book.titlepage.verso.auto.mode"/>

						<xsl:apply-templates
							select="(d:bookinfo/d:abstract | d:info/d:abstract)[1]"
							mode="book.titlepage.verso.auto.mode"/>
						<!-- Empty fo:block to fix openSUSE/suse-xsl#97 -->
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>

				<!-- Everything else is in a second block of text at the bottom -->
				<fo:table-row>
					<fo:table-cell display-align="after" height="{0.9 * $table.height}{$unit}">
						<!--background-color="yellow">-->

						<xsl:call-template name="date.and.revision"/>

						<xsl:apply-templates
							select="(d:bookinfo/d:corpauthor | d:info/d:corpauthor)[1]"
							mode="book.titlepage.verso.auto.mode"/>
						<xsl:apply-templates
							select="(d:bookinfo/d:othercredit | d:info/d:othercredit)[1]"
							mode="book.titlepage.verso.auto.mode"/>
						<xsl:apply-templates
							select="(d:bookinfo/d:editor | d:info/d:editor)[1]"
							mode="book.titlepage.verso.auto.mode"/>

						<xsl:apply-templates
							select="(d:bookinfo/d:legalnotice | d:info/d:legalnotice)[1]"
							mode="book.titlepage.verso.auto.mode"/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:bookinfo/d:othercredit"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:info/d:othercredit"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:bookinfo/d:releaseinfo"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:info/d:releaseinfo"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:bookinfo/d:revhistory"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:info/d:revhistory"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:bookinfo/d:pubdate"/>
		<xsl:apply-templates mode="book.titlepage.verso.auto.mode" select="d:info/d:pubdate"/>

	</xsl:template>

	<xsl:template name="msxpertsuite.imprint"> </xsl:template>


	<xsl:template name="date.and.revision">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style">
			<xsl:call-template name="date.and.revision.inner"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="imprint.label">
		<xsl:param name="label" select="'PubDate'"/>
	</xsl:template>

	<xsl:template name="book.titlepage.separator"><fo:block break-after="page"/>
	</xsl:template>

	<xsl:template name="book.titlepage.before.recto">
	</xsl:template>

	<xsl:template name="book.titlepage.before.verso"><fo:block break-after="page"/>
	</xsl:template>

	<xsl:template name="book.titlepage">
		<fo:block>
			<xsl:variable name="recto.content">
				<xsl:call-template name="book.titlepage.before.recto"/>
				<xsl:call-template name="book.titlepage.recto"/>
			</xsl:variable>
			<xsl:variable name="recto.elements.count">
				<xsl:choose>
					<xsl:when test="function-available('exsl:node-set')"><xsl:value-of select="count(exsl:node-set($recto.content)/*)"/></xsl:when>
					<xsl:when test="contains(system-property('xsl:vendor'), 'Apache Software Foundation')">
						<!--Xalan quirk--><xsl:value-of select="count(exsl:node-set($recto.content)/*)"/></xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="(normalize-space($recto.content) != '') or ($recto.elements.count &gt; 0)">
				<fo:block><xsl:copy-of select="$recto.content"/></fo:block>
			</xsl:if>
			<xsl:variable name="verso.content">
				<xsl:call-template name="book.titlepage.before.verso"/>
				<xsl:call-template name="book.titlepage.verso"/>
			</xsl:variable>
			<xsl:variable name="verso.elements.count">
				<xsl:choose>
					<xsl:when test="function-available('exsl:node-set')"><xsl:value-of select="count(exsl:node-set($verso.content)/*)"/></xsl:when>
					<xsl:when test="contains(system-property('xsl:vendor'), 'Apache Software Foundation')">
						<!--Xalan quirk--><xsl:value-of select="count(exsl:node-set($verso.content)/*)"/></xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="(normalize-space($verso.content) != '') or ($verso.elements.count &gt; 0)">
				<fo:block><xsl:copy-of select="$verso.content"/></fo:block>
			</xsl:if>
			<xsl:call-template name="book.titlepage.separator"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*" mode="book.titlepage.recto.mode">
		<!-- if an element isn't found in this mode, -->
		<!-- try the generic titlepage.mode -->
		<xsl:apply-templates select="." mode="titlepage.mode"/>
	</xsl:template>

	<xsl:template match="*" mode="book.titlepage.verso.mode">
		<!-- if an element isn't found in this mode, -->
		<!-- try the generic titlepage.mode -->
		<xsl:apply-templates select="." mode="titlepage.mode"/>
	</xsl:template>

	<xsl:template match="d:mediaobject" mode="book.titlepage.recto.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.recto.style">
			<xsl:apply-templates select="." mode="book.titlepage.recto.mode"/>
		</fo:block>
	</xsl:template>


	<xsl:template match="d:title" mode="book.titlepage.verso.auto.mode">
		<fo:block
			xsl:use-attribute-sets="book.titlepage.verso.style sans.bold"
			font-size="{&x-large; * $sans-fontsize-adjust}pt" font-family="{$title.fontset}">
			<xsl:call-template name="book.verso.title"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:corpauthor" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:authorgroup" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style">
			<xsl:call-template name="verso.authorgroup">
			</xsl:call-template>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:author" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:othercredit" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:releaseinfo" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" space-before="0.5em" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:revhistory" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" space-before="1em" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:pubdate" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" space-before="1em" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:copyright" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:abstract" mode="book.titlepage.verso.auto.mode">
		<fo:block xsl:use-attribute-sets="book.titlepage.verso.style" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="." mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:legalnotice" mode="book.titlepage.verso.auto.mode">
		<fo:block
			xsl:use-attribute-sets="book.titlepage.verso.style" font-size="{&small; * $fontsize-adjust}pt">
			<xsl:apply-templates select="*" mode="book.titlepage.verso.mode"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="d:legalnotice/d:para" mode="book.titlepage.verso.mode">
		<fo:block space-after="0.25em" space-before="0em">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>


</xsl:stylesheet>

