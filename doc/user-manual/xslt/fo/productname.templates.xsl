<?xml version="1.0" encoding="UTF-8"?>


<!--Purpose:-->
<!--Make sure the productname element is used to output the line-->
<!--<productname> is part of the msXpertSuite software package-->

	<!DOCTYPE xsl:stylesheet
	[
	<!ENTITY % fonts SYSTEM "fonts.ent">
	<!ENTITY % colors SYSTEM "colors.ent">
	<!ENTITY % metrics SYSTEM "metrics.ent">
	%fonts;
	%colors;
	%metrics;
	]>
	<xsl:stylesheet exclude-result-prefixes="d"
		version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:d="http://docbook.org/ns/docbook"
		xmlns:exsl="http://exslt.org/common" 
		xmlns:fo="http://www.w3.org/1999/XSL/Format">


		<xsl:template name="book.info.productname" mode="book.titlepage.productname.info">
			<xsl:param name="productname"/>

			<!--We want to make sure the line about the fact that <productname/> is part-->
			<!--of the X!TandemPipeline software project.-->

			<!--As of 20210428, the line says: -->
			<!--"$productname is one of the PAPPSO facility software projects"-->

			<fo:block font-weight="bold" font-size="&x-large;" text-align="center">
				<xsl:value-of select="$productname"/> is one of the
				PAPPSO facility software projects</fo:block>

		</xsl:template>

	</xsl:stylesheet>
