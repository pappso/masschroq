#pragma once

#define SOFTWARE_NAME "MassChroQ"
#define MASSCHROQ_VERSION "2.4.33"
#define MASSCHROQ_XSD "masschroq-2.7.xsd"
#define MASSCHROQ_SCHEMA_VERSION "2.7"
#define MASSCHROQ_SCHEMA_FILE \
  ":/resources/schema/masschroq-2.7.xsd"
/* #undef MASSCHROQ_ICON */
#define QT_V_4_5 0x040500
#define QT_V_4_6 0x040600


#include <QDebug>
#include "mcq_types.h"
