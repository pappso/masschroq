#pragma once

#include <QtCore/QtGlobal>

#if defined(MCQ_LIBRARY)
// #pragma message("MCQ_LIBRARY is defined: defining PMSPP_LIB_DECL
//  Q_DECL_EXPORT")
#define MCQ_LIB_DECL Q_DECL_EXPORT
#else
// #pragma message("MCQ_LIBRARY is not defined: defining PMSPP_LIB_DECL
//  Q_DECL_IMPORT")
#define MCQ_LIB_DECL Q_DECL_IMPORT
#endif
