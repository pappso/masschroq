/*
 * alignment_widget.cpp
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#include "alignment_widget.h"
#include <QMessageBox>

AlignmentWidget::AlignmentWidget(QWidget *parent) : MasschroQWidget(parent)
{

  _loadingThreadRef = new MsrunSimpleLoaderThread();
  _loadingThreadRef->setMaxProgress(2);

  _loadingThreadToAligned = new MsrunSimpleLoaderThread();
  _loadingThreadToAligned->setMaxProgress(2);

  _alignment_thread = new AlignmentThread();
  _alignment_thread->setMaxProgress(2);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_loadingThreadRef,
          &MsrunSimpleLoaderThread::loadedMsrun,
          this,
          &AlignmentWidget::doneLoadindMsrunRef);
  connect(_loadingThreadRef,
          &MsrunSimpleLoaderThread::errorDuringLoading,
          this,
          &AlignmentWidget::errorLoadindMsrunRef);
  connect(_loadingThreadToAligned,
          &MsrunSimpleLoaderThread::loadedMsrun,
          this,
          &AlignmentWidget::doneLoadindMsrunAligned);
  connect(_loadingThreadToAligned,
          &MsrunSimpleLoaderThread::errorDuringLoading,
          this,
          &AlignmentWidget::errortLoadindMsrunAligned);

  connect(_alignment_thread,
          &AlignmentThread::finishAlignment,
          this,
          &AlignmentWidget::doneAlignment);
  connect(_alignment_thread,
          &AlignmentThread::errorDuringAlignment,
          this,
          &AlignmentWidget::errorAlignment);
#else
  // Qt4 code

  connect(_loadingThreadRef,
          SIGNAL(loadedMsrun(Msrun *)),
          this,
          SLOT(doneLoadindMsrunRef(Msrun *)));
  connect(_loadingThreadRef,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(errorLoadindMsrunRef(QString)));
  connect(_loadingThreadToAligned,
          SIGNAL(loadedMsrun(Msrun *)),
          this,
          SLOT(doneLoadindMsrunAligned(Msrun *)));
  connect(_loadingThreadToAligned,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(errortLoadindMsrunAligned(QString)));

  connect(
    _alignment_thread, SIGNAL(finishAlignment()), this, SLOT(doneAlignment()));
  connect(_alignment_thread,
          SIGNAL(errorDuringAlignment(QString)),
          this,
          SLOT(errorAlignment(QString)));
#endif
}

AlignmentWidget::~AlignmentWidget()
{
}

void
AlignmentWidget::doneLoadindMsrunRef(MsrunSp ref)
{
  _msrunRef = ref;
  if(_msrunRef == 0)
    {
      this->viewError("Msrun Ref are null?");
      _loadingMsrunRefLabel->stopLoading("");
    }
  else
    {
      _loadingMsrunRefLabel->stopLoading("MsRun correctly load");
      this->completDataToMsrun(_msrunRef);

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " _msrunRef->getPeptideList().size()="
               << _msrunRef->getPeptideList().size();
    }
}

void
AlignmentWidget::doneLoadindMsrunAligned(MsrunSp aligned)
{
  _msunToAligned = aligned;
  if(_msunToAligned == 0)
    {
      this->viewError("Msrun aligned are null?");
      _loadingMsruntoALignedLabel->stopLoading("");
    }
  else
    {
      _loadingMsruntoALignedLabel->stopLoading("MsRun correctly load");
      completDataToMsrun(_msunToAligned);
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " _msunToAligned->getPeptideList().size()="
               << _msunToAligned->getPeptideList().size();
    }
}

void
AlignmentWidget::viewError(QString error)
{
  QMessageBox::warning(
    this, tr("MassChroQ encountered an error:"), error);
}

QString
AlignmentWidget::strippedFilename(const QString &fullFilename)
{
  return QFileInfo(fullFilename).fileName();
}

void
AlignmentWidget::errorAlignment(QString error)
{
  _runningQLabel->stopLoading("");
  this->viewError(error);
}

void
AlignmentWidget::errorLoadindMsrunRef(QString error)
{
  _loadingMsrunRefLabel->stopLoading("Problems");
  _msrunRef = 0;
  this->viewError(error);
}

void
AlignmentWidget::errortLoadindMsrunAligned(QString error)
{
  _loadingMsruntoALignedLabel->stopLoading("Problems");
  _msunToAligned = 0;
  this->viewError(error);
}

void
AlignmentWidget::doneAlignment()
{
  qDebug() << "AlignmentWidget::doneAlignment begin";
  _runningQLabel->stopLoading("");
  this->emitSignalDoneAlignement();
  qDebug() << "AlignmentWidget::doneAlignment end";
}

void
AlignmentWidget::startAlignment()
{
  if(this->isLoadingRunning())
    {
      this->viewError(
        tr("Unable to start alignment while loading MsRuns.\n Please wait"));
      return;
    }
  if(_msunToAligned == 0)
    {
      this->viewError(tr("There is no MsRun to align.\n Please choose one!"));
      return;
    }
  else if(_msrunRef == 0)
    {
      this->viewError(tr("There is no MsRun reference.\n Please choose one!"));
      return;
    }
  _runningQLabel->startLoading("Alignment in progress");
  _alignment_thread->performedAlignment(
    _alignmentBase, _msrunRef, _msunToAligned);
}

void
AlignmentWidget::deleteLoadingThread()
{
  qDebug() << "delete Loading thread";
  delete(_loadingThreadRef);
  delete(_loadingThreadToAligned);
  delete(_alignment_thread);
}

void
AlignmentWidget::triggerLoadingMsRunToAlign(MsrunSp msrun)
{

  qDebug() << "AlignmentWidget::triggerLoadingMsRunToAlign begin";
  if(msrun != 0)
    {
      //_text_edit_ref->setText(msrun->getXmlFileInfo().fileName());
      this->_loadingMsruntoALignedLabel->startLoading("Loading");
      _loadingThreadToAligned->loadMsrun(msrun);
    }
  else
    {
      this->viewError(tr("There is no MsRun to align.\n Please choose one!"));
      return;
    }
  qDebug() << "AlignmentWidget::triggerLoadingMsRunToAlign end";
}

void
AlignmentWidget::triggerLoadingMsRunRef(MsrunSp msrun)
{
  if(msrun != 0)
    {
      //_text_edit_ref->setText(msrun->getXmlFileInfo().fileName());
      _loadingMsrunRefLabel->startLoading("Loading");
      _loadingThreadRef->loadMsrun(msrun);
    }
  else
    {
      this->viewError(tr("There is no MsRun reference.\n Please choose one!"));
      return;
    }
}

bool
AlignmentWidget::isAlignmentRunning()
{
  if(_alignment_thread->isRunning())
    return true;
  return false;
}

bool
AlignmentWidget::isLoadingRunning()
{
  if(_loadingThreadToAligned->isRunning())
    return true;
  if(_loadingThreadRef->isRunning())
    return true;
  return false;
}
