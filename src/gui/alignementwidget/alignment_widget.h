/*
 * alignment_widget.h
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/alignments/alignment_base.h"
#include "../../lib/msrun/msrun.h"
#include "../masschroQWidget.h"
#include "../runningQLabel.h"
#include "../thread/alignment_thread.h"
#include "../thread/msrunSimpleLoaderThread.h"
#include <QWidget>

class AlignmentWidget : public MasschroQWidget
{
  Q_OBJECT

  public:
  AlignmentWidget(QWidget *parent = 0);
  virtual ~AlignmentWidget();

  signals:
  void newAlignmentCurve();
  void finishAlignment(const MsrunSp aligned);

  protected slots:
  void startAlignment();
  void doneAlignment();
  void errorAlignment(QString error);
  void errorLoadindMsrunRef(QString error);
  void errortLoadindMsrunAligned(QString error);
  void doneLoadindMsrunRef(MsrunSp ref);
  void doneLoadindMsrunAligned(MsrunSp align);

  protected:
  void triggerLoadingMsRunToAlign(MsrunSp msrun);
  void triggerLoadingMsRunRef(MsrunSp msrun);
  bool isAlignmentRunning();
  bool isLoadingRunning();

  QString strippedFilename(const QString &fullFilename);
  void viewError(QString error);
  virtual void completDataToMsrun(MsrunSp msrun) = 0;
  virtual void emitSignalDoneAlignement()        = 0;
  void deleteLoadingThread();

  MsrunSp _msrunRef;
  MsrunSp _msunToAligned;
  RunningQLabel *_loadingMsrunRefLabel;
  RunningQLabel *_loadingMsruntoALignedLabel;
  RunningQLabel *_runningQLabel;
  AlignmentBase *_alignmentBase;
  MonitorAlignmentBase *_monitor;

  private:
  MsrunSimpleLoaderThread *_loadingThreadRef;
  MsrunSimpleLoaderThread *_loadingThreadToAligned;
  AlignmentThread *_alignment_thread;
};

