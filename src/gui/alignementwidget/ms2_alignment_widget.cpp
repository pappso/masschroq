/*
 * ms2_alignment_widget.cpp
 *
 *  Created on: 2 août 2012
 *      Author: valot
 */

#include "ms2_alignment_widget.h"
#include "../../lib/msrun/ms_run_hash_group.h"
#include "../engine/masschroq_gui_engin.h"
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QSettings>

Q_DECLARE_METATYPE(MsrunSp);

Ms2AlignmentWidget::Ms2AlignmentWidget(QWidget *parent)
  : AlignmentWidget(parent),
    MS2_TENDENCY(10),
    MS2_SMOOTHING(15),
    MS1_SMOOTHING(0)
{
  // kill including widget
  setAttribute(Qt::WA_DeleteOnClose);

  _mainLayout = new QVBoxLayout;
  this->addMasschroqmlSelectionGroup();
  this->addMsRunSelectionGroup();
  this->addParametersGroup();
  this->addAlignButton();

  this->setLayout(_mainLayout);

  this->initializeMs2Method();

  _masschroqml_loader_thread.setMaxProgress(3);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(&_masschroqml_loader_thread,
          &MasschroqmlLoaderThread::loadedMasschroqml,
          this,
          &Ms2AlignmentWidget::doneLoadingMasschroqml);
  connect(&_masschroqml_loader_thread,
          &MasschroqmlLoaderThread::errorDuringLoading,
          this,
          &Ms2AlignmentWidget::ErrorLoadingMasschroqml);
#else
  // Qt4 code
  connect(&_masschroqml_loader_thread,
          SIGNAL(loadedMasschroqml(MasschroqGuiData *)),
          this,
          SLOT(doneLoadingMasschroqml(MasschroqGuiData *)));
  connect(&_masschroqml_loader_thread,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(ErrorLoadingMasschroqml(QString)));
#endif
}

Ms2AlignmentWidget::~Ms2AlignmentWidget()
{
  qDebug() << "delete Ms2alignmentWidget";
  this->deleteLoadingThread();
  this->deleteData();
  delete(_monitor);
}

void
Ms2AlignmentWidget::addMasschroqmlSelectionGroup()
{
  QGroupBox *masschroqml_group = new QGroupBox("MassChroqML selection");

  QVBoxLayout *groupLayout = new QVBoxLayout;

  _masschroqml_edit = new QLineEdit("No MassChroqML Selected");
  _masschroqml_edit->setReadOnly(true);
  groupLayout->addWidget(_masschroqml_edit);

  QHBoxLayout *layout  = new QHBoxLayout;
  _masschroqml_message = new RunningQLabel(this);
  layout->addWidget(_masschroqml_message, 2);
  QPushButton *extractButton = new QPushButton(tr("&Load"));
  extractButton->setDefault(true);
  layout->addWidget(extractButton, 0);

  groupLayout->addLayout(layout);

  masschroqml_group->setLayout(groupLayout);
  _mainLayout->addWidget(masschroqml_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton,
          &QPushButton::clicked,
          this,
          &Ms2AlignmentWidget::startLoadingMasschroqml);
#else
  // Qt4 code
  connect(
    extractButton, SIGNAL(clicked()), this, SLOT(startLoadingMasschroqml()));
#endif
}

void
Ms2AlignmentWidget::addMsRunSelectionGroup()
{
  QGroupBox *msrun_group = new QGroupBox("MsRun selection");

  QVBoxLayout *groupLayout = new QVBoxLayout;
  _msrun_select_ref        = new QComboBox();

  groupLayout->addWidget(_msrun_select_ref);
  _loadingMsrunRefLabel = new RunningQLabel(this);
  groupLayout->addWidget(_loadingMsrunRefLabel);

  _msrun_select_align = new QComboBox();

  groupLayout->addWidget(_msrun_select_align);
  _loadingMsruntoALignedLabel = new RunningQLabel(this);
  groupLayout->addWidget(_loadingMsruntoALignedLabel);

  msrun_group->setLayout(groupLayout);
  _mainLayout->addWidget(msrun_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_msrun_select_ref,
          static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
          this,
          &Ms2AlignmentWidget::startLoadindMsrunRef);
  connect(_msrun_select_align,
          static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
          this,
          &Ms2AlignmentWidget::startLoadindMsrunAligned);
#else
  // Qt4 code
  connect(_msrun_select_ref,
          SIGNAL(activated(int)),
          this,
          SLOT(startLoadindMsrunRef(int)));
  connect(_msrun_select_align,
          SIGNAL(activated(int)),
          this,
          SLOT(startLoadindMsrunAligned(int)));
#endif
}

void
Ms2AlignmentWidget::addParametersGroup()
{
  QGroupBox *_parameter_group = new QGroupBox("MS2 Parameters");
  QFormLayout *layout         = new QFormLayout;

  QDoubleSpinBox *ms2_tendency_box = new QDoubleSpinBox;
  ms2_tendency_box->setMaximum(1000);
  ms2_tendency_box->setWrapping(true);
  ms2_tendency_box->setSingleStep(1);
  ms2_tendency_box->setDecimals(0);
  ms2_tendency_box->setValue(MS2_TENDENCY);
  layout->addRow(tr("MS2 tendency half window"), ms2_tendency_box);

  QDoubleSpinBox *ms2_smoothing_box = new QDoubleSpinBox;
  ms2_smoothing_box->setMaximum(1000);
  ms2_smoothing_box->setWrapping(true);
  ms2_smoothing_box->setSingleStep(1);
  ms2_smoothing_box->setDecimals(0);
  ms2_smoothing_box->setValue(MS2_SMOOTHING);
  layout->addRow(tr("MS2 smoothing half window"), ms2_smoothing_box);

  QDoubleSpinBox *ms1_smoothing_box = new QDoubleSpinBox;
  ms1_smoothing_box->setMaximum(1000);
  ms1_smoothing_box->setWrapping(true);
  ms1_smoothing_box->setSingleStep(1);
  ms1_smoothing_box->setDecimals(0);
  ms1_smoothing_box->setValue(MS1_SMOOTHING);
  layout->addRow(tr("MS1 smoothing half window"), ms1_smoothing_box);

  _parameter_group->setLayout(layout);

  _mainLayout->addWidget(_parameter_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ms2_tendency_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &Ms2AlignmentWidget::setMs2Tendency);

  connect(ms2_smoothing_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &Ms2AlignmentWidget::setMs2Smoothing);

  connect(ms1_smoothing_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &Ms2AlignmentWidget::setMs1Smoothing);
#else
  // Qt4 code
  connect(ms2_tendency_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setMs2Tendency(double)));

  connect(ms2_smoothing_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setMs2Smoothing(double)));

  connect(ms1_smoothing_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setMs1Smoothing(double)));
#endif
}

void
Ms2AlignmentWidget::addAlignButton()
{
  QHBoxLayout *layout = new QHBoxLayout;
  _runningQLabel      = new RunningQLabel(this);
  layout->addWidget(_runningQLabel, 2);
  QPushButton *extractButton = new QPushButton(tr("&Align"));
  extractButton->setDefault(true);
  layout->addWidget(extractButton, 0);
  _mainLayout->addLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton,
          &QPushButton::clicked,
          this,
          &Ms2AlignmentWidget::startAlignment);
#else
  // Qt4 code
  connect(extractButton, SIGNAL(clicked()), this, SLOT(startAlignment()));
#endif
}

void
Ms2AlignmentWidget::initializeMs2Method()
{
  _monitor       = new MonitorAlignmentBase();
  _alignmentBase = new AlignmentMs2(_monitor);
  AlignmentMs2 *align((AlignmentMs2 *)_alignmentBase);
  align->setMs2TendencyWindow(MS2_TENDENCY);
  align->setMs2SmoothingWindow(MS2_SMOOTHING);
  align->setMs1SmoothingWindow(MS1_SMOOTHING);
  _masschroqGuidata = 0;
  _msunToAligned    = 0;
  _msrunRef         = 0;
}

void
Ms2AlignmentWidget::setMs2Tendency(double win)
{
  AlignmentMs2 *align((AlignmentMs2 *)_alignmentBase);
  align->setMs2TendencyWindow(win);
}

void
Ms2AlignmentWidget::setMs2Smoothing(double win)
{
  AlignmentMs2 *align((AlignmentMs2 *)_alignmentBase);
  align->setMs2SmoothingWindow(win);
}

void
Ms2AlignmentWidget::setMs1Smoothing(double win)
{
  AlignmentMs2 *align((AlignmentMs2 *)_alignmentBase);
  align->setMs1SmoothingWindow(win);
}

void
Ms2AlignmentWidget::startLoadindMsrunRef(int index)
{
  if(this->isAlignmentRunning())
    return;
  qDebug() << "Load MsRun Ref index : " << index;

  QVariant index_value = _msrun_select_ref->currentData();
  if(index_value.canConvert<MsrunSp>())
    {
      this->triggerLoadingMsRunRef(index_value.value<MsrunSp>());
    }
}

void
Ms2AlignmentWidget::startLoadindMsrunAligned(int index)
{
  qDebug() << "Ms2AlignmentWidget::startLoadindMsrunAligned begin ";
  if(this->isAlignmentRunning())
    return;
  qDebug() << "Load MsRun Align index : " << index;

  QVariant index_value = _msrun_select_align->currentData();
  if(index_value.canConvert<MsrunSp>())
    {
      this->triggerLoadingMsRunToAlign(index_value.value<MsrunSp>());
    }

  qDebug() << "Ms2AlignmentWidget::startLoadindMsrunAligned end ";
}

void
Ms2AlignmentWidget::startLoadingMasschroqml()
{
  if(this->isAlignmentRunning())
    return;
  qDebug() << "Begin MassChroqML Loading";

  QSettings settings;
  QString default_location =
    settings.value("path/mcqml_default_path", "").toString();

  QString filename = QFileDialog::getOpenFileName(
    this,
    tr("Open MasschroqML File"),
    default_location,
    tr("masschroqML files (*.masschroqML &  *.xml & *.masschroqml);;all "
       "files (*)"));
  if(filename.isEmpty())
    {
      return;
    }

  settings.setValue("path/mcqml_default_path",
                    QFileInfo(filename).absolutePath());
  QFileInfo filenameInfo(filename);

  if(!filenameInfo.exists())
    {
      this->viewError(
        tr("The chosen MasschroqML file '%1', does not exist..\nPlease, change "
           "the read permissions on this file or load another one. ")
          .arg(filename));
      return;
    }
  else if(!filenameInfo.isReadable())
    {
      this->viewError(
        tr("The chosen MasschroqML file '%1', is not readable.\nPlease, change "
           "the read permissions on this file or load another one. ")
          .arg(filename));
      return;
    }
  _masschroqml_message->startLoading(tr("Loading"));
  _masschroqml_edit->setText(this->strippedFilename(filename));
  _loadingMsrunRefLabel->setText("");
  _loadingMsruntoALignedLabel->setText("");

  this->deleteData();
  this->_msrun_select_ref->clear();
  this->_msrun_select_align->clear();
  // emit resetData();

  QDir::setCurrent(filenameInfo.absolutePath());
  _masschroqml_loader_thread.loadMasschroqml(filename);
}

void
Ms2AlignmentWidget::doneLoadingMasschroqml(MasschroqGuiData *guiData)
{
  _masschroqGuidata = guiData;
  _masschroqml_message->stopLoading(tr("MasschroqML correctly loaded"));

  // add msrun selection
  MasschroqGuiEngin::getInstance()->setMasschroqGuiData(guiData);

  for(auto pair_msrun : guiData->getMsrunHashGroup())
    {
      this->_msrun_select_ref->addItem(
        QFileInfo(pair_msrun.second->getMsRunIdCstSPtr().get()->getFileName())
          .fileName(),
        QVariant::fromValue(pair_msrun.second));
      this->_msrun_select_align->addItem(
        QFileInfo(pair_msrun.second->getMsRunIdCstSPtr().get()->getFileName())
          .fileName(),
        QVariant::fromValue(pair_msrun.second));
    }

  // emit updateMasschroqData();
}

void
Ms2AlignmentWidget::ErrorLoadingMasschroqml(QString error)
{
  _masschroqml_edit->setText(tr("No MassChroqML selected"));
  _masschroqml_message->stopLoading(tr(""));

  this->viewError(error);
}

void
Ms2AlignmentWidget::completDataToMsrun(MsrunSp msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " msrun=" << msrun->getMsRunIdCstSPtr().get()->getXmlId();
  msrun->setPeptideList(
    _masschroqGuidata->getNoIsotopePeptideList().getPeptideListObservedInMsrun(
      msrun.get()));
}

void
Ms2AlignmentWidget::emitSignalDoneAlignement()
{
  qDebug() << "Ms2AlignmentWidget::emitSignalDoneAlignement begin";
  emit newAlignmentCurve();
  emit finishMonitorAlignment(&_msunToAligned.get()->getMsRunRetentionTime(),
                              &_msrunRef.get()->getMsRunRetentionTime());
  emit finishAlignment(_msunToAligned);
  qDebug() << "Ms2AlignmentWidget::emitSignalDoneAlignement end";
}

void
Ms2AlignmentWidget::deleteData()
{
  if(_masschroqGuidata != 0)
    {
      delete(_masschroqGuidata);
      _masschroqGuidata = 0;
    }
}

void
Ms2AlignmentWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  AlignmentMs2 *align((AlignmentMs2 *)_alignmentBase);
  domDocument->addMs2AlignmentMethod(*align);
}
