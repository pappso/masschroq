/**
 * \file plot.cpp
 * \date November 23, 2011
 * \author Edlira Nano
 */

#include "plot_aligned.h"

PlotAligned::PlotAligned(QWidget *parent) : QCustomPlot(parent)
{

  xAxis->setLabel("Retention time (s)");
  yAxis->setLabel("Delta RT (s)");
  legend->setVisible(true);
  legend->setFont(QFont("Helvetica", 9));
  initZoomer();
}

PlotAligned::~PlotAligned()
{
  this->clear();
}

void
PlotAligned::viewAlignedMsRun(MsrunSp msrun)
{
  qDebug() << "PlotAligned::viewAlignedMsRun begin";
  const std::vector<mcq_double> originalRt = msrun->getOriginalRetentionTimes();
  const std::vector<mcq_double> alignedRt  = msrun->getAlignedRetentionTimes();
  if(originalRt.size() == alignedRt.size())
    {
      std::vector<mcq_double>::const_iterator itOriginal = originalRt.begin();
      std::vector<mcq_double>::const_iterator itAligned  = alignedRt.begin();

      unsigned int plotsize(alignedRt.size());
      int i = 0;

      addGraph();
      // QPen pen;
      // pen.setColor(QColor(Qt::blue));
      graph()->setPen(QPen(QColor(Qt::blue), 2));
      graph()->setName("retention time corrections");
      // graph()->setName(lineNames.at(i-QCPGraph::lsNone));
      graph()->setLineStyle(QCPGraph::LineStyle::lsLine);
      // graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
      // 5)); generate data:
      QVector<double> x(plotsize), y(plotsize);
      for(; itOriginal != originalRt.end(); ++itOriginal, ++itAligned)
        {
          x[i] = (*itOriginal);
          y[i] = (*itOriginal) - (*itAligned);
          i++;
        }
      graph()->setData(x, y);
      graph()->rescaleAxes(true);
      replot();
    }
  qDebug() << "PlotAligned::viewAlignedMsRun end";
}

void
PlotAligned::viewAlignedMS2Peaks(
  const pappso::MsRunRetentionTime<QString> *msrun_retention_time,
  const pappso::MsRunRetentionTime<QString> *msrun_retention_time_reference)
{
  qDebug() << "PlotAligned::viewAlignedMS2Peaks begin";
  if(!msrun_retention_time->isAligned())
    return;

  qDebug() << "View Ms2 point";
  /*
  _p_curve_ms2->detach();
  delete (_p_curve_ms2);
  _p_curve_ms2 = nullptr;

  this->QwtPlot::replot();
  initZoomer();
  //delete _p_curve_ms2;
  _p_curve_ms2 = new QwtPlotCurve("Common identification");
  _p_curve_ms2->attach(this);
  //QwtPlotCurve * curve = new QwtPlotCurve("Common identification");
  _p_curve_ms2->setStyle(QwtPlotCurve::Dots);
  _p_curve_ms2->setRenderHint(QwtPlotItem::RenderAntialiased);
  _p_curve_ms2->setSymbol(new QwtSymbol(QwtSymbol::Star2, QColor(Qt::black),
  QColor(Qt::black),
                                        QSize(9, 9)));
  //_alignedPlots.push_back(curve);

  const std::map<mcq_double,mcq_double>& mapDeltaPeaks =
  monitorAlignment->getMs2CommonPeak();


  //     QVector<QPointF> *data = static_cast<QVector<QPointF>
  *>(_p_curve_ms2->data());
  //_p_msms_data->clear();

  //_msms_data.clear();
  //_p_curve_ms2->detach();
  QVector< double > x;
  QVector< double > y;

  //unsigned int plotsize(mapDeltaPeaks.size());
  //mcq_double x1[plotsize], y1[plotsize];
  std::map<mcq_double,mcq_double>::const_iterator it;
  //int i = 0;
  for (it = mapDeltaPeaks.begin(); it != mapDeltaPeaks.end(); ++it) {
      QPointF one_point;
      x.push_back(it->first);
      y.push_back(it->second);
      //one_point.setX(it->first);
      //one_point.setY(it->second);
      //_msms_data.push_back(one_point);
      //y1[i] = it->second;
      //x1[i] =  it->first;
      //i++;
  }
  _p_msms_data = new QwtPointArrayData(x,y);
  _p_curve_ms2->setData(_p_msms_data);


  this->replot();
  initZoomer();
  */

  // addGraph();

  QCPGraph *points = addGraph(xAxis, yAxis);
  // addPlottable(points);
  points->setAdaptiveSampling(false);
  points->setName("common MS2 events");
  // points->removeFromLegend();
  points->setLineStyle(QCPGraph::lsNone);
  points->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 3));
  QPen pen;
  pen.setColor(QColor(Qt::red));
  points->setPen(pen);

  // QPen pen;
  // pen.setColor(QColor(Qt::red));
  // graph()->setPen(pen);
  // generate data:
  QVector<double> x, y;

  pappso::Trace delta_peaks = msrun_retention_time->getCommonSeamarksDeltaRt(
    *msrun_retention_time_reference);
  for(auto &point : delta_peaks)
    {
      x.push_back(point.x);
      y.push_back(point.y);
    }
  points->setData(x, y);
  graph()->rescaleAxes(true);

  yAxis->rescale();
  replot();
  qDebug() << "PlotAligned::viewAlignedMS2Peaks end x.size()=" << x.size();
}

void
PlotAligned::clear()
{
  qDebug() << "PlotAligned::clear() begin";
  /*
  if (_msms_data.size() > 0) {
      const QwtSymbol symbol(QwtSymbol::Star1, QColor(Qt::white),
  QColor(Qt::white),
              QSize(9, 9));
      _p_curve_ms2->setSymbol(symbol);
      //QwtData data(_p_curve_ms2->data());
      //QwtData * p_data = _p_curve_ms2->data().copy();
      _p_curve_ms2->setData(_msms_data);
      replot();
  }*/
  //_p_curve_ms2->setSamples(QVector<QPointF>());
  for(int g = 0; g < graphCount(); g++)
    {
      graph(g)->data()->clear();
    }
  int n = clearPlottables();
  replot();

  qDebug() << "PlotAligned::clear() end clearPlottables removed " << n;
}

void
PlotAligned::clearPlot()
{
  this->clear();
}

void
PlotAligned::initZoomer()
{
  // LeftButton for the zooming
  // MidButton for the panning
  // RightButton: zoom out by 1
  // Ctrl+RighButton: zoom out to full size
  setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}
