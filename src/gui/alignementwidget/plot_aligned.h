/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file plot.h
 * \brief widget to plot alignements
 * \date November 23, 2011
 * \author Edlira Nano
 *
 * it receives alignment signals from alignment workers (connected by
 * AlignmentWidget)
 * clear display and draw new alignments
 */

#pragma once

//#include <qwt_compat.h>
#include "../treatment/treatment_box.h"
#include "../treatment/treatment_box_xic_detect.h"
#include "../treatment/treatment_box_xic_extract.h"
#include "../treatment/treatment_box_xic_filter.h"
#include "../../lib/alignments/monitors/monitor_alignment_base.h"
#include <qcustomplot.h>
#include <vector>

class PlotAligned : public QCustomPlot
{
  Q_OBJECT

  public:
  PlotAligned(QWidget *parent);

  virtual ~PlotAligned();

  void clear();

  void initZoomer();

  public slots:
  void viewAlignedMsRun(MsrunSp msrun);
  void viewAlignedMS2Peaks(
    const pappso::MsRunRetentionTime<QString> *msrun_retention_time,
    const pappso::MsRunRetentionTime<QString> *msrun_retention_time_reference);
  void clearPlot();

  private:
  // std::vector<QwtPlotCurve *> _alignedPlots;
};
