/*
 * masschroqDomDocument.h
 *
 *  Created on: 18 sept. 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/alignments/alignment_ms2.h"
#include <QDomDocument>
#include <QDomElement>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include <pappsomspp/processing/filters/filtermorpho.h>

class MasschroqDomDocument
{
  public:
  MasschroqDomDocument();
  virtual ~MasschroqDomDocument();

  void newQuantificationMethod();
  void addExtractionMethod(pappso::XicExtractMethod,
                           pappso::PrecisionPtr min,
                           pappso::PrecisionPtr max);
  void addBackgroundFilterMethod(const pappso::FilterMorphoBackground &filter);
  void addSpikeFilterMethod(const pappso::FilterMorphoAntiSpike &filter);
  void addDetectionZivyMethod(const pappso::TraceDetectionZivy &detect);
  bool verifyQuantificationMethod();

  void newAlignementMethod();
  void addMs2AlignmentMethod(const AlignmentMs2 &align);

  QString getXMLString() const;

  private:
  QDomDocument _dom;
  QDomElement _quantimethod;
  QDomElement _alignmethod;
};
