/*
 * masschroq_gui_data.cpp
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#include "masschroq_gui_data.h"
#include "../../lib/msrun/ms_run_hash_group.h"
#include "../../lib/msrun/msrun.h"
#include "../../lib/peptides/peptide_isotope.h"
#include <pappsomspp/msfile/msfileaccessor.h>

Q_DECLARE_METATYPE(MsrunSp);

MasschroqGuiData::MasschroqGuiData()
{
}

MasschroqGuiData::~MasschroqGuiData()
{
}

void
MasschroqGuiData::clear()
{
  qDebug();
  _msrun_group.clear();

  qDebug();
  _peptideList.clear();
  qDebug();
  _peptideIsotopeList.clear();
  qDebug();
  _map_p_isotope_labels.clear();
  qDebug();
}

void
MasschroqGuiData::addPeptide(PeptideSPtr pep)
{
  _peptideList.push_back(pep);
}

const PeptideList &
MasschroqGuiData::getNoIsotopePeptideList()
{
  return (_peptideList);
}
const PeptideList &
MasschroqGuiData::getPeptideList()
{
  if(_map_p_isotope_labels.size() == 0)
    {

      qDebug() << "MasschroqGuiData::getPeptideList() "
                  "_map_p_isotope_labels.size() == 0";
      qDebug() << "MasschroqGuiData::getPeptideList() _peptideList.size() "
               << _peptideList.size();
      return (_peptideList);
      //_map_p_isotope_labels
    }
  _peptideIsotopeList.resize(0);
  PeptideList::const_iterator it;
  const IsotopeLabel *isotope_label;
  for(it = _peptideList.begin(); it != _peptideList.end(); ++it)
    {

      std::map<QString, const IsotopeLabel *>::const_iterator it_label;
      for(it_label = _map_p_isotope_labels.begin();
          it_label != _map_p_isotope_labels.end();
          ++it_label)
        {
          qDebug()
            << "MasschroqGuiData::getPeptideList() _map_p_isotope_labels " +
                 it_label->first;
          isotope_label = it_label->second;
          std::shared_ptr<PeptideIsotope> isotope =
            std::make_shared<PeptideIsotope>(*it, isotope_label);
          if(isotope->getMass() != (*it)->getMass())
            {
              qDebug() << "MasschroqGuiData::getPeptideList() new isotope " +
                            isotope->getSequence() + " " + isotope->getMods() +
                            " " + isotope->getIsotopeLabel()->getXmlId();
              _peptideIsotopeList.push_back(isotope);
            }
          else
            {
              // delete(isotope);
            }
        }
    }
  qDebug() << " _peptideIsotopeList.size()=" << _peptideIsotopeList.size();
  return (_peptideIsotopeList);
}

void
MasschroqGuiData::addIsotopeLabel(IsotopeLabel *p_isotope_label)
{
  _map_p_isotope_labels[p_isotope_label->getXmlId()] = p_isotope_label;
}

void
MasschroqGuiData::addMsrunFilename(const QString &id, const QString &filename)
{
  qDebug() << " " << id;


  pappso::MsFileAccessor file_access(filename, QString("msfile"));

  file_access.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                         pappso::FileReaderType::tims_ms2);


  pappso::MsRunReaderSPtr run_reader =
    file_access.getMsRunReaderSPtrByRunId("", id);

  MsrunSp msrun = std::make_shared<Msrun>(run_reader);

  _msrun_group.addMsRun(msrun);
}

MsrunSp
MasschroqGuiData::getMsrunSp(const QString &id) const
{
  return (_msrun_group.getMsRunSp(id));
}
