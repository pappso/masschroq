/*
 * masschroq_gui_data.h
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/msrun/ms_run_hash_group.h"
#include "../../lib/peptides/isotope_label.h"
#include "../../lib/peptides/peptide_list.h"
#include <QString>

class MasschroqGuiData
{
  public:
  MasschroqGuiData();
  virtual ~MasschroqGuiData();

  void addPeptide(PeptideSPtr pep);

  void addIsotopeLabel(IsotopeLabel *p_isotope_label);

  void addMsrunFilename(const QString &id, const QString &filename);

  const PeptideList &getNoIsotopePeptideList();

  const PeptideList &getPeptideList();

  const msRunHashGroup &
  getMsrunHashGroup() const
  {
    return _msrun_group;
  }

  MsrunSp getMsrunSp(const QString &id) const;

  void clear();

  private:
  msRunHashGroup _msrun_group;

  PeptideList _peptideList;
  PeptideList _peptideIsotopeList;
  std::map<QString, const IsotopeLabel *> _map_p_isotope_labels;
};
