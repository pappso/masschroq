/*
 * masschroq_gui_engin.h
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#ifndef MASSCHROQ_GUI_ENGIN_H_
#define MASSCHROQ_GUI_ENGIN_H_

#include "../../lib/msrun/msrun.h"
#include "../thread/msrunLoaderThread.h"
#include "../treatment/treatment_chain.h"
#include "masschroq_gui_data.h"

class MasschroqGuiEngin
{

  private:
  MasschroqGuiEngin();
  virtual ~MasschroqGuiEngin();
  MasschroqGuiEngin(const MasschroqGuiEngin &);
  void operator=(const MasschroqGuiEngin &);

  public:
  // Fonctions de création et destruction du singleton
  static MasschroqGuiEngin *getInstance();

  TreatmentChain &getChain();

  void setMasschroqGuiData(MasschroqGuiData *mcq_gui_data);
  MasschroqGuiData *getMasschroqGuiData() const;

  //	void LoadedMsrun(const QString & filename, MsrunLoaderThread * thread,
  //			const QString & id = "");

  MsrunSp getCurrentLoadedMsrun();
  void setCurrentLoadedMsrun(MsrunSp);

  private:
  MasschroqGuiData *_masschroqdata;

  TreatmentChain _chain;

  static MasschroqGuiEngin *_singleton;

  MsrunSp _current_msrun_sp;
};

#endif /* MASSCHROQ_GUI_ENGIN_H_ */
