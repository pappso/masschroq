/*
 * logQIODevice.h
 *
 *  Created on: 27 sept. 2012
 *      Author: valot
 */

#pragma once

#include <QIODevice>

class LogQIODevice : public QIODevice
{

  Q_OBJECT

  public:
  LogQIODevice(QObject *parent = 0);
  virtual ~LogQIODevice();

  signals:
  void appendLogString(QString log);

  protected:
  qint64
  readData(char *data [[maybe_unused]], qint64 maxSize [[maybe_unused]])
  {
    return 0;
  }
  qint64 writeData(const char *data, qint64 maxSize);
};
