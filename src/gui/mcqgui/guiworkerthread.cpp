/**
 * \file /gui/mcqgui/guiworkerthread.cpp
 * \date 21/10/2017
 * \author Olivier Langella
 * \brief GUI worker thread
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "guiworkerthread.h"

GuiWorkerThread::GuiWorkerThread(LauncherMainWindow *parent [[maybe_unused]])
{
}
GuiWorkerThread::~GuiWorkerThread()
{
}

void
GuiWorkerThread::doRunningMassChroQ(MassChroqRun mcq_run [[maybe_unused]])
{
}

void
GuiWorkerThread::doCheckMassChroQ(MassChroqRun mcq_run [[maybe_unused]])
{
}
