/**
 * \file /gui/mcqgui/guiworkerthread.h
 * \date 21/10/2017
 * \author Olivier Langella
 * \brief GUI worker thread
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QObject>

class LauncherMainWindow;

struct MassChroqRun
{
  QString _masschroq_file_path;
  QString _masschroq_tmp_dir;
  unsigned int _number_of_threads = 1;
};

class GuiWorkerThread : public QObject
{
  Q_OBJECT

  public:
  GuiWorkerThread(LauncherMainWindow *parent);
  ~GuiWorkerThread();

  public slots:
  void doRunningMassChroQ(MassChroqRun mcq_run);
  void doCheckMassChroQ(MassChroqRun mcq_run);
};
