#include "../../lib/consoleout.h"
#include "launcherMainWindow.h"
#include <QApplication>
#include <QTimer>
#include <pappsomspp/pappsoexception.h>

int
main(int argc, char **argv)
{

  QApplication app(argc, argv);
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

  QCoreApplication::setOrganizationName("PAPPSO");
  QCoreApplication::setOrganizationDomain("pappso.inra.fr");
  QCoreApplication::setApplicationName("masschroq");

  qRegisterMetaType<MassChroqRun>("MassChroqRun");
  LauncherMainWindow window;
  window.show();

  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &window, SLOT(run()));

  return app.exec();
}
