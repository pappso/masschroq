/*
 * launcherMainWindow.cpp
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#include "launcherMainWindow.h"
#include "../../lib/consoleout.h"
#include "../../lib/share/utilities.h"
#include "../../mcqsession.h"
#include "../logQIODevice.h"
#include <QAction>
#include <QApplication>
#include <QCheckBox>
#include <QCloseEvent>
#include <QDir>
#include <QDockWidget>
#include <QFileDialog>
#include <QGroupBox>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>

#include "ui_mcqgui.h"

LauncherMainWindow::LauncherMainWindow(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::GuiMainWindow)
{
  ui->setupUi(this);
  _number_of_cpus = 1;
  setWindowIcon(QIcon(":/resources/logos/masschroq.svg"));

  GuiWorkerThread *worker = new GuiWorkerThread(this);
  worker->moveToThread(&_worker_thread);
  _worker_thread.start();

  _masschroqRunningThread = nullptr;
  //_logQTextEdit=0;
  _masschroq = new MassChroq();

  QSettings settings;
  QString tmp_dir = settings.value("path/tmp_dir", QDir::tempPath()).toString();

  try
    {
      McqSession::getInstance().setTmpDir(tmp_dir);
    }
  catch(pappso::PappsoException &error)
    {
      qDebug();
      ConsoleOut::mcq_cerr() << "MassChroQ encountered an error:" << Qt::endl;
      ConsoleOut::mcq_cerr() << error.qwhat() << Qt::endl;
    }
  this->initialiseMassChroqRunningThread();
  ui->actionQuit->setShortcuts(QKeySequence::Quit);

  qDebug() << "addLogWidget begin";
  //_logQTextEdit = new LogQTextEdit(this);
  LogQIODevice *logQIODevice = new LogQIODevice();
  ConsoleOut::setCout(new QTextStream(logQIODevice));
  ConsoleOut::setCerr(new QTextStream(logQIODevice));
  // setCentralWidget(_logQTextEdit);

  ConsoleOut::mcq_cout() << "Initialising logger" << Qt::endl;

  ui->tempDirLineEdit->setText(McqSession::getInstance().getTmpDirName());
  // ui->cpuNumberSpinBox->se
  // cpu_number->setDecimals(0);

  setAttribute(Qt::WA_AlwaysShowToolTips, true);
  // Button pas to stop
  ui->startPushButton->show();
  ui->stop_push_button->hide();

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(logQIODevice,
          &LogQIODevice::appendLogString,
          this,
          &LauncherMainWindow::appendLogString);
  connect(this,
          &LauncherMainWindow::operateMassChroqFile,
          worker,
          &GuiWorkerThread::doRunningMassChroQ);
#else
  // Qt4 code
  connect(logQIODevice,
          SIGNAL(appendLogString(QString)),
          this,
          SLOT(appendLogString(QString)));
#endif
}

LauncherMainWindow::~LauncherMainWindow()
{
  delete(_masschroqRunningThread);
  delete ui;
}

void
LauncherMainWindow::run()
{
}

void
LauncherMainWindow::closeEvent(QCloseEvent *event)
{
  if(maybeSave())
    {
      _masschroqRunningThread->canceled();
      _masschroqRunningThread->terminate();
      event->accept();
    }
  else
    {
      event->ignore();
    }
}

bool
LauncherMainWindow::maybeSave()
{
  QMessageBox::StandardButton ret;
  ret = QMessageBox::warning(
    this,
    tr("MassChroQ GUI"),
    tr("Do you want to quit MassChroQ Gui and stop all current processing?"),
    QMessageBox::Ok | QMessageBox::Cancel);
  if(ret == QMessageBox::Ok)
    return true;
  else if(ret == QMessageBox::Cancel)
    return false;
  else
    return true;
}

void
LauncherMainWindow::initialiseMassChroqRunningThread()
{
  qDebug() << "initialiseMassChroqRunningThread";
  if(_masschroqRunningThread != nullptr)
    {
      delete(_masschroqRunningThread);
    }
  _masschroqRunningThread = new MasschroqRunningThread();
  // add connection
  _masschroqRunningThread->setMaxProgress(4);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_masschroqRunningThread,
          &MasschroqRunningThread::finishRunning,
          this,
          &LauncherMainWindow::finishRunningMassChroq);
  connect(_masschroqRunningThread,
          &MasschroqRunningThread::errorDuringRunning,
          this,
          &LauncherMainWindow::viewErrorDuringRunning);
#else
  // Qt4 code
  connect(_masschroqRunningThread,
          SIGNAL(finishRunning()),
          this,
          SLOT(finishRunningMassChroq()));
  connect(_masschroqRunningThread,
          SIGNAL(errorDuringRunning(QString)),
          this,
          SLOT(viewErrorDuringRunning(QString)));
#endif
  qDebug() << "Finish initialiseMassChroqRunningThread";
}

void
LauncherMainWindow::setNumberOfCpus(int z)
{
  _number_of_cpus = z;
  /*
  int i = QThread::idealThreadCount();
  if (i == -1 ) i = 1;
  ui->cpuNumberSpinBox->setMaximum(i);
  */
}

void
LauncherMainWindow::selectMassChroqFile()
{
  QSettings settings;
  QString default_location =
    settings.value("path/mcqml_default_path", "").toString();

  QString filename = QFileDialog::getOpenFileName(
    this,
    tr("Open MasschroqML File"),
    default_location,
    tr("masschroqML files (*.masschroqML &  *.xml & *.masschroqml);;all "
       "files (*)"));
  if(filename.isEmpty())
    {
      return;
    }

  settings.setValue("path/mcqml_default_path",
                    QFileInfo(filename).absolutePath());

  try
    {
      _masschroq->setXmlFilename(filename);
      QFileInfo filenameInfo(filename);
      ui->filePathLineEdit->setText(filenameInfo.fileName());
    }
  catch(mcqError &error)
    {
      viewError(error.qwhat());
    }
}

void
LauncherMainWindow::selectTempDirectory()
{
  QSettings settings;
  QString tmp_dir = settings.value("path/tmp_dir", QDir::tempPath()).toString();

  QString fdirname = QFileDialog::getExistingDirectory(
    this, tr("Choose Temporary directory"), tmp_dir, QFileDialog::ShowDirsOnly);
  if(fdirname.isEmpty())
    {
      return;
    }
  try
    {
      settings.setValue("path/tmp_dir", fdirname);
      McqSession::getInstance().setTmpDir(fdirname);
      ui->tempDirLineEdit->setText(fdirname);
    }
  catch(mcqError &error)
    {
      viewError(error.qwhat());
    }
}

void
LauncherMainWindow::startMassChroq()
{
  qDebug() << "LauncherMainWindow::startMassChroq Begin";
  QString filename = _masschroq->getXmlFilename();
  if(filename.isEmpty())
    {
      this->viewError(tr("No MassChroqML file selected"));
      return;
    }
  QSettings settings;

  MassChroqRun mcq_run;
  mcq_run._masschroq_file_path = filename;
  mcq_run._masschroq_tmp_dir =
    settings.value("path/tmp_dir", QDir::tempPath()).toString();
  mcq_run._number_of_threads = _number_of_cpus;
  McqSession::getInstance().setUnassignedPeaksOnDisk(
    ui->ondisk_checkbox->isChecked());

  emit operateMassChroqFile(mcq_run);

  MassChroq *masschroqTmp = new MassChroq();
  try
    {
      masschroqTmp->setMasschroqDir(QApplication::applicationDirPath());
      masschroqTmp->setXmlFilename(filename);
      Quantificator::setCpuNumber(_number_of_cpus);
    }
  catch(mcqError &error)
    {
      viewError(error.qwhat());
    }
  // TImer
  _dt_begin = QDateTime::currentDateTime();
  // Remove Text on logger
  ui->mcqLogTextEdit->setText("");
  // Start Running
  _masschroqRunningThread->onlyParsePeptide(ui->parseOnlyBox->isChecked());
  _masschroqRunningThread->runMassChroqML(masschroqTmp);

  // Button pas to stop
  ui->startPushButton->hide();
  ui->stop_push_button->show();
}

void
LauncherMainWindow::AbordMassChroqRunning()
{
  if(this->close())
    {
      // Stop Thread
      qDebug() << "AbordMassChroqRunning";
      _masschroqRunningThread->canceled();
      _masschroqRunningThread->terminate();
      qDebug() << "Finish AbordMassChroqRunning";
    }
}
void
LauncherMainWindow::finishRunningMassChroq()
{
  this->initialiseMassChroqRunningThread();
  // Add Time
  QDateTime dt_end = QDateTime::currentDateTime();

  Duration dur = Utilities::getDurationFromDates(_dt_begin, dt_end);

  ConsoleOut::mcq_cout() << "MassChroQ finished" << Qt::endl;
  ConsoleOut::mcq_cout() << "MassChroQ execution time : "
                         << Utilities::getDaysFromDuration(dur) << " days, "
                         << Utilities::getHoursFromDuration(dur) << " hours, "
                         << Utilities::getMinutesFromDuration(dur)
                         << " minutes, "
                         << Utilities::getSecondsFromDuration(dur)
                         << " seconds." << Qt::endl
                         << Qt::endl;

  // Button pas to stop
  ui->startPushButton->show();
  ui->stop_push_button->hide();
}
void
LauncherMainWindow::viewErrorDuringRunning(QString error)
{
  this->initialiseMassChroqRunningThread();
  // view Error
  this->viewError(error);

  // Button pas to stop
  ui->startPushButton->show();
  ui->stop_push_button->hide();
}
void
LauncherMainWindow::viewError(QString error)
{
  QMessageBox::warning(this, tr("MassChroQ encountered an error:"), error);
}

void
LauncherMainWindow::about()
{
  QMessageBox::about(
    this,
    tr("About MassChroQ"),
    tr("This is <b>MassChroQ Gui</b> version %1").arg(MASSCHROQ_VERSION));
}

void
LauncherMainWindow::appendLogString(QString log)
{
  ui->mcqLogTextEdit->moveCursor(QTextCursor::End);
  ui->mcqLogTextEdit->insertPlainText(log);
}
