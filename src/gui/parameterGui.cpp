#include "../lib/consoleout.h"
#include "../mcqsession.h"
#include "parameterMainWindow.h"
#include <QApplication>
#include "../lib/peptides/peptide.h"

int
main(int argc, char **argv)
{

  QApplication app(argc, argv);
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

  qRegisterMetaType<MsrunSp>("MsrunSp");
  qRegisterMetaType<PeptideSPtr>("PeptideSPtr");
  ParameterMainWindow window;
  window.show();

  McqSession::getInstance().setTmpDir(QDir::tempPath());

  return app.exec();
}
