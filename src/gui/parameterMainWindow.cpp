#include <QApplication>
#include <QCloseEvent>
#include <QDialog>
#include <QDockWidget>
#include <QFileDialog>
#include <QMenuBar>
#include <QMessageBox>
#include <QScrollArea>
#include <QTextEdit>

#include "../lib/consoleout.h"
#include "../mcqsession.h"
#include "alignementwidget/ms2_alignment_widget.h"
#include "dom_methods/masschroqDomDocument.h"
#include "engine/masschroq_gui_engin.h"
#include "logQIODevice.h"
#include "parameterMainWindow.h"
#include "quantificationwidget/detectionZivyWidget.h"
#include "quantificationwidget/filterBackgroundWidget.h"
#include "quantificationwidget/filterSpikeWidget.h"
#include "quantificationwidget/masschroqml_selection_widget.h"
#include "quantificationwidget/msrun_selection_widget.h"
#include "quantificationwidget/peptideSelectionWidget.h"
#include "quantificationwidget/xicSelectionWidget.h"

ParameterMainWindow::ParameterMainWindow(QWidget *parent) : QMainWindow(parent)
{
  setWindowIcon(QIcon(":/resources/logos/masschroq.svg"));
  dock               = 0;
  _dockLog           = 0;
  _plot_area         = 0;
  _plot_area_aligned = 0;
  _logQTextEdit      = 0;

  McqSession::getInstance().setTmpDir(QDir::tempPath());
  // Add menus and Actions
  LoadFileMenus();

  QTextEdit *welcomeMessage = new QTextEdit(this);
  QString message;
  message.append("<p><b>Welcome to the MassChroq Studio application</b></p>");
  message.append(
    "<p>This application helps you to test parameters before "
    "starting masschroq analysis</p>");
  message.append("<p>Please select one of the workspace to evaluate :");
  message.append(
    "<ul><li>Quantification (file->workspace->quantification)</li>");
  message.append("<li>Alignment (file->workspace->Alignment)</li></ul></p>");
  welcomeMessage->setReadOnly(true);
  welcomeMessage->setText(message);
  this->setCentralWidget(welcomeMessage);

  this->setAttribute(Qt::WA_AlwaysShowToolTips, true);

  setWindowTitle(tr("MassChroQ Studio"));
  // cannot resize the main window to less than 200x200
  setMinimumSize(200, 200);
  // size at creation is 480x320
  resize(480, 320);
}

void
ParameterMainWindow::ResetWorkspace()
{
  qDebug() << "ResetWorkspace begin";
  // Remove older menu
  menuBar()->clear();
  if(_plot_area != 0)
    {
      delete(_plot_area);
      _plot_area = 0;
    }
  if(_plot_area_aligned != 0)
    {
      delete(_plot_area_aligned);
      _plot_area_aligned = 0;
    }
  qDebug() << "ResetWorkspace end";
}
void
ParameterMainWindow::LoadFileMenus()
{
  qDebug() << "LoadFileMenus begin";
  QMenu *_fileMenu = menuBar()->addMenu(tr("&File"));

  QMenu *_workscapeMenu = _fileMenu->addMenu(tr("&Workspace"));

  QAction *_quantiAct = new QAction(tr("&Quantification"), this);
  _quantiAct->setShortcut(tr("Ctrl+Q"));

  _quantiAct->setToolTip(tr("Load quantification workspace"));
  _workscapeMenu->addAction(_quantiAct);

  QAction *_alignAct = new QAction(tr("&Alignment"), this);
  _alignAct->setShortcut(tr("Ctrl+A"));
  _alignAct->setToolTip(tr("Load quantification workspace"));
  _workscapeMenu->addAction(_alignAct);

  //_fileMenu->addAction(_loadMsrunAct);
  _fileMenu->addSeparator();
  QAction *_quitAct = new QAction(tr("&Quit"), this);
  _quitAct->setShortcuts(QKeySequence::Quit);
  _quitAct->setToolTip(tr("Quit MassChroQ GUI"));
  _fileMenu->addAction(_quitAct);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_quantiAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::LoadQuantificationWorkspace);
  connect(_alignAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::LoadAlignmentWorkspace);
  connect(_quitAct, &QAction::triggered, this, &ParameterMainWindow::close);
#else
  // Qt4 code
  connect(
    _quantiAct, SIGNAL(triggered()), this, SLOT(LoadQuantificationWorkspace()));
  connect(_alignAct, SIGNAL(triggered()), this, SLOT(LoadAlignmentWorkspace()));
  connect(_quitAct, SIGNAL(triggered()), this, SLOT(close()));
#endif
  qDebug() << "LoadFileMenus end";
}

void
ParameterMainWindow::LoadQuantificationWorkspace()
{
  qDebug() << "Load quantification Workspace";
  this->ResetWorkspace();
  this->LoadFileMenus();
  qDebug() << "Load quantification Workspace 1";

  QMenu *_toolsMenu = menuBar()->addMenu(tr("&Tools"));
  QAction *extractionXicAct =
    new QAction(tr("&MsRun data (mzXML or mzML)"), this);
  extractionXicAct->setToolTip(tr("Select MsRun data mode to extracted XIC"));
  _toolsMenu->addAction(extractionXicAct);

  QAction *masschroqmlXicAct =
    new QAction(tr("&Peptide data (masschroqML)"), this);
  masschroqmlXicAct->setToolTip(
    tr("Select MassChroqML resources and extracted XIC from peptide"));
  _toolsMenu->addAction(masschroqmlXicAct);

  _toolsMenu->addSeparator();

  QMenu *_filtersMenu = _toolsMenu->addMenu(tr("&Filter XIC"));

  QAction *_filterBackgroundAct = new QAction(tr("&Background filter"), this);
  _filterBackgroundAct->setToolTip(
    tr("Apply a backround noise removal filter to the XIC"));
  _filtersMenu->addAction(_filterBackgroundAct);

  QAction *_filterSpikeAct = new QAction(tr("&Spike filter"), this);
  _filterSpikeAct->setToolTip(tr("Apply a spike removal filter to the XIC"));
  _filtersMenu->addAction(_filterSpikeAct);

  _toolsMenu->addSeparator();

  QMenu *_detectionsMenu = _toolsMenu->addMenu(tr("&Detect peaks"));

  QAction *_detectionZivyAct = new QAction(tr("&Zivy peak detection"), this);
  _detectionZivyAct->setToolTip(tr("Detect peaks on XIC with the Zivy method"));
  _detectionsMenu->addAction(_detectionZivyAct);

  _toolsMenu->addSeparator();

  QAction *_exportAct = new QAction(tr("&Export method"), this);
  _toolsMenu->addAction(_exportAct);

  QMenu *_helpMenu = menuBar()->addMenu(tr("&Help"));

  QAction *_aboutAct = new QAction(tr("&About"), this);
  _aboutAct->setToolTip(tr("About MassChroQ GUI"));
  _helpMenu->addAction(_aboutAct);

  qDebug() << "Load quantification Workspace 2";

  // qDebug() << "QCoreApplication argc "
  //		<< QCoreApplication::instance()->argc();
  // Central Widget
  _plot_area = new Plot(this);
  this->setCentralWidget(_plot_area);
  qDebug() << "setCentralWidget after";

  qDebug() << "Load quantification Workspace 3";
  // Add dock
  this->extractXicWidget();
  qDebug() << "LoadQuantificationWorkspace end";
  this->addLogWidget();

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractionXicAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::extractXicWidget);
  connect(masschroqmlXicAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::masschroqmlXicWidget);
  connect(_filterBackgroundAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::filterBackgroundWidget);
  connect(_filterSpikeAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::filterSpikeWidget);
  connect(_detectionZivyAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::detectionZivyWidget);
  connect(_exportAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::exportXmlQuantificationMethod);
  connect(_aboutAct, &QAction::triggered, this, &ParameterMainWindow::about);
#else
  // Qt4 code
  connect(
    extractionXicAct, SIGNAL(triggered()), this, SLOT(extractXicWidget()));
  connect(
    masschroqmlXicAct, SIGNAL(triggered()), this, SLOT(masschroqmlXicWidget()));
  connect(_filterBackgroundAct,
          SIGNAL(triggered()),
          this,
          SLOT(filterBackgroundWidget()));
  connect(
    _filterSpikeAct, SIGNAL(triggered()), this, SLOT(filterSpikeWidget()));
  connect(
    _detectionZivyAct, SIGNAL(triggered()), this, SLOT(detectionZivyWidget()));
  connect(_exportAct,
          SIGNAL(triggered()),
          this,
          SLOT(exportXmlQuantificationMethod()));
  connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));
#endif
}

void
ParameterMainWindow::LoadAlignmentWorkspace()
{
  qDebug() << "Load Alignement Workspace";
  this->ResetWorkspace();
  this->LoadFileMenus();

  QMenu *_toolsMenu   = menuBar()->addMenu(tr("&Tools"));
  QAction *obiwarpAct = new QAction(tr("&Obiwarp alignment (MS data)"), this);
  obiwarpAct->setToolTip(
    tr("Select MsRun resources and aligned run based on MS data"));
  _toolsMenu->addAction(obiwarpAct);

  QAction *ms2Act = new QAction(tr("&MS2 alignment (MS/MS data)"), this);
  ms2Act->setToolTip(
    tr("Select MassChroqML resources and aligned run based on MS/MS data"));
  _toolsMenu->addAction(ms2Act);

  _toolsMenu->addSeparator();

  QAction *_exportAct = new QAction(tr("&Export method"), this);
  _toolsMenu->addAction(_exportAct);

  QMenu *_helpMenu = menuBar()->addMenu(tr("&Help"));

  QAction *_aboutAct = new QAction(tr("&About"), this);
  _aboutAct->setToolTip(tr("About MassChroQ GUI"));
  _helpMenu->addAction(_aboutAct);

  // Central Widget
  _plot_area_aligned = new PlotAligned(this);
  this->setCentralWidget(_plot_area_aligned);

  this->ms2AlignmentWidget();
  qDebug() << "Load Alignement Workspace end";
  this->addLogWidget();

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ms2Act,
          &QAction::triggered,
          this,
          &ParameterMainWindow::ms2AlignmentWidget);
  connect(_exportAct,
          &QAction::triggered,
          this,
          &ParameterMainWindow::exportXmlALignmentMethod);
  connect(_aboutAct, &QAction::triggered, this, &ParameterMainWindow::about);
#else
  // Qt4 code
  connect(
    obiwarpAct, SIGNAL(triggered()), this, SLOT(obiwarpAlignmentWidget()));
  connect(ms2Act, SIGNAL(triggered()), this, SLOT(ms2AlignmentWidget()));
  connect(
    _exportAct, SIGNAL(triggered()), this, SLOT(exportXmlALignmentMethod()));
  connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));
#endif
}

void
ParameterMainWindow::closeEvent(QCloseEvent *event)
{
  if(maybeSave())
    {
      event->accept();
    }
  else
    {
      event->ignore();
    }
}

bool
ParameterMainWindow::maybeSave()
{
  QMessageBox::StandardButton ret;
  ret = QMessageBox::warning(this,
                             tr("MassChroQ GUI"),
                             tr("Do you want to quit MassChroQ Gui?"),
                             QMessageBox::Ok | QMessageBox::Cancel);
  if(ret == QMessageBox::Ok)
    return true;
  else if(ret == QMessageBox::Cancel)
    return false;
  else
    return true;
}

void
ParameterMainWindow::extractXicWidget()
{
  qDebug() << "New extract xic widget";
  this->resetDockTools();

  // Test msRun selection
  MsrunSelectionWidget *select = new MsrunSelectionWidget(dockwidget);
  vbox->addWidget(select);

  TreatmentBoxXicExtract *treat =
    MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicExtract();
  XicSelectionWidget *xic = new XicSelectionWidget(treat, dockwidget);
  vbox->addWidget(xic);
  vbox->addStretch(0);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(
    select, &MsrunSelectionWidget::resetData, _plot_area, &Plot::clearPlot);
  connect(
    treat, &TreatmentBoxXicExtract::createdXic, _plot_area, &Plot::viewNewPlot);
  connect(treat, &TreatmentBoxXicExtract::onDelete, _plot_area, &Plot::remove);
#else
  // Qt4 code
  connect(select, SIGNAL(resetData()), _plot_area, SLOT(clearPlot()));
  connect(treat,
          SIGNAL(createdXic(const TreatmentBoxXicExtract *)),
          _plot_area,
          SLOT(viewNewPlot(const TreatmentBoxXicExtract *)));
  connect(treat,
          SIGNAL(onDelete(TreatmentBox *)),
          _plot_area,
          SLOT(remove(TreatmentBox *)));
#endif
}

void
ParameterMainWindow::masschroqmlXicWidget()
{
  qDebug() << "New masschroqml xic widget";
  this->resetDockTools();

  MasschroqmlSelectionWidget *select =
    new MasschroqmlSelectionWidget(dockwidget);
  vbox->addWidget(select);

  TreatmentBoxXicExtract *treat =
    MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicExtract();
  PeptideSelectionWidget *xic = new PeptideSelectionWidget(treat, dockwidget);
  vbox->addWidget(xic);
  vbox->addStretch(0);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(select,
          &MasschroqmlSelectionWidget::resetData,
          _plot_area,
          &Plot::clearPlot);
  connect(
    treat, &TreatmentBoxXicExtract::createdXic, _plot_area, &Plot::viewNewPlot);
  connect(treat, &TreatmentBoxXicExtract::onDelete, _plot_area, &Plot::remove);
  connect(select,
          &MasschroqmlSelectionWidget::updateMsRunData,
          xic,
          &PeptideSelectionWidget::updatePeptideList);
  connect(select,
          &MasschroqmlSelectionWidget::resetData,
          xic,
          &PeptideSelectionWidget::clearPeptideList);
#else
  // Qt4 code
  connect(select, SIGNAL(resetData()), _plot_area, SLOT(clearPlot()));
  connect(treat,
          SIGNAL(createdXic(const TreatmentBoxXicExtract *)),
          _plot_area,
          SLOT(viewNewPlot(const TreatmentBoxXicExtract *)));
  connect(treat,
          SIGNAL(onDelete(TreatmentBox *)),
          _plot_area,
          SLOT(remove(TreatmentBox *)));
  connect(select, SIGNAL(updateMsRunData()), xic, SLOT(updatePeptideList()));
  connect(select, SIGNAL(resetData()), xic, SLOT(clearPeptideList()));
#endif
}

void
ParameterMainWindow::addLogWidget()
{
  qDebug() << "addLogWidget begin";
  if(_logQTextEdit == 0)
    {
      _logQTextEdit = new QTextEdit(this);
      _logQTextEdit->setReadOnly(true);
      LogQIODevice *logQIODevice = new LogQIODevice();
      ConsoleOut::setCout(new QTextStream(logQIODevice));
      ConsoleOut::setCerr(new QTextStream(logQIODevice));

#if QT_VERSION >= 0x050000
      // Qt5 code
      connect(logQIODevice,
              &LogQIODevice::appendLogString,
              this,
              &ParameterMainWindow::appendLogString);
#else
      // Qt4 code
      connect(logQIODevice,
              &LogQIODevice::appendLogString,
              this,
              &ParameterMainWindow::appendLogString);
#endif
    }
  if(_dockLog == 0)
    {
      _dockLog = new QDockWidget(tr("Log"), this);
      _dockLog->setFeatures(QDockWidget::DockWidgetMovable |
                            QDockWidget::DockWidgetFloatable);
      _dockLog->setWidget(_logQTextEdit);
      addDockWidget(Qt::BottomDockWidgetArea, _dockLog);
      ConsoleOut::mcq_cout() << "Initialising logger\n";
    }

  qDebug() << "addLogWidget end";
}

void
ParameterMainWindow::resetDockTools()
{
  if(dock != 0)
    {
      delete(dock);
      dock = 0;
    }
  MasschroqGuiEngin::getInstance()->getChain().removedAll();
  dock = new QDockWidget(tr("Tools"), this);
  dock->setFeatures(QDockWidget::DockWidgetMovable |
                    QDockWidget::DockWidgetFloatable);
  QScrollArea *scrollArea = new QScrollArea();

  vbox = new QVBoxLayout;
  vbox->setSizeConstraint(QLayout::SetMinAndMaxSize);
  dockwidget = new QWidget();
  dockwidget->setLayout(vbox);

  scrollArea->setWidget(dockwidget);
  scrollArea->setWidgetResizable(true);
  dock->setWidget(scrollArea);
  addDockWidget(Qt::LeftDockWidgetArea, dock);
  qDebug() << "New Dock";
}

void
ParameterMainWindow::filterBackgroundWidget()
{
  TreatmentBoxXicFilter *treat =
    MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicFilter();
  FilterBackgroundWidget *backgroundFilter =
    new FilterBackgroundWidget(treat, dockwidget);
  vbox->removeItem(vbox->itemAt(vbox->count() - 1));
  vbox->addWidget(backgroundFilter);
  vbox->addStretch(0);


  _plot_area->updatedCurrentPlot(treat);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(treat,
          &TreatmentBoxXicFilter::changedXic,
          _plot_area,
          &Plot::updatedCurrentPlot);
  connect(treat, &TreatmentBoxXicFilter::onDelete, _plot_area, &Plot::remove);
#else
  // Qt4 code
  connect(treat,
          SIGNAL(changedXic(const TreatmentBoxXicFilter *)),
          _plot_area,
          SLOT(updatedCurrentPlot(const TreatmentBoxXicFilter *)));
  connect(treat,
          SIGNAL(onDelete(TreatmentBox *)),
          _plot_area,
          SLOT(remove(TreatmentBox *)));
#endif
}

void
ParameterMainWindow::filterSpikeWidget()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  TreatmentBoxXicFilter *treat =
    MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicFilter();
  FilterSpikeWidget *spikeFilter = new FilterSpikeWidget(treat, dockwidget);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  vbox->removeItem(vbox->itemAt(vbox->count() - 1));
  vbox->addWidget(spikeFilter);
  vbox->addStretch(0);

  _plot_area->updatedCurrentPlot(treat);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(treat,
          &TreatmentBoxXicFilter::changedXic,
          _plot_area,
          &Plot::updatedCurrentPlot);
  connect(treat, &TreatmentBoxXicFilter::onDelete, _plot_area, &Plot::remove);
#else
  // Qt4 code
  connect(treat,
          SIGNAL(changedXic(const TreatmentBoxXicFilter *)),
          _plot_area,
          SLOT(updatedCurrentPlot(const TreatmentBoxXicFilter *)));
  connect(treat,
          SIGNAL(onDelete(TreatmentBox *)),
          _plot_area,
          SLOT(remove(TreatmentBox *)));
#endif
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
ParameterMainWindow::detectionZivyWidget()
{
  TreatmentBoxXicDetect *treat =
    MasschroqGuiEngin::getInstance()->getChain().addNewTreatmentBoxXicDetect();
  DetectionZivyWidget *detectionZivy =
    new DetectionZivyWidget(treat, dockwidget);
  vbox->removeItem(vbox->itemAt(vbox->count() - 1));
  vbox->addWidget(detectionZivy);
  vbox->addStretch(0);

  _plot_area->updatedPeaks(treat);
#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(treat,
          &TreatmentBoxXicDetect::detectedPeaks,
          _plot_area,
          &Plot::updatedPeaks);
  connect(treat, &TreatmentBoxXicDetect::onDelete, _plot_area, &Plot::remove);
#else
  // Qt4 code
  connect(treat,
          SIGNAL(detectedPeaks(const TreatmentBoxXicDetect *)),
          _plot_area,
          SLOT(updatedPeaks(const TreatmentBoxXicDetect *)));
  connect(treat,
          SIGNAL(onDelete(TreatmentBox *)),
          _plot_area,
          SLOT(remove(TreatmentBox *)));
#endif
}

void
ParameterMainWindow::ms2AlignmentWidget()
{
  qDebug() << "ParameterMainWindow::ms2AlignmentWidget begin";
  this->resetDockTools();
  Ms2AlignmentWidget *ms2 = new Ms2AlignmentWidget(dockwidget);
  vbox->addWidget(ms2);
  vbox->addStretch(1);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ms2,
          &Ms2AlignmentWidget::newAlignmentCurve,
          _plot_area_aligned,
          &PlotAligned::clearPlot);
  connect(ms2,
          &Ms2AlignmentWidget::finishAlignment,
          _plot_area_aligned,
          &PlotAligned::viewAlignedMsRun);
  connect(ms2,
          &Ms2AlignmentWidget::finishMonitorAlignment,
          _plot_area_aligned,
          &PlotAligned::viewAlignedMS2Peaks);
#else
  // Qt4 code
  connect(
    ms2, SIGNAL(newAlignmentCurve()), _plot_area_aligned, SLOT(clearPlot()));
  connect(ms2,
          SIGNAL(finishAlignment(const Msrun *)),
          _plot_area_aligned,
          SLOT(viewAlignedMsRun(const Msrun *)));
  connect(ms2,
          SIGNAL(finishMonitorAlignment(const MonitorAlignmentPlot *)),
          _plot_area_aligned,
          SLOT(viewAlignedMS2Peaks(const MonitorAlignmentPlot *)));
#endif
  qDebug() << "ParameterMainWindow::ms2AlignmentWidget end";
}

void
ParameterMainWindow::about()
{
  QMessageBox::about(
    this,
    tr("About MassChroQ"),
    tr("This is <b>MassChroQ Studio</b> version %1").arg(MASSCHROQ_VERSION));
}

void
ParameterMainWindow::exportXmlQuantificationMethod()
{
  qDebug() << "Export quantification xml method";
  MasschroqDomDocument *dom = new MasschroqDomDocument();
  try
    {
      dom->newQuantificationMethod();
      for(int i = 0; i < (vbox->count() - 1); i++)
        {
          QWidget *temp          = vbox->itemAt(i)->widget();
          MasschroQWidget *quant = (MasschroQWidget *)temp;
          quant->writeElement(dom);
        }
      dom->verifyQuantificationMethod();
      // VIew xml on QDialog
      QDialog *dialog = new QDialog(this);
      dialog->setModal(true);
      dialog->setWindowTitle(tr("Quantification Method on XML"));
      QVBoxLayout *item = new QVBoxLayout();
      QTextEdit *edit   = new QTextEdit(dialog);
      edit->setText(dom->getXMLString());
      item->addWidget(edit);
      dialog->setLayout(item);
      dialog->setMinimumSize(400, 350);
      dialog->show();
    }
  catch(mcqError &error)
    {
      QMessageBox::warning(
        this,
        tr("MassChroQ encountered an error:"),
        error.qwhat());
    }
  delete(dom);
}

void
ParameterMainWindow::exportXmlALignmentMethod()
{
  qDebug() << "Export alignment xml method";
  MasschroqDomDocument *dom = new MasschroqDomDocument();
  try
    {
      dom->newAlignementMethod();
      QWidget *temp          = vbox->itemAt(0)->widget();
      MasschroQWidget *align = (MasschroQWidget *)temp;
      align->writeElement(dom);
      // VIew xml on QDialog
      QDialog *dialog = new QDialog(this);
      dialog->setModal(true);
      dialog->setWindowTitle(tr("Alignment Method on XML"));
      QVBoxLayout *item = new QVBoxLayout();
      QTextEdit *edit   = new QTextEdit(dialog);
      edit->setText(dom->getXMLString());
      item->addWidget(edit);
      dialog->setLayout(item);
      dialog->setMinimumSize(450, 200);
      dialog->show();
    }
  catch(mcqError &error)
    {
      QMessageBox::warning(
        this,
        tr("MassChroQ encountered an error:"),
        error.qwhat());
    }
  delete(dom);
}

void
ParameterMainWindow::appendLogString(QString log)
{
  _logQTextEdit->moveCursor(QTextCursor::End);
  _logQTextEdit->insertPlainText(log);
}
