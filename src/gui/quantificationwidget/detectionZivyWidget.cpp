/**
 * \file detectionZivyWidget.cpp
 * \date February 29, 2012
 * \author Edlira Nano
 */

#include "detectionZivyWidget.h"
#include "../../lib/consoleout.h"
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QSpinBox>
#include <QVBoxLayout>
#include <limits>

DetectionZivyWidget::DetectionZivyWidget(TreatmentBoxXicDetect *treatmentBox,
                                         QWidget *parent)
  : MasschroQWidget(parent),
    _smoothing_filter_half_window(1),
    _minmax_filter_half_window(3),
    _maxmin_filter_half_window(2),
    _threshold_on_max(2000),
    _threshold_on_min(1000),
    _p_treatmentBox(treatmentBox)
{
  // initialize detection
  initializeDetectionMethod();

  QVBoxLayout *mainLayout = new QVBoxLayout;
  setWidgetParameters();
  mainLayout->addWidget(_parameter_group);
  // mainLayout->addWidget(_button_box);
  setLayout(mainLayout);
  setWindowTitle(tr("Zivy peak detection"));

  // Trigger Detect
  detectPeaks();
}

DetectionZivyWidget::~DetectionZivyWidget()
{
  ConsoleOut::mcq_cout() << "delete detection widget" << Qt::endl;
}

void
DetectionZivyWidget::initializeDetectionMethod()
{
  _detection_method =
    std::make_shared<pappso::TraceDetectionZivy>(_smoothing_filter_half_window,
                                                 _minmax_filter_half_window,
                                                 _maxmin_filter_half_window,
                                                 _threshold_on_max,
                                                 _threshold_on_min);
}

void
DetectionZivyWidget::setWidgetParameters()
{
  _parameter_group    = new QGroupBox("Peak detection");
  QFormLayout *layout = new QFormLayout;

  QSpinBox *smooth_box = new QSpinBox;
  smooth_box->setMaximum(20);
  smooth_box->setWrapping(true);
  smooth_box->setSingleStep(1);
  smooth_box->setValue(_smoothing_filter_half_window);
  layout->addRow(tr("Smoothing filter half window"), smooth_box);

  QSpinBox *minmax_box = new QSpinBox;
  minmax_box->setMaximum(20);
  minmax_box->setWrapping(true);
  minmax_box->setSingleStep(1);
  minmax_box->setValue(_minmax_filter_half_window);
  layout->addRow(tr("MinMax close filter half window"), minmax_box);

  QSpinBox *maxmin_box = new QSpinBox;
  maxmin_box->setMaximum(20);
  maxmin_box->setWrapping(true);
  maxmin_box->setSingleStep(1);
  maxmin_box->setValue(_maxmin_filter_half_window);
  layout->addRow(tr("MaxMin open filter half window"), maxmin_box);

  QDoubleSpinBox *thresholdmax_box = new QDoubleSpinBox;
  thresholdmax_box->setMaximum(1E9);
  thresholdmax_box->setDecimals(0);
  thresholdmax_box->setWrapping(true);
  thresholdmax_box->setSingleStep(1000);
  thresholdmax_box->setValue(_threshold_on_max);
  layout->addRow(tr("Threshold on Max"), thresholdmax_box);

  QDoubleSpinBox *thresholdmin_box = new QDoubleSpinBox;
  thresholdmin_box->setMaximum(1E9);
  thresholdmin_box->setDecimals(0);
  thresholdmin_box->setWrapping(true);
  thresholdmin_box->setSingleStep(1000);
  thresholdmin_box->setValue(_threshold_on_min);
  layout->addRow(tr("Threshold on Min"), thresholdmin_box);

  _parameter_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(smooth_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &DetectionZivyWidget::setSmoothingFilterHalfWindow);

  connect(minmax_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &DetectionZivyWidget::setMinMaxFilterHalfWindow);

  connect(maxmin_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &DetectionZivyWidget::setMaxMinFilterHalfWindow);

  connect(thresholdmax_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &DetectionZivyWidget::setThresholdOnMax);

  connect(thresholdmin_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &DetectionZivyWidget::setThresholdOnMin);
#else
  // Qt4 code
  connect(smooth_box,
          SIGNAL(valueChanged(int)),
          this,
          SLOT(setSmoothingFilterHalfWindow(int)));

  connect(minmax_box,
          SIGNAL(valueChanged(int)),
          this,
          SLOT(setMinMaxFilterHalfWindow(int)));

  connect(maxmin_box,
          SIGNAL(valueChanged(int)),
          this,
          SLOT(setMaxMinFilterHalfWindow(int)));

  connect(thresholdmax_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setThresholdOnMax(double)));

  connect(thresholdmin_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setThresholdOnMin(double)));
#endif
}

void
DetectionZivyWidget::setSmoothingFilterHalfWindow(int i)
{
  _smoothing_filter_half_window = i;
  _detection_method->setFilterMorphoMean(
    pappso::FilterMorphoMean(_smoothing_filter_half_window));
  detectPeaks();
}

void
DetectionZivyWidget::setMinMaxFilterHalfWindow(int i)
{
  _minmax_filter_half_window = i;
  _detection_method->setFilterMorphoMinMax(
    pappso::FilterMorphoMinMax(_minmax_filter_half_window));
  detectPeaks();
}

void
DetectionZivyWidget::setMaxMinFilterHalfWindow(int i)
{
  _maxmin_filter_half_window = i;
  _detection_method->setFilterMorphoMaxMin(
    pappso::FilterMorphoMaxMin(_maxmin_filter_half_window));
  detectPeaks();
}

void
DetectionZivyWidget::setThresholdOnMin(double d)
{
  _threshold_on_min = d;
  _detection_method->setDetectionThresholdOnMaxmin(_threshold_on_min);
  detectPeaks();
}

void
DetectionZivyWidget::setThresholdOnMax(double d)
{
  _threshold_on_max = d;
  _detection_method->setDetectionThresholdOnMinmax(_threshold_on_max);
  detectPeaks();
}

void
DetectionZivyWidget::detectPeaks()
{
  _p_treatmentBox->setPeakDetectionBase(_detection_method);
}

void
DetectionZivyWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  domDocument->addDetectionZivyMethod(*_detection_method);
}
