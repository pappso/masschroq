/*
 *
 *  File detectionWidget.h in
 *  MassChroQ: Mass Chromatogram Quantification software.
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
 * \file detectionWidget.h
 * \date February 29, 2012
 * \author Edlira Nano
 */

#pragma once

#include "../masschroQWidget.h"
#include "../treatment/treatment_box_xic_detect.h"
#include <QWidget>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>

class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;
class QSpinBox;

/**
 * \class DetectionZivyWidget
 * \brief Zivy peak detection method Widget
 *
 *
 **/

class DetectionZivyWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  DetectionZivyWidget(TreatmentBoxXicDetect *treatmentBox, QWidget *parent = 0);

  virtual ~DetectionZivyWidget();

  void writeElement(MasschroqDomDocument *domDocument) const;

  private slots:

  void detectPeaks();

  void setSmoothingFilterHalfWindow(int i);
  void setMinMaxFilterHalfWindow(int i);
  void setMaxMinFilterHalfWindow(int i);
  void setThresholdOnMax(double d);
  void setThresholdOnMin(double d);

  protected:
  std::shared_ptr<pappso::TraceDetectionZivy> _detection_method;
  int _smoothing_filter_half_window;
  int _minmax_filter_half_window;
  int _maxmin_filter_half_window;
  double _threshold_on_max;
  double _threshold_on_min;

  virtual void setWidgetParameters();

  private:
  QGroupBox *_parameter_group;
  QDialogButtonBox *_button_box;
  TreatmentBoxXicDetect *_p_treatmentBox;

  void initializeDetectionMethod();
};
