/**
 * \file filterBackgroundWidget.cpp
 * \date November 24, 2011
 * \author Edlira Nano
 */

#include "filterBackgroundWidget.h"
#include "../../lib/consoleout.h"
#include <QLabel>
#include <QSpinBox>
#include <QVBoxLayout>

FilterBackgroundWidget::FilterBackgroundWidget(
  TreatmentBoxXicFilter *treatmentBox, QWidget *parent)
  : MasschroQWidget(parent),
    _p_treatmentBox(treatmentBox),
    MEDIAN_HALF_WINDOW(5),
    MINMAX_HALF_WINDOW(20)
{
  setAttribute(Qt::WA_DeleteOnClose);
  // initialize filter
  initializeFilter();

  //	QPushButton * filterButton = new QPushButton(tr("&Filter"));
  //	filterButton->setDefault(true);
  //	_button_box = new QDialogButtonBox(Qt::Vertical);
  //	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
  //	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));
  //	QPushButton * removeButton = new QPushButton(tr("&Remove"));
  //	_button_box->addButton(removeButton, QDialogButtonBox::DestructiveRole);
  //	connect(_button_box, SIGNAL(clicked()), this, SLOT(removeFilter()));

  QVBoxLayout *mainLayout = new QVBoxLayout;
  setWidgetParameters();
  mainLayout->addWidget(_parameter_group);
  // mainLayout->addWidget(_button_box);
  setLayout(mainLayout);
  setWindowTitle(tr("Background noise filter"));

  // Trigger filter
  this->filterXic();
}

FilterBackgroundWidget::~FilterBackgroundWidget()
{
  ConsoleOut::mcq_cout() << "delete filter background" << Qt::endl;
}

void
FilterBackgroundWidget::initializeFilter()
{
  _median_half_window  = MEDIAN_HALF_WINDOW;
  _min_max_half_window = MINMAX_HALF_WINDOW;

  _filter_background = std::make_shared<pappso::FilterMorphoBackground>(
    _median_half_window, _min_max_half_window);
  _p_treatmentBox->setFilter(_filter_background);
}

void
FilterBackgroundWidget::setWidgetParameters()
{

  _parameter_group    = new QGroupBox("Background Filter");
  QGridLayout *layout = new QGridLayout;

  QLabel *median_label = new QLabel("Median filtering half window");
  QSpinBox *median_box = new QSpinBox;
  median_box->setMaximum(100);
  median_box->setWrapping(true);
  median_box->setSingleStep(1);
  median_box->setValue(MEDIAN_HALF_WINDOW);
  layout->addWidget(median_label, 0, 0);
  layout->addWidget(median_box, 0, 1);

  QLabel *minmax_label = new QLabel("MinMax filtering half window");
  QSpinBox *minmax_box = new QSpinBox;
  minmax_box->setMaximum(100);
  minmax_box->setWrapping(true);
  minmax_box->setSingleStep(1);
  minmax_box->setValue(MINMAX_HALF_WINDOW);
  layout->addWidget(minmax_label, 1, 0);
  layout->addWidget(minmax_box, 1, 1);

  _parameter_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(median_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &FilterBackgroundWidget::setMedianHalfWindow);
  connect(minmax_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &FilterBackgroundWidget::setMinMaxHalfWindow);
#else
  // Qt4 code
  connect(median_box,
          SIGNAL(valueChanged(int)),
          this,
          SLOT(setMedianHalfWindow(int)));
  connect(minmax_box,
          SIGNAL(valueChanged(int)),
          this,
          SLOT(setMinMaxHalfWindow(int)));
#endif
}

void
FilterBackgroundWidget::setMedianHalfWindow(int i)
{
  _median_half_window = i;
  _filter_background  = std::make_shared<pappso::FilterMorphoBackground>(
    _median_half_window, _min_max_half_window);
  this->filterXic();
}

void
FilterBackgroundWidget::setMinMaxHalfWindow(int i)
{
  _min_max_half_window = i;
  _filter_background   = std::make_shared<pappso::FilterMorphoBackground>(
    _median_half_window, _min_max_half_window);
  this->filterXic();
}

int
FilterBackgroundWidget::getMedianHalfWindow() const
{
  return _median_half_window;
}

int
FilterBackgroundWidget::getMinMaxHalfWindow() const
{
  return _min_max_half_window;
}

void
FilterBackgroundWidget::filterXic()
{
  _p_treatmentBox->setFilter(_filter_background);
}

void
FilterBackgroundWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  domDocument->addBackgroundFilterMethod(*_filter_background);
}
