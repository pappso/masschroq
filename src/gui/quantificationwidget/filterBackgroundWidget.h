/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file filterBackgroundWidget.h
 * \date November 24, 2011
 * \author Edlira Nano
 */

#pragma once

#include "../../mcq_types.h"
#include "../masschroQWidget.h"
#include "../treatment/treatment_box_xic_filter.h"

#include <QGroupBox>
#include <QWidget>
class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;

/**
 * \class FilterBackgroundWidget
 * \brief Background noise removal XIC filter Widget
 *
 *
 **/

class FilterBackgroundWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  FilterBackgroundWidget(TreatmentBoxXicFilter *treatmentBox,
                         QWidget *parent = 0);

  virtual ~FilterBackgroundWidget();

  void writeElement(MasschroqDomDocument *domDocument) const;

  private slots:

  void filterXic();
  void setMedianHalfWindow(int i);
  void setMinMaxHalfWindow(int i);

  protected:
  int _median_half_window;
  int _min_max_half_window;

  virtual void setWidgetParameters();

  private:
  TreatmentBoxXicFilter *_p_treatmentBox;
  std::shared_ptr<pappso::FilterMorphoBackground> _filter_background;
  QGroupBox *_parameter_group;
  QDialogButtonBox *_button_box;

  void initializeFilter();

  const int MEDIAN_HALF_WINDOW;
  const int MINMAX_HALF_WINDOW;

  int getMedianHalfWindow() const;
  int getMinMaxHalfWindow() const;
};
