/**
 * \file filterMaxMinWidget.cpp
 * \date February 29, 2012
 * \author Edlira Nano
 */

#include "filterMaxMinWidget.h"
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

FilterMaxMinWidget::FilterMaxMinWidget(QWidget *parent)
  : QWidget(parent), HALF_WINDOW_DEFAULT(2)
{
  // initialize filter
  _half_window   = HALF_WINDOW_DEFAULT;
  _filter_maxmin = std::make_shared<pappso::FilterMorphoMaxMin>(_half_window);

  QPushButton *filterButton = new QPushButton(tr("&Filter"));
  filterButton->setDefault(true);
  _button_box = new QDialogButtonBox(Qt::Vertical);
  _button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  setWidgetParameters();
  mainLayout->addWidget(_half_window_group);
  mainLayout->addWidget(_button_box);
  setLayout(mainLayout);
  setWindowTitle(tr("MaxMin (Open) filter"));

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_button_box,
          &QDialogButtonBox::accepted,
          this,
          &FilterMaxMinWidget::filterXic);
#else
  // Qt4 code
  connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));

#endif
}

FilterMaxMinWidget::~FilterMaxMinWidget()
{
}

void
FilterMaxMinWidget::setWidgetParameters()
{

  _half_window_group  = new QGroupBox;
  QGridLayout *layout = new QGridLayout;

  QLabel *half_label = new QLabel("MaxMin/Open filtering half window");
  QSpinBox *half_box = new QSpinBox;

  half_box->setMaximum(50);
  half_box->setWrapping(true);
  half_box->setSingleStep(1);
  half_box->setValue(HALF_WINDOW_DEFAULT);
  layout->addWidget(half_label, 0, 0);
  layout->addWidget(half_box, 0, 1);
  _half_window_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(half_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &FilterMaxMinWidget::setHalfWindow);
#else
  // Qt4 code
  connect(half_box,
          SIGNAL(valueChanged(int)),
          this,
          &FilterMaxMinWidget::setHalfWindow);

#endif
}

void
FilterMaxMinWidget::setHalfWindow(int i)
{
  _half_window   = i;
  _filter_maxmin = std::make_shared<pappso::FilterMorphoMaxMin>(_half_window);
}

int
FilterMaxMinWidget::getHalfWindow() const
{
  return _half_window;
}

void
FilterMaxMinWidget::filterXic()
{
  emit filter(_filter_maxmin.get());
}
