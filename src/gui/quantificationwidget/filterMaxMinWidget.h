/*
 *
 *  File filterMaxMinWidget.h in
 *  MassChroQ: Mass Chromatogram Quantification software.
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
 * \file filterMaxMinWidget.h
 * \date February 29, 2012
 * \author Edlira Nano
 */

#pragma once

#include <pappsomspp/processing/filters/filtermorpho.h>
//#include "../mcq_types.h"

#include <QButtonGroup>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QWidget>

/**
 * \class FilterMaxMinWidget
 * \brief MaxMin Open XIC filter Widget
 *
 **/

class FilterMaxMinWidget : public QWidget
{

  Q_OBJECT

  public:
  FilterMaxMinWidget(QWidget *parent = 0);

  virtual ~FilterMaxMinWidget();

  private slots:

  void filterXic();
  void setHalfWindow(int i);

  signals:

  void filter(pappso::FilterMorphoMaxMin *filter);

  protected:
  int _half_window;

  virtual void setWidgetParameters();

  private:
  std::shared_ptr<pappso::FilterMorphoMaxMin> _filter_maxmin;
  QDialogButtonBox *_button_box;
  QGroupBox *_half_window_group;

  const int HALF_WINDOW_DEFAULT;
  int getHalfWindow() const;
};
