/*
 *
 *  File filterMinMaxWidget.h in
 *  MassChroQ: Mass Chromatogram Quantification software.
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
 * \file filterMinMaxWidget.h
 * \date 22 February, 2012
 * \author Edlira Nano
 */

#pragma once

#include "../../mcq_types.h"
#include <pappsomspp/processing/filters/filtermorpho.h>

#include <QWidget>
class QDialogButtonBox;
class QVBoxLayout;
class QButtonGroup;
class QGroupBox;

/**
 * \class FilterMinMaxWidget
 * \brief MinMax or Close XIC filter Widget
 *
 *
 **/

class FilterMinMaxWidget : public QWidget
{

  Q_OBJECT

  public:
  FilterMinMaxWidget(QWidget *parent = 0);

  virtual ~FilterMinMaxWidget();

  private slots:

  void filterXic();
  void setHalfWindow(int i);

  signals:

  void filter(pappso::FilterMorphoMinMax *filter);

  protected:
  int _half_window;

  virtual void setWidgetParameters();

  private:
  std::shared_ptr<pappso::FilterMorphoMinMax> _filter_minmax;
  QDialogButtonBox *_button_box;
  QGroupBox *_half_window_group;

  const int HALF_WINDOW_DEFAULT;
  int getHalfWindow() const;
};
