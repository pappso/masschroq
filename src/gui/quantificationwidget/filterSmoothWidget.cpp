/**
 * \file filterSmoothWidget.cpp
 * \date February 21, 2012
 * \author Edlira Nano
 */

#include "filterSmoothWidget.h"
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

FilterSmoothWidget::FilterSmoothWidget(QWidget *parent)
  : QWidget(parent), HALF_WINDOW_DEFAULT(1)
{
  // initialize filter
  _half_window   = HALF_WINDOW_DEFAULT;
  _filter_smooth = std::make_shared<pappso::FilterMorphoMean>(_half_window);

  QPushButton *filterButton = new QPushButton(tr("&Filter"));
  filterButton->setDefault(true);
  _button_box = new QDialogButtonBox(Qt::Vertical);
  _button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  setWidgetParameters();
  mainLayout->addWidget(_half_window_group);
  mainLayout->addWidget(_button_box);
  setLayout(mainLayout);
  setWindowTitle(tr("Smoothing filter"));

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_button_box,
          &QDialogButtonBox::accepted,
          this,
          &FilterSmoothWidget::filterXic);

#else
  // Qt4 code
  connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));

#endif
}

FilterSmoothWidget::~FilterSmoothWidget()
{
}

void
FilterSmoothWidget::setWidgetParameters()
{

  _half_window_group  = new QGroupBox;
  QGridLayout *layout = new QGridLayout;

  QLabel *half_label = new QLabel("Smoothing filter half window");
  QSpinBox *half_box = new QSpinBox;

  half_box->setMaximum(100);
  half_box->setWrapping(true);
  half_box->setSingleStep(1);
  half_box->setValue(HALF_WINDOW_DEFAULT);
  layout->addWidget(half_label, 0, 0);
  layout->addWidget(half_box, 0, 1);
  _half_window_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code

  connect(half_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &FilterSmoothWidget::setHalfWindow);
#else
  // Qt4 code

  connect(half_box, SIGNAL(valueChanged(int)), this, SLOT(setHalfWindow(int)));
#endif
}

void
FilterSmoothWidget::setHalfWindow(int i)
{
  _half_window   = i;
  _filter_smooth = std::make_shared<pappso::FilterMorphoMean>(_half_window);
}

int
FilterSmoothWidget::getHalfWindow() const
{
  return _half_window;
}

void
FilterSmoothWidget::filterXic()
{
  emit filter(_filter_smooth.get());
}
