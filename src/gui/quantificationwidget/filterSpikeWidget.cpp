/**
 * \file filterSpikeWidget.cpp
 * \date February 15, 2012
 * \author Edlira Nano
 */

#include "filterSpikeWidget.h"
#include "../../lib/consoleout.h"
#include <QLabel>
#include <QSpinBox>

FilterSpikeWidget::FilterSpikeWidget(TreatmentBoxXicFilter *treatmentBox,
                                     QWidget *parent)
  : MasschroQWidget(parent), _half_window(5), _p_treatmentBox(treatmentBox)
{
  // initialize filter
  _filter_spike = std::make_shared<pappso::FilterMorphoAntiSpike>(_half_window);
  treatmentBox->setFilter(_filter_spike);

  //	QPushButton * filterButton = new QPushButton(tr("&Filter"));
  //	filterButton->setDefault(true);
  //	_button_box = new QDialogButtonBox(Qt::Vertical);
  //	_button_box->addButton(filterButton, QDialogButtonBox::AcceptRole);
  //
  //	connect(_button_box, SIGNAL(accepted()), this, SLOT(filterXic()));

  QVBoxLayout *mainLayout = new QVBoxLayout;
  setWidgetParameters();
  mainLayout->addWidget(_half_window_group);
  // mainLayout->addWidget(_button_box);
  setLayout(mainLayout);
  setWindowTitle(tr("Spike noise filter"));

  // Trigger filter
  this->filterXic();
}

FilterSpikeWidget::~FilterSpikeWidget()
{
}

void
FilterSpikeWidget::setWidgetParameters()
{

  _half_window_group  = new QGroupBox("Spike Filter");
  QGridLayout *layout = new QGridLayout;

  QLabel *half_label = new QLabel("Spike filtering half window");
  QSpinBox *half_box = new QSpinBox;

  half_box->setMaximum(30);
  half_box->setWrapping(true);
  half_box->setSingleStep(1);
  half_box->setValue(_half_window);
  layout->addWidget(half_label, 0, 0);
  layout->addWidget(half_box, 0, 1);
  _half_window_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code

  connect(half_box,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          this,
          &FilterSpikeWidget::setHalfWindow);
#else
  // Qt4 code

  connect(half_box, SIGNAL(valueChanged(int)), this, SLOT(setHalfWindow(int)));
#endif
}

void
FilterSpikeWidget::setHalfWindow(int i)
{
  _half_window  = i;
  _filter_spike = std::make_shared<pappso::FilterMorphoAntiSpike>(_half_window);
  this->filterXic();
}

int
FilterSpikeWidget::getHalfWindow() const
{
  return _half_window;
}

void
FilterSpikeWidget::filterXic()
{
  _p_treatmentBox->setFilter(_filter_spike);
}

void
FilterSpikeWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  domDocument->addSpikeFilterMethod(*_filter_spike);
}
