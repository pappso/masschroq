/*
 *
 *  File filterSpikeWidget.h in
 *  MassChroQ: Mass Chromatogram Quantification software.
 *  Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>
 */

/**
 * \file filterSpikeWidget.h
 * \date February 15, 2012
 * \author Edlira Nano
 */

#pragma once

#include "../../mcq_types.h"
#include "../masschroQWidget.h"
#include "../treatment/treatment_box_xic_filter.h"
#include <QWidget>
#include <pappsomspp/processing/filters/filtermorpho.h>

#include <QButtonGroup>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QVBoxLayout>

/**
 * \class FilterSpikeWidget
 * \brief Spike noise removal XIC filter Widget
 *
 *
 **/

class FilterSpikeWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  FilterSpikeWidget(TreatmentBoxXicFilter *treatmentBox, QWidget *parent = 0);

  virtual ~FilterSpikeWidget();

  void writeElement(MasschroqDomDocument *domDocument) const;

  private slots:

  void filterXic();
  void setHalfWindow(int i);

  protected:
  int _half_window;

  virtual void setWidgetParameters();

  private:
  TreatmentBoxXicFilter *_p_treatmentBox;
  std::shared_ptr<pappso::FilterMorphoAntiSpike> _filter_spike;
  QDialogButtonBox *_button_box;
  QGroupBox *_half_window_group;

  int getHalfWindow() const;
};
