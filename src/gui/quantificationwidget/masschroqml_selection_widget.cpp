/*
 * msrun_selection_widget.cpp
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#include "masschroqml_selection_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QVBoxLayout>
#include <vector>

Q_DECLARE_METATYPE(MsrunSp);

MasschroqmlSelectionWidget::MasschroqmlSelectionWidget(QWidget *parent)
  : MasschroQWidget(parent)
{
  // kill including widget
  setAttribute(Qt::WA_DeleteOnClose);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  this->addMasschroqmlSelectionGroup(mainLayout);
  this->addMsRunSelectionGroup(mainLayout);

  this->setLayout(mainLayout);

  _msrun_loader_thread.setMaxProgress(3);

  _masschroqml_loader_thread.setMaxProgress(3);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(&_msrun_loader_thread,
          &MsrunLoaderThread::loadedMsrun,
          this,
          &MasschroqmlSelectionWidget::doneLoadingMsRun);
  connect(&_msrun_loader_thread,
          &MsrunLoaderThread::errorDuringLoading,
          this,
          &MasschroqmlSelectionWidget::ErrorLoadingMsrun);
  connect(&_masschroqml_loader_thread,
          &MasschroqmlLoaderThread::loadedMasschroqml,
          this,
          &MasschroqmlSelectionWidget::doneLoadingMasschroqml);
  connect(&_masschroqml_loader_thread,
          &MasschroqmlLoaderThread::errorDuringLoading,
          this,
          &MasschroqmlSelectionWidget::ErrorLoadingMasschroqml);
#else
  // Qt4 code
  connect(&_msrun_loader_thread,
          SIGNAL(loadedMsrun(Msrun *)),
          this,
          SLOT(doneLoadingMsRun(Msrun *)));
  connect(&_msrun_loader_thread,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(ErrorLoadingMsrun(QString)));
  connect(&_masschroqml_loader_thread,
          SIGNAL(loadedMasschroqml(MasschroqGuiData *)),
          this,
          SLOT(doneLoadingMasschroqml(MasschroqGuiData *)));
  connect(&_masschroqml_loader_thread,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(ErrorLoadingMasschroqml(QString)));
#endif
}

MasschroqmlSelectionWidget::~MasschroqmlSelectionWidget()
{
  qDebug() << "Delete Msrun Selection widget";
}

void
MasschroqmlSelectionWidget::addMasschroqmlSelectionGroup(
  QVBoxLayout *mainlayout)
{
  QGroupBox *masschroqml_group = new QGroupBox("MassChroqML selection");

  QVBoxLayout *groupLayout = new QVBoxLayout;

  _masschroqml_edit = new QLineEdit("No MassChroqML Selected");
  _masschroqml_edit->setReadOnly(true);
  groupLayout->addWidget(_masschroqml_edit);

  QHBoxLayout *layout  = new QHBoxLayout;
  _masschroqml_message = new RunningQLabel(this);
  layout->addWidget(_masschroqml_message, 2);
  QPushButton *extractButton = new QPushButton(tr("&Load"));
  extractButton->setDefault(true);
  layout->addWidget(extractButton, 0);

  groupLayout->addLayout(layout);

  masschroqml_group->setLayout(groupLayout);
  mainlayout->addWidget(masschroqml_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton,
          &QPushButton::clicked,
          this,
          &MasschroqmlSelectionWidget::loadMasschroqml);
#else
  // Qt4 code
  connect(extractButton, SIGNAL(clicked()), this, SLOT(loadMasschroqml()));
#endif
}

void
MasschroqmlSelectionWidget::addMsRunSelectionGroup(QVBoxLayout *mainlayout)
{
  QGroupBox *msrun_group = new QGroupBox("MsRun selection");

  QVBoxLayout *groupLayout = new QVBoxLayout;
  _msrun_select            = new QComboBox();

  groupLayout->addWidget(_msrun_select);
  _msrun_message = new RunningQLabel(this);
  groupLayout->addWidget(_msrun_message);

  msrun_group->setLayout(groupLayout);
  mainlayout->addWidget(msrun_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_msrun_select,
          static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
          this,
          &MasschroqmlSelectionWidget::loadMsrun);
#else
  // Qt4 code
  connect(_msrun_select, SIGNAL(activated(int)), this, SLOT(loadMsrun(int)));
#endif
}

void
MasschroqmlSelectionWidget::doneLoadingMsRun(MsrunSp msrun)
{
  // QString filename = (msrun->getMzDataFileInfo()).filePath();
  MasschroqGuiEngin::getInstance()->setCurrentLoadedMsrun(msrun);

  _msrun_message->stopLoading(tr("MsRun correctly loaded"));
  emit updateMsRunData();
}

void
MasschroqmlSelectionWidget::doneLoadingMasschroqml(
  MasschroqGuiData *masschroqdata)
{
  _masschroqml_message->stopLoading(tr("MasschroqML correctly loaded"));

  // add msrun selection
  MasschroqGuiEngin::getInstance()->setMasschroqGuiData(masschroqdata);
  for(auto pair_msrun : masschroqdata->getMsrunHashGroup())
    {
      this->_msrun_select->addItem(
        strippedFilename(
          QFileInfo(pair_msrun.second->getMsRunIdCstSPtr().get()->getFileName())
            .absoluteFilePath()),
        QVariant::fromValue(pair_msrun.second));
    }

  emit updateMasschroqData();
}

void
MasschroqmlSelectionWidget::loadMasschroqml()
{
  qDebug() << "Begin MassChroqML Loading";
  // TODO parse masschroml and return a masschroqdata
  QSettings settings;
  QString default_location =
    settings.value("path/mcqml_default_path", "").toString();

  QString filename = QFileDialog::getOpenFileName(
    this,
    tr("Open MasschroqML File"),
    default_location,
    tr("masschroqML files (*.masschroqML &  *.xml & *.masschroqml);;all "
       "files (*)"));
  //
  if(!filename.isEmpty())
    {
      settings.setValue("path/mcqml_default_path",
                        QFileInfo(filename).absolutePath());
      QFileInfo filenameInfo(filename);

      if(!filenameInfo.exists())
        {
          this->ErrorLoadingMsrun(
            tr("The chosen MasschroqML file '%1', does not exist..\nPlease, "
               "change the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }
      else if(!filenameInfo.isReadable())
        {
          this->ErrorLoadingMsrun(
            tr("The chosen MasschroqML file '%1', is not readable.\nPlease, "
               "change the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }
      _masschroqml_message->startLoading(tr("Loading"));
      _masschroqml_edit->setText(this->strippedFilename(filename));
      _msrun_message->setText("");

      this->_msrun_select->clear();
      emit resetData();

      _masschroqml_loader_thread.loadMasschroqml(filename);
    }
}

void
MasschroqmlSelectionWidget::loadMsrun(int index)
{
  qDebug() << "Load MsRun index : " << index;
  MsrunSp msrun;
  QVariant index_value = _msrun_select->currentData();
  if(index_value.canConvert<MsrunSp>())
    {
      msrun = index_value.value<MsrunSp>();
    }

  if(msrun == nullptr)
    {
      this->ErrorLoadingMsrun(tr("no MS run selected"));
      return;
    }

  QString filename = QFileInfo(msrun->getMsRunIdCstSPtr().get()->getFileName())
                       .absoluteFilePath();
  if(!filename.isEmpty())
    {

      QFileInfo filenameInfo(filename);

      if(!filenameInfo.exists())
        {
          this->ErrorLoadingMsrun(
            tr("The chosen MS run file '%1', does not exist..\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }
      else if(!filenameInfo.isReadable())
        {
          this->ErrorLoadingMsrun(
            tr("The chosen MS run file '%1', is not readable.\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }

      emit resetData();

      _msrun_message->startLoading(tr("Loading"));
      // MasschroqGuiEngin::getInstance()->setCurrentLoadedMsrun(msrun);

      _msrun_loader_thread.loadMsrun(msrun);
    }
  else
    {
    }
}

void
MasschroqmlSelectionWidget::ErrorLoadingMsrun(QString error)
{
  _msrun_message->stopLoading("");
  this->viewErrorMessage(error);
}

void
MasschroqmlSelectionWidget::ErrorLoadingMasschroqml(QString error)
{

  _masschroqml_edit->setText(tr("No MassChroqML selected"));
  _masschroqml_message->stopLoading(tr(""));

  this->viewErrorMessage(error);
}

void
MasschroqmlSelectionWidget::viewErrorMessage(QString error)
{
  QMessageBox::warning(
    this, tr("MassChroQ encountered an error:"), error);
}

QString
MasschroqmlSelectionWidget::strippedFilename(const QString &fullFilename)
{
  return QFileInfo(fullFilename).fileName();
}
