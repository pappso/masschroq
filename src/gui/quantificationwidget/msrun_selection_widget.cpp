/*
 * msrun_selection_widget.cpp
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#include "msrun_selection_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <QSettings>

MsrunSelectionWidget::MsrunSelectionWidget(QWidget *parent)
  : MasschroQWidget(parent)
{
  // kill including widget
  setAttribute(Qt::WA_DeleteOnClose);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  _selection_group        = new QGroupBox("MsRun selection");

  QVBoxLayout *groupLayout = new QVBoxLayout;

  _text_edit = new QLineEdit("No MsRun Selected");
  _text_edit->setReadOnly(true);
  groupLayout->addWidget(_text_edit);

  QHBoxLayout *layout = new QHBoxLayout;
  _text_message       = new RunningQLabel(this);
  layout->addWidget(_text_message, 2);
  QPushButton *extractButton = new QPushButton(tr("&Load"));
  extractButton->setDefault(true);
  layout->addWidget(extractButton, 0);

  groupLayout->addLayout(layout);

  _selection_group->setLayout(groupLayout);
  mainLayout->addWidget(_selection_group);
  this->setLayout(mainLayout);

  _msrun_loader_thread.setMaxProgress(3);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton,
          &QPushButton::clicked,
          this,
          &MsrunSelectionWidget::loadMsrun);
  connect(&_msrun_loader_thread,
          &MsrunLoaderThread::loadedMsrun,
          this,
          &MsrunSelectionWidget::doneLoading);
  connect(&_msrun_loader_thread,
          &MsrunLoaderThread::errorDuringLoading,
          this,
          &MsrunSelectionWidget::ErrorLoading);
#else
  // Qt4 code
  connect(extractButton, SIGNAL(clicked()), this, SLOT(loadMsrun()));
  connect(&_msrun_loader_thread,
          SIGNAL(loadedMsrun(Msrun *)),
          this,
          SLOT(doneLoading(Msrun *)));
  connect(&_msrun_loader_thread,
          SIGNAL(errorDuringLoading(QString)),
          this,
          SLOT(ErrorLoading(QString)));
#endif
}

MsrunSelectionWidget::~MsrunSelectionWidget()
{
  qDebug() << "Delete Msrun Selection widget";
}

void
MsrunSelectionWidget::doneLoading(MsrunSp msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  MasschroqGuiEngin::getInstance()->setCurrentLoadedMsrun(msrun);
  _text_edit->setText(
    QFileInfo(msrun->getMsRunIdCstSPtr().get()->getFileName()).fileName());
  _text_message->stopLoading("MsRun correctly loaded");
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
MsrunSelectionWidget::loadMsrun()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QSettings settings;
  QString default_location =
    settings.value("path/mzdata_default_path", "").toString();

  QString filename =
    QFileDialog::getOpenFileName(this,
                                 tr("Open Msrun File"),
                                 default_location,
                                 tr("mzXML or mzML files (*.mzXML *.mzML)"));

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(!filename.isEmpty())
    {
      settings.setValue("path/mzdata_default_path",
                        QFileInfo(filename).absolutePath());

      QFileInfo filenameInfo(filename);

      if(!filenameInfo.exists())
        {
          this->ErrorLoading(
            tr("The chosen MS run file '%1', does not exist..\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }
      else if(!filenameInfo.isReadable())
        {
          this->ErrorLoading(
            tr("The chosen MS run file '%1', is not readable.\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return;
        }

      qDebug();
      emit resetData();

      qDebug();
      _text_message->startLoading("Loading");

      qDebug();


      pappso::MsFileAccessor file_access(filename, QString("msfile"));

      file_access.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                             pappso::FileReaderType::tims_ms2);

      pappso::MsRunReaderSPtr run_reader =
        file_access.getMsRunReaderSPtrByRunId("", "msrun");


      _current_msrun = std::make_shared<Msrun>(run_reader);
      _text_edit->setText(
        QFileInfo(_current_msrun->getMsRunIdCstSPtr().get()->getFileName())
          .fileName());
      _msrun_loader_thread.loadMsrun(_current_msrun);
    }
  else
    {
    }
  qDebug();
}

void
MsrunSelectionWidget::ErrorLoading(QString error)
{
  _text_edit->setText(tr("No Msrun selected"));
  _text_message->stopLoading("");
  QMessageBox::warning(this, tr("MassChroQ encountered an error:"), error);
}
