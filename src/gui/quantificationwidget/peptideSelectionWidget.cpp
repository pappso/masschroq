/**
 * \file xicSelectionWidget.cpp
 * \date November 15, 2011
 * \author Edlira Nano
 */

#include "peptideSelectionWidget.h"

#include "../../lib/mcq_error.h"
#include "../../lib/peptides/peptide_isotope.h"
#include "../../lib/quanti_items/quantiItemMzRt.h"
#include "../engine/masschroq_gui_engin.h"
#include <QComboBox>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QPushButton>
#include <QStandardItem>
#include <QVBoxLayout>

Q_DECLARE_METATYPE(PeptideSPtr);
Q_DECLARE_METATYPE(Peptide *);

PeptideSelectionWidget::PeptideSelectionWidget(
  TreatmentBoxXicExtract *treatment, QWidget *parent)
  : XicSelectionWidget(parent)
{
  _treatmentBoxXicExtract = treatment;
  // close action
  setAttribute(Qt::WA_DeleteOnClose);

  _xic_selection = new QGroupBox("XIC Extraction");
  create_xic_type_group();
  create_xic_range_group();
  create_mz_group();

  QPushButton *extractButton = new QPushButton(tr("&Extract"));
  extractButton->setDefault(true);
  QDialogButtonBox *_button_box = new QDialogButtonBox(Qt::Horizontal);
  _button_box->addButton(extractButton, QDialogButtonBox::AcceptRole);
  //_button_box->addButton(QDialogButtonBox::Cancel);
  // connect(_button_box, SIGNAL(rejected()), this, SLOT(cancel()));

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(_xic_selection);
  QVBoxLayout *selectLayout = new QVBoxLayout;
  selectLayout->addWidget(_mz_group);
  selectLayout->addWidget(_xic_type_group);
  selectLayout->addWidget(_xic_range_group);
  selectLayout->addWidget(_button_box);
  _xic_selection->setLayout(selectLayout);
  setLayout(mainLayout);
  setWindowTitle(tr("XIC extraction"));

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_button_box,
          &QDialogButtonBox::accepted,
          this,
          &PeptideSelectionWidget::extractXic);
#else
  // Qt4 code
  connect(_button_box, SIGNAL(accepted()), this, SLOT(extractXic()));
#endif
}

PeptideSelectionWidget::~PeptideSelectionWidget()
{
  qDebug() << "delete xic selection";
}

void
PeptideSelectionWidget::create_mz_group()
{
  qDebug() << "peptideSelectionWidget peptide group";
  _mz_group                = new QGroupBox(tr("Peptide selection"));
  QVBoxLayout *groupLayout = new QVBoxLayout;
  QHBoxLayout *layout      = new QHBoxLayout;

  _p_isotope_number_combobox = new QComboBox;
  _p_isotope_number_combobox->setEditable(false);
  _p_isotope_number_combobox->addItem("+0");
  _p_isotope_number_combobox->addItem("+1");
  _p_isotope_number_combobox->addItem("+2");
  _p_isotope_number_combobox->addItem("+3");

  _peptideComboBox = new QComboBox;
  _peptideComboBox->setEditable(false);

  layout->addWidget(_peptideComboBox, 2);

  _zComboBox = new QComboBox;
  _zComboBox->setEditable(false);
  layout->addWidget(_zComboBox, 0);
  layout->addWidget(_p_isotope_number_combobox, 0);

  groupLayout->addLayout(layout);
  _peptide_message = new QLineEdit();
  _peptide_message->setReadOnly(true);
  groupLayout->addWidget(_peptide_message);

  _mz_group->setLayout(groupLayout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(
    _peptideComboBox,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    this,
    &PeptideSelectionWidget::setPeptideToExtract);
#else
  // Qt4 code
  connect(_zComboBox,
          SIGNAL(currentIndexChanged(const QString &)),
          this,
          SLOT(setChargeToExtract(const QString &)));
  connect(_peptideComboBox,
          SIGNAL(currentIndexChanged(int)),
          this,
          SLOT(setPeptideToExtract(int)));
#endif
}

void
PeptideSelectionWidget::updatePeptideList()
{
  MasschroqGuiData *data =
    MasschroqGuiEngin::getInstance()->getMasschroqGuiData();
  if(data != 0)
    {
      PeptideList list = data->getPeptideList();
      qDebug() << "PeptideSelectionWidget::updatePeptideList() list.size() "
               << list.size();
      PeptideList::iterator it;
      _peptideComboBox->clear();
      _peptide_message->setText("");
      if(MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun() != nullptr)
        {
          QString msRunID = MasschroqGuiEngin::getInstance()
                              ->getCurrentLoadedMsrun()
                              ->getMsRunIdCstSPtr()
                              ->getXmlId();
          qDebug() << "Select Peptide to msRUn id : " << msRunID;
          int i;
          for(i = 0, it = list.begin(); it != list.end(); it++, i++)
            {
              if((*it)->getIsotopeLabel() == nullptr)
                {
                  if((*it)->isObservedIn(msRunID))
                    {
                      //_peptideComboBox->addItem((*it)->getXmlId(),
                      // QVariant(i));

                      _peptideComboBox->addItem((*it)->getXmlId(),
                                                QVariant::fromValue(*it));
                    }
                }
              else
                {
                  PeptideIsotope *p_peptideIsotope =
                    (PeptideIsotope *)(it->get());
                  if(p_peptideIsotope->isObservedIn(msRunID))
                    {
                      _peptideComboBox->addItem(
                        QString("%1 %2")
                          .arg((*it)->getXmlId())
                          .arg((*it)->getIsotopeLabel()->getXmlId()),
                        QVariant::fromValue(*it));

                      //_peptideComboBox->addItem(
                      //    p_peptideIsotope->getXmlId() + " "
                      //   + p_peptideIsotope->getIsotopeLabel()->getXmlId(),
                      //   QVariant(i));
                      qDebug()
                        << "PeptideSelectionWidget::updatePeptideList() "
                           "isotope "
                        << p_peptideIsotope->getSequence() << " "
                        << p_peptideIsotope->getMods() << " "
                        << p_peptideIsotope->getIsotopeLabel()->getXmlId()
                        << " IS observed in msRun";
                    }
                  else
                    {
                    }
                }
            }
        }
    }
}

void
PeptideSelectionWidget::clearPeptideList()
{
  _peptideComboBox->clear();
  _zComboBox->clear();
  _peptide_message->setText("");
}
const Peptide *
PeptideSelectionWidget::getPeptide() const
{

  QVariant index_value = _peptideComboBox->currentData();
  if(index_value.canConvert<Peptide *>())
    {
      const Peptide *p_peptide = index_value.value<Peptide *>();
      return p_peptide;
    }
  else
    {
    }
  return nullptr;
}

void
PeptideSelectionWidget::setPeptideToExtract(int index)
{
  qDebug() << "PeptideSelectionWidget::setPeptideToExtract() index " << index
           << "  _peptideComboBox->currentIndex() "
           << _peptideComboBox->currentIndex();
  MasschroqGuiData *data =
    MasschroqGuiEngin::getInstance()->getMasschroqGuiData();
  if(data != 0)
    {
      const Peptide *pep = getPeptide();
      if(pep != nullptr)
        {
          // Reload charge selector
          std::set<unsigned int> *charges =
            pep->getCharges(data->getMsrunHashGroup());
          qDebug() << "Peptide Selection : " << pep->getXmlId()
                   << " with different charge " << charges->size();
          std::set<unsigned int>::const_iterator it;
          _zComboBox->clear();
          for(it = charges->begin(); it != charges->end(); it++)
            {
              _zComboBox->addItem(QString().setNum(*it));
            }
          QString peptide_text =
            QString("%1 %2").arg(pep->getSequence()).arg(pep->getMods());

          if(pep->getIsotopeLabel() != nullptr)
            {
              peptide_text.append(
                QString(" %1").arg(pep->getIsotopeLabel()->getXmlId()));
            }
          _peptide_message->setText(peptide_text);
        }
    }
}

void
PeptideSelectionWidget::extractXic()
{
  const Peptide *peptide = getPeptide();
  if(peptide != nullptr)
    {
      unsigned int charge = 1;

      QString charge_txt = _zComboBox->currentText();
      if(!charge_txt.isEmpty())
        {
          charge = charge_txt.toInt();
        }
      unsigned int isotope_number =
        _p_isotope_number_combobox->currentText().mid(1, 1).toUInt();

      mcq_double mz =
        (peptide->getMass() + (pappso::DIFFC12C13 * isotope_number) +
         (pappso::MHPLUS * charge)) /
        (pappso::pappso_double)charge;
      QuantiItemBase *quanti_item;
      // if (_rt_to_extract != 0) {
      //	quanti_item = new QuantiItemMzRt(*_xic_extraction_method,
      //			_rt_to_extract);
      //} else {

      pappso::XicCoordSPtr xic_coord = std::make_shared<pappso::XicCoord>();
      xic_coord.get()->mzRange =
        pappso::MzRange(mz, _precision_widget->getPrecision());
      quanti_item = new QuantiItemBase(false, xic_coord);

      _quantification_method.setXicExtractMethod(pappso::XicExtractMethod::sum);
      if(_xic_type_max->isChecked())
        {
          _quantification_method.setXicExtractMethod(
            pappso::XicExtractMethod::max);
        }
      _quantification_method.setXicExtractPrecision(
        _precision_widget->getPrecision());

      //}
      //_treatmentBoxXicExtract->setQuantiItem(quanti_item,
      //_quantification_method);
      _treatmentBoxXicExtract->setQuantiItem(quanti_item,
                                             &_quantification_method);
    }
}
