/**
 * \file plot.cpp
 * \date November 23, 2011
 * \author Edlira Nano
 */

#include "plot.h"
#include "../../lib/quanti_items/quantiItemBase.h"
#include <QVector>
#include <pappsomspp/processing/filters/filterresample.h>

Plot::Plot(QWidget *parent) : QCustomPlot(parent)
{

  qDebug() << "Plot(QWidget *parent) begin";

  xAxis->setLabel("retention time (seconds)");
  yAxis->setLabel("intensity");
  setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
  axisRects().at(0)->setRangeDrag(Qt::Horizontal);
  axisRects().at(0)->setRangeZoom(Qt::Horizontal);
  legend->setVisible(false);
  legend->setFont(QFont("Helvetica", 9));
  // set locale to english, so we get english decimal separator:
  // setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom));
  setFocusPolicy(Qt::ClickFocus);
  qDebug() << "Plot(QWidget *parent) end";
}

Plot::~Plot()
{
  this->clear();
}

void
Plot::viewNewPlot(const TreatmentBoxXicExtract *p_treatment)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  this->clear();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  pappso::XicSPtr p_xic = p_treatment->getCurrentXic();
  if(p_xic != nullptr)
    {
      QCPGraph *p_graph = this->addGraph();
      p_graph->setName("raw xic");
      QPen pen;
      pen.setColor(getNewColors());

      _xicPlots.insert(
        std::pair<const TreatmentBox *, QCPGraph *>(p_treatment, p_graph));

      p_graph->setPen(pen);
      // graph()->setName(lineNames.at(i-QCPGraph::lsNone));
      p_graph->setLineStyle(QCPGraph::LineStyle::lsLine);
      p_graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2.0));

      p_graph->setData(
        QVector<double>(p_xic->xValues().begin(), p_xic->xValues().end()),
        QVector<double>(p_xic->yValues().begin(), p_xic->yValues().end()));

      p_graph->rescaleAxes(true);
      this->rescaleAxes();
      replot();
    }
  qDebug();
}
void
Plot::updatedCurrentPlot(const TreatmentBoxXicFilter *p_treatment)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  pappso::XicSPtr p_xic = p_treatment->getCurrentXic();
  if(p_xic != nullptr)
    {
      QCPGraph *p_graph;
      if(_xicPlots.count(p_treatment) > 0)
        {
          p_graph = _xicPlots.find(p_treatment)->second;
        }
      else
        {
          p_graph = this->addGraph();
          p_graph->setName("filter");
          QPen pen;
          pen.setColor(getNewColors());

          _xicPlots.insert(
            std::pair<const TreatmentBox *, QCPGraph *>(p_treatment, p_graph));
          p_graph->setPen(pen);
        }

      p_graph->setLineStyle(QCPGraph::LineStyle::lsLine);
      p_graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2.0));

      p_graph->setData(
        QVector<double>(p_xic->xValues().begin(), p_xic->xValues().end()),
        QVector<double>(p_xic->yValues().begin(), p_xic->yValues().end()));

      this->replot();
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}
void
Plot::updatedPeaks(const TreatmentBoxXicDetect *p_treatment)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  pappso::XicSPtr p_xic = p_treatment->getCurrentXic();

  if(p_xic == nullptr)
    {
      return;
    }

  std::vector<pappso::TracePeak> tracepeak_list = p_treatment->getAllXicPeak();

  for(QCPGraph *p_graph_peak : m_graph_trace_peak_list)
    {
      this->removeGraph(p_graph_peak);
    }
  m_graph_trace_peak_list.resize(0);

  if(tracepeak_list.size() > 0)
    {
      //_p_graph_peak_border = this->addGraph();

      QCPGraph *p_graph;
      std::map<const TreatmentBox *, QCPGraph *>::iterator it =
        _xicPlots.find(p_treatment);
      if(it != _xicPlots.end())
        {
          p_graph = it->second;
        }
      else
        {
          p_graph = this->addGraph();
          p_graph->setName("detected peaks");
          QPen pen;
          pen.setColor(getNewColors());
          _xicPlots.insert(
            std::pair<const TreatmentBox *, QCPGraph *>(p_treatment, p_graph));
          p_graph->setPen(pen);
        }
      p_graph->setLineStyle(QCPGraph::LineStyle::lsNone); // cross on
      p_graph->setScatterStyle(QCPScatterStyle::ssCross);
      // p_graph->setBrush(QBrush(QColor(110, 170, 110, 30)));
      QVector<double> intensity;
      QVector<double> rt;
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
      for(auto peak : tracepeak_list)
        {
          intensity.push_back(peak.getMaxXicElement().y);
          rt.push_back(peak.getMaxXicElement().x);


          pappso::Xic peak_surface(*p_xic);


          peak_surface.filter(pappso::FilterResampleKeepXRange(
            peak.getLeftBoundary().x, peak.getRightBoundary().x));

          QCPGraph *p_tracepeak_graph = this->addGraph();
          m_graph_trace_peak_list.push_back(p_tracepeak_graph);
          p_tracepeak_graph->setData(
            QVector<double>(peak_surface.xValues().begin(),
                            peak_surface.xValues().end()),
            QVector<double>(peak_surface.yValues().begin(),
                            peak_surface.yValues().end()));
          p_tracepeak_graph->setChannelFillGraph(0);
          p_tracepeak_graph->setLineStyle(QCPGraph::LineStyle::lsLine);
          p_tracepeak_graph->setScatterStyle(QCPScatterStyle::ssDot);
          p_tracepeak_graph->setBrush(QBrush(QColor(170, 110, 110, 30)));
        }
      p_graph->setData(rt, intensity);
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";

      this->replot();
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
Plot::remove(TreatmentBox *box)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  if(_xicPlots.count(box) > 0)
    {
      QCPGraph *graph = _xicPlots.find(box)->second;
      removeGraph(graph);

      _xicPlots.erase(_xicPlots.find(box));
      this->replot();
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
Plot::clear()
{
  qDebug();
  for(auto graph_pair : _xicPlots)
    {
      removeGraph(graph_pair.second);
    }
  _xicPlots.clear();
  for(int g = 0; g < graphCount(); g++)
    {
      graph(g)->data()->clear();
    }
  // int n = clearPlottables();
  replot();
  qDebug() << "Plot::clear end";
}

void
Plot::clearPlot()
{
  this->clear();
}

const QColor
Plot::getNewColors()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  QColor color;
  switch(_xicPlots.size())
    {
      case 0:
        qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
        return (QColor(Qt::black));
      case 1:
        qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
        return (QColor(Qt::blue));
      case 2:
        qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
        return (QColor(Qt::red));
      case 3:
        qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
        return (QColor(Qt::green));
      case 4:
        return (QColor(Qt::yellow));
      default:
        return (QColor(Qt::black));
    }
  return color;
}

void
Plot::keyPressEvent(QKeyEvent *event)
{
  if(event->key() == Qt::Key_Control)
    {
      _control_key = true;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
Plot::keyReleaseEvent(QKeyEvent *event)
{
  if(event->key() == Qt::Key_Control)
    {
      _control_key = false;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
Plot::mousePressEvent(QMouseEvent *event)
{
  // qDebug() << "Plot::mousePressEvent begin " <<
  // xAxis->pixelToCoord(event->x())
  //         << " " << yAxis->pixelToCoord(event->y());
  _click = true;
  _old_x = event->x();
  _old_y = yAxis->pixelToCoord(event->y());
  if(_old_y < 0)
    _old_y = 0;
  // qDebug() << "Plot::mousePressEvent end";
}
void
Plot::mouseReleaseEvent(QMouseEvent *event [[maybe_unused]])
{
  // qDebug() << "Plot::mouseReleaseEvent begin "
  // << xAxis->pixelToCoord(event->x()) << " " <<
  // yAxis->pixelToCoord(event->y());
  _click = false;
  // qDebug() << "Plot::mouseReleaseEvent end";
}
void
Plot::mouseMoveEvent(QMouseEvent *event)
{
  if(_click)
    {
      //   qDebug() << "Plot::mouseMoveEvent begin "
      //            << xAxis->pixelToCoord(event->x()) << " "
      //            << yAxis->pixelToCoord(event->y());
      pappso::pappso_double y = yAxis->pixelToCoord(event->y());
      if(y < 0)
        {
          y = 0;
        }
      if(_control_key)
        {
          if(y > 0)
            {
              this->yAxis->scaleRange(_old_y / y, 0);
            }
        }
      else
        {
          this->xAxis->moveRange(xAxis->pixelToCoord(_old_x) -
                                 xAxis->pixelToCoord(event->x()));
        }
      _old_x = event->x();
      _old_y = y;
      replot();
      //   qDebug() << "Plot::mouseMoveEvent end";
    }
}
