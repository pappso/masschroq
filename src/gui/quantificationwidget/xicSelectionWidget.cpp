/**
 * \file xicSelectionWidget.cpp
 * \date November 15, 2011
 * \author Edlira Nano
 */

#include "xicSelectionWidget.h"

#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

#include <pappsomspp/mzrange.h>
#include "../../lib/mcq_error.h"
#include "../../lib/quanti_items/quantiItemMzRt.h"
#include "../engine/masschroq_gui_engin.h"

XicSelectionWidget::XicSelectionWidget(QWidget *parent)
  : MasschroQWidget(parent)
{
}

XicSelectionWidget::XicSelectionWidget(TreatmentBoxXicExtract *treatment,
                                       QWidget *parent)
  : MasschroQWidget(parent)
{
  qDebug() << "XicSelectionWidget::XicSelectionWidget begin";
  _treatmentBoxXicExtract = treatment;
  // close action
  setAttribute(Qt::WA_DeleteOnClose);

  _xic_selection = new QGroupBox("XIC Extraction");
  create_xic_type_group();
  create_xic_range_group();
  create_mz_group();

  QPushButton *extractButton = new QPushButton(tr("&Extract"));
  extractButton->setDefault(true);
  QDialogButtonBox *_button_box = new QDialogButtonBox(Qt::Horizontal);
  _button_box->addButton(extractButton, QDialogButtonBox::AcceptRole);
  //_button_box->addButton(QDialogButtonBox::Cancel);
  // connect(_button_box, SIGNAL(rejected()), this, SLOT(cancel()));

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(_xic_selection);
  QVBoxLayout *selectLayout = new QVBoxLayout;
  selectLayout->addWidget(_mz_group);
  selectLayout->addWidget(_xic_type_group);
  selectLayout->addWidget(_xic_range_group);
  selectLayout->addWidget(_button_box);
  _xic_selection->setLayout(selectLayout);
  setLayout(mainLayout);
  setWindowTitle(tr("XIC extraction"));

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_button_box,
          &QDialogButtonBox::accepted,
          this,
          &XicSelectionWidget::extractXic);
#else
  // Qt4 code
  connect(_button_box, SIGNAL(accepted()), this, SLOT(extractXic()));
#endif
  qDebug() << "XicSelectionWidget::XicSelectionWidget end";
}

XicSelectionWidget::~XicSelectionWidget()
{
  qDebug() << "delete xic selection";
}

void
XicSelectionWidget::create_xic_type_group()
{
  _xic_type_group     = new QGroupBox(tr("XIC type"));
  QHBoxLayout *layout = new QHBoxLayout;

  _xic_type_max = new QCheckBox(tr("max xic"));
  _xic_type_max->setChecked(true);
  QCheckBox *_xic_type_sum = new QCheckBox(tr("sum xic"));

  QButtonGroup *xic_type_buttons = new QButtonGroup();
  xic_type_buttons->setExclusive(true);
  xic_type_buttons->addButton(_xic_type_max);
  xic_type_buttons->addButton(_xic_type_sum);

  layout->addWidget(_xic_type_max);
  layout->addWidget(_xic_type_sum);

  _xic_type_group->setLayout(layout);

#if QT_VERSION >= 0x050000
// Qt5 code ())
#else
  // Qt4 code
  connect(xic_type_buttons,
          SIGNAL(buttonClicked(QAbstractButton *)),
          this,
          SLOT(setXicType(QAbstractButton *)));
#endif
}

void
XicSelectionWidget::create_xic_range_group()
{
  _xic_range_group    = new QGroupBox(tr("XIC extraction range"));
  QGridLayout *layout = new QGridLayout;

  QLabel *min_label = new QLabel("precision range");
  _precision_widget = new pappso::PrecisionWidget(this);
  layout->addWidget(min_label, 1, 0);
  layout->addWidget(_precision_widget, 1, 1);
  _xic_range_group->setLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code

  connect(_precision_widget,
          &pappso::PrecisionWidget::precisionChanged,
          this,
          &XicSelectionWidget::onPrecisionChange);

#else
  // Qt4 code
  connect(xic_range_buttons,
          SIGNAL(buttonClicked(QAbstractButton *)),
          this,
          SLOT(setXicExtractionMethod(QAbstractButton *)));

#endif
}

void
XicSelectionWidget::create_mz_group()
{
  qDebug() << "XicSelectionWidget::create_mz_group() begin xiSelectionWidget "
              "m/z group";
  _mz_group           = new QGroupBox(tr("m/z value selection"));
  QGridLayout *layout = new QGridLayout;

  _mz_type_combobox = new QComboBox;
  _mz_type_combobox->addItem("m/z");
  _mz_type_combobox->addItem("Mr");
  _mz_type_combobox->addItem("MH+");
  _mz_type_combobox->setEditable(false);

  layout->addWidget(_mz_type_combobox, 0, 0);

  _mz_value_spinbox = new QDoubleSpinBox;
  _mz_value_spinbox->setDecimals(4);
  _mz_value_spinbox->setMaximum(5000.0000);
  _mz_value_spinbox->setSingleStep(10.0000);
  _mz_value_spinbox->setValue(421.756);

  layout->addWidget(_mz_value_spinbox, 0, 1);

  _charge_spinbox = new QSpinBox;
  _charge_spinbox->setMaximum(10);
  _charge_spinbox->setMinimum(1);
  _charge_spinbox->setSingleStep(1);
  _charge_spinbox->setValue(1);

  layout->addWidget(new QLabel("Charge (z)"), 1, 0);
  layout->addWidget(_charge_spinbox, 1, 1);

  _mz_group->setLayout(layout);

#if QT_VERSION >= 0x050000
// Qt5 code
#else
// Qt4 code
#endif
  qDebug() << "XicSelectionWidget::create_mz_group() end";
}

void
XicSelectionWidget::extractXic()
{
  mcq_double mz = _mz_value_spinbox->value();

  if(_mz_type_combobox->currentText() == "MH+")
    {
      mz = (_mz_value_spinbox->value() +
            ((_charge_spinbox->value() - 1) * MHPLUS)) /
           _charge_spinbox->value();
    }
  else if(_mz_type_combobox->currentText() == "Mr")
    {
      mz = (_mz_value_spinbox->value() + (_charge_spinbox->value() * MHPLUS)) /
           _charge_spinbox->value();
    }
  QuantiItemBase *quanti_item;
  // if (_rt_to_extract != 0) {
  //	quanti_item = new QuantiItemMzRt(*_xic_extraction_method,
  //			_rt_to_extract);
  //} else {

  pappso::XicCoordSPtr xic_coord = std::make_shared<pappso::XicCoord>();
  xic_coord.get()->mzRange =
    pappso::MzRange(mz, _precision_widget->getPrecision());

  quanti_item = new QuantiItemBase(false, xic_coord);
  //}
  //_treatmentBoxXicExtract->setQuantiItem(quanti_item, _quantification_method);
  _quantification_method.setXicExtractMethod(pappso::XicExtractMethod::sum);
  if(_xic_type_max->isChecked())
    {
      _quantification_method.setXicExtractMethod(pappso::XicExtractMethod::max);
    }
  _quantification_method.setXicExtractPrecision(
    _precision_widget->getPrecision());

  qDebug() << _quantification_method.getMeanPrecision()->getNominal();
  _treatmentBoxXicExtract->setQuantiItem(quanti_item, &_quantification_method);
}

const QuantificationMethod &
XicSelectionWidget::getQuantificationMethod() const
{
  return _quantification_method;
}

void
XicSelectionWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  domDocument->addExtractionMethod(_quantification_method.getXicExtractMethod(),
                                   _precision_widget->getPrecision(),
                                   _precision_widget->getPrecision());
}

void
XicSelectionWidget::onPrecisionChange(pappso::PrecisionPtr precision
                                      [[maybe_unused]])
{
}
