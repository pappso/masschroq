/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file xicSelectionWidget.h
 * \date November 07, 2011
 * \author Edlira Nano
 */

#ifndef XIC_SELECTION_WIDGET_H_
#define XIC_SELECTION_WIDGET_H_ 1

#include "../../lib/quantifications/quantificationMethod.h"
#include "../../mcq_types.h"
#include "../masschroQWidget.h"
#include "../treatment/treatment_box_xic_extract.h"
#include <QWidget>

#include <QAbstractButton>
#include <QButtonGroup>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QVBoxLayout>
#include <pappsomspp/widget/precisionwidget/precisionwidget.h>

class XicExtractionMethodBase;

/**
 * \class XicSelectionWidget
 * \brief Xic Selection Widget
 *
 *
 */

class XicSelectionWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  XicSelectionWidget(TreatmentBoxXicExtract *treatment, QWidget *parent = 0);
  XicSelectionWidget(QWidget *parent = 0);

  virtual ~XicSelectionWidget();

  const QuantificationMethod &getQuantificationMethod() const;

  pappso::XicExtractMethod getXicExtractMethod() const;

  mcq_double getMzToExtract() const;

  mcq_double getRtToExtract() const;

  void writeElement(MasschroqDomDocument *domDocument) const;

  protected:
  virtual void extractXic();

  private slots:
  void onPrecisionChange(pappso::PrecisionPtr precision);

  protected:
  QuantificationMethod _quantification_method = QuantificationMethod("noid");
  QGroupBox *_mz_group;

  virtual void create_mz_group();
  void create_xic_type_group();
  void create_xic_range_group();
  void initializeExtractionMethod();

  QGroupBox *_xic_type_group;
  QGroupBox *_xic_range_group;
  QGroupBox *_xic_selection;
  pappso::PrecisionWidget *_precision_widget;
  QCheckBox *_xic_type_max;
  QCheckBox *_xic_type_sum;
  QDoubleSpinBox *_mz_value_spinbox;
  QSpinBox *_charge_spinbox;
  QComboBox *_mz_type_combobox;

  TreatmentBoxXicExtract *_treatmentBoxXicExtract;
};

#endif /* XIC_SELECTION_WIDGET_H_ */
