/*
 * obiwarp_thread.cpp
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#include "alignment_thread.h"
#include "../../lib/consoleout.h"
#include "../../lib/msrun/ms_run_hash_group.h"
#include <pappsomspp/pappsoexception.h>

AlignmentThread::AlignmentThread(QObject *parent,
                                 const unsigned int maxProgress)
  : McqThread(parent, maxProgress)
{
}

AlignmentThread::~AlignmentThread()
{
  _mutex.lock();
  _abort = true;
  _condition.wakeOne();
  _mutex.unlock();
  wait();
}

void
AlignmentThread::performedAlignment(AlignmentBase *alignment,
                                    MsrunSp p_msrun_ref,
                                    MsrunSp p_msrun)
{
  setProgressValue(1);
  _mutex.lock();
  _alignment   = alignment;
  _p_msrun_ref = p_msrun_ref;
  _p_msrun     = p_msrun;
  _abort       = false;
  _mutex.unlock();

  if(!isRunning())
    {
      start();
    }
  else
    {
      _restart = true;
      _condition.wakeOne();
    }
}

void
AlignmentThread::run()
{
  AlignmentBase *alignment = 0;
  msRunHashGroup *group    = new msRunHashGroup("G1");
  try
    {
      int current = 0;
      while(current < 2)
        {
          if(current == 0)
            {
              qDebug() << "Intialisation Alignment";
              // initialisation
              _mutex.lock();
              alignment = this->_alignment;
              group->addMsRun(_p_msrun_ref);
              group->addMsRun(_p_msrun);
              group->setReferenceMsrun(
                _p_msrun_ref->getMsRunIdCstSPtr()->getXmlId());
              alignment->printInfos(ConsoleOut::mcq_cout());
              _mutex.unlock();
            }
          else if(current == 1)
            {
              qDebug() << "Do Alignment";
              _mutex.lock();
              throw pappso::PappsoException(
                QObject::tr("ERROR: alignment broken in MassChroQ GUI"));
              _mutex.unlock();
            }
          else
            {
              break;
            }

          // Add progress
          current++;
          incrementProgressValue();

          if(this->getAbord())
            {
              // Loading is stop
              if(group != 0)
                delete(group);
              group = 0;
              break;
            }
          if(this->getRestart())
            {
              // Loading is restart with new param
              if(group != 0)
                delete(group);
              group   = 0;
              current = 0;
              _mutex.lock();
              _restart = false;
              _mutex.unlock();
            }
        }
    }
  catch(mcqError &error)
    {
      ConsoleOut::mcq_cerr() << "MassChroQ encountered an error:" << Qt::endl;
      ConsoleOut::mcq_cerr() << error.qwhat() << Qt::endl;
      emit errorDuringAlignment(error.qwhat());
      return;
    }

  // If all ok, emit loading finish
  if(!this->getRestart() & !this->getAbord())
    emit finishAlignment();

  qDebug() << "Finish Alignment";

  //	_mutex.lock();
  //	if (!_restart)
  //		_condition.wait(&_mutex);
  //	_restart = false;
  //	_mutex.unlock();
}
