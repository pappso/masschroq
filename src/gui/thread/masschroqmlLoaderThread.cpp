/**
 * \file msrunLoaderThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "masschroqmlLoaderThread.h"
#include "../../lib/consoleout.h"
#include "../../lib/mass_chroq.h"
#include "masschroqml_gui_parser.h"
#include <QApplication>
#include <QXmlInputSource>
#include <QXmlSimpleReader>
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>

MasschroqmlLoaderThread::MasschroqmlLoaderThread(QObject *parent,
                                                 const unsigned int maxProgress)
  : McqThread(parent, maxProgress)
{
}

MasschroqmlLoaderThread::~MasschroqmlLoaderThread()
{
  _mutex.lock();
  _abort = true;
  _condition.wakeOne();
  _mutex.unlock();
  wait();
}

void
MasschroqmlLoaderThread::loadMasschroqml(const QString &filename)
{
  setProgressValue(1);
  _mutex.lock();
  _masschroqData_filename = filename;
  _abort                  = false;
  _mutex.unlock();

  if(!isRunning())
    {
      start();
    }
  else
    {
      _restart = true;
      _condition.wakeOne();
    }
}

void
MasschroqmlLoaderThread::run()
{
  MasschroqGuiData *masschroqData = new MasschroqGuiData();
  pappso::UiMonitorTextPercent ui_monitor(ConsoleOut::mcq_cout());
  QString filename;
  try
    {
      int current = 0;
      while(current < 3)
        {
          if(current == 0)
            {
              qDebug() << "Intialisation Loading";
              // initialisation
              _mutex.lock();
              filename = this->_masschroqData_filename;
              _mutex.unlock();
            }
          else if(current == 1)
            {
              // Test validity of XML file
              MassChroq masschroq;
              masschroq.setMasschroqDir(QApplication::applicationDirPath());
              masschroq.setXmlFilename(filename);
              masschroq.validateXmlFile(ui_monitor);
            }
          else if(current == 2)
            {
              // Parse MassChroqML file
              qDebug();
              QFile file(filename);
              MasschroqmlGuiParser parser(masschroqData);
              qDebug();
              QXmlSimpleReader reader;
              reader.setContentHandler(&parser);
              reader.setErrorHandler(&parser);

              QXmlInputSource xmlInputSource(&file);
              qDebug();
              if(reader.parse(xmlInputSource))
                {
                  file.close();
                }
              else
                {
                  file.close();
                  throw mcqError(
                    QObject::tr("error reading masschroqML input :\n")
                      .append(parser.errorString()));
                }
              qDebug() << "Finish reading";
            }
          else
            {
              break;
            }

          // Add progress
          current++;
          incrementProgressValue();

          if(this->getAbord())
            {
              // Loading is stop
              if(masschroqData != nullptr)
                delete(masschroqData);
              masschroqData = nullptr;
              break;
            }
          if(this->getRestart())
            {
              // Loading is restart with new param
              if(masschroqData != nullptr)
                delete(masschroqData);
              masschroqData = nullptr;
              current       = 0;
              _mutex.lock();
              _restart = false;
              _mutex.unlock();
            }
        }
    }
  catch(mcqError &error)
    {
      ConsoleOut::mcq_cerr() << "MassChroQ encountered an error:" << Qt::endl;
      ConsoleOut::mcq_cerr() << error.qwhat() << Qt::endl;
      emit errorDuringLoading(error.qwhat());
      return;
    }

  // If all ok, emit loading finish
  if(!this->getRestart() & !this->getAbord())
    emit loadedMasschroqml(masschroqData);

  //	_mutex.lock();
  //	if (!_restart)
  //		_condition.wait(&_mutex);
  //	_restart = false;
  //	_mutex.unlock();
}
