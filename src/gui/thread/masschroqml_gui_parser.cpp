/*
 * masschroqml_gui_parser.cpp
 *
 *  Created on: 26 juil. 2012
 *      Author: valot
 */

#include "masschroqml_gui_parser.h"
#include <pappsomspp/pappsoexception.h>

MasschroqmlGuiParser::MasschroqmlGuiParser(MasschroqGuiData *masschroqGuiData)
  : _masschroqGuiData(masschroqGuiData)
{
  qDebug() << masschroqGuiData;
  _masschroqGuiData->clear();
  msp_currentPeptide = nullptr;
  qDebug();
}

MasschroqmlGuiParser::~MasschroqmlGuiParser()
{
}

bool
MasschroqmlGuiParser::startElement(const QString &namespaceURI [[maybe_unused]],
                                   const QString &localName [[maybe_unused]],
                                   const QString &qName,
                                   const QXmlAttributes &attributes)
{

  _tag_stack.push_back(qName);
  bool is_ok = true;

  // startElement_data_file
  if(qName == "data_file")
    {
      is_ok = startElement_data_file(attributes);
    }
  else if(qName == "peptide")
    {
      is_ok = startElement_peptide(attributes);
    }
  // startElement_observed_in
  else if(qName == "observed_in")
    {
      is_ok = startElement_observed_in(attributes);
    }
  // startElement_isotope_label
  else if(qName == "isotope_label")
    {
      is_ok = startElement_isotope_label(attributes);
    }
  // startElement_mod
  else if(qName == "mod")
    {
      is_ok = startElement_mod(attributes);
    }

  _currentText.clear();
  return is_ok;
}

bool
MasschroqmlGuiParser::endElement(const QString &namespaceURI [[maybe_unused]],
                                 const QString &localName [[maybe_unused]],
                                 const QString &qName)
{
  bool is_ok = true;

  if(qName == "peptide")
    {
      is_ok = endElement_peptide();
    }
  // endElement_isotope_label
  else if(qName == "isotope_label")
    {
      is_ok = endElement_isotope_label();
    }

  _currentText.clear();
  _tag_stack.pop_back();

  return is_ok;
}

bool
MasschroqmlGuiParser::startElement_data_file(const QXmlAttributes &attributes)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QString filename = attributes.value("path");
  if(filename.isEmpty())
    {
      _errorStr = QObject::tr("The data_file tag must have a path attribute.");
      return false;
    }

  QString idname = attributes.value("id");
  if(idname.isEmpty())
    {
      _errorStr = QObject::tr("The data_file tag must have an id attribute.");
      return false;
    }

  QString format = attributes.value("format");
  if(format.isEmpty())
    {
      _errorStr =
        QObject::tr("The data_file tag must have a mime_type attribute.");
      return false;
    }

  QFileInfo filenameInfo(filename);
  if(!filenameInfo.exists())
    {
      _errorStr = QObject::tr("cannot find the '%1' input file : %2 \n")
                    .arg(format, filename);
      return false;
    }
  else if(!filenameInfo.isReadable())
    {
      _errorStr = QObject::tr("cannot read the '%1' input file : %2 \n")
                    .arg(format, filename);
      return false;
    }

  _masschroqGuiData->addMsrunFilename(idname, filename);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return (true);
}

bool
MasschroqmlGuiParser::startElement_peptide(const QXmlAttributes &attributes)
{
  QString idname = attributes.value("id");
  QString mh     = attributes.value("mh");
  QString seq    = attributes.value("seq");
  QString mods   = attributes.value("mods");
  if(idname.isEmpty())
    {
      _errorStr = QObject::tr("the peptide tag must have an id attribute.");
      return false;
    }
  if(mh.isEmpty())
    {
      _errorStr = QObject::tr("the peptide tag must have a mh attribute.");
      return false;
    }
  /// create a new Peptide object and set its members
  msp_currentPeptide = std::make_shared<Peptide>(idname);

  if(!mods.isEmpty())
    {
      msp_currentPeptide->setMods(mods);
    }
  if(!seq.isEmpty())
    {
      try
        {
          pappso::Peptide peptide(seq);
          _current_peptide_sp = peptide.makeNoConstPeptideSp();
          msp_currentPeptide->setPappsoPeptideSp(_current_peptide_sp);
        }
      catch(const pappso::PappsoException &e)
        {
          _errorStr = QObject::tr("error building peptide %1\n%2")
                        .arg(seq)
                        .arg(e.qwhat());
          return false;
        }
    }
  msp_currentPeptide->setMh(mh.toDouble());

  return true;
}

/// <observed_in data="delumeau1" scan="33" z="2">
bool
MasschroqmlGuiParser::startElement_observed_in(const QXmlAttributes &attributes)
{

  QString data  = attributes.value("data");
  QString scan  = attributes.value("scan");
  QString index = attributes.value("index");
  QString z     = attributes.value("z");

  if(data.isEmpty())
    {
      _errorStr = QObject::tr(
        "the observed_in tag must have a data attribute "
        "(reference on a valid msRun id).");
      return false;
    }
  if(scan.isEmpty() && index.isEmpty())
    {
      _errorStr =
        QObject::tr("the observed_in tag must have an index attribute.");
      return false;
    }
  if(z.isEmpty())
    {
      _errorStr = QObject::tr("the observed_in tag must have a z attribute.");
      return false;
    }

  Msrun *p_msrun = _masschroqGuiData->getMsrunSp(data).get();
  if(p_msrun == nullptr)
    {
      _errorStr = QObject::tr(
        "the observed_in tag contains a data attribute "
        "that does not reference any msrun...");
      return false;
    }
  /// add the observed in infos to the current peptide

  if(scan.isEmpty())
    {
      msp_currentPeptide.get()->observed_in(
        p_msrun, (std::size_t)index.toULongLong(), z.toInt(), true);
    }
  else
    {

      msp_currentPeptide.get()->observed_in(
        p_msrun, (std::size_t)scan.toULongLong(), z.toInt(), false);
    }
  return true;
}

bool
MasschroqmlGuiParser::endElement_peptide()
{
  /// add this peptide to _peptide_list (: PeptideList)
  _masschroqGuiData->addPeptide(msp_currentPeptide);
  return true;
}

bool
MasschroqmlGuiParser::startElement_isotope_label(
  const QXmlAttributes &attributes)
{
  /// create a new IsotopeLabel object and set the current one to it
  _p_isotope_label = new IsotopeLabel();
  QString id       = attributes.value("id");
  if(id.isEmpty())
    {
      _errorStr =
        QObject::tr("the isotope_label tag must have an id attribute.");
      return false;
    }
  else
    {
      _p_isotope_label->setXmlId(id);
    }
  return true;
}

/// isotope_label modification
bool
MasschroqmlGuiParser::startElement_mod(const QXmlAttributes &attributes)
{
  QString value = attributes.value("value");
  QString at    = attributes.value("at");
  if(value.isEmpty())
    {
      _errorStr = QObject::tr(
        "the mod tag must have a value attribute : the "
        "mass of this modification");
      return false;
    }

  if(at.isEmpty())
    {
      _errorStr = QObject::tr(
        "the mod tag must have an at attribute : the "
        "position of the modification given by AA one "
        "letter code or Cter or Nter");
      return false;
    }
  /// create a new IsotopeLabelModification and add it to the current isotope
  IsotopeLabelModification *p_mod = new IsotopeLabelModification();
  p_mod->setAA(at);
  p_mod->setMass(value.toDouble());

  _p_isotope_label->addIsotopeLabelModification(p_mod);
  return true;
}

bool
MasschroqmlGuiParser::endElement_isotope_label()
{
  _masschroqGuiData->addIsotopeLabel(_p_isotope_label);
  //  _p_isotope_label = NULL;
  return true;
}

bool
MasschroqmlGuiParser::characters(const QString &str)
{
  _currentText += str;
  return true;
}

bool
MasschroqmlGuiParser::fatalError(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2:\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
MasschroqmlGuiParser::errorString() const
{
  return _errorStr;
}

bool
MasschroqmlGuiParser::endDocument()
{
  return true;
}
