/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope thatx it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file mcqThread.h
 * \date December 19, 2011
 * \author Edlira Nano
 */

#ifndef MCQ_THREAD_H_
#define MCQ_THREAD_H_ 1

#include <QDebug>
#include <QMutex>
#include <QThread>

class McqThread : public QThread
{
  Q_OBJECT

  public:
  McqThread(QObject *parent = 0, const unsigned int maxProgress = 0);
  ~McqThread();

  unsigned int getProgressValue();

  void setMaxProgress(const unsigned int value);

  unsigned int getMaxProgress();

  public slots:
  void canceled();

  protected:
  virtual void run() = 0;

  void setProgressValue(const unsigned int val);
  void incrementProgressValue();
  bool getAbord();
  bool getRestart();

  bool _abort;
  bool _restart;

  private:
  QMutex _progressMutex;
  unsigned int _maxProgressValue;
  unsigned int _progressValue;
};
#endif /* MCQ_THREAD_H_ */
