/**
 * \file msrunLoaderThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "msrunLoaderThread.h"
#include "../../lib/consoleout.h"
#include "../../mcqsession.h"
#include <pappsomspp/pappsoexception.h>

MsrunLoaderThread::MsrunLoaderThread(QObject *parent,
                                     const unsigned int maxProgress)
  : McqThread(parent, maxProgress)
{
}

MsrunLoaderThread::~MsrunLoaderThread()
{
  _mutex.lock();
  _abort = true;
  _condition.wakeOne();
  _mutex.unlock();
  wait();
}

void
MsrunLoaderThread::loadMsrun(MsrunSp msrun)
{
  setProgressValue(1);
  _mutex.lock();
  _current_msrun = msrun;
  _abort         = false;
  _mutex.unlock();

  if(!isRunning())
    {
      start();
    }
  else
    {
      _restart = true;
      _condition.wakeOne();
    }
}

void
MsrunLoaderThread::run()
{
  MsrunSp msrun = _current_msrun;
  QString filename;
  bool read_time_values = false;
  QString time_dir("");
  if(msrun == nullptr)
    {
      emit errorDuringLoading(QObject::tr("no MS run to load"));
      return;
    }
  try
    {
      int current = 0;
      while(current < 3)
        {
          if(current == 0)
            {
              qDebug() << "Intialisation Loading";
              // initialisation
              _mutex.lock();
              _mutex.unlock();
            }
          else if(current == 1)
            {
              qDebug() << "Load Scan number";
              msrun->readPrecursorsAndRetentionTimesInMzDataFile(
                read_time_values, time_dir);
            }
          else if(current == 2)
            {
              qDebug() << "Load Spectra";
              McqSession::getInstance().setTmpDir(QDir::tempPath());
              msrun->getMsRunXicExtractor(pappso::XicExtractMethod::max);
            }
          else
            {
              break;
            }

          // Add progress
          current++;
          incrementProgressValue();

          if(this->getAbord())
            {
              // Loading is stop
              msrun = nullptr;
              break;
            }
          if(this->getRestart())
            {
              // Loading is restart with new param
              msrun   = nullptr;
              current = 0;
              _mutex.lock();
              _restart = false;
              _mutex.unlock();
            }
        }
    }
  catch(pappso::PappsoException &errorp)
    {
      ConsoleOut::mcq_cerr() << "MassChroQ encountered an error:" << Qt::endl;
      ConsoleOut::mcq_cerr() << errorp.qwhat() << Qt::endl;
      emit errorDuringLoading(errorp.qwhat());
      return;
    }

  // If all ok, emit loading finish
  if(!this->getRestart() & !this->getAbord())
    emit loadedMsrun(msrun);

  //	_mutex.lock();
  //	if (!_restart)
  //		_condition.wait(&_mutex);
  //	_restart = false;
  //	_mutex.unlock();
}
