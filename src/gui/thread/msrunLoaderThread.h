/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope thatx it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file msrunLoaderThread.h
 * \date December 09, 2011
 * \author Edlira Nano
 */

#ifndef MSRUN_LOADER_THREAD_H_
#define MSRUN_LOADER_THREAD_H_ 1

#include <QWaitCondition>

#include "../../lib/mcq_error.h"
#include "../../lib/msrun/msrun.h"
#include "mcqThread.h"

/**
 * \class MsrunLoaderThread
 * \brief The thread that loads MSrun files in the MassChroq GUI.
 */

class MsrunLoaderThread : public McqThread
{
  Q_OBJECT

  public:
  MsrunLoaderThread(QObject *parent = 0, const unsigned int maxProgress = 0);
  ~MsrunLoaderThread();

  void loadMsrun(MsrunSp msrun);

  signals:

  void loadedMsrun(MsrunSp msrun);

  void errorDuringLoading(QString error);

  protected:
  virtual void run();

  private:
  QMutex _mutex;
  QWaitCondition _condition;

  MsrunSp _current_msrun;
};

#endif /* MSRUN_LOADER_THREAD_H_ */
