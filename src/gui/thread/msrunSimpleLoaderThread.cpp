/**
 * \file msrunLoaderThread.cpp
 * \date December 09, 2011
 * \author Edlira Nano
 */

#include "msrunSimpleLoaderThread.h"
#include "../../lib/consoleout.h"

MsrunSimpleLoaderThread::MsrunSimpleLoaderThread(QObject *parent,
                                                 const unsigned int maxProgress)
  : McqThread(parent, maxProgress)
{
}

MsrunSimpleLoaderThread::~MsrunSimpleLoaderThread()
{
  _mutex.lock();
  _abort = true;
  _condition.wakeOne();
  _mutex.unlock();
  wait();
}

void
MsrunSimpleLoaderThread::loadMsrun(MsrunSp msrun)
{
  qDebug() << "MsrunSimpleLoaderThread::loadMsrun begin";
  setProgressValue(1);
  _mutex.lock();
  _current_msrun = msrun;
  _abort         = false;
  _mutex.unlock();

  if(!isRunning())
    {
      start();
    }
  else
    {
      _restart = true;
      _condition.wakeOne();
    }
  qDebug() << "MsrunSimpleLoaderThread::loadMsrun end";
}

void
MsrunSimpleLoaderThread::run()
{
  qDebug() << "MsrunSimpleLoaderThread::run begin";
  MsrunSp msrun         = 0;
  bool read_time_values = false;
  QString time_dir("");
  try
    {
      int current = 0;
      while(current < 2)
        {
          if(current == 0)
            {
              qDebug() << "Intialisation Loading";
              // initialisation
              _mutex.lock();
              msrun = this->_current_msrun;
              _mutex.unlock();
            }
          else if(current == 1)
            {
              qDebug() << "Load Scan number";
              msrun->readPrecursorsAndRetentionTimesInMzDataFile(
                read_time_values, time_dir);
            }
          else
            {
              break;
            }

          // Add progress
          current++;
          incrementProgressValue();

          if(this->getAbord())
            {
              msrun = 0;
              break;
            }
          if(this->getRestart())
            {
              msrun   = 0;
              current = 0;
              _mutex.lock();
              _restart = false;
              _mutex.unlock();
            }
        }
    }
  catch(mcqError &error)
    {
      ConsoleOut::mcq_cerr() << "MassChroQ encountered an error:" << Qt::endl;
      ConsoleOut::mcq_cerr() << error.qwhat() << Qt::endl;
      emit errorDuringLoading(error.qwhat());
      return;
    }

  // If all ok, emit loading finish
  if(!this->getRestart() & !this->getAbord())
    emit loadedMsrun(msrun);

  qDebug() << "MsrunSimpleLoaderThread::run end";
  //	_mutex.lock();
  //	if (!_restart)
  //		_condition.wait(&_mutex);
  //	_restart = false;
  //	_mutex.unlock();
}
