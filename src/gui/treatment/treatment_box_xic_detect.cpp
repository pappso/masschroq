/*
 * treatment_box_xic_detection.cpp
 *
 *  Created on: 19 July 2012
 *      Author: valot
 */

#include "treatment_box_xic_detect.h"
#include <QDebug>

class XicDetectionSink : public pappso::TraceDetectionSinkInterface
{
  public:
  XicDetectionSink()
  {
  }
  void
  setTracePeak(pappso::TracePeak &xic_peak) override
  {
    _all_peaks_list.push_back(xic_peak);
  };
  const std::vector<pappso::TracePeak> &
  getNewPeakList() const
  {
    return _all_peaks_list;
  };

  private:
  std::vector<pappso::TracePeak> _all_peaks_list;
};

TreatmentBoxXicDetect::TreatmentBoxXicDetect(const TreatmentChain &chain)
  : TreatmentBox(chain)
{
  _p_detect = nullptr;
}

TreatmentBoxXicDetect::~TreatmentBoxXicDetect()
{
}

void
TreatmentBoxXicDetect::setPeakDetectionBase(
  pappso::TraceDetectionInterfaceSPtr detect)
{
  if(detect != nullptr)
    {
      _p_detect = detect;
      this->updatedAndEmitChanged();
    }
}

void
TreatmentBoxXicDetect::doTreatment()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  if((this->getPrevious() != nullptr) &&
     (this->getPrevious()->getCurrentXic() != nullptr))
    {

      pappso::XicCstSPtr previous_filter_sp =
        this->getPrevious()->getCurrentXic();
      pappso::Xic new_xic = pappso::Xic(*previous_filter_sp);
      _current_xic_sp     = new_xic.makeXicSPtr();
      if(_p_detect != nullptr)
        {
          XicDetectionSink detect_all_peaks;
          qDebug();
          _p_detect->detect(*_current_xic_sp, detect_all_peaks, false);
          qDebug();
          _all_peaks_list = detect_all_peaks.getNewPeakList();
          qDebug();
          emit detectedPeaks(this);
        }
    }
  qDebug();
}
