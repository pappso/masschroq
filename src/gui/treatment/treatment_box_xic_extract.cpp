/*
 * treatment_box_xic_extract.cpp
 *
 *  Created on: 16 mai 2012
 *      Author: valot
 */

#include "treatment_box_xic_extract.h"
#include "../../lib/quanti_items/quantiItemBase.h"
#include "../engine/masschroq_gui_engin.h"
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>

TreatmentBoxXicExtract::TreatmentBoxXicExtract(const TreatmentChain &chain)
  : TreatmentBox(chain)
{
}

TreatmentBoxXicExtract::~TreatmentBoxXicExtract()
{
  if(_quantiItem != nullptr)
    {
      delete(_quantiItem);
    }
}

void
TreatmentBoxXicExtract::setQuantiItem(
  QuantiItemBase *quantiItem,
  const QuantificationMethod *p_quantification_method)
{
  if(quantiItem != 0)
    {
      if(_quantiItem != 0)
        {
          delete(_quantiItem);
        }
      _quantiItem              = quantiItem;
      _p_quantification_method = p_quantification_method;
      this->updatedAndEmitChanged();
    }
}

void
TreatmentBoxXicExtract::doTreatment()
{
  qDebug();
  _msrun_sp = MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun();
  if(_quantiItem != nullptr)
    {
      // extract XIC
      if(_msrun_sp.get() != nullptr)
        {
          pappso::MsRunXicExtractorInterfaceSp xic_extractor =
            _msrun_sp.get()->getMsRunXicExtractor(
              _p_quantification_method->getXicExtractMethod());
          pappso::XicCoordSPtr xic_coord =
            pappso::XicCoord().initializeAndClone();
          xic_coord.get()->mzRange =
            _p_quantification_method->getMzRange(_quantiItem->getMz());
          std::vector<pappso::XicCoordSPtr> xic_coord_list;
          xic_coord_list.push_back(xic_coord);
          pappso::UiMonitorVoid monitor;
          xic_extractor.get()->extractXicCoordSPtrList(monitor, xic_coord_list);
          _current_xic_sp = xic_coord.get()->xicSptr;
          if(_current_xic_sp != nullptr)
            {
              //_currentXic->toStream(std::cout);
              emit createdXic(this);
            }
        }
    }
  qDebug();
}
