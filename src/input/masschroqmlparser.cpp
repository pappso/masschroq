/**
 * \file src/input/masschroqmlparser.cpp
 * \date 8/7/2022
 * \author Olivier Langella
 * \brief read MassChroQml file to perform quantification
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "masschroqmlparser.h"
#include <QDir>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <QtConcurrent>
#include "../lib/alignments/monitors/monitor_alignment_time.h"
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/processing/filters/filtermorpho.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include <pappsomspp/processing/detection/tracedetectionmoulon.h>
#include "../output/monitors/monitorcomparoutput.h"
#include "../output/monitors/monitorodsoutput.h"
#include "../output/monitors/monitorpeakshapeoutput.h"
#include "../output/monitors/monitorspeedlist.h"
#include "../output/monitors/monitorxiccoordoutput.h"
#include <odsstream/tsvdirectorywriter.h>
#include "../output/masschroqWriter.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotrecognized.h>

MasschroqmlParser::MasschroqmlParser(pappso::UiMonitorInterface &ui_monitor,
                                     MassChroq *p_my_chroq)
  : m_uiMonitor(ui_monitor)
{
  mp_massChroq = p_my_chroq;
}

MasschroqmlParser::~MasschroqmlParser()
{
}

bool
MasschroqmlParser::readFile(const QString &fileName)
{
  QDir::setCurrent(QFileInfo(fileName).absolutePath());
  return pappso::XmlStreamReaderInterface::readFile(fileName);
}

void
MasschroqmlParser::readStream()
{

  /*
  <masschroq>
    <rawdata><!-- time_values_dir="directory" to read retention time
  corrections--> <data_file id="samp0" format="mzxml" path="bsa1.mzXML"
  type="profile" />
  */


  qDebug();
  try
    {
      if(m_qxmlStreamReader.readNextStartElement())
        {
          if(m_qxmlStreamReader.name().toString() == "masschroq")
            {
              readRawdata();
              readGroups();
              readProteinList();
              readPeptideList();
              // isotope_label_list

              while(m_qxmlStreamReader.readNextStartElement())
                {
                  qDebug() << m_qxmlStreamReader.name();
                  if(m_qxmlStreamReader.name().toString() ==
                     "isotope_label_list")
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      readIsotopeLabelList();
                    }
                  else if(m_qxmlStreamReader.name().toString() == "alignments")
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      readAlignments();
                    }
                  else if(m_qxmlStreamReader.name().toString() ==
                          "quantification_methods")
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      //<quantification_methods>
                      readQuantificationMethods();
                    }
                  else if(m_qxmlStreamReader.name().toString() ==
                          "quantification")
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      // quantification
                      readQuantification();
                    }
                  else
                    {

                      qDebug() << m_qxmlStreamReader.name();
                      m_qxmlStreamReader.raiseError(
                        QObject::tr(
                          "Not a MassChroqML file, %1 not allowed here ")
                          .arg(m_qxmlStreamReader.name()));
                    }
                }
            }
        }

      else
        {
          m_qxmlStreamReader.raiseError(QObject::tr("Not a MassChroqML file"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  catch(const pappso::PappsoException &e)
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("ERROR parsing XML file at tag %1:\n%2")
          .arg(m_qxmlStreamReader.name())
          .arg(e.qwhat()));
    }
}

void
MasschroqmlParser::readQuantification()
{
  qDebug() << m_qxmlStreamReader.name();
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification")
    {

      MonitorSpeedList monitor_speed_list;
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          // quantification_results
          if(m_qxmlStreamReader.name().toString() == "quantification_results")
            {
              read_quantification_results(monitor_speed_list);
              // set the result/monitor list
              mp_massChroq->setResults(&monitor_speed_list);
            }
          else if(m_qxmlStreamReader.name().toString() ==
                  "quantification_traces")
            {
              // quantification_traces
              read_quantification_traces();
            }
          // quantify
          else if(m_qxmlStreamReader.name().toString() == "quantify")
            {
              read_quantify();
            }
          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr("unexpected %1 element")
                                              .arg(m_qxmlStreamReader.name()));
            }
        }

      // m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a quantification element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::readQuantificationMethods()
{
  qDebug() << m_qxmlStreamReader.name();
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification_methods")
    {
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          read_quantification_method();
        }

      qDebug() << m_qxmlStreamReader.name();
    }
  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("missing quantification_methods element"));
      m_qxmlStreamReader.skipCurrentElement();
    }

  qDebug() << m_qxmlStreamReader.name();
}

void
MasschroqmlParser::readAlignments()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "alignments")
    {
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "alignment_methods")
            {
              // alignment_methods
              while(m_qxmlStreamReader.readNextStartElement())
                {
                  qDebug() << m_qxmlStreamReader.name();
                  read_alignment_method();
                }
            }

          else
            {
              //<align group_id="G1" method_id="my_ms2"
              // reference_data_id="samp0" />
              read_align();
            }
        }

      // m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::readIsotopeLabelList()
{

  while(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      read_isotope_label();
    }

  qDebug() << m_qxmlStreamReader.name();
  // m_qxmlStreamReader.skipCurrentElement();
}

void
MasschroqmlParser::readPeptideList()
{
  if(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.name().toString() == "peptide_list")
        {

          m_uiMonitor.setTitle("Reading peptide list");
          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              read_peptide();
            }


          if(m_hasIonMobility)
            {
              m_uiMonitor.setTitle("Peptide list OK, IM corrections");

              m_ionMobilityGrid.computeCorrections();
            }
          else
            {
              m_uiMonitor.setTitle("Peptide list OK");
            }

          /*
          for(auto pair_it : m_ionMobilityGrid.getMapCorrectionsStart())
            {
              qInfo() << pair_it.first << " " << pair_it.second;
            }
            */

          qDebug() << m_qxmlStreamReader.name();
          mp_massChroq->setPeptideListInMsruns();


          qDebug() << m_qxmlStreamReader.name();
          // m_qxmlStreamReader.skipCurrentElement();
        }
    }
}

void
MasschroqmlParser::read_peptide()
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "peptide")
    {
      QString idname = m_qxmlStreamReader.attributes().value("id").toString();
      QString mh     = m_qxmlStreamReader.attributes().value("mh").toString();
      QString seq    = m_qxmlStreamReader.attributes().value("seq").toString();
      QString mods   = m_qxmlStreamReader.attributes().value("mods").toString();
      QString prot_ids =
        m_qxmlStreamReader.attributes().value("prot_ids").toString();
      if(idname.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the peptide tag must have an id attribute."));
        }
      if(mh.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the peptide tag must have a mh attribute."));
        }
      /// create a new Peptide object and set its members

      PeptideSPtr xtp_peptide_sp;
      xtp_peptide_sp = std::make_shared<Peptide>(idname);
      pappso::NoConstPeptideSp pappso_peptide_sp;

      if(!mods.isEmpty())
        {
          xtp_peptide_sp->setMods(mods);
        }
      if(seq.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the peptide tag must have a seq attribute."));
        }
      else
        {
          try
            {
              pappso::Peptide peptide(seq);
              pappso_peptide_sp = peptide.makeNoConstPeptideSp();
              xtp_peptide_sp->setPappsoPeptideSp(pappso_peptide_sp);
            }
          catch(const pappso::PappsoException &e)
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("error building peptide %1\n%2")
                  .arg(seq)
                  .arg(e.qwhat()));
            }
        }

      //_p_current_peptide->setMh(mh.toDouble());
      if(!prot_ids.isEmpty())
        {
          QStringList list;
          list = prot_ids.split(" ", Qt::SkipEmptyParts);
          for(int i = 0; i < list.size(); ++i)
            {
              const Protein *p_prot = mp_massChroq->findProtein(list.at(i));
              if(p_prot == NULL)
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("protein id %1 not defined.").arg(list.at(i)));
                }
              else
                {
                  xtp_peptide_sp->addProtein(p_prot);
                }
            }
        }
      /// add this peptide to _peptide_list (: PeptideList)
      mp_massChroq->addPeptideInPeptideList(xtp_peptide_sp);

      // modifications + observed_in
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "modifications")
            {
              while(m_qxmlStreamReader.readNextStartElement())
                {
                  if(m_qxmlStreamReader.name().toString() == "psimod")
                    {
                      read_psimod(pappso_peptide_sp);
                    }
                  else
                    {
                      m_qxmlStreamReader.raiseError(
                        QObject::tr("%1 element not allowed here")
                          .arg(m_qxmlStreamReader.name()));
                      m_qxmlStreamReader.skipCurrentElement();
                    }
                }
            }
          else if(m_qxmlStreamReader.name().toString() == "observed_in")
            {
              read_observed_in(xtp_peptide_sp);

              qDebug();
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("%1 element not allowed here")
                  .arg(m_qxmlStreamReader.name()));
              m_qxmlStreamReader.skipCurrentElement();
            }
        }

      qDebug();
      xtp_peptide_sp->setMh(mh.toDouble());
      qDebug();
      // qInfo() << idname;
      // m_qxmlStreamReader.skipCurrentElement();
      // TODO get only the best observation per msrun
      std::vector<PeptideObservedIn *> all_obser =
        xtp_peptide_sp.get()->getObservedInList();

      qDebug();
      for(unsigned int charge = 1; charge < 4; charge++)
        {
          populateIonMobilityGridWithObservedPeptideByCharges(all_obser,
                                                              charge);
        }
      qDebug()
        << "peptide ProForma=" << pappso_peptide_sp.get()->toProForma() << " "
        << xtp_peptide_sp.get()->getPappsoPeptideSp().get()->toProForma();
    }
  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a peptide element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}

void
MasschroqmlParser::populateIonMobilityGridWithObservedPeptideByCharges(
  std::vector<PeptideObservedIn *> all_obser, unsigned int charge)
{
  if(m_hasIonMobility)
    {
      qDebug();
      all_obser.erase(std::remove_if(all_obser.begin(),
                                     all_obser.end(),
                                     [charge](PeptideObservedIn *x) {
                                       return x->getZ() != charge;
                                     }),
                      all_obser.end());

      qDebug();
      std::sort(all_obser.begin(),
                all_obser.end(),
                [](const PeptideObservedIn *a, const PeptideObservedIn *b) {
                  if(a->getPmsRun() == b->getPmsRun())
                    {
                      return (a->getIntensity() > b->getIntensity());
                    }
                  else
                    {
                      return (a->getPmsRun() < b->getPmsRun());
                    };
                });
      auto last =
        std::unique(all_obser.begin(),
                    all_obser.end(),
                    [](const PeptideObservedIn *a, const PeptideObservedIn *b) {
                      if(a->getPmsRun() == b->getPmsRun())
                        {
                          return true;
                        }
                      else
                        {
                          return false;
                        };
                    });
      qDebug();
      // v now holds {1 2 3 4 5 x x}, where 'x' is indeterminate
      all_obser.erase(last, all_obser.end());

      for(const PeptideObservedIn *observed_in_a : all_obser)
        {
          // qInfo() << " observed_in_a";
          const pappso::MsRunId &msrun_id_a =
            *observed_in_a->getPmsRun()->getMsRunIdCstSPtr().get();
          pappso::XicCoordSPtr xic_coord_a =
            observed_in_a->getPrecursorSPtr().get()->getXicCoordSPtr();

          if(xic_coord_a == nullptr)
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("error xic_coord_a == nullptr %1 %2 %3")
                  .arg(__FILE__)
                  .arg(__FUNCTION__)
                  .arg(__LINE__));
            }
          qDebug();
          for(const PeptideObservedIn *observed_in_b : all_obser)
            {
              if(observed_in_a == observed_in_b)
                {
                  break;
                }
              else
                {

                  // qInfo() << " observed_in_b";
                  const pappso::MsRunId &msrun_id_b =
                    *observed_in_b->getPmsRun()->getMsRunIdCstSPtr().get();
                  if(msrun_id_b == msrun_id_a)
                    {
                    }
                  else
                    {
                      pappso::XicCoordSPtr xic_coord_b =
                        observed_in_b->getPrecursorSPtr()
                          .get()
                          ->getXicCoordSPtr();


                      if(xic_coord_b == nullptr)
                        {
                          m_qxmlStreamReader.raiseError(
                            QObject::tr("error xic_coord_b == nullptr %1 %2 %3")
                              .arg(__FILE__)
                              .arg(__FUNCTION__)
                              .arg(__LINE__));
                        }
                      /*
                  qInfo()
                    << " xic_coord_a " << xic_coord_a.get()->toString();
                  qInfo()
                    << " xic_coord_b " << xic_coord_b.get()->toString();*/
                      m_ionMobilityGrid.storeObservedIdentityBetween(
                        msrun_id_a,
                        xic_coord_a.get(),
                        msrun_id_b,
                        xic_coord_b.get());
                    }
                }
            }
        }
    }
  qDebug();
}
void
MasschroqmlParser::readProteinList()
{
  if(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.name().toString() == "protein_list")
        {

          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              //<protein desc="conta|P02769|ALBU_BOVIN SERUM ALBUMIN PRECURSOR."
              // id="P1.1" />
              read_protein();
            }
        }
    }
}


//<protein desc="conta|P02769|ALBU_BOVIN SERUM ALBUMIN PRECURSOR."
// id="P1.1" />
void
MasschroqmlParser::read_protein()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "protein")
    {
      QString id   = m_qxmlStreamReader.attributes().value("id").toString();
      QString desc = m_qxmlStreamReader.attributes().value("desc").toString();
      if(id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the protein tag must have an id attribute."));
        }
      /// create a new Protein object and set its description
      ProteinSp p_protein;
      try
        {
          Protein protein(id);
          protein.setDescription(desc);
          p_protein = std::make_shared<const Protein>(protein);
        }
      catch(mcqError &error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("problem creating protein :\n%1").arg(error.what()));
        }
      /// add this protein to _p_proteins (: map<id, Protein *>)
      mp_massChroq->addProtein(p_protein);
      qDebug();
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a protein element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}


void
MasschroqmlParser::readRawdata()
{
  if(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.name().toString() == "project_parameters")
        {
          m_qxmlStreamReader.skipCurrentElement();
          if(m_qxmlStreamReader.readNextStartElement())
            {
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("Not a MassChroqML file"));
              m_qxmlStreamReader.skipCurrentElement();
            }
        }
      if(m_qxmlStreamReader.name().toString() == "rawdata")
        {
          QString time_dir =
            m_qxmlStreamReader.attributes().value("time_values_dir").toString();
          if(!time_dir.isEmpty())
            {
              m_isTimeValues  = true;
              m_timeDirectory = time_dir;
            }
          if(!QDir(m_timeDirectory).exists())
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("The directory  :\n%1\n from the attribute "
                            "'time_values_dir'  in the element 'rawdata' does "
                            "not exists.")
                  .arg(m_timeDirectory));
            }

          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              //<data_file id="samp0" format="mzxml" path="bsa1.mzXML"
              // type="profile" />
              read_data_file();
            }
          // m_qxmlStreamReader.skipCurrentElement();

          endElement_rawdata();
        }
      else
        {
          m_qxmlStreamReader.raiseError(QObject::tr("missing rawdata element"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a MassChroqML file"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


void
MasschroqmlParser::endElement_rawdata()
{
  try
    {

      /* not ready for multithread*/
      MassChroq *local_my_chroq_p  = mp_massChroq;
      bool read_time_values_local  = m_isTimeValues;
      QString time_directory_local = m_timeDirectory;

      pappso::UiMonitorInterface *local_monitor = &m_uiMonitor;


      std::function<MsrunSp(const std::pair<QString, QFileInfo> &)>
        mapFilenameList = [local_my_chroq_p,
                           read_time_values_local,
                           time_directory_local](
                            const std::pair<QString, QFileInfo> &mapit) {
          pappso::MsFileAccessor file_access(
            mapit.second.absoluteFilePath(),
            QString("msfile%1").arg(mapit.first));
          file_access.setPreferredFileReaderType(
            pappso::MsDataFormat::brukerTims, pappso::FileReaderType::tims_ms2);

          std::vector<pappso::MsRunIdCstSPtr> msrunid_list =
            file_access.getMsRunIds();


          if(file_access.getFileFormat() == pappso::MsDataFormat::unknown)
            {
              throw pappso::ExceptionNotRecognized(
                "MS data file format not recognized");
            }
          if(msrunid_list.size() == 0)
            {
              throw pappso::PappsoException("msrunid_list.size() == 0");
            }


          pappso::MsRunReaderSPtr run_reader =
            file_access.getMsRunReaderSPtrByRunId("", mapit.first);
          run_reader.get()->setMonoThread(true);
          MsrunSp msrun = local_my_chroq_p->newMsRun(
            run_reader, read_time_values_local, time_directory_local);
          run_reader.get()->releaseDevice();
          run_reader.get()->setMonoThread(false);
          return msrun;
        };


      m_uiMonitor.setTitle(
        QObject::tr("reading %1 msruns").arg(m_msfileList.size()));


      std::function<void(std::size_t, const MsrunSp)> reduce_function =
        [local_monitor, local_my_chroq_p](std::size_t result,
                                          const MsrunSp msrun) {
          local_monitor->setStatus(
            QObject::tr("MS run '%1' from file %2: added")
              .arg(msrun.get()->getMsRunIdCstSPtr().get()->getXmlId())
              .arg(msrun.get()->getMsRunIdCstSPtr().get()->getFileName()));

          local_my_chroq_p->getMsRunHashGroupAllCollection().addMsRun(msrun);
          result++;
        };

      QFuture<std::size_t> res =
        QtConcurrent::mappedReduced<std::size_t>(m_msfileList.begin(),
                                                 m_msfileList.end(),
                                                 mapFilenameList,
                                                 reduce_function,
                                                 QtConcurrent::OrderedReduce);
      res.waitForFinished();


      m_hasIonMobility = mp_massChroq->hasIonMobility();
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      m_qxmlStreamReader.raiseError(exception_pappso.qwhat());
    }
}

void
MasschroqmlParser::readGroups()
{
  if(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.name().toString() == "groups")
        {

          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              //<data_file id="samp0" format="mzxml" path="bsa1.mzXML"
              // type="profile" />
              read_group();
            }
          // m_qxmlStreamReader.skipCurrentElement();
        }
    }
}


/// <data_file id=.../>
void
MasschroqmlParser::read_data_file()
{

  if(m_qxmlStreamReader.name().toString() == "data_file")
    {
      QString filename =
        m_qxmlStreamReader.attributes().value("path").toString();
      if(filename.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("The data_file tag must have a path attribute."));
        }

      QString idname = m_qxmlStreamReader.attributes().value("id").toString();
      if(idname.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("The data_file tag must have an id attribute."));
        }

      QFileInfo filenameInfo(filename);
      if(!filenameInfo.exists())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("cannot find input file : %2 \n").arg(filename));
        }
      else if(!filenameInfo.isReadable())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("cannot read input file : %2 \n").arg(filename));
        }

      QString type = m_qxmlStreamReader.attributes().value("type").toString();
      if(!type.isEmpty() && type == "srm")
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "The SRM mode is not yet implemented in MassChroQ, but it will: "
            "\n%1"));
        }

      try
        {

          m_msfileList.push_back(
            std::pair<QString, QFileInfo>(idname, QFileInfo(filename)));
          qDebug() << m_msfileList.size();

          m_qxmlStreamReader.skipCurrentElement();
        }
      catch(pappso::PappsoException &pappso_error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("problem creating msrun %1:\n%2")
              .arg(idname, pappso_error.qwhat()));
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a data_file element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <group id="G1" data_ids="delumeau1 delumeau2"></group>
void
MasschroqmlParser::read_group()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "group")
    {
      QString idname = m_qxmlStreamReader.attributes().value("id").toString();
      QString msrun_ids =
        m_qxmlStreamReader.attributes().value("data_ids").toString();
      if(idname.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the group tag must have an id attribute."));
        }
      if(msrun_ids.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the group tag must have a data_ids attribute."));
        }

      qDebug();
      QStringList list_msrun_ids = msrun_ids.split(" ", Qt::SkipEmptyParts);
      /// add this group of msruns to _groups (: map<group_id, msRunHashGroup
      /// *>) the first msrun in the group will be the reference one
      qDebug();
      mp_massChroq->newMsRunGroup(idname, list_msrun_ids);
      qDebug();

      m_qxmlStreamReader.skipCurrentElement();
      qDebug();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a group element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}


void
MasschroqmlParser::read_psimod(pappso::NoConstPeptideSp pappso_peptide_sp)
{

  qDebug() << m_qxmlStreamReader.name();
  QString at  = m_qxmlStreamReader.attributes().value("at").toString();
  QString acc = m_qxmlStreamReader.attributes().value("acc").toString();
  if(at.isEmpty())
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("the psimod tag must have an at attribute."));
    }
  if(acc.isEmpty())
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("the psimod tag must have an acc attribute."));
    }
  try
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstance(acc);
      pappso_peptide_sp.get()->addAaModification(modification, at.toUInt() - 1);
    }
  catch(const pappso::PappsoException &e)
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("ERROR setting psimod:\n%1").arg(e.qwhat()));
    }

  m_qxmlStreamReader.skipCurrentElement();
}

/// <observed_in data="delumeau1" scan="33" z="2">
void
MasschroqmlParser::read_observed_in(std::shared_ptr<Peptide> &peptide_sp)
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "observed_in")
    {
      QString data  = m_qxmlStreamReader.attributes().value("data").toString();
      QString scan  = m_qxmlStreamReader.attributes().value("scan").toString();
      QString index = m_qxmlStreamReader.attributes().value("index").toString();
      QString z     = m_qxmlStreamReader.attributes().value("z").toString();
      if(data.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the observed_in tag must have a data attribute "
                        "(reference on a valid msRun id)."));
        }
      if(scan.isEmpty() && index.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the observed_in tag must have an index attribute."));
        }
      if(z.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the observed_in tag must have a z attribute."));
        }

      Msrun *p_msrun = mp_massChroq->findMsRun(data);
      if(p_msrun == nullptr)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the observed_in tag contains a data attribute "
                        "that does not reference any msrun..."));
        }
      /// add the observed in infos to the current peptide
      try
        {
          if(scan.isEmpty())
            {
              peptide_sp.get()->observed_in(
                p_msrun, (std::size_t)index.toULongLong(), z.toInt(), true);
            }
          else
            {

              peptide_sp.get()->observed_in(
                p_msrun, (std::size_t)scan.toULongLong(), z.toInt(), false);
            }
        }
      catch(pappso::PappsoException &err)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("error reading observed_in tag : %1").arg(err.qwhat()));
        }

      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not an observed_in element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


void
MasschroqmlParser::read_isotope_label()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "isotope_label")
    {
      /// create a new IsotopeLabel object and set the current one to it
      IsotopeLabel *p_isotope_label = new IsotopeLabel();
      QString id = m_qxmlStreamReader.attributes().value("id").toString();
      if(id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the isotope_label tag must have an id attribute."));
        }
      else
        {
          p_isotope_label->setXmlId(id);
        }
      // read mod

      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          read_mod(p_isotope_label);
        }


      qDebug() << m_qxmlStreamReader.name();
      mp_massChroq->addIsotopeLabel(p_isotope_label);
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not an isotope_label element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

/// isotope_label modification
void
MasschroqmlParser::read_mod(IsotopeLabel *p_isotope_label)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mod")
    {
      QString value = m_qxmlStreamReader.attributes().value("value").toString();
      QString at    = m_qxmlStreamReader.attributes().value("at").toString();
      QString acc   = m_qxmlStreamReader.attributes().value("acc").toString();
      if(value.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the mod tag must have a value attribute : the "
                        "mass of this modification"));
        }

      if(at.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the mod tag must have an at attribute : the "
                        "position of the modification given by AA one "
                        "letter code or Cter or Nter"));
        }
      /// create a new IsotopeLabelModification and add it to the current
      /// isotope
      IsotopeLabelModification *p_mod = new IsotopeLabelModification();
      p_mod->setAA(at);
      p_mod->setMass(value.toDouble());

      if(!acc.isEmpty())
        {
          pappso::AaModificationP modification =
            pappso::AaModification::getInstance(acc);
          p_mod->setPappsoModification(modification);
        }
      p_isotope_label->addIsotopeLabelModification(p_mod);
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a mod element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <alignment_method id="obiwarp1">
void
MasschroqmlParser::read_alignment_method()
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "alignment_method")
    {
      try
        {
          QString alignment_method_id =
            m_qxmlStreamReader.attributes().value("id").toString();
          if(alignment_method_id.isEmpty())
            {
              m_qxmlStreamReader.raiseError(QObject::tr(
                "the alignment_method tag must have an id attribute."));
            }

          /// we will set the current_alignment_method later (in the <obiwarp or
          /// <ms2> tag that follows this one )
          //  _p_current_alignment_method = NULL;

          if(m_qxmlStreamReader.readNextStartElement())
            {
              AlignmentBase *p_alignment_base = nullptr;
              qDebug() << m_qxmlStreamReader.name();
              if(m_qxmlStreamReader.name().toString() == "obiwarp")
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("sorry, obiwarp alignment method is no more "
                                "available since MassChroQ "
                                "version 2.2.23"));
                }
              else if(m_qxmlStreamReader.name().toString() == "ms2")
                {
                  p_alignment_base = read_ms2();
                }

              mp_massChroq->addAlignmentMethod(alignment_method_id,
                                               p_alignment_base);
            }
        }

      catch(pappso::PappsoException &error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("error reading %1 tag :\n%2")
              .arg(m_qxmlStreamReader.name().toString())
              .arg(error.qwhat()));
        }
      qDebug() << m_qxmlStreamReader.name();
      m_qxmlStreamReader.skipCurrentElement();
      qDebug() << m_qxmlStreamReader.name();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not an alignment_method element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

/// <ms2>
AlignmentBase *
MasschroqmlParser::read_ms2()
{
  AlignmentMs2 *p_ms2_meth = nullptr;
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "ms2")
    {

      try
        {
          QString time_output_dir = m_qxmlStreamReader.attributes()
                                      .value("write_time_values_output_dir")
                                      .toString();
          if(!time_output_dir.isEmpty())
            {
              // Print time value
              MonitorAlignmentTime *time_monitor = new MonitorAlignmentTime();
              time_monitor->setOutputDirectory(time_output_dir);
              p_ms2_meth = new AlignmentMs2(time_monitor);
            }
          else
            {
              // not print time value
              MonitorAlignmentBase *base_monitor = new MonitorAlignmentBase();
              p_ms2_meth = new AlignmentMs2(base_monitor);
            }

          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              QString inside = m_qxmlStreamReader.readElementText();
              qDebug() << m_qxmlStreamReader.name();
              /// <ms2_tendency_halfwindow>10</ms2_tendency_halfwindow>
              if(m_qxmlStreamReader.name().toString() ==
                 "ms2_tendency_halfwindow")
                {
                  p_ms2_meth->setMs2TendencyWindow(inside.toDouble());
                }

              /// <ms2_smoothing_halfwindow>5</ms2_smoothing_halfwindow>

              else if(m_qxmlStreamReader.name().toString() ==
                      "ms2_smoothing_halfwindow")
                {
                  p_ms2_meth->setMs2SmoothingWindow(inside.toDouble());
                }

              /// <ms1_smoothing_halfwindow>15</ms1_smoothing_halfwindow>
              else if(m_qxmlStreamReader.name().toString() ==
                      "ms1_smoothing_halfwindow")
                {
                  p_ms2_meth->setMs1SmoothingWindow(inside.toDouble());
                }
            }
        }

      catch(pappso::PappsoException &error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("error reading %1 tag :\n%2")
              .arg(m_qxmlStreamReader.name().toString())
              .arg(error.qwhat()));
          m_qxmlStreamReader.skipCurrentElement();
        }
      return p_ms2_meth;
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not an ms2 element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  return nullptr;
}

/// <align group_id="G1" method_id="obiwarp1">
void
MasschroqmlParser::read_align()
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "align")
    {
      QString group_id =
        m_qxmlStreamReader.attributes().value("group_id").toString();
      QString align_method_id =
        m_qxmlStreamReader.attributes().value("method_id").toString();
      QString ref_msrun_id =
        m_qxmlStreamReader.attributes().value("reference_data_id").toString();

      if(group_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the align tag must have a group_id attribute."));
        }
      if(align_method_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the align tag must have a method_id attribute."));
        }
      if(ref_msrun_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the align tag must have a ref_msrun_id attribute."));
        }
      /// launch alignment
      try
        {
          mp_massChroq->alignGroup(
            m_uiMonitor, group_id, align_method_id, ref_msrun_id);
        }

      catch(const pappso::PappsoException &e)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("ERROR aligning group %1 :\n %2")
              .arg(group_id)
              .arg(e.qwhat()));
        }
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not an align element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <quantification_method id="my_qzivy">
void
MasschroqmlParser::read_quantification_method()
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification_method")
    {
      QString quantification_id =
        m_qxmlStreamReader.attributes().value("id").toString();
      if(quantification_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the quantification_method tag must have an id attribute."));
        }

      QuantificationMethod *p_quantification_method =
        new QuantificationMethod(quantification_id);

      // xic_extraction
      m_qxmlStreamReader.readNextStartElement();
      if(m_qxmlStreamReader.name().toString() != "xic_extraction")
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("xic_extraction element expected here"));
        }
      else
        {
          QString xic_type =
            m_qxmlStreamReader.attributes().value("xic_type").toString();
          if(xic_type == "sum")
            {
              p_quantification_method->setXicExtractMethod(
                pappso::XicExtractMethod::sum);
            }
          else if(xic_type == "max")
            {
              p_quantification_method->setXicExtractMethod(
                pappso::XicExtractMethod::max);
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("the xic_extraction tag must have a xic_type "
                            "attribute whose value is 'sum' or 'max'."));
            }

          if(m_qxmlStreamReader.readNextStartElement())
            {
              // mz_range
              if(m_qxmlStreamReader.name().toString() == "mz_range")
                {
                  /// <xic_extraction>
                  /// <mz_range min="0.5" max="1.5"/>
                  QString min =
                    m_qxmlStreamReader.attributes().value("min").toString();
                  QString max =
                    m_qxmlStreamReader.attributes().value("max").toString();
                  if(min.isEmpty())
                    {
                      m_qxmlStreamReader.raiseError(QObject::tr(
                        "the mz_range tag must have a min attribute."));
                    }
                  if(max.isEmpty())
                    {
                      m_qxmlStreamReader.raiseError(QObject::tr(
                        "the mz_range tag must have a max attribute."));
                    }

                  p_quantification_method->setXicExtractPrecisionUnit(
                    pappso::PrecisionUnit::mz);
                  p_quantification_method->setXicExtractLowerPrecision(
                    min.toDouble());
                  p_quantification_method->setXicExtractUpperPrecision(
                    max.toDouble());
                  m_qxmlStreamReader.skipCurrentElement();
                }
              else if(m_qxmlStreamReader.name().toString() == "ppm_range")
                {
                  /// <xic_extraction>
                  /// <ppm_range min="" max=""/>

                  QString min =
                    m_qxmlStreamReader.attributes().value("min").toString();
                  QString max =
                    m_qxmlStreamReader.attributes().value("max").toString();
                  if(min.isEmpty())
                    {
                      m_qxmlStreamReader.raiseError(QObject::tr(
                        "the ppm_range tag must have a min attribute."));
                    }
                  if(max.isEmpty())
                    {
                      m_qxmlStreamReader.raiseError(QObject::tr(
                        "the ppm_range tag must have a max attribute."));
                    }

                  p_quantification_method->setXicExtractPrecisionUnit(
                    pappso::PrecisionUnit::ppm);
                  p_quantification_method->setXicExtractLowerPrecision(
                    min.toDouble());
                  p_quantification_method->setXicExtractUpperPrecision(
                    max.toDouble());
                  m_qxmlStreamReader.skipCurrentElement();
                }
              else
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("missing mz_range or ppm_range element"));
                }
            }

          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("missing mz_range or ppm_range element"));
            }

          if(m_qxmlStreamReader.readNextStartElement())
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("%1 element not allowed here")
                  .arg(m_qxmlStreamReader.name()));
            }

          qDebug() << m_qxmlStreamReader.name();
        }
      qDebug() << m_qxmlStreamReader.name();


      m_qxmlStreamReader.readNextStartElement();
      // xic_filters
      if(m_qxmlStreamReader.name().toString() == "xic_filters")
        {
          read_xic_filters(p_quantification_method);
          // peak_detection
          m_qxmlStreamReader.readNextStartElement();
        }

      qDebug() << m_qxmlStreamReader.name();
      read_peak_detection(p_quantification_method);

      qDebug() << m_qxmlStreamReader.name();
      mp_massChroq->addQuantificationMethod(p_quantification_method);
      //_p_current_quantification_method = NULL;
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.readNextStartElement())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("%1 element not allowed here")
              .arg(m_qxmlStreamReader.name()));
        }
      qDebug() << m_qxmlStreamReader.name();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a quantification_method element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::read_peak_detection(
  QuantificationMethod *p_quantification_method)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "peak_detection")
    {
      if(m_qxmlStreamReader.readNextStartElement())
        {

          if(m_qxmlStreamReader.name().toString() == "detection_zivy")
            {
              read_detection_zivy(p_quantification_method);
            }
          else if(m_qxmlStreamReader.name().toString() == "detection_moulon")
            {
              read_detection_moulon(p_quantification_method);
            }

          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr(
                "detection_zivy or detection_moulon element expected"));
              m_qxmlStreamReader.skipCurrentElement();
            }
        }
      else
        {

          m_qxmlStreamReader.raiseError(
            QObject::tr("missing detection_zivy or detection_moulon element"));
        }


      if(m_qxmlStreamReader.readNextStartElement())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("%1 element not allowed here")
              .arg(m_qxmlStreamReader.name()));
        }
      qDebug() << m_qxmlStreamReader.name();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a peak_detection element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::read_xic_filters(
  QuantificationMethod *p_quantification_method)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "xic_filters")
    {
      while(m_qxmlStreamReader.readNextStartElement())
        {
          read_xic_filter(p_quantification_method);
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a xic_filters element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::read_xic_filter(
  QuantificationMethod *p_quantification_method)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "anti_spike")
    {
      /// <anti_spike half="8">
      QString half = m_qxmlStreamReader.attributes().value("half").toString();
      if(half.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the anti_spike tag must have a half attribute."));
        }

      // pappso::FilterMorphoAntiSpike f_spike(half.toDouble());
      p_quantification_method->addFilter(
        std::make_shared<pappso::FilterMorphoAntiSpike>(half.toDouble()));
    }
  else if(m_qxmlStreamReader.name().toString() == "background")
    {

      //<background half_mediane="5" half_min_max="20">
      QString half_mediane =
        m_qxmlStreamReader.attributes().value("half_mediane").toString();
      QString half_min_max =
        m_qxmlStreamReader.attributes().value("half_min_max").toString();

      if(half_mediane.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the background tag must have a half_mediane attribute."));
        }
      if(half_min_max.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the background tag must have a half_min_max attribute."));
        }

      pappso::FilterMorphoBackground f_bkg(half_mediane.toDouble(),
                                           half_min_max.toDouble());
      p_quantification_method->addFilter(
        std::make_shared<pappso::FilterMorphoBackground>(f_bkg));
    }

  else if(m_qxmlStreamReader.name().toString() == "remove_intensity")
    {
      QString value = m_qxmlStreamReader.attributes().value("value").toString();

      if(value.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the remove_intensity tag must have a value attribute."));
        }

      pappso::FilterRemoveY f_remove(value.toDouble());
      p_quantification_method->addFilter(
        std::make_shared<pappso::FilterRemoveY>(f_remove));
    }

  else if(m_qxmlStreamReader.name().toString() == "remove_quantile")
    {
      QString value = m_qxmlStreamReader.attributes().value("value").toString();

      if(value.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the remove_quantile tag must have a value attribute."));
        }

      pappso::FilterQuantileBasedRemoveY f_remove(value.toDouble());
      p_quantification_method->addFilter(
        std::make_shared<pappso::FilterQuantileBasedRemoveY>(f_remove));
    }

  else if(m_qxmlStreamReader.name().toString() == "smoothing")
    {
      QString half = m_qxmlStreamReader.attributes().value("half").toString();
      if(half.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the smoothing tag must have a half attribute."));
        }

      pappso::FilterMorphoMean f_smooth(half.toDouble());

      p_quantification_method->addFilter(
        std::make_shared<pappso::FilterMorphoMean>(f_smooth));
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("filter element, one of smoothing, remove_quantile, "
                    "remove_intensity, background, anti_spike expected"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  m_qxmlStreamReader.skipCurrentElement();
}

/// <detection_zivy>
void
MasschroqmlParser::read_detection_zivy(
  QuantificationMethod *p_quantification_method)

{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "detection_zivy")
    {
      std::shared_ptr<pappso::TraceDetectionZivy> sp_detection_zivy =
        std::make_shared<pappso::TraceDetectionZivy>(0, 2, 3, 1000, 100);

      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "mean_filter_half_edge")
            {
              /// <detection_zivy>
              /// <mean_filter_half_edge>3</mean_filter_half_edge>
              sp_detection_zivy.get()->setFilterMorphoMean(
                pappso::FilterMorphoMean(
                  m_qxmlStreamReader.readElementText().toDouble()));
            }
          else if(m_qxmlStreamReader.name().toString() == "minmax_half_edge")
            {
              /// <detection_zivy>
              /// <minmax_half_edge>3</minmax_half_edge>
              sp_detection_zivy.get()->setFilterMorphoMinMax(
                pappso::FilterMorphoMinMax(
                  m_qxmlStreamReader.readElementText().toDouble()));
            }
          else if(m_qxmlStreamReader.name().toString() == "maxmin_half_edge")
            {

              /// <detection_zivy>
              /// <maxmin_half_edge>3</maxmin_half_edge>
              sp_detection_zivy.get()->setFilterMorphoMaxMin(
                pappso::FilterMorphoMaxMin(
                  m_qxmlStreamReader.readElementText().toDouble()));
            }

          else if(m_qxmlStreamReader.name().toString() ==
                  "detection_threshold_on_max")
            {

              /// <detection_zivy>
              /// <detection_threshold_on_max>5000</detection_threshold_on_max>
              sp_detection_zivy.get()->setDetectionThresholdOnMinmax(
                m_qxmlStreamReader.readElementText().toDouble());
            }

          else if(m_qxmlStreamReader.name().toString() ==
                  "detection_threshold_on_min")
            {
              /// <detection_zivy>
              /// <detection_threshold_on_min>3000</detection_threshold_on_min>
              sp_detection_zivy.get()->setDetectionThresholdOnMaxmin(
                m_qxmlStreamReader.readElementText().toDouble());
            }

          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("detection_zivy element has no %1 element")
                  .arg(m_qxmlStreamReader.name()));
            }
        }


      qDebug() << m_qxmlStreamReader.name();
      p_quantification_method->setDetectionMethod(sp_detection_zivy);
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a detection_zivy element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::read_detection_moulon(
  QuantificationMethod *p_quantification_method)
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "detection_moulon")
    {
      /// <detection_moulon>
      std::shared_ptr<pappso::TraceDetectionMoulon>
        sp_current_detection_moulon =
          std::make_shared<pappso::TraceDetectionMoulon>(0, 100, 10);

      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "TIC_stop")
            {
              /// <detection_moulon>
              /// <TIC_stop>4</TIC_stop>
              sp_current_detection_moulon->setTicStop(
                m_qxmlStreamReader.readElementText().toDouble());
            }
          else if(m_qxmlStreamReader.name().toString() == "TIC_start")
            {
              /// <detection_moulon>
              /// <TIC_start>4</TIC_start>
              sp_current_detection_moulon->setTicStart(
                m_qxmlStreamReader.readElementText().toDouble());
            }
          else if(m_qxmlStreamReader.name().toString() == "smoothing_point")
            {
              /// <detection_moulon>
              /// <smoothing_point>5</smoothing_point>
              sp_current_detection_moulon->setFilterMorphoMean(
                m_qxmlStreamReader.readElementText().toDouble());
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("detection_moulon element has no %1 element")
                  .arg(m_qxmlStreamReader.name()));
            }
        }


      p_quantification_method->setDetectionMethod(
        std::make_shared<const pappso::TraceDetectionMoulon>(
          *sp_current_detection_moulon));
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a detection_moulon element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


void
MasschroqmlParser::read_quantification_results(
  MonitorSpeedList &monitor_speed_list)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification_results")
    {

      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "quantification_result")
            {
              read_quantification_result(monitor_speed_list);
            }
          else if(m_qxmlStreamReader.name().toString() == "peak_shape")
            {
              read_peak_shape(monitor_speed_list);
            }
          else if(m_qxmlStreamReader.name().toString() == "compar_result")
            {
              read_compar_result(monitor_speed_list);
            }
          else if(m_qxmlStreamReader.name().toString() == "xic_coords")
            {
              read_xic_coords(monitor_speed_list);
            }
          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr("unexpected %1 element")
                                              .arg(m_qxmlStreamReader.name()));
            }
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a quantification_results element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}


void
MasschroqmlParser::read_quantification_traces()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification_traces")
    {
      /*
       *
            <peptide_traces peptide_ids="pep0 pep1" output_dir="pep_traces"
              format="tsv" />
            <all_xics_traces output_dir="all_xics_traces" format="tsv" />
            <mz_traces mz_values="634.635 449.754 552.234"
       output_dir="mz_traces" format="tsv" /> <mzrt_traces
       output_dir="mzrt_traces" format="tsv"> <mzrt_values> <mzrt_value
       mz="732.317" rt="230.712" /> <mzrt_value mz="575.256" rt="254.788" />
              </mzrt_values>
            </mzrt_traces>
            */
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "peptide_traces")
            {
              read_peptide_traces();
            }
          else if(m_qxmlStreamReader.name().toString() == "all_xics_traces")
            {
              read_all_xics_traces();
            }
          else if(m_qxmlStreamReader.name().toString() == "mz_traces")
            {
              read_mz_traces();
            }
          else if(m_qxmlStreamReader.name().toString() == "mzrt_traces")
            {
              read_mzrt_traces();
            }

          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr("unexpected %1 element")
                                              .arg(m_qxmlStreamReader.name()));
            }
        }

      // m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a quantification_traces element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}

void
MasschroqmlParser::read_quantification_result(
  MonitorSpeedList &monitor_speed_list)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantification_result")
    {
      QString output_file =
        m_qxmlStreamReader.attributes().value("output_file").toString();
      if(output_file.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the quantification_result tag must have an output_file "
            "attribute."));
        }

      if(!ensureFilePathExists(output_file))
        {
          QDir file_dir(output_file);
          m_qxmlStreamReader.raiseError(
            QObject::tr("problem writing output to directory %1\n Please "
                        "check output file path and permissions")
              .arg(file_dir.absolutePath()));
        }

      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the quantification_result tag must have a format attribute."));
        }

      bool with_traces;
      QString xic_traces =
        m_qxmlStreamReader.attributes().value("xic_traces").toString();
      if(xic_traces == "true")
        {
          with_traces = true;
        }
      else
        {
          with_traces = false;
        }
      try
        {
          if(format == "gnumeric")
            {
              // return startGnumericResult(output_file);
            }
          else if(format == "xhtmltable")
            {
              // return startXhtmltableResult(output_file);
            }
          else if(format == "ods")
            {
              output_file = ensureFileExtension(output_file, "ods");
              MonitorSpeedInterfaceSp sp_monitor_speed =
                std::make_shared<MonitorOdsOutput>(output_file);
              monitor_speed_list.addMonitor(sp_monitor_speed);
            }
          else if(format == "tsv")
            {
              output_file = ensureFileExtension(output_file, "d");
              MonitorSpeedInterfaceSp sp_monitor_speed =
                std::make_shared<MonitorTsvOutput>(output_file);
              monitor_speed_list.addMonitor(sp_monitor_speed);
            }
          else if(format == "rdata")
            {

              m_qxmlStreamReader.raiseError(
                QObject::tr("output format 'rdata' is not available"));
            }
          else if(format == "masschroqml")
            {
              startXmlResult(monitor_speed_list, output_file, with_traces);
            }
          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr(
                "value '%1' of attribute format in quantification_result tag "
                "is "
                "not "
                "supported.\n Supported values are : ods, tsv and "
                "masschroqml."));
            }
        }
      catch(OdsException &ods_error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("error creating result output file :\n%2")
              .arg(ods_error.qwhat()));
        }
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a quantification_result element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug() << m_qxmlStreamReader.name();
}


bool
MasschroqmlParser::ensureFilePathExists(const QString &output_file)
{
  QFileInfo complete_path(output_file);
  //  qDebug()<< "MasschroqmlParser::ensureFilePathExists complete_path.dir()"
  //  << complete_path.dir().absolutePath() ;
  // qDebug() << QFileInfo("~/examples/191697/main.cpp").dir().absolutePath();

  // qDebug() << QFileInfo("~/examples/191697/main").dir().absolutePath();
  QDir only_dir(complete_path.dir().absolutePath());
  if(only_dir.exists())
    {
      return true;
    }
  else
    {
      // try to create it
      if(only_dir.mkpath(only_dir.absolutePath()) == false)
        {
          return false;
          qDebug() << "MasschroqmlParser::ensureFilePathExists FALSE "
                   << only_dir.absolutePath();
        }
      else
        {
          return true;
        }
    }
  return false;
}


QString
MasschroqmlParser::ensureFileExtension(const QString &output_file,
                                       const QString &file_extension)
{
  QFileInfo filename(output_file);
  if(filename.suffix().isEmpty())
    {
      return QString(output_file + "." + file_extension);
    }
  return output_file;
}


void
MasschroqmlParser::startXmlResult(MonitorSpeedList &monitor_speed_list,
                                  const QString &output_file,
                                  const bool with_traces)
{

  QString mcq_file = ensureFileExtension(output_file, "masschroqML");
  MonitorSpeedInterfaceSp sp_results;
  QString input_file = mp_massChroq->getXmlFilename();
  if(input_file.isEmpty())
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("problem creating masschroqML output results :\n; "
                    "cannot find input masschroqML file"));
    }
  try
    {
      sp_results =
        std::make_shared<MasschroqWriter>(input_file, mcq_file, with_traces);
    }
  catch(mcqError &error)
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("problem creating xml output results :\n%1")
          .arg(error.what()));
    }
  monitor_speed_list.addMonitor(sp_results);
}


void
MasschroqmlParser::read_peak_shape(MonitorSpeedList &monitor_speed_list)
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "peak_shape")
    {

      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the peak_shape tag must have an output_dir attribute."));
        }

      qDebug();
      QDir shape_dir(output_dir);
      if(!shape_dir.mkpath(shape_dir.absolutePath()))
        {
          throw mcqError(
            QObject::tr("mkpath %1 failed ").arg(shape_dir.absolutePath()));
        }


      MonitorSpeedInterfaceSp sp_monitor_speed =
        std::make_shared<MonitorPeakShapeOutput>(shape_dir);
      monitor_speed_list.addMonitor(sp_monitor_speed);
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a peak_shape element"));
      m_qxmlStreamReader.skipCurrentElement();
    }

  m_qxmlStreamReader.skipCurrentElement();
}


void
MasschroqmlParser::read_compar_result(MonitorSpeedList &monitor_speed_list)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "compar_result")
    {

      QString output_file =
        m_qxmlStreamReader.attributes().value("output_file").toString();
      if(output_file.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the compar_result tag must have an output_file attribute."));
        }

      if(!ensureFilePathExists(output_file))
        {
          QDir file_dir(output_file);
          m_qxmlStreamReader.raiseError(
            QObject::tr("problem writing output to directory %1\n Please "
                        "check output file path and permissions")
              .arg(file_dir.absolutePath()));
        }
      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the compar_result tag must have a format attribute."));
        }

      try
        {
          if(format == "ods")
            {
              output_file = ensureFileExtension(output_file, "ods");
              MonitorSpeedInterfaceSp sp_monitor_speed =
                std::make_shared<MonitorComparOdsOutput>(output_file);
              monitor_speed_list.addMonitor(sp_monitor_speed);
            }
          else if(format == "tsv")
            {
              output_file = ensureFileExtension(output_file, "d");
              MonitorSpeedInterfaceSp sp_monitor_speed =
                std::make_shared<MonitorComparTsvOutput>(output_file);
              monitor_speed_list.addMonitor(sp_monitor_speed);
            }
          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr(
                "value '%1' of attribute format in compar_result tag is "
                "not supported.\n Supported values are : ods, tsv."));
            }
        }
      catch(OdsException &ods_error)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("error creating compar output file :\n%2")
              .arg(ods_error.qwhat()));
        }
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a compar_result element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <all_xics_traces output_dir="xics_traces" format="tsv"/>
void
MasschroqmlParser::read_all_xics_traces()
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "all_xics_traces")
    {
      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the all_xics_traces tag must have an output_dir attribute."));
        }
      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty())
        {
          format = "ods";
        }
      if(format != "tsv" && format != "ods")
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the value '%1' of the format attribute in "
                        "all_xics_traces tag is not supported.\n Supported "
                        "formats are : 'ods'(default) and 'tsv'.")
              .arg(format));
        }
      if(format == "ods")
        {
          mp_massChroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::ods);
        }
      else
        {
          mp_massChroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::tsv);
        }

      mp_massChroq->allPeptideTrace();
      // TsvAllXics * p_results = new TsvAllXics(output_dir);
      //_p_current_monitor_list->addMonitor(p_results);
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a all_xics_traces element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <peptide_traces peptide_ids="pep0 pep6" output_dir="bla" format="tsv"/>
void
MasschroqmlParser::read_peptide_traces()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "peptide_traces")
    {
      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the peptides_xics tag must have an output_dir attribute."));
        }
      QString peptides =
        m_qxmlStreamReader.attributes().value("peptide_ids").toString();
      if(peptides.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the peptides_xics tag must have a peptide_ids attribute."));
        }

      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty())
        {
          format = "ods";
        }
      if(format != "tsv" && format != "ods")
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the value '%1' of the format attribute in "
                        "peptide_traces tag is not supported.\n Supported "
                        "formats are : 'ods'(default) and 'tsv'.")
              .arg(format));
        }
      if(format == "ods")
        {
          mp_massChroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::ods);
        }
      else
        {
          mp_massChroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::tsv);
        }
      QStringList list_peptide_ids = peptides.split(" ", Qt::SkipEmptyParts);
      foreach(const QString &peptide_id, list_peptide_ids)
        {
          PeptideCstSPtr peptide = mp_massChroq->getPeptide(peptide_id);
          if(peptide == nullptr)
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("the peptide '%1' is not defined.")
                  .arg(peptide_id));
            }
          mp_massChroq->addPeptideTrace(peptide);
        }

      // TsvPepListXics * p_results = new TsvPepListXics(output_dir);
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a peptide_traces element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <mz_traces mz_values="6 4" output_dir="mz_xics_traces" format="tsv"/>
void
MasschroqmlParser::read_mz_traces()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mz_traces")
    {
      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the mz_traces tag must have an output_dir attribute."));
        }
      QString mz_values =
        m_qxmlStreamReader.attributes().value("mz_values").toString();
      if(mz_values.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the mz_traces tag must have a mz_values attribute."));
        }
      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty() || format != "tsv")
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the value '%1' of the format attribute in "
                        "mz_traces tag is not supported.\n Supported "
                        "formats are : 'tsv'(default)."));
        }
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a mz_traces element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


/// <mzrt_traces output_dir="mzrt_xics_traces" format="tsv"/>
void
MasschroqmlParser::read_mzrt_traces()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mzrt_traces")
    {
      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the mzrt_traces tag must have an output_dir attribute."));
        }

      QString format =
        m_qxmlStreamReader.attributes().value("format").toString();
      if(format.isEmpty() || format != "tsv")
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the value '%1' of the format attribute in "
                        "mzrt_traces tag is not supported.\n Supported "
                        "formats are : 'tsv'(default)."));
        }

      //<mzrt_values>
      m_qxmlStreamReader.readNextStartElement();
      if(m_qxmlStreamReader.name().toString() == "mzrt_values")
        {
          while(m_qxmlStreamReader.readNextStartElement())
            {
              read_mzrt_value();
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an mzrt_values element"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not an mzrt_traces element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


void
MasschroqmlParser::read_mzrt_value()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mzrt_value")
    {
      QString mz = m_qxmlStreamReader.attributes().value("mz").toString();
      QString rt = m_qxmlStreamReader.attributes().value("rt").toString();
      if(mz.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the mzrt_value tag must have a mz attribute."));
        }
      if(rt.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the mzrt_value tag must have a rt attribute."));
        }
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not an mzrt_value element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


void
MasschroqmlParser::read_quantify()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "quantify")
    {
      QString quantify_id =
        m_qxmlStreamReader.attributes().value("id").toString();
      /// <quantify id="q1" withingroup="G1" quantification_method_id="q1"
      /// compute_isotope_threshold="0.00001"> selection of masses to extract
      /// xic, detect peaks, quantify in this group

      QString quanti_group_id =
        m_qxmlStreamReader.attributes().value("withingroup").toString();
      QString compute_isotope_threshold = m_qxmlStreamReader.attributes()
                                            .value("compute_isotope_threshold")
                                            .toString();
      QString quanti_method_id = m_qxmlStreamReader.attributes()
                                   .value("quantification_method_id")
                                   .toString();

      // im_fix="false"
      bool use_im_grid   = true;
      QStringView im_fix = m_qxmlStreamReader.attributes().value("im_fix");
      if(!im_fix.isNull())
        {
          if(im_fix == QString("false"))
            {
              use_im_grid = false;
            }
        }


      if(quantify_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the quantify tag must have a non empty id attribute."));
        }

      if(quanti_group_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the quantify tag must have a withingroup attribute "
                        "containing a valid reference on a group."));
        }

      if(quanti_method_id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the quantify tag must have a withingroup attribute "
                        "containing a valid reference on a group."));
        }

      QuantificationMethod *quanti_method =
        mp_massChroq->findQuantificationMethod(quanti_method_id);
      if(quanti_method == NULL)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("cannot find quantification method with id '%1'")
              .arg(quanti_method_id));
        }

      const msRunHashGroup *quanti_group =
        mp_massChroq->findGroup(quanti_group_id);

      if(quanti_group == NULL)
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("cannot find group with id '%1'").arg(quanti_group_id));
        }

      Quantificator *current_quantificator =
        new Quantificator(quantify_id, quanti_group, quanti_method);
      if(!compute_isotope_threshold.isEmpty())
        {
          current_quantificator
            ->setPeptideNaturalIsotopeListMinimumRatioToCompute(
              compute_isotope_threshold.toDouble());
        }
      mp_massChroq->addQuantificator(current_quantificator);
      /*
      <peptides_in_peptide_list mode="post_matching"
        isotope_label_refs="iso1 iso2" />
      <mz_list>732.317 449.754 552.234 464.251 381.577 569.771 575.256</mz_list>
      <mzrt_list>
        <mzrt mz="732.317" rt="230.712" />
        <mzrt mz="575.256" rt="254.788" />
      </mzrt_list>
      */

      while(m_qxmlStreamReader.readNextStartElement())
        {
          if(m_qxmlStreamReader.name().toString() == "peptides_in_peptide_list")
            {
              read_peptides_in_peptide_list(quantify_id, current_quantificator);
            }
          else if(m_qxmlStreamReader.name().toString() == "mz_list")
            {
              read_mz_list(quantify_id);
            }
          else if(m_qxmlStreamReader.name().toString() == "mzrt_list")
            {
              read_mzrt_list(quantify_id);
            }

          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr("unexpected %1 element")
                                              .arg(m_qxmlStreamReader.name()));
            }
        }


      // time to launch quantification in the group :
      if(m_hasIonMobility && use_im_grid)
        {

          m_uiMonitor.setTitle("using ion mobility grid");
          mp_massChroq->executeQuantification(
            m_uiMonitor, &m_ionMobilityGrid, quantify_id);
        }
      else
        {
          // no ion mobility correction
          m_uiMonitor.setTitle("no ion mobility grid");
          mp_massChroq->executeQuantification(
            m_uiMonitor, nullptr, quantify_id);
        }

      mp_massChroq->deleteQuantificator(quantify_id);
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a quantify element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}


// <peptides_in_peptide_list mode="mean">
void
MasschroqmlParser::read_peptides_in_peptide_list(const QString &quantifiy_id,
                                                 Quantificator *p_quantificator)
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "peptides_in_peptide_list")
    {
      QString mode = m_qxmlStreamReader.attributes().value("mode").toString();
      if(mode.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the peptides_in_peptide_list tag must have a mode attribute."));
        }
      else if(mode == "realxic_or_mean")
        { // realxic_or_mean is deprecated since 2.4.1, replaced by real_or_mean
          p_quantificator->setMatchingMode(McqMatchingMode::real_or_mean);
        }
      else if(mode == "mean")
        {
          // mean is deprecated since 2.4.1, not replaced

          m_qxmlStreamReader.raiseError(QObject::tr(
            "in peptides_in_peptide_list tag the mode attribute must "
            "be 'post_matching', 'real_or_mean' or 'no_matching'. 'mean' is "
            "deprecated since version 2.4.1"));
        }
      else if(mode == "real_or_mean")
        {
          p_quantificator->setMatchingMode(McqMatchingMode::real_or_mean);
        }
      else if(mode == "post_matching")
        {
          p_quantificator->setMatchingMode(McqMatchingMode::post_matching);
        }
      else if(mode == "no_matching")
        {
          p_quantificator->setMatchingMode(McqMatchingMode::no_matching);
        }
      else
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "in peptides_in_peptide_list tag the mode attribute must "
            "be 'post_matching', 'real_or_mean' or 'no_matching'."));
        }

      /// get the list of the isotopes
      QStringList list_isotope_refs;
      QString isotope_label_refs =
        m_qxmlStreamReader.attributes().value("isotope_label_refs").toString();
      if(isotope_label_refs.isEmpty())
        {
        }
      else
        {
          list_isotope_refs = isotope_label_refs.split(" ", Qt::SkipEmptyParts);
        }

      mcq_double ni_min_abundance = 0;
      if(m_qxmlStreamReader.attributes().value("ni_min_abundance").isEmpty())
        {
        }
      else
        {
          ni_min_abundance = m_qxmlStreamReader.attributes()
                               .value("ni_min_abundance")
                               .toDouble();
        }

      /********* to put in the quantificator
       * *************************************/
      mp_massChroq->addQuantificatorPeptideItems(
        m_uiMonitor, quantifiy_id, list_isotope_refs, ni_min_abundance);

      /********** end to put in the quantificator *******/
      m_qxmlStreamReader.skipCurrentElement();
    }

  else
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("Not a peptides_in_peptide_list element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

/// <mzrt mz="354.45" rt="7549.25"/>
void
MasschroqmlParser::read_mzrt_list(const QString &quantifiy_id)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mzrt_list")
    {
      while(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name().toString() == "mzrt")
            {
              QString mz =
                m_qxmlStreamReader.attributes().value("mz").toString();
              QString rt =
                m_qxmlStreamReader.attributes().value("rt").toString();
              if(mz.isEmpty())
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("the mzrt tag must have an mz attribute."));
                }
              if(rt.isEmpty())
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("the mzrt tag must have an rt attribute."));
                }

              mp_massChroq->addQuantificatorMzrtItem(quantifiy_id, mz, rt);
            }

          else
            {
              m_qxmlStreamReader.raiseError(QObject::tr("Not a mzrt element"));
              m_qxmlStreamReader.skipCurrentElement();
            }
        }
    }
  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a mzrt_list element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

/// <mz_list>45.154 56.15465 0354.45</mz_list>
void
MasschroqmlParser::read_mz_list(const QString &quantifiy_id)
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "mz_list")
    {
      QStringList str_list_mass =
        m_qxmlStreamReader.readElementText().split(" ", Qt::SkipEmptyParts);

      mp_massChroq->addQuantificatorMzItems(quantifiy_id, str_list_mass);
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a mz_list element"));
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
MasschroqmlParser::read_xic_coords(MonitorSpeedList &monitor_speed_list)
{

  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name().toString() == "xic_coords")
    {

      QString output_dir =
        m_qxmlStreamReader.attributes().value("output_dir").toString();
      if(output_dir.isEmpty())
        {
          m_qxmlStreamReader.raiseError(QObject::tr(
            "the xic_coords tag must have an output_dir attribute."));
        }

      qDebug();
      QDir shape_dir(output_dir);
      if(!shape_dir.mkpath(shape_dir.absolutePath()))
        {
          throw mcqError(
            QObject::tr("mkpath %1 failed ").arg(shape_dir.absolutePath()));
        }


      MonitorSpeedInterfaceSp sp_monitor_speed =
        std::make_shared<MonitorXicCoordOutput>(shape_dir);
      monitor_speed_list.addMonitor(sp_monitor_speed);
    }

  else
    {
      m_qxmlStreamReader.raiseError(QObject::tr("Not a xic_coords element"));
      m_qxmlStreamReader.skipCurrentElement();
    }

  m_qxmlStreamReader.skipCurrentElement();
}
