/**
 * \file src/input/masschroqmlparser.h
 * \date 8/7/2022
 * \author Olivier Langella
 * \brief read MassChroQml file to perform quantification
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <QFileInfo>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>
#include "../lib/alignments/alignment_base.h"
#include "../lib/mass_chroq.h"


/**
 * @todo write docs
 */
class MasschroqmlParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  MasschroqmlParser(pappso::UiMonitorInterface &ui_monitor,
                    MassChroq *p_my_chroq);

  /**
   * Destructor
   */
  virtual ~MasschroqmlParser();


  virtual bool readFile(const QString &fileName) override;

  protected:
  virtual void readStream() override;


  private:
  void readRawdata();
  void endElement_rawdata();
  void readGroups();
  void read_data_file();
  void read_group();
  void readProteinList();
  void read_protein();
  void readPeptideList();
  void read_peptide();
  void read_psimod(pappso::NoConstPeptideSp pappso_peptide_sp);
  void read_observed_in(std::shared_ptr<Peptide> &peptide_sp);
  void readIsotopeLabelList();
  void read_isotope_label();
  void read_mod(IsotopeLabel *p_isotope_label);
  void readAlignments();
  void read_alignment_method();
  AlignmentBase *read_ms2();
  void read_align();
  void readQuantificationMethods();
  void read_quantification_method();
  void read_xic_filters(QuantificationMethod *p_quantification_method);
  void read_xic_filter(QuantificationMethod *p_quantification_method);
  void read_peak_detection(QuantificationMethod *p_quantification_method);
  void read_detection_zivy(QuantificationMethod *p_quantification_method);
  void read_detection_moulon(QuantificationMethod *p_quantification_method);
  void readQuantification();

  void read_quantification_results(MonitorSpeedList &monitor_speed_list);
  void read_quantification_result(MonitorSpeedList &monitor_speed_list);
  bool ensureFilePathExists(const QString &output_file);
  QString ensureFileExtension(const QString &output_file,
                              const QString &file_extension);


  void startXmlResult(MonitorSpeedList &monitor_speed_list,
                      const QString &output_file,
                      const bool with_traces);


  void read_peak_shape(MonitorSpeedList &monitor_speed_list);
  void read_compar_result(MonitorSpeedList &monitor_speed_list);
  void read_xic_coords(MonitorSpeedList &monitor_speed_list);
  void read_quantification_traces();
  void read_all_xics_traces();
  void read_peptide_traces();
  void read_mz_traces();
  void read_mzrt_traces();
  void read_mzrt_value();
  void read_quantify();
  void read_peptides_in_peptide_list(const QString &quantifiy_id,
                                     Quantificator *p_quantificator);
  void read_mz_list(const QString &quantifiy_id);
  void read_mzrt_list(const QString &quantifiy_id);

  void populateIonMobilityGridWithObservedPeptideByCharges(
    std::vector<PeptideObservedIn *> all_obser, unsigned int charge);

  private:
  pappso::UiMonitorInterface &m_uiMonitor;
  std::vector<std::pair<QString, QFileInfo>> m_msfileList;
  bool m_isTimeValues = false;
  QString m_timeDirectory;

  MassChroq *mp_massChroq = nullptr;

  bool m_hasIonMobility = false;
  pappso::IonMobilityGrid m_ionMobilityGrid;
};
