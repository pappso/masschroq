/**
 * \file src/input/masschroqmlpeptideparser.cpp
 * \date 6/12/2021
 * \author Olivier Langella
 * \brief read MassChroQml file as input and translated petide tsv files to XML
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "masschroqmlpeptideparser.h"
#include <QFileInfo>
#include <pappsomspp/pappsoexception.h>
#include <QDir>
#include <QFileInfo>


MasschroqmlPeptideParser::MasschroqmlPeptideParser(const QString &output)
  : m_masschroqmlOutputFile(output)
{

  qDebug() << output;
  if(!m_masschroqmlOutputFile.open(QIODevice::WriteOnly))
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: unable to open %1 to write XML output")
          .arg(output));
    }
  m_writerMasschroqmlOutput.setDevice(&m_masschroqmlOutputFile);
  m_writerMasschroqmlOutput.writeStartDocument("1.0");
  m_writerMasschroqmlOutput.setAutoFormatting(true);
}

MasschroqmlPeptideParser::~MasschroqmlPeptideParser()
{
}

bool
MasschroqmlPeptideParser::readFile(const QString &fileName)
{
  QDir::setCurrent(QFileInfo(fileName).absolutePath());
  return pappso::XmlStreamReaderInterface::readFile(fileName);
}

void
MasschroqmlPeptideParser::readStream()
{
  qDebug();
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name().toString() == "masschroq")
        {
          // cloneElement(m_writerMasschroqmlOutput);


          cloneStartElement(m_writerMasschroqmlOutput);
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {

              qDebug() << m_qxmlStreamReader.name();

              if(m_qxmlStreamReader.name().toString() == "peptide_files_list")
                {
                  qDebug() << m_qxmlStreamReader.name();

                  while(m_qxmlStreamReader.readNextStartElement())
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      // read_note();
                      // cloneStartElement(m_writerMasschroqmlOutput);

                      //          <peptide_files_list>
                      //      <peptide_file data="samp0"
                      //      path="peptides_BSA_Orbitrap.txt"/>
                      //    </peptide_files_list>

                      // m_writerMasschroqmlOutput.writeEndElement();

                      m_sampleId = m_qxmlStreamReader.attributes()
                                     .value("data")
                                     .toString();
                      QString path = m_qxmlStreamReader.attributes()
                                       .value("path")
                                       .toString();

                      if(m_sampleId.isEmpty())
                        {
                          m_qxmlStreamReader.raiseError(QObject::tr(
                            "the "
                            "peptide_file tag must have a data attribute."));
                        }

                      if(path.isEmpty())
                        {
                          m_qxmlStreamReader.raiseError(QObject::tr(
                            "the "
                            "peptide_file tag must have a path attribute."));
                        }

                      try
                        {
                          PepParser2 peptide_parser(path);
                          peptide_parser.parse(this);
                        }
                      catch(pappso::PappsoException &error)
                        {
                          m_qxmlStreamReader.raiseError(
                            QObject::tr("error reading TSV file %1:\n%2")
                              .arg(QFileInfo(path).absoluteFilePath())
                              .arg(error.qwhat()));
                        }

                      m_qxmlStreamReader.skipCurrentElement();
                    }
                  // m_qxmlStreamReader.skipCurrentElement();
                  writeProteinList();
                }
              else
                {
                  qDebug() << m_qxmlStreamReader.name();
                  cloneElement(m_writerMasschroqmlOutput);
                }
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(QObject::tr("Not a MassChroqML file"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }
  m_writerMasschroqmlOutput.writeEndDocument();
  m_masschroqmlOutputFile.close();
  qDebug();
}

void
MasschroqmlPeptideParser::updateMaps(const QString &sequence,
                                     const QString &mh,
                                     const QString &prot_desc,
                                     const std::size_t scan,
                                     const int z,
                                     const QString &mods)
{

  peptideObservedInStruct observation;
  observation.data = m_sampleId;
  observation.scan = scan;
  observation.z    = z;

  // update _proteins_map : if prot_desc is a new protein add it in the
  // map and generate a new id for this protein, else find the id of this
  // in the map

  std::map<const QString, const QString>::const_iterator itprot =
    m_proteinsMap.find(prot_desc);
  QString protein_id;
  if(itprot == m_proteinsMap.end())
    {
      protein_id = QString("P%1").arg(m_proteinsMap.size());
      m_proteinsMap.insert(m_proteinsMap.begin(),
                           std::pair<QString, QString>(prot_desc, protein_id));
    }
  else
    {
      protein_id = itprot->second;
    }


  // update _peptides_map : if new peptide add a new QDomElement, else :
  // update the existing one

  const PepId cur_pep = std::pair<const QString, const QString>(sequence, mh);

  std::map<const PepId, peptideStruct>::iterator itpep =
    m_peptidesMap.find(cur_pep);

  // if it is a new peptide
  if(itpep == m_peptidesMap.end())
    {

      peptideStruct new_pep;
      new_pep.prot_ids << protein_id;
      new_pep.mh  = mh;
      new_pep.seq = sequence;
      new_pep.id  = QString("pep%1").arg(m_peptidesMap.size());
      ;
      if(!mods.isEmpty())
        {
          new_pep.mods = mods;
        }

      new_pep.observations.push_back(observation);
      /*
            QDomElement new_obs = _domDoc.createElement("observed_in");
            new_obs.setAttribute("data", _current_data);
            new_obs.setAttribute("z", z);
            new_obs.setAttribute("scan", scan);

            new_pep.appendChild(new_obs);
      */
      m_peptidesMap[cur_pep] = new_pep;
    }
  else
    { // if the peptide already exists, update domchildelements +
      // domelement, scan, z, prots and mods
      //     peptide_pair.second.prot_ids.removeDuplicates()

      itpep->second.prot_ids << protein_id;
      itpep->second.prot_ids.removeDuplicates();
      itpep->second.observations.push_back(observation);
    }
}

void
MasschroqmlPeptideParser::writeProteinList()
{

  //<protein_list>
  //	<protein desc="conta|P02769|ALBU_BOVIN SERUM ALBUMIN PRECURSOR."
  //		id="P1.1" />
  //	<protein desc="conta|P02770|ALBU_RAT SERUM ALBUMIN PRECURSOR."
  //		id="P1.2" />
  //</protein_list>
  m_writerMasschroqmlOutput.writeStartElement("protein_list");

  for(auto &protein_pair : m_proteinsMap)
    {
      m_writerMasschroqmlOutput.writeStartElement("protein");
      m_writerMasschroqmlOutput.writeAttribute("id", protein_pair.second);
      m_writerMasschroqmlOutput.writeAttribute("desc", protein_pair.first);
      m_writerMasschroqmlOutput.writeEndElement();
    }
  m_writerMasschroqmlOutput.writeEndElement();


  m_writerMasschroqmlOutput.writeStartElement("peptide_list");
  for(auto &peptide_pair : m_peptidesMap)
    {
      //<peptide id="pep0" mh="1463.626" mods="114.08" prot_ids="P1.1"
      //	seq="TCVADESHAGCEK">
      //<observed_in data="samp0" scan="655" z="2" />
      m_writerMasschroqmlOutput.writeStartElement("peptide");
      m_writerMasschroqmlOutput.writeAttribute("id", peptide_pair.second.id);
      m_writerMasschroqmlOutput.writeAttribute("mh", peptide_pair.second.mh);
      m_writerMasschroqmlOutput.writeAttribute(
        "prot_ids", peptide_pair.second.prot_ids.join(" "));
      if(!peptide_pair.second.mods.isEmpty())
        {
          m_writerMasschroqmlOutput.writeAttribute("mods",
                                                   peptide_pair.second.mods);
        }
      m_writerMasschroqmlOutput.writeAttribute("seq", peptide_pair.second.seq);
      for(auto &observation : peptide_pair.second.observations)
        {
          m_writerMasschroqmlOutput.writeStartElement("observed_in");
          m_writerMasschroqmlOutput.writeAttribute("data", observation.data);
          m_writerMasschroqmlOutput.writeAttribute(
            "scan", QString("%1").arg(observation.scan));
          m_writerMasschroqmlOutput.writeAttribute(
            "z", QString("%1").arg(observation.z));
          m_writerMasschroqmlOutput.writeEndElement();
        }
      m_writerMasschroqmlOutput.writeEndElement();
    }
  m_writerMasschroqmlOutput.writeEndElement();
}
