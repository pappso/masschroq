/**
 * \file src/input/masschroqmlpeptideparser.h
 * \date 6/12/2021
 * \author Olivier Langella
 * \brief read MassChroQml file as input and translated petide tsv files to XML
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "pepParser.h"


/**
 * @todo write docs
 */
class MasschroqmlPeptideParser : public pappso::XmlStreamReaderInterface
{
  friend PepParser2;

  public:
  /**
   * Default constructor
   */
  MasschroqmlPeptideParser(const QString &output);

  /**
   * Destructor
   */
  virtual ~MasschroqmlPeptideParser();

  virtual bool readFile(const QString &fileName) override;

  protected:
  virtual void readStream() override;


  /// method called by the peptide text parsers to put peptides
  /// and proteins in the member hash maps
  void updateMaps(const QString &sequence,
                  const QString &mh,
                  const QString &prot_desc,
                  const std::size_t scan,
                  const int z,
                  const QString &mods);


  private:
  void writeProteinList();

  private:
  struct peptideObservedInStruct
  {
    QString data;
    std::size_t scan;
    int z;
  };

  /// type corresponding to a unique peptide identification :
  /// pair of an amino-acid sequence (I translated to L) and an mh value.
  typedef std::pair<const QString, const QString> PepId;

  struct peptideStruct
  {
    QString id;
    QStringList prot_ids;
    QString mh;
    QString mods;
    QString seq;
    std::vector<peptideObservedInStruct> observations;
  };

  QFile m_masschroqmlOutputFile;
  QXmlStreamWriter m_writerMasschroqmlOutput;

  QString m_sampleId;

  /// map where each peptide and its data is stocked
  /// with peptide unicity : two peptides are identified if
  /// they have the same sequence (where I is translated to L)
  /// and the same mh, i.e. the same PepId.
  std::map<const PepId, peptideStruct> m_peptidesMap;

  /// map protein description -> unique protein xml id
  std::map<const QString, const QString> m_proteinsMap;
};
