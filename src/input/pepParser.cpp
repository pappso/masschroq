/**
 * \file pepParser.cpp
 * \date December 03, 2010
 * \author Edlira Nano
 */

#include "pepParser.h"
#include "../lib/share/utilities.h"
#include "masschroqmlpeptideparser.h"
#include <pappsomspp/pappsoexception.h>

#include <QDebug>
#include <QStringList>
#include <iostream>
#include <QRegularExpression>

PepParser2::PepParser2(const QString &filename)
{

  _pep_file = new QFile(filename);
  if(!_pep_file->exists())
    {
      throw pappso::PappsoException(
        QObject::tr("error : cannot find the peptide text input file : '%1' \n")
          .arg(filename));
    }

  if(!_pep_file->open(QIODevice::ReadOnly))
    {
      throw pappso::PappsoException(
        QObject::tr("error : cannot read the peptide text input file : '%1' \n")
          .arg(filename));
    }

  _pep_txt_stream = new QTextStream(_pep_file);
  _pep_txt_stream->setRealNumberNotation(QTextStream::ScientificNotation);
#if QT_VERSION >= QT_V_4_5
  _pep_txt_stream->setLocale(QLocale::c());
#endif
  std::cout << "Parsing peptide text file '" << filename.toStdString() << "'"
            << std::endl;
}

PepParser2::~PepParser2()
{
  if(_pep_txt_stream != 0)
    {
      delete _pep_txt_stream;
      _pep_txt_stream = 0;
    }

  if(_pep_file != 0)
    {
      delete _pep_file;
      _pep_file = 0;
    }
}

void
PepParser2::parse(MasschroqmlPeptideParser *masschroq_dom)
{
  // initialize the line counter
  _line_number = 0;

  // get the header and use it to determine the columns separator
  while(!_pep_txt_stream->atEnd())
    {
      QString line = _pep_txt_stream->readLine();
      ++_line_number;

      if(isHeader(line))
        {
          _sep = getSeparator(line);
          break;
        }
    }

  if(_pep_txt_stream->atEnd())
    {
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 : the header is absent or "
                    "incorrectly set.\nA valid header must contain the first 5 "
                    "mandatory columns and an optional 6th column ordered as "
                    "follows "
                    ":\nscan[sep]sequence[sep]mh[sep]z[sep]proteins[sep]mods "
                    "\n where [sep] is the separator choosen among tabulation, "
                    "comma or semicolon and mods is optional.\n")
          .arg(_pep_file->fileName()));
    }

  if(_sep.isEmpty())
    {
      QString ln;
      ln.setNum(_line_number);
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 :\n in line %2 cannot determine "
                    "the column separator. Valid separators are tabulation, "
                    "comma or semicolon. The same separator has to be used in "
                    "a file, do not mix them.\n")
          .arg(_pep_file->fileName(), ln));
    }

  while(!_pep_txt_stream->atEnd())
    {
      QString line = _pep_txt_stream->readLine();
      ++_line_number;
      processLine(line);
      // send back current protein and peptide info to the dom engine
      masschroq_dom->updateMaps(_current_sequence,
                                _current_mh,
                                _current_prot_desc,
                                _current_scan,
                                _current_z,
                                _current_mods);
    }
}

void
PepParser2::processLine(QString &line)
{
  if(line.isEmpty())
    return;

  QStringList token_list = line.split(_sep, Qt::SkipEmptyParts);
  int count              = token_list.count();
  if(count < 5 || count > 6)
    {
      QString ln;
      ln.setNum(_line_number);
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 :\n in line %2, wrong number of "
                    "columns (each line must contain 5 mandatory columns) or "
                    "another separator than the choosen one ('%3') is used.\n")
          .arg(_pep_file->fileName(), ln, whatSeparator()));
    };

  _current_sequence.clear();
  _current_prot_desc.clear();
  _current_mods.clear();
  _current_mh.clear();
  _current_scan = 0;
  _current_z    = -1;

  // first token is the scan number
  QString scan_token = token_list.at(0);
  _current_scan      = processScan(scan_token);

  // second token is the sequence
  QString seq_token = token_list.at(1);
  _current_sequence = processSequence(seq_token);

  // third token is the mh value
  QString mh_token = token_list.at(2);
  _current_mh      = processMh(mh_token);

  // forth token is the charge
  QString charge_token = token_list.at(3);
  _current_z           = processCharge(charge_token);

  // fifth token is the protein
  QString prot_token = token_list.at(4);
  _current_prot_desc = processProtOrMods(prot_token);

  if(count == 6)
    {
      // sixth optional token is the modification "mods" string
      QString mods_token = token_list.at(5);
      _current_mods      = processProtOrMods(mods_token);
    }
}

const QString
PepParser2::whatSeparator() const
{
  QString ret;
  if(_sep == "\t")
    ret = "tabulation";
  else if(_sep == ";")
    ret = "semicolon";
  else if(_sep == " ")
    ret = "space";
  else
    ret = "undefined";
  return ret;
}

bool
PepParser2::isHeader(const QString &line)
{
  QRegularExpression header("scan.+sequence.+mh.+z.+protein",
                            QRegularExpression::CaseInsensitiveOption);
  return (line.contains(header));
}

const QString
PepParser2::getSeparator(const QString &header)
{

  QStringList separators = (QStringList() << "\t"
                                          << ";"
                                          << ",");
  foreach(const QString &sep, separators)
    {
      QStringList header_list = header.split(sep, Qt::SkipEmptyParts);
      int count               = header_list.count();
      if(count != 1 && count >= 5)
        return sep;
    }

  throw pappso::PappsoException(
    QObject::tr("In peptide text file %1 : cannot determine the column "
                "separator in the header.\nValid separators are tabulation, "
                "comma or semicolon. Only one separator has to be used "
                "everywhere in a file, do not mix them.\n")
      .arg(_pep_file->fileName()));
}

const QString
PepParser2::processSequence(QString &seq_token)
{
  QString error_seq(seq_token);
  // seq_token.remove(QString(" "));
  QRegularExpression not_aaseq("[^ARNDCEQGHILKMFPSTWYV]",
                               QRegularExpression::CaseInsensitiveOption);
  seq_token.remove(not_aaseq);
  if(Utilities::isValidSequence(seq_token,
                                QRegularExpression::CaseInsensitiveOption))
    {
      QString seq_token_upper = seq_token.toUpper();
      return seq_token_upper;
    }
  else
    {
      QString ln;
      ln.setNum(_line_number);
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 : \n in line %2 peptide sequence "
                    "'%3' is not a valid amino-acid sequence.\n")
          .arg(_pep_file->fileName(), ln, error_seq));
    }
}

const QString
PepParser2::processMh(QString &mh_token)
{
  QString error_token(mh_token);
  // replace ',' with '.' (french people are bizarre :)
  mh_token.replace(QString(","), QString("."));
  // replace consecutive dots with just one dot
  QString result =
    mh_token.replace(QRegularExpression("\\.\\.+"), QString("."));
  // match a floating point number or an integer
  auto match_float_it =
    QRegularExpression("(\\d+\\.?\\d+)",
                       QRegularExpression::CaseInsensitiveOption)
      .globalMatch(mh_token);

  while(match_float_it.hasNext())
    {
      QRegularExpressionMatch match = match_float_it.next();
      QString mh_value              = match.captured(1);
      bool ok;
      mh_value.toDouble(&ok);
      if(ok)
        // if we return the float, problems for PepId
        return mh_value;
    }

  QString ln;
  ln.setNum(_line_number);
  throw pappso::PappsoException(
    QObject::tr("In peptide text file %1 :\n in line %2 peptide "
                "mh '%3' is not a valid number.")
      .arg(_pep_file->fileName(), ln, mh_token));
}

const QString
PepParser2::processProtOrMods(QString &prot_token)
{
  // remove '"' and spaces
  prot_token.remove(QChar('"'));
  prot_token.remove(QChar(' '));
  return prot_token;
}

std::size_t
PepParser2::processScan(QString &scan_token)
{
  QString error_token(scan_token);
  QRegularExpression not_scan("\\D*",
                              QRegularExpression::CaseInsensitiveOption);
  scan_token.remove(not_scan);
  bool ok;
  std::size_t scan = scan_token.toULongLong(&ok);
  if(ok)
    return scan;
  else
    {
      QString ln;
      ln.setNum(_line_number);
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 :\n in line %2 peptide "
                    "scan number '%3' is not a valid number.")
          .arg(_pep_file->fileName(), ln, error_token));
    }
}

int
PepParser2::processCharge(QString &charge_token)
{
  QString error_token(charge_token);
  QRegularExpression not_charge("\\D*",
                                QRegularExpression::CaseInsensitiveOption);

  charge_token.remove(not_charge);
  bool ok;
  int charge = charge_token.toInt(&ok);
  if(ok)
    return charge;
  else
    {
      QString ln;
      ln.setNum(_line_number);
      throw pappso::PappsoException(
        QObject::tr("In peptide text file %1 :\n in line %2 peptide "
                    "scan number '%3' is not a valid number.")
          .arg(_pep_file->fileName(), ln, error_token));
    }
}
