/**
 * \file alignment_base.cpp
 * \date 23 sept. 2009
 * \author: Olivier Langella
 */

#include "alignment_base.h"
#include "../msrun/ms_run_hash_group.h"

AlignmentBase::AlignmentBase(MonitorAlignmentBase *monitor)
{
  _monitorAlignment = monitor;
}

AlignmentBase::~AlignmentBase()
{
}

MonitorAlignmentBase *
AlignmentBase::getMonitorAlignmentBase()
{
  return _monitorAlignment;
}

