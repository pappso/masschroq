/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file alignment_base.h
 * \date September 23, 2009
 * \author Olivier Langella
 */

#pragma once

#include "../mcq_error.h"
#include "../msrun/msrun.h"
#include "monitors/monitor_alignment_base.h"
#include <QMutex>

class msRunHashGroup;

/**
 * \class AlignmentBase
 * \brief Virtual base class representing the alignment of the msruns of a group
 * in QuantiMSCpp
 *
 * The msrun with xml id "samp0" is always the reference msrun for
 * the alignment of the group. The other msruns of the group are each
 * aligned, sequentially, with this reference msrun (in method
 * alignTwoMsRuns).
 */

class AlignmentBase
{

  public:
  AlignmentBase(MonitorAlignmentBase *monitor);

  virtual ~AlignmentBase();

  virtual void printInfos(QTextStream &out) const = 0;
  
  MonitorAlignmentBase * getMonitorAlignmentBase();

  protected:

  MonitorAlignmentBase *_monitorAlignment;

  QMutex _mutex;
};
