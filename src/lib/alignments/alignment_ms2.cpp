/**
 * \file alignment_ms2.cpp
 * \date 9 juillet 2010
 * \author: Edlira Nano
 */
#include "alignment_ms2.h"
#include "../consoleout.h"
#include "../peptides/peptide.h"
#include "../share/utilities.h"
#include <QDebug>
#include <algorithm>
//#include <odsstream/odsexception.h>
//#include <odsstream/odsdocwriter.h>

Q_DECLARE_METATYPE(MsrunSp);


AlignmentMs2::AlignmentMs2(MonitorAlignmentBase *monitor)
  : AlignmentBase(monitor)
{
  _ms2_tendency_halfwindow  = 10;
  _ms2_smoothing_halfwindow = 10;
  _ms1_smoothing_halfwindow = 10;
}

AlignmentMs2::~AlignmentMs2()
{
  //_shared_peptide_list.clear();
  //_new_time_values.clear();
}

/// sets the _ms2_tendency_halfwindow
void
AlignmentMs2::setMs2TendencyWindow(mcq_double ms2_tendency_halfwindow)
{
  _ms2_tendency_halfwindow = ms2_tendency_halfwindow;
}

/// sets the _ms2_smoothing_halfwindow
void
AlignmentMs2::setMs2SmoothingWindow(mcq_double ms2_smoothing_halfwindow)
{
  _ms2_smoothing_halfwindow = ms2_smoothing_halfwindow;
}

/// sets the _ms1_smoothing_halfwindow
void
AlignmentMs2::setMs1SmoothingWindow(mcq_double ms1_smoothing_halfwindow)
{
  _ms1_smoothing_halfwindow = ms1_smoothing_halfwindow;
}

int
AlignmentMs2::getMs2TendencyWindow() const
{
  return (_ms2_tendency_halfwindow);
}

int
AlignmentMs2::getMs2SmoothingWindow() const
{
  return (_ms2_smoothing_halfwindow);
}

int
AlignmentMs2::getMs1SmoothingWindow() const
{
  return (_ms1_smoothing_halfwindow);
}

void
AlignmentMs2::printInfos(QTextStream &out) const
{
  out << "\t MS2 alignment parameters :" << Qt::endl;
  out << "\t ms2_tendency_halfwindow = " << _ms2_tendency_halfwindow
      << Qt::endl;
  out << "\t ms2_smoothing_halfwindow = " << _ms2_smoothing_halfwindow
      << Qt::endl;
  out << "\t ms1_smoothing_halfwindow = " << _ms1_smoothing_halfwindow
      << Qt::endl;
}
