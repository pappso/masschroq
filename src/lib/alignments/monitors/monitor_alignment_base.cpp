/**
 * \file monitor_alignement_base.cpp
 * \date September 12, 2012
 * \author Benoit Valot
 */

#include "monitor_alignment_base.h"

MonitorAlignmentBase::MonitorAlignmentBase()
{
  // TODO Auto-generated constructor stub
}

MonitorAlignmentBase::~MonitorAlignmentBase()
{
  // TODO Auto-generated destructor stub
}

void
MonitorAlignmentBase::setTimeValues(
  const pappso::MsRunId &msrun_id [[maybe_unused]],
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time
  [[maybe_unused]])
{
  // Do nothing by default
}
void
MonitorAlignmentBase::setTraceValues(
  const pappso::MsRunId &msrun_id [[maybe_unused]],
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time
  [[maybe_unused]],
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time_reference
  [[maybe_unused]])
{
  // Do nothing by default
}
