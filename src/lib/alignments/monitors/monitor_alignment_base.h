/**
 * \file monitor_alignement_base.h
 * \date September 12, 2012
 * \author Benoit Valot
 */

#pragma once

#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <QMutex>
#include "../../../exportimportconfig.h"

class MCQ_LIB_DECL MonitorAlignmentBase
{
  public:
  MonitorAlignmentBase();
  virtual ~MonitorAlignmentBase();

  virtual void setTimeValues(
    const pappso::MsRunId &msrun_id,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time);
  virtual void setTraceValues(
    const pappso::MsRunId &msrun_id,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time_reference);

  protected:
  QMutex _mutex;
};
