/**
 * \file monitor_alignement_time.cpp
 * \date September 12, 2012
 * \author Benoit Valot
 */

#include "monitor_alignment_time.h"
#include "../../consoleout.h"
#include "../../mcq_error.h"

#include <QDir>
#include <QDebug>
#include <QMutexLocker>

MonitorAlignmentTime::MonitorAlignmentTime() : _outDir("")
{
}

MonitorAlignmentTime::~MonitorAlignmentTime()
{
}

void
MonitorAlignmentTime::setOutputDirectory(const QString &dir)
{
  QDir output_dir = QDir(dir);
  if(!output_dir.exists())
    {
      if(!output_dir.mkpath(output_dir.absolutePath()))
        {
          throw mcqError(
            QObject::tr("unable to create output directory  :\n%1\n to write "
                        "time values.")
              .arg(output_dir.absolutePath()));
        }
    }
  _outDir = dir;
}

void
MonitorAlignmentTime::setTimeValues(
  const pappso::MsRunId &msrun_id,
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time)
{
  qDebug();
  QMutexLocker lock(&_mutex);
  const QFileInfo fileInfo(msrun_id.getFileName());
  if(_outDir.isEmpty())
    {
      _outDir = fileInfo.absolutePath();
    }
  qDebug();
  QFileInfo out_time_file(_outDir + "/" + fileInfo.completeBaseName() +
                          ".time");
  QFile file(out_time_file.absoluteFilePath());

  qDebug();
  if(file.exists())
    {
      ConsoleOut::mcq_cout()
        << "WARNING : alignment time file '" << out_time_file.absoluteFilePath()
        << "' : already exists, it will be overwrited" << Qt::endl;
    }

  qDebug();
  if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
    }
  else
    {
      throw mcqError(QObject::tr("ERROR : alignment time file '%1' : failed to "
                                 "open, no time file for this alignment\n%2")
                       .arg(out_time_file.absoluteFilePath())
                       .arg(file.errorString()));
      return;
    }

  qDebug();
  QTextStream out(&file);

  if(msrun_retention_time.isAligned())
    {
      std::vector<double> original_rt_list =
        msrun_retention_time.getMs1RetentionTimeVector();
      std::vector<double> aligned_rt_list =
        msrun_retention_time.getAlignedRetentionTimeVector();
      for(std::size_t i = 0; i < original_rt_list.size(); i++)
        {
          out << original_rt_list[i] << "\t" << aligned_rt_list[i] << "\n";
        }
    }
  else
    {
      // no aligned retention times to write, only the original ones
      for(double rt : msrun_retention_time.getMs1RetentionTimeVector())
        {
          out << rt << "\n";
        }
    }
  file.close();
  qDebug();
}

void
MonitorAlignmentTime::setTraceValues(
  const pappso::MsRunId &msrun_id,
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time,
  const pappso::MsRunRetentionTime<QString> &msrun_retention_time_reference)
// const Msrun *pmsrun,
//  const std::map<mcq_double, mcq_double> *map_bestRt_deltaRt,
//  const std::map<mcq_double, mcq_double> *corrected_map_bestRt_deltaRt,
//  const std::map<mcq_double, mcq_double> *corrected_mean_map_bestRt_deltaRt,
// const std::map<mcq_double, mcq_double> *map_oldRt_smoothedDelta)
{
  if(!msrun_retention_time.isAligned())
    return;
  qDebug();
  _mutex.lock();
  const QFileInfo fileInfo(msrun_id.getFileName());
  if(_outDir.isEmpty())
    {
      _outDir = fileInfo.absolutePath();
    }
  QFileInfo out_trace_file(_outDir + "/" + fileInfo.completeBaseName() +
                           ".trace");
  QFile file(out_trace_file.absoluteFilePath());

  if(file.exists())
    {
      ConsoleOut::mcq_cout()
        << "WARNING : alignment trace file '"
        << out_trace_file.absoluteFilePath()
        << "' : already exists, it will be overwrited" << Qt::endl;
    }

  if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
    }
  else
    {
      ConsoleOut::mcq_cerr()
        << "WARNING : alignment trace file '"
        << out_trace_file.absoluteFilePath()
        << "' : failed to open, no traces for this alignment\n"
        << file.errorString() << Qt::endl;
      return;
    }

  QTextStream out(&file);

  qDebug();
  // printing header
  out << "rt_MS2 "
      << "\t"
      << "deltaRt_MS2"
      << "\t"
      << "post-median-deltaRt_MS2"
      << "\t"
      << "post-mean-deltaRt_MS2"
      << "\t"
      << "rt_MS1"
      << "\t"
      << "smoothed-deltaRt_MS1 " << Qt::endl;

  // Begin printing the values
  pappso::Trace ms2_deltart = msrun_retention_time.getCommonSeamarksDeltaRt(
    msrun_retention_time_reference);
  pappso::Trace ms2_post_median_deltart(ms2_deltart);
  msrun_retention_time.getMs2MedianFilter().filter(ms2_post_median_deltart);
  pappso::Trace ms2_post_mean_deltart(ms2_post_median_deltart);
  msrun_retention_time.getMs2MeanFilter().filter(ms2_post_mean_deltart);

  qDebug();
  std::vector<double> rt_aligned_ms1 =
    msrun_retention_time.getAlignedRetentionTimeVector();
  std::vector<double> rt_ms1 = msrun_retention_time.getMs1RetentionTimeVector();

  qDebug();

  std::size_t max_size     = ms2_deltart.size();
  std::size_t sizeSmoothed = rt_ms1.size();

  if(max_size < sizeSmoothed)
    {
      max_size = sizeSmoothed;
    }
  qDebug() << "ms2_deltart.size()=" << ms2_deltart.size();
  qDebug() << "ms2_post_median_deltart.size()="
           << ms2_post_median_deltart.size();
  qDebug() << "ms2_post_mean_deltart.size()=" << ms2_post_mean_deltart.size();
  qDebug() << "rt_ms1.size()=" << rt_ms1.size();
  qDebug() << "rt_aligned_ms1.size()=" << rt_aligned_ms1.size();
  for(std::size_t i = 0; i < max_size; i++)
    {
      qDebug() << i;
      if(i < ms2_deltart.size())
        {
          out << ms2_deltart[i].x << "\t" << ms2_deltart[i].y << "\t"
              << ms2_post_median_deltart[i].y << "\t"
              << ms2_post_mean_deltart[i].y << "\t";
        }
      else
        {
          out << "\t\t\t\t";
        }
      if(i < sizeSmoothed)
        {
          out << rt_ms1[i] << "\t" << (rt_aligned_ms1[i] - rt_ms1[i]);
        }
      else
        {
          out << "\t";
        }
      out << "\n";
    }
  file.close();
  _mutex.unlock();
  qDebug();
}
