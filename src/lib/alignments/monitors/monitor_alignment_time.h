/**
 * \file monitor_alignement_time.h
 * \date September 12, 2012
 * \author Benoit Valot
 */

#pragma once
#include "monitor_alignment_base.h"

class MonitorAlignmentTime : public MonitorAlignmentBase
{

  public:
  MonitorAlignmentTime();
  virtual ~MonitorAlignmentTime();

  void setOutputDirectory(const QString &dir);

  virtual void setTimeValues(const pappso::MsRunId& msrun_id,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time) override;

  virtual void setTraceValues(const pappso::MsRunId& msrun_id,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time,
    const pappso::MsRunRetentionTime<QString> &msrun_retention_time_reference)
    override;

  private:
  QString _outDir;
};
