/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope thatx it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file mass_chroq.h
 * \date September 18, 2009
 * \author Olivier Langella
 */

#pragma once

#include <QDateTime>
#include <QDir>
#include <QTemporaryDir>
#include <QTextStream>

#include "../config.h"
#include "alignments/alignment_base.h"
#include "msrun/ms_run_hash_group.h"
#include "msrun/msrun.h"
#include "peptides/isotope_label.h"
#include "peptides/peptide_list.h"
#include "peptides/protein.h"
#include "quantifications/quantificationMethod.h"
#include "quantificator.h"
#include <pappsomspp/processing/filters/filtersuite.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>
#include "../exportimportconfig.h"

/**
 * \class MassChroq
 * \brief The super class in MassChroq : it knows and coordinates everything
 * else

 * This class launches the parsing of the XML input file and keeps track of
 all of the elements parsed : msruns, groups, peptides, and of all of the
 parameters on them : alignment methods, quantification methods.
 */

class MCQ_LIB_DECL MassChroq
{
  public:
  /// masschroq's beginning of execution date and time static variable
  static QDateTime begin_date_time;

  /// set the masschroq's beginning date and time static variable
  static void setBeginDateTime(const QDateTime &dt_begin);
  static QDateTime getBeginDateTime();

  MassChroq();
  virtual ~MassChroq();

  /// set the input XML filename
  void setXmlFilename(QString &);

  const QString getXmlFilename() const;

  /// validation of the XSD schema and of the XML input file against it
  bool validateXmlFile(pappso::UiMonitorInterface &ui_monitor);

  /// parse the XML input file with Dom to look for peptide_files_list tag
  void runDomParser();

  /// parsing of the XML input file
  void runXmlFile(pappso::UiMonitorInterface &ui_monitor);

  void setResults(MonitorSpeedInterface *p_results);

  void setPeptideListInMsruns() const;

  const IsotopeLabel *getIsotopeLabel(const QString &label_id);

  /// creates and sets an msrun
  MsrunSp newMsRun(pappso::MsRunReaderSPtr run_reader,
                   const bool read_time_values,
                   const QString &time_dir) const;

  void addAlignmentMethod(const QString &method_id,
                          AlignmentBase *alignment_method);

  void addQuantificationMethod(QuantificationMethod *quantification_method);

  void addQuantificator(Quantificator *quantificator);

  void deleteQuantificator(const QString &quantify_id);

  Quantificator *findQuantificator(const QString &quantify_id) const;

  void setQuantificatorMatchMode(const QString &quantify_id,
                                 McqMatchingMode match_mode);

  void addQuantificatorPeptideItems(pappso::UiMonitorInterface &ui_monitor,
                                    const QString &quantify_id,
                                    const QStringList &isotope_labels_list,
                                    mcq_double ni_minimum_abundance);

  void addQuantificatorMzrtItem(const QString &quantify_id,
                                const QString &mz,
                                const QString &rt);

  void addQuantificatorMzItems(const QString &quantify_id,
                               const QStringList &mz_list);

  void addIsotopeLabel(const IsotopeLabel *p_isotope_label);

  void addPeptideInPeptideList(PeptideSPtr p_peptide);

  PeptideCstSPtr getPeptide(const QString pepid) const;

  void addProtein(ProteinSp p_protein);

  void newMsRunGroup(const QString &idname, const QStringList &list_msrun_ids);

  const PeptideList getPeptideListInGroup(const QString &group_xml_id) const;

  Msrun *findMsRun(const QString &msrun_id) const;

  msRunHashGroup *findGroup(const QString &group_id) const;

  const Protein *findProtein(const QString &protein_id) const;

  void alignGroup(pappso::UiMonitorInterface &ui_monitor,
                  const QString &group_id,
                  const QString &method_id,
                  const QString &ref_msrun_id);

  const pappso::FilterSuite &getXicFilters() const;

  void executeQuantification(pappso::UiMonitorInterface &ui_monitor, const pappso::IonMobilityGrid * p_ion_mobility_grid,
                             const QString &quantify_id);

  AlignmentBase *findAlignmentMethod(const QString &method_id) const;

  QuantificationMethod *
  findQuantificationMethod(const QString &method_id) const;

  void setMasschroqDir(const QString &masschroq_dir_path);

  void setTracesDirectory(QDir QDir, McqTsvFormat tsv_format);
  void addPeptideTrace(PeptideCstSPtr peptide);
  void allPeptideTrace();

  msRunHashGroup &getMsRunHashGroupAllCollection();

  bool hasIonMobility() const;

  protected:
  QFileInfo _param_file_info;

  msRunHashGroup m_msRunHashGroupAllCollection;

  // QTextStream * _output_stream;

  ///
  std::map<QString, msRunHashGroup *> _groups;

  std::map<const QString, AlignmentBase *> _alignment_methods;

  std::map<const QString, QuantificationMethod *> _quantification_methods;

  std::map<const QString, Quantificator *> _quantificators;

  MonitorSpeedInterface *_p_quantif_results = nullptr;

  PeptideList m_peptideList;

  std::map<QString, const IsotopeLabel *> _map_p_isotope_labels;

  std::map<QString, ProteinSp> _p_proteins;

  QString _masschroq_dir_path;

  private:
  /** @brief trace directory path */
  QDir _traces_directory;
  /** @brief traces output format */
  McqTsvFormat _traces_format;
  /** @brief list of peptides to trace */
  std::vector<PeptideCstSPtr> m_tracePeptideList;
};
