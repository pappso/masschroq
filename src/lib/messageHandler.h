/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file messageHandler.h
 * \date July 23, 2010
 * \author Edlira Nano
 */
#pragma once

#include <QAbstractMessageHandler>

class MessageHandler : public QAbstractMessageHandler
{

  public:
  MessageHandler() : QAbstractMessageHandler(0)
  {
  }

  QString
  description()
  {
    _description.replace(QRegularExpression("<[^>]*>"), QString(""));
    return _description;
  }

  int
  line() const
  {
    return _sourceLocation.line();
  }

  int
  column() const
  {
    return _sourceLocation.column();
  }

  protected:
  virtual void
  handleMessage(QtMsgType type,
                const QString &description,
                const QUrl &identifier,
                const QSourceLocation &sourceLocation)
  {
    Q_UNUSED(type);
    Q_UNUSED(identifier);

    _messageType = type;

    _description    = description;
    _sourceLocation = sourceLocation;
  }

  private:
  QtMsgType _messageType;
  QString _description;
  QSourceLocation _sourceLocation;
};
