/**
 * \file ms_run_hash_group.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include "ms_run_hash_group.h"
#include "../consoleout.h"
#include "../quantifications/quantificationMethod.h"
#include <QtConcurrentMap>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <pappsomspp/exception/exceptionnotpossible.h>

msRunHashGroup::msRunHashGroup()
{
  _p_msrun_ref  = nullptr;
  _group_xml_id = "root";
  _ms_run_map   = std::map<QString, MsrunSp>();

  qDebug() << _ms_run_map.size();
}

msRunHashGroup::msRunHashGroup(const QString group_xml_id)
{
  _p_msrun_ref  = nullptr;
  _group_xml_id = group_xml_id;
  _ms_run_map   = std::map<QString, MsrunSp>();

  qDebug() << _ms_run_map.size();
}

msRunHashGroup::msRunHashGroup(const msRunHashGroup &other)
{

  _p_msrun_ref  = other._p_msrun_ref;
  _group_xml_id = other._group_xml_id;
  _ms_run_map   = other._ms_run_map;
}
msRunHashGroup::~msRunHashGroup()
{
}

/// delete msruns and clear container
void
msRunHashGroup::clear()
{

  qDebug();
  _group_xml_id = "";
  qDebug();
  _p_msrun_ref = nullptr;
  qDebug();
  _ms_run_map.clear();
  qDebug();
}

/// returns true if this msrun group contains the given msrun
bool
msRunHashGroup::containsMsRun(const Msrun *p_msrun) const
{

  auto it =
    std::find_if(_ms_run_map.begin(),
                 _ms_run_map.end(),
                 [p_msrun](const std::pair<QString, MsrunSp> &pair_msrun) {
                   return (pair_msrun.second.get() == p_msrun);
                 });
  if(it == _ms_run_map.end())
    return false;
  return true;
}

/// returns a pointer to the msrun with the given id if it is part of
/// this group, otherwise NULL is returned
Msrun *
msRunHashGroup::getMsRun(const QString &idname) const
{
  auto it = _ms_run_map.find(idname);
  if(it == _ms_run_map.end())
    {
      return (nullptr);
    }
  else
    {
      return (it->second.get());
    }
}
MsrunSp
msRunHashGroup::getMsRunSp(const QString &idname) const
{

  auto it = _ms_run_map.find(idname);
  if(it == _ms_run_map.end())
    {
      MsrunSp msrun;
      return (msrun);
    }
  else
    {
      return (it->second);
    }
}
/// adds the given msrun to this group (if it is not already in)
/// the first msrun setted here will be the reference one
void
msRunHashGroup::addMsRun(MsrunSp p_msrun)
{
  QMutexLocker lock(&m_mutex);
  qDebug() << p_msrun->getMsRunIdCstSPtr()->getXmlId();
  const QString idname(p_msrun->getMsRunIdCstSPtr()->getXmlId());
  auto ret = _ms_run_map.insert(std::pair<QString, MsrunSp>(idname, p_msrun));
  if(ret.second == true)
    {
      // OK this key does not exist
    }
  else
    {
      throw mcqError(
        QObject::tr("error in msRunHashGroup::setMsRun :\nthe msrun "
                    "with id %1 already exists")
          .arg(idname));
    }
  qDebug() << p_msrun->getMsRunIdCstSPtr()->getXmlId();
}

void
msRunHashGroup::setReferenceMsrun(const QString &ref_msrun_id)
{
  Msrun *ref_msrun = this->getMsRun(ref_msrun_id);
  if(ref_msrun == nullptr)
    {
      /// we have a problem
      throw mcqError(
        QObject::tr("error in msRunHashGroup::setReferenceMsrun :\nthe msrun "
                    "with id %1 does not exist or is not part of group %2")
          .arg(ref_msrun_id, _group_xml_id));
    }
  else
    {
      _p_msrun_ref = ref_msrun;
    }
}

const Msrun *
msRunHashGroup::getReferenceMsrun() const
{
  return (_p_msrun_ref);
}

/// aligns the msruns of this group (one by one, towards the reference msrun)
void
msRunHashGroup::alignMsRuns(pappso::UiMonitorInterface &ui_monitor,
                            AlignmentBase &alignment_method)
{

  ui_monitor.setTitle(
    QObject::tr("Starting alignment of group %1 containing %2 msruns")
      .arg(getXmlId())
      .arg(_ms_run_map.size()));
  mp_alignmentMs2 = dynamic_cast<AlignmentMs2 *>(&alignment_method);
  if(mp_alignmentMs2 == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in %1 :\nthe alignment method is not known")
          .arg(__FUNCTION__));
    }

  AlignmentMs2 *p_alignment_ms2 = mp_alignmentMs2;
  if(_ms_run_map.size() > 1)
    {
      pappso::MsRunRetentionTime<QString> &msrunRetentionTimeReference =
        _p_msrun_ref->getMsRunRetentionTime();
      msrunRetentionTimeReference.computeSeamarks();


      ui_monitor.appendText(
        QObject::tr(
          "\t MS2 alignment parameters :\n\t ms2_tendency_halfwindow = %1 \n\t "
          "ms2_smoothing_halfwindow = %2\n\t ms1_smoothing_halfwindow = %3\n")
          .arg(p_alignment_ms2->getMs2TendencyWindow())
          .arg(p_alignment_ms2->getMs2SmoothingWindow())
          .arg(p_alignment_ms2->getMs1SmoothingWindow()));

      MonitorAlignmentBase *local_p_monitor_align =
        p_alignment_ms2->getMonitorAlignmentBase();

      Msrun *local_p_msrunref                       = _p_msrun_ref;
      pappso::UiMonitorInterface *localp_ui_monitor = &ui_monitor;
      QString msrunref_id = _p_msrun_ref->getMsRunIdCstSPtr().get()->getXmlId();
      std::function<MsrunSp(std::pair<QString, MsrunSp>)> msrun_align =
        [localp_ui_monitor,
         msrunref_id,
         msrunRetentionTimeReference,
         p_alignment_ms2](std::pair<QString, MsrunSp> mapit) -> MsrunSp {
        pappso::MsRunRetentionTime<QString> &msrunRetentionTimeToAlign =
          mapit.second->getMsRunRetentionTime();
        if(msrunref_id ==
           mapit.second.get()->getMsRunIdCstSPtr().get()->getXmlId())
          {
          }
        else
          {
            msrunRetentionTimeToAlign.setMs1MeanFilter(
              p_alignment_ms2->getMs1SmoothingWindow());
            msrunRetentionTimeToAlign.setMs2MeanFilter(
              p_alignment_ms2->getMs2SmoothingWindow());
            msrunRetentionTimeToAlign.setMs2MedianFilter(
              p_alignment_ms2->getMs2TendencyWindow());

            msrunRetentionTimeToAlign.computeSeamarks();


            try
              {
                msrunRetentionTimeToAlign.align(msrunRetentionTimeReference);
              }
            catch(pappso::ExceptionNotPossible &error)
              {


                localp_ui_monitor->appendText(
                  QObject::tr(
                    "Warning : alignment failed for %1 vs reference %2 : %3")
                    .arg(mapit.second.get()
                           ->getMsRunIdCstSPtr()
                           .get()
                           ->getSampleName())
                    .arg(msrunref_id)
                    .arg(error.qwhat()));
              }
          }
        return mapit.second;
      };

      QFuture<std::size_t> res = QtConcurrent::mappedReduced<std::size_t>(
        _ms_run_map.begin(),
        _ms_run_map.end(),
        msrun_align,
        [local_p_monitor_align, local_p_msrunref](std::size_t &result,
                                                  MsrunSp msrun_sp) {
          local_p_monitor_align->setTimeValues(
            *(msrun_sp.get()->getMsRunIdCstSPtr().get()),
            msrun_sp.get()->getMsRunRetentionTime());
          local_p_monitor_align->setTraceValues(
            *(msrun_sp.get()->getMsRunIdCstSPtr().get()),
            msrun_sp.get()->getMsRunRetentionTime(),
            local_p_msrunref->getMsRunRetentionTime());
          result++;
        },
        QtConcurrent::UnorderedReduce);
      res.waitForFinished();
    }
  else
    {
      ui_monitor.appendText(
        QObject::tr("WARNING : Skipping alignment in group '%1' : not enough "
                    "msruns in this group.")
          .arg(this->_group_xml_id));
    }
}

std::size_t
msRunHashGroup::size() const
{
  qDebug();
  return _ms_run_map.size();
}


const QString &
msRunHashGroup::getXmlId() const
{
  return _group_xml_id;
}

std::vector<MsrunSp>
msRunHashGroup::getMsRunSpList() const
{
  std::vector<MsrunSp> msrun_list;
  for(auto &&msrunit : _ms_run_map)
    {
      msrun_list.push_back(msrunit.second);
    }
  return msrun_list;
}

const AlignmentMs2 *
msRunHashGroup::getAlignmentMs2CstPtr() const
{
  return mp_alignmentMs2;
}
