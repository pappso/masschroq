/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file ms_run_hash_group.h
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#pragma once

#include "msrun.h"
#include <QString>
#include <map>
#include <QMutex>

#include "../alignments/alignment_base.h"
#include "../alignments/alignment_ms2.h"

class MonitorBase;
// using namespace std;

/**
 * \class msRunHashGroup
 * \brief Class representing a group of msruns in the form of a hash map
 * of msrun id -> msrun *

 * An msRunHashGroup has a unique xml group id and a reference msrun.
 */

class MCQ_LIB_DECL msRunHashGroup
{

  public:
  msRunHashGroup();
  msRunHashGroup(const QString group_id);
  msRunHashGroup(const msRunHashGroup &other);
  virtual ~msRunHashGroup();

  void clear();

  void addMsRun(MsrunSp p_msrun);

  Msrun *getMsRun(const QString &idname) const;
  MsrunSp getMsRunSp(const QString &idname) const;

  std::vector<MsrunSp> getMsRunSpList() const;

  const QString &getXmlId() const;

  void setReferenceMsrun(const QString &ref_msrun_id);

  const Msrun *getReferenceMsrun() const;

  void alignMsRuns(pappso::UiMonitorInterface &ui_monitor,
                   AlignmentBase &alignment_method);

  bool containsMsRun(const Msrun *p_msrun) const;

  typedef std::map<QString, MsrunSp>::const_iterator const_iterator;

  const_iterator
  begin() const
  {
    return _ms_run_map.begin();
  }
  const_iterator
  end() const
  {
    return _ms_run_map.end();
  }

  std::size_t size() const;


  const AlignmentMs2 *getAlignmentMs2CstPtr() const;

  private:
  std::map<QString, MsrunSp> _ms_run_map;

  Msrun *_p_msrun_ref;
  QString _group_xml_id;
  AlignmentMs2 *mp_alignmentMs2 = nullptr;

  QMutex m_mutex;
};
