/**
 * \file msrun.cpp
 * \date June 14, 2011
 * \author Edlira Nano
 */

#include "msrun.h"
#include "../../lib/consoleout.h"
#include "../../mcqsession.h"
#include "../../saxparsers/xmlSimpleParser.h"
#include "../mcq_error.h"
#include "../../output/monitors/monitorspeedinterface.h"

#include "../quantifications/mapquanti.h"
#include "../quantificator.h"

#include <math.h>
//#include <iostream>
#include <QDebug>
#include <QElapsedTimer>
#include <QtConcurrent>
#include <fstream>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/xicextractor/msrunxicextractorfactory.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <QThread>

Msrun::Msrun(pappso::MsRunReaderSPtr msrunid) : msp_msrunReader(msrunid)
{
}

Msrun::Msrun(const Msrun &other) : msp_msrunReader(other.msp_msrunReader)
{
}

Msrun::~Msrun()
{
  qDebug();
  _map_scan_precursor.clear();
  qDebug();
}


const pappso::MsRunIdCstSPtr &
Msrun::getMsRunIdCstSPtr() const
{
  return msp_msrunReader.get()->getMsRunId();
}


/// set the list of peptides observed in this msrun
void
Msrun::setPeptideList(const PeptideList &pep_list)
{
  _peptide_list = pep_list;
}

/// get the list of peptides observed in this msrun
const PeptideList &
Msrun::getPeptideList() const
{
  return _peptide_list;
}

/// adds an element to the hash map scan_num -> precursor
void
Msrun::mapPrecursor(std::size_t scan_num, PrecursorSPtr precursor)
{
  qDebug() << QThread::currentThreadId() << "_map_scan_precursor[" << scan_num
           << "]";

  if(precursor.get()->getXicCoordSPtr().get() == nullptr)
    {
      throw mcqError(
        QObject::tr("precursor.get()->getXicCoordSPtr().get() == nullptr \n")
          .arg(getMsRunIdCstSPtr()->getXmlId())
          .arg(scan_num));
    }
  _map_scan_precursor[scan_num] = precursor;
  qDebug() << QThread::currentThreadId() << "_map_scan_precursor[" << scan_num
           << "]";
}

void
Msrun::mapSpectrumIndexToPrecursor(std::size_t spectrum_index,
                                   PrecursorSPtr precursor)
{
  qDebug() << QThread::currentThreadId()
           << " spectrum_index=" << spectrum_index;

  if(precursor.get()->getXicCoordSPtr().get() == nullptr)
    {
      throw mcqError(
        QObject::tr("precursor.get()->getXicCoordSPtr().get() == nullptr \n")
          .arg(getMsRunIdCstSPtr()->getXmlId())
          .arg(spectrum_index));
    }
  m_spectrumIndex2precursorMap[spectrum_index] = precursor;
  qDebug() << QThread::currentThreadId()
           << " spectrum_index=" << spectrum_index;
}


const PrecursorSPtr &
Msrun::getPrecursorByScanNumber(std::size_t scan_num) const
{

  auto it = _map_scan_precursor.find(scan_num);
  if(it != _map_scan_precursor.end())
    {
      qDebug() << "scan_num=" << scan_num;
      if(it->second.get() == nullptr)
        {
          qDebug();
          throw pappso::PappsoException(
            QObject::tr("it->second.get() == nullptr\nindex=%1 %2 %3 %4")
              .arg(scan_num)
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      qDebug();
      if(it->second.get()->getXicCoordSPtr().get() == nullptr)
        {
          qDebug();
          throw pappso::PappsoException(
            QObject::tr("it->second.get()->getXicCoordSPtr().get() == "
                        "nullptr\nindex=%1 %2 %3 %4")
              .arg(scan_num)
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      qDebug();
      return (it->second);
    }
  else
    {
      throw mcqError(QObject::tr("error in msrun '%1' (in %2 method): scan "
                                 "(MS level >=2) number %3 does not exist\n")
                       .arg(getMsRunIdCstSPtr()->getXmlId())
                       .arg(__FUNCTION__)
                       .arg(scan_num));
    }
}

const PrecursorSPtr &
Msrun::getPrecursorBySpectrumIndex(std::size_t spectrum_index) const
{
  auto it = m_spectrumIndex2precursorMap.find(spectrum_index);
  if(it != m_spectrumIndex2precursorMap.end())
    {
      qDebug() << "spectrum_index=" << spectrum_index;
      if(it->second.get() == nullptr)
        {
          qDebug();
          throw pappso::PappsoException(
            QObject::tr("it->second.get() == nullptr\nindex=%1 %2 %3 %4")
              .arg(spectrum_index)
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      qDebug();
      if(it->second.get()->getXicCoordSPtr().get() == nullptr)
        {
          qDebug();
          throw pappso::PappsoException(
            QObject::tr("it->second.get()->getXicCoordSPtr().get() == "
                        "nullptr\nindex=%1 %2 %3 %4")
              .arg(spectrum_index)
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      qDebug();
      return (it->second);
    }
  else
    {
      throw mcqError(
        QObject::tr("error in msrun '%1' (in %2 method): "
                    "(MS level >=2) spectrum index %3 does not exist\n")
          .arg(getMsRunIdCstSPtr()->getXmlId())
          .arg(__FUNCTION__)
          .arg(spectrum_index));
    }
}


/**
 * read_time_values reads the time values in the .time file in the
 * given directory time_dir if it exists and calls setNewTimeValues with them.
 */
bool
Msrun::read_time_values(const QString &time_dir)
{
  QString dir = time_dir;
  const QFileInfo fileInfo(getMsRunIdCstSPtr()->getFileName());
  if(time_dir.isEmpty())
    {
      dir = fileInfo.absolutePath();
    }
  QFile file(dir + "/" + fileInfo.completeBaseName() + ".time");

  if(file.exists())
    {
      ConsoleOut::mcq_cout()
        << "MS run '" << (getMsRunIdCstSPtr()->getXmlId())
        << "' : reading time values file '" << file.fileName() << "'"
        << "\n";
      // try to read file
      std::vector<mcq_double> retention_times;
      mcq_double time;
      std::ifstream file_in;
      file_in.open(file.fileName().toStdString().c_str());
      while(file_in.good())
        {
          file_in >> time;
          if(file_in.good())
            {
              retention_times.push_back(time);
            }
        }
      file_in.close();

      if(msp_msrunRetentionTime.get() == nullptr)
        {
          throw pappso::PappsoException(
            QObject::tr(
              "error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
              .arg(__FUNCTION__));
        }

      msp_msrunRetentionTime.get()->setAlignedRetentionTimeVector(
        retention_times);
    }
  else
    {
      ConsoleOut::mcq_cout()
        << "WARNING : alignment time file '" << file.fileName()
        << "' : does not exist, no time values to read."
        << "\n";
    }
  return true;
}

pappso::MsRunRetentionTime<QString> &
Msrun::getMsRunRetentionTime()
{

  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  return *msp_msrunRetentionTime.get();
}

const pappso::MsRunRetentionTime<QString> &
Msrun::getMsRunRetentionTime() const
{
  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  return *msp_msrunRetentionTime.get();
}


/**
 * \fn void printRetentionTimes(std::ostream & out)
 * \brief prints to the output stream out (ex. on file .time)
 * the retention times : (original_rt aligned_rt) if there are aligned times,
 * otherwise it prints the retention times per spectrum
 */
void
Msrun::printRetentionTimes(QTextStream &out) const
{
  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  if(msp_msrunRetentionTime.get()->isAligned())
    {
      std::vector<double> original_rt_list =
        msp_msrunRetentionTime.get()->getMs1RetentionTimeVector();
      std::vector<double> aligned_rt_list =
        msp_msrunRetentionTime.get()->getAlignedRetentionTimeVector();
      for(std::size_t i = 0; i < original_rt_list.size(); i++)
        {
          out << original_rt_list[i] << "\t" << aligned_rt_list[i] << "\n";
        }
    }
  else
    {
      // no aligned retention times to write, only the original ones
      for(double rt : msp_msrunRetentionTime.get()->getMs1RetentionTimeVector())
        {
          out << rt << "\n";
        }
    }
}

/**
 * \fn bool write_time_values()
 * \brief calls printRetentionTimes to print the rt in the
 * _xml_file.time file in the current directory. If the
 * file already exists it will be overwrited.
 */
bool
Msrun::write_time_values() const
{
  /// look for the .time file
  QFileInfo xml_file(getMsRunIdCstSPtr()->getFileName());
  QFile file(xml_file.absolutePath() + "/" + xml_file.completeBaseName() +
             ".time");
  QFileInfo fileInfo(file);

  /// if it exists it will be overwrited,
  if(file.exists())
    {
      ConsoleOut::mcq_cout()
        << "WARNING : MS run '" << (getMsRunIdCstSPtr()->getXmlId())
        << "' :  time values file '" << fileInfo.fileName()
        << "' : already exists, it will be overwrited\n";
    }
  else
    {
      ConsoleOut::mcq_cout()
        << "MS run '" << (getMsRunIdCstSPtr()->getXmlId())
        << "' : writing time values : file '" << fileInfo.fileName()
        << "' : does not exist, it will be created\n";
    }
  /// try to write in file
  QFile file_out(file.fileName());
  file_out.open(QIODevice::WriteOnly | QIODevice::Text);
  QTextStream out(&file_out);
  // std::ofstream file_out;
  // file_out.open(file.fileName().toStdString().c_str());
  printRetentionTimes(out);
  file_out.close();

  return (true);
}

/**
   Returns true if this msrun has been aligned. This happens in two cases :
   if a .time file has been read in the beginning of the MassChroQ process
   (i.e. an old alignment has been charged) or if an alignment has occurred
   during this process.
*/
bool
Msrun::hasBeenAligned() const
{
  if(msp_msrunRetentionTime.get() == nullptr)
    return false;
  return (msp_msrunRetentionTime.get()->isAligned());
}

/**
   clears the alignment effects on this Ms-run. We need to call this method
   when we begin a new alignment (which will in any case overwrite the
   previous one). See alignment classes for more information.
*/
void
Msrun::resetAlignmentTimeValues()
{
}

/**
   Get the aligned rt corresponding to a given orginal rt in the
   original rt -> aligned rt map _map_aligned_rt. If the given
   original rt does not correspond exactly to a value in the map,
   which is often the case, we make a linear extrapolation of the
   aligned rt-s to find the nearest rt corresponding to it.
*/
mcq_double
Msrun::getAlignedRtByOriginalRt(mcq_double original_rt) const
{

  /// if the hash table of "originaRt -> alignedRt" is empty,
  /// no alignment has occurred so return original_rt


  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  if(!msp_msrunRetentionTime.get()->isAligned())
    {
      return (original_rt);
    }
  /// else
  /// get the alignedRt in the table if the corresponding originalRt exists
  return msp_msrunRetentionTime.get()->translateOriginal2AlignedRetentionTime(
    original_rt);
}

mcq_double
Msrun::getOriginalRtByAlignedRt(mcq_double aligned_rt) const
{

  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  /// if the hash table of "originaRt -> alignedRt" is empty,
  /// no alignment has occurred so return original_rt
  if(!msp_msrunRetentionTime.get()->isAligned())
    {
      return (aligned_rt);
    }
  /// else
  /// get the alignedRt in the table if the corresponding originalRt exists
  return msp_msrunRetentionTime.get()->translateAligned2OriginalRetentionTime(
    aligned_rt);
}

const std::vector<mcq_double>
Msrun::getOriginalRetentionTimes() const
{
  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  return msp_msrunRetentionTime.get()->getMs1RetentionTimeVector();
}

const std::vector<mcq_double>
Msrun::getAlignedRetentionTimes() const
{

  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  if(this->hasBeenAligned())
    {
      return msp_msrunRetentionTime.get()->getAlignedRetentionTimeVector();
    }
  else
    return this->getOriginalRetentionTimes();
}

const std::vector<mcq_double>
Msrun::getVectorOfTimeValues() const
{
  if(msp_msrunRetentionTime.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("error in '%1' :\n(msp_msrunRetentionTime.get() == nullptr")
          .arg(__FUNCTION__));
    }
  std::vector<mcq_double> v_time;

  if(msp_msrunRetentionTime.get()->isAligned())
    {
      std::vector<double> original_rt_list =
        msp_msrunRetentionTime.get()->getMs1RetentionTimeVector();
      std::vector<double> aligned_rt_list =
        msp_msrunRetentionTime.get()->getAlignedRetentionTimeVector();
      for(std::size_t i = 0; i < original_rt_list.size(); i++)
        {
          v_time.push_back(original_rt_list[i]);
          v_time.push_back(aligned_rt_list[i]);
        }
    }
  return v_time;
}

/// simple parsing of this msrun's xml file
bool
Msrun::readPrecursorsAndRetentionTimesInMzDataFile(const bool read_time,
                                                   const QString &time_dir)
{
  qDebug();

  msp_msrunRetentionTime =
    std::make_shared<pappso::MsRunRetentionTime<QString>>(
      msp_msrunReader.get()->getRetentionTimeLine());
  QElapsedTimer tparsing;
  tparsing.start();
  try
    {

      XmlSimpleParserHandlerInterface handler(msp_msrunReader, this);


      pappso::MsRunReadConfig config;
      config.setNeedPeakList(false);
      config.setMsLevels({2});
      qDebug();
      // msrunA01.get()->readSpectrumCollection(spectrum_list_reader);
      msp_msrunReader.get()->readSpectrumCollection2(config, handler);
    }

  catch(pappso::PappsoException &errorp)
    {
      throw mcqError(QObject::tr("error reading xml input file '%1' :\n%2")
                       .arg(getMsRunIdCstSPtr().get()->getFileName())
                       .arg(errorp.qwhat()));

      ConsoleOut::mcq_cerr() << "error reading msrun xml input file \n";
      ConsoleOut::mcq_cerr() << errorp.qwhat().toStdString().c_str() << "\n";
      return (false);
    }

  qDebug() << " Total simple parsing time of msrun "
           << getMsRunIdCstSPtr()->getXmlId();
  qDebug() << QString("is %1 ms").arg(tparsing.elapsed());

  // read time values if asked so by the user
  if(read_time)
    {
      read_time_values(time_dir);
    }

  return (true);
}

pappso::MsRunXicExtractorInterfaceSp &
Msrun::getMsRunXicExtractor(pappso::XicExtractMethod xic_extract_method)
{
  qDebug() << McqSession::getInstance().getSessionTmpDirName();

  if(msp_msRunXicExtractorInterfaceSp.get() != nullptr)
    {
      msp_msRunXicExtractorInterfaceSp.get()->setXicExtractMethod(
        xic_extract_method);
    }
  else
    {
      // create it

      pappso::MsRunXicExtractorFactory::getInstance().setTmpDir(
        McqSession::getInstance().getSessionTmpDirName());
      pappso::MsRunXicExtractorFactory::getInstance()
        .setMsRunXicExtractorFactoryType(
          pappso::MsRunXicExtractorFactoryType::diskbuffer);
      msp_msRunXicExtractorInterfaceSp =
        pappso::MsRunXicExtractorFactory::getInstance()
          .buildMsRunXicExtractorSp(msp_msrunReader);

      msp_msRunXicExtractorInterfaceSp.get()->setXicExtractMethod(
        xic_extract_method);
      qDebug() << McqSession::getInstance().getSessionTmpDirName();
    }

  return msp_msRunXicExtractorInterfaceSp;
}


void
Msrun::deleteMsRunXicExtractor()
{
  msp_msRunXicExtractorInterfaceSp = nullptr;
  msp_msrunReader.get()->releaseDevice();
}

void
Msrun::quantify(pappso::UiMonitorInterface &ui_monitor,
                std::vector<pappso::XicCoordSPtr> &xic_coord_list,
                MonitorSpeedInterface *p_monitor_speed,
                QuantificationMethod *p_quanti_method,
                Quantificator *p_quantificator)
{

  std::vector<QuantiItemBaseSp> &quanti_item_list =
    p_quantificator->getQuantiItemList();

  pappso::MsRunXicExtractorInterfaceSp xic_extractor =
    getMsRunXicExtractor(p_quanti_method->getXicExtractMethod());

  pappso::FilterInterfaceCstSPtr csp_filter_suite =
    std::make_shared<const pappso::FilterSuite>(
      p_quanti_method->getXicFilters());

  xic_extractor.get()->setPostExtractionTraceFilterCstSPtr(csp_filter_suite);


  xic_extractor.get()->setRetentionTimeAroundTarget(
    McqSession::getInstance().getRetentionTimeRange());

  qDebug();
  // msp_msrunReader.get()->setMonoThread(true);

  xic_extractor.get()->extractXicCoordSPtrListParallelized(ui_monitor,
                                                           xic_coord_list);
  // msp_msrunReader.get()->setMonoThread(false);
  qDebug();
  // ui_monitor.setTotalSteps(0);
  /**********************************
   * XIC extraction end *
   **********************************/

  pappso::TraceDetectionInterfaceCstSPtr detection_method =
    p_quanti_method->getDetectionMethod();


  MapQuanti mapQuanti(this, p_quantificator, detection_method);
  MonitorSpeedInterface *local_p_monitor_speed = p_monitor_speed;

  QFuture<std::size_t> res = QtConcurrent::mappedReduced<std::size_t>(
    quanti_item_list.begin(),
    quanti_item_list.end(),
    mapQuanti,
    [local_p_monitor_speed, p_quantificator](std::size_t &result,
                                             DetectionResult detection_result) {
      qDebug() << "detection_result.p_msrun=" << detection_result.p_msrun;
      local_p_monitor_speed->writeDetectionResult(detection_result);

      p_quantificator->getXicTraceWriterSp().get()->writeTrace(
        detection_result);

      result++;
    },
    QtConcurrent::UnorderedReduce);
  res.waitForFinished();
  deleteMsRunXicExtractor();
}

const pappso::MsRunReaderSPtr &
Msrun::getMsRunReaderSPtr() const
{
  return msp_msrunReader;
}
