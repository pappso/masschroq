/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file msrun.h
 * \date June 14, 2011
 * \author Edlira Nano
 */

#pragma once

#include "precursor.h"

#include "../peptides/peptide_list.h"
#include <QFileInfo>
#include <QString>
#include <QTemporaryFile>
#include <memory>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/xicextractor/msrunxicextractorfactory.h>
#include <set>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include "../quantifications/quantificationMethod.h"
#include "../../exportimportconfig.h"

class QuantiItemBase;
class XicExtractionMethodBase;
class MonitorSpeedInterface;
class Quantificator;

class Msrun;
typedef std::shared_ptr<Msrun> MsrunSp;

/**
 * \class Msrun
 * \brief Interface class representing an LC-MS run.

 * - An LC-MS run is constructed with an unique id : _xml_id.
 * - It corresponds to a raw data file  _original_file in the system.
 * - It keeps a set member _original_rt_set containing the original retention
 times.
 * This set is never modified.
 * - It keeps a hash map _map_aligned_rt. This map is filled after an
 * alignment with original observed rt -> corresponding
 * aligned rt. If no alignment has occurred this map stays empty.
 * - It keeps a hash map _map_scan_precursor : scan number -> precursor.
 * This map is filled during the parsing of the raw data file : the scans of
 * MS level >=2 have a precursor ion choosed for the fragmentation.
 * - It keeps a list of all the peptides observed in this msrun.
 *
 */

class MCQ_LIB_DECL Msrun
{

  public:
  Msrun(pappso::MsRunReaderSPtr msrun_reader);
  Msrun(const Msrun &other);
  virtual ~Msrun();

  const pappso::MsRunIdCstSPtr &getMsRunIdCstSPtr() const;

  /// set the list of peptides observed in this msrun
  void setPeptideList(const PeptideList &pep_list);

  const PeptideList &getPeptideList() const;

  /// map precursor to its scan number as parsed in the xml file of this msrun
  void mapPrecursor(std::size_t scan_num, PrecursorSPtr precursor);

  /** @brief map spectrum index to precursor
   * new modern method to replace obsolete scan number
   */
  void mapSpectrumIndexToPrecursor(std::size_t spectrum_index,
                                   PrecursorSPtr precursor);


  /** @brief get precursor by scan number
   */
  const PrecursorSPtr &getPrecursorByScanNumber(std::size_t scan_num) const;


  /** @brief get precursor by spectrum index
   */
  const PrecursorSPtr &
  getPrecursorBySpectrumIndex(std::size_t spectrum_index) const;

  /// read the retention times for this msrun (original or aligned)
  bool read_time_values(const QString &time_dir);
  /**
   * \fn bool write_time_values()
   * \brief calls printRetentionTimes to print the rt in the
   * _xml_file.time file in the current directory. If the
   * file already exists it will be overwrited.
   */
  bool write_time_values() const;

  /// print the retention times for this msrun (two columns original rt -
  /// aligned rt)
  void printRetentionTimes(QTextStream &out) const;

  /**
   Returns true if this msrun has been aligned. This happens in two cases :
   if a .time file has been read in the beginning of the MassChroQ process
   (i.e. an old alignment has been charged) or if an alignment has occurred
   during this process.
  */
  bool hasBeenAligned() const;

  /**
   clears the alignment effects on this Ms-run. We need to call this method
   when we begin a new alignment (which will in any case overwrite the
   previous one). See alignment classes for more information.
  */
  void resetAlignmentTimeValues();

  /**
   Get the aligned rt corresponding to a given orginal rt in the
   original rt -> aligned rt map _map_aligned_rt. If the given
   original rt does not correspond exactly to a value in the map,
   which is often the case, we make a linear extrapolation of the
   aligned rt-s to find the nearest rt corresponding to it.
  */
  mcq_double getAlignedRtByOriginalRt(mcq_double rt) const;
  mcq_double getOriginalRtByAlignedRt(mcq_double rt) const;

  const std::vector<mcq_double> getOriginalRetentionTimes() const;

  const std::vector<mcq_double> getAlignedRetentionTimes() const;

  const std::vector<mcq_double> getVectorOfTimeValues() const;


  /**
   * \fn bool set_from_xml(const QString & fileName, const mcq_xml_format &
   * format) \brief parses (simple)the .mzxml/.mzml file corresponding to this
   * msrun and sets all information apart from spectra
   */
  bool readPrecursorsAndRetentionTimesInMzDataFile(const bool read_time_values,
                                                   const QString &time_dir);


  pappso::MsRunXicExtractorInterfaceSp &
  getMsRunXicExtractor(pappso::XicExtractMethod xic_extract_method);

  void deleteMsRunXicExtractor();


  pappso::MsRunRetentionTime<QString> &getMsRunRetentionTime();
  const pappso::MsRunRetentionTime<QString> &getMsRunRetentionTime() const;

  void quantify(pappso::UiMonitorInterface &ui_monitor,
                std::vector<pappso::XicCoordSPtr> &xic_coord_list,
                MonitorSpeedInterface *p_monitor_speed,
                QuantificationMethod *p_quanti_method,
                Quantificator *p_quantificator);

  const pappso::MsRunReaderSPtr &getMsRunReaderSPtr() const;

  private:
  /// hash map : scan_number -> precursor, as parsed in the mzXML file
  std::map<std::size_t, PrecursorSPtr> _map_scan_precursor;

  /** @brief new map dedicated to spectrum index to replace obsolete scan number
   */
  std::map<std::size_t, PrecursorSPtr> m_spectrumIndex2precursorMap;

  /// list of the peptides observed in this msrun
  PeptideList _peptide_list;

  pappso::MsRunXicExtractorInterfaceSp msp_msRunXicExtractorInterfaceSp;

  /// unique xml id for this msrun
  pappso::MsRunReaderSPtr msp_msrunReader;

  std::shared_ptr<pappso::MsRunRetentionTime<QString>> msp_msrunRetentionTime;
};
