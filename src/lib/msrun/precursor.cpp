/**
 * \file  precursor.cpp
 * \date 8 juin 2010
 * \author Edlira Nano
 */

#include "precursor.h"
#include <pappsomspp/pappsoexception.h>
#include <QObject>

Precursor::Precursor(std::size_t spectrum_index,
                     double intensity,
                     pappso::XicCoordSPtr xic_coord)
  : m_spectrumIndex(spectrum_index), m_intensity(intensity)
{
  msp_xicCoord = xic_coord;

  if(msp_xicCoord.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("xic_coord == nullptr\nindex=%1 %2 %3 %4")
          .arg(spectrum_index)
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
}

Precursor::~Precursor()
{
}

mcq_double
Precursor::getIntensity() const
{
  return m_intensity;
}

std::size_t
Precursor::getSpectrumIndex() const
{
  qDebug() << " m_spectrumIndex=" << m_spectrumIndex;
  return m_spectrumIndex;
}

const pappso::XicCoordSPtr &
Precursor::getXicCoordSPtr() const
{
  qDebug();
  return msp_xicCoord;
}
