/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file  precursor.h
 * \date 8 juin 2010
 * \author Edlira Nano
 */

#pragma once

#include "../../config.h"
#include <pappsomspp/msrun/xiccoord/xiccoord.h>
#include <memory>
#include "../../exportimportconfig.h"


class Precursor;

typedef std::shared_ptr<Precursor> PrecursorSPtr;
typedef std::shared_ptr<const Precursor> PrecursorCstSPtr;

/**
 * \class Precursor
 * \brief A Precursor object represents the parent/precursor of the MS level 2
 * scan with number _scan_num in the mzxml file.

 * In mzxml files, MS scans of level 2 have each a precursor object
 * representing their MS level 1 parent/precursor. A Precursor object has :
 * - a scan number
 * - a retention time value
 * - an intensity value
 * - an mz value
 * - Xic coordinates
 */

class MCQ_LIB_DECL Precursor
{

  public:
  Precursor(std::size_t spectrum_index,
            double intensity,
            pappso::XicCoordSPtr xic_coord);

  virtual ~Precursor();

  double getIntensity() const;

  std::size_t getSpectrumIndex() const;

  const pappso::XicCoordSPtr &getXicCoordSPtr() const;

  private:
  ///< MS level 2 scan number whome this Precursor is parent
  std::size_t m_spectrumIndex=0;
  /// intensity of this precursor
  double m_intensity=0;
  /// Xic coordinates
  pappso::XicCoordSPtr msp_xicCoord = nullptr;
};
