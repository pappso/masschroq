/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionbase.cpp
 * \date January 06, 2017
 * \author Olivier Langella
 */

#include "alignedpeakcollectionbase.h"
#include "../../mcqsession.h"
#include "../quantificator.h"
#include "alignedpeakcollectionmap.h"
#include "alignedpeakcollectionondisk.h"

AlignedPeakCollectionBase::AlignedPeakCollectionBase()
{
}

AlignedPeakCollectionBase::~AlignedPeakCollectionBase()
{
}

AlignedPeakCollectionBase::AlignedPeakCollectionBase(
  const AlignedPeakCollectionBase &other [[maybe_unused]])
{
}

AlignedPeakCollectionBaseSp
AlignedPeakCollectionBase::newInstance(const QuantiItemBase *p_quanti_item_base)
{

  if(McqSession::getInstance().getUnassignedPeaksOnDisk())
    {
      // return
      // std::make_shared<AlignedPeakCollectionRocksDb>(AlignedPeakCollectionRocksDb(p_quanti_item_base));
      return std::make_shared<AlignedPeakCollectionOnDisk>(
        AlignedPeakCollectionOnDisk(p_quanti_item_base));
    }
  else
    {
      return std::make_shared<AlignedPeakCollectionMap>(
        AlignedPeakCollectionMap());
    }
}
