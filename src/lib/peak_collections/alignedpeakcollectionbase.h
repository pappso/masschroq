
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionbase.h
 * \date January 06, 2017
 * \author Olivier Langella
 * \brief handles a collection of aligned peaks per MSruns
 */

#ifndef ALIGNEDPEAKCOLLECTIONBASE_H
#define ALIGNEDPEAKCOLLECTIONBASE_H

#include "../msrun/msrun.h"
#include "../xic/alignedxicpeak.h"
#include <memory>

class QuantiItemBase;

class AlignedPeakCollectionBase;
typedef std::shared_ptr<AlignedPeakCollectionBase> AlignedPeakCollectionBaseSp;

class AlignedPeakCollectionBase
{
  public:
  AlignedPeakCollectionBase();
  AlignedPeakCollectionBase(const AlignedPeakCollectionBase &other);
  virtual ~AlignedPeakCollectionBase();

  static AlignedPeakCollectionBaseSp
  newInstance(const QuantiItemBase *p_quanti_item_base);
  virtual void add(const Msrun *p_current_msrun,
                   AlignedXicPeakSp &xic_peak_sp)   = 0;
  virtual void clear(const Msrun *p_current_msrun)  = 0;
  virtual std::vector<const Msrun *> getMsRunList() = 0;
  virtual std::vector<AlignedXicPeakSp> &
  getMsRunAlignedPeakList(const Msrun *p_msrun) = 0;

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun) = 0;
};

#endif // ALIGNEDPEAKCOLLECTIONBASE_H
