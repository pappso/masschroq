
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionmap.cpp
 * \date January 06, 2017
 * \author Olivier Langella
 */

#include "alignedpeakcollectionmap.h"

AlignedPeakCollectionMap::AlignedPeakCollectionMap()
{
}

AlignedPeakCollectionMap::~AlignedPeakCollectionMap()
{
}

AlignedPeakCollectionMap::AlignedPeakCollectionMap(
  const AlignedPeakCollectionMap &other)
  : AlignedPeakCollectionBase(other)
{
  _map_post_matched_aligned_peaks = other._map_post_matched_aligned_peaks;
}

void
AlignedPeakCollectionMap::add(const Msrun *p_current_msrun,
                              AlignedXicPeakSp &xic_peak_sp)
{
  qDebug() << "AlignedPeakCollectionMap::add begin";
  std::vector<AlignedXicPeakSp> vec_peak;
  vec_peak.push_back(xic_peak_sp);
  std::pair<std::map<const Msrun *, std::vector<AlignedXicPeakSp>>::iterator,
            bool>
    ret = _map_post_matched_aligned_peaks.insert(
      std::pair<const Msrun *, std::vector<AlignedXicPeakSp>>(p_current_msrun,
                                                              vec_peak));
  if(ret.second == false)
    {
      ret.first->second.push_back(xic_peak_sp);
    }

  qDebug() << "AlignedPeakCollectionMap::add end";
}

void
AlignedPeakCollectionMap::clear(const Msrun *p_current_msrun)
{
  _map_post_matched_aligned_peaks.erase(p_current_msrun);
}
std::vector<const Msrun *>
AlignedPeakCollectionMap::getMsRunList()
{
  std::vector<const Msrun *> retval;
  for(auto const &element : _map_post_matched_aligned_peaks)
    {
      retval.push_back(element.first);
    }
  return retval;
}
std::vector<AlignedXicPeakSp> &
AlignedPeakCollectionMap::getMsRunAlignedPeakList(const Msrun *p_msrun)
{
  return _map_post_matched_aligned_peaks.at(p_msrun);
}

void
AlignedPeakCollectionMap::endMsrunQuantification(const Msrun *p_current_msrun
                                                 [[maybe_unused]])
{
}
