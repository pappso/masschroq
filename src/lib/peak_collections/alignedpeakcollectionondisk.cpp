
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionondisk.cpp
 * \date January 06, 2017
 * \author Olivier Langella
 * \brief handles a collection of aligned peaks per MSruns on disk
 */

#include "alignedpeakcollectionondisk.h"

#include "../../mcqsession.h"
#include "../consoleout.h"
#include "../mass_chroq.h"
#include "../quanti_items/quantiItemBase.h"

AlignedPeakCollectionOnDisk::AlignedPeakCollectionOnDisk(
  const QuantiItemBase *p_quanti_item_base)
{
  _peak_collection_file.setFileName(
    McqSession::getInstance().getSessionTmpDirName() + "/" +
    QString("peak_collection_%1").arg(p_quanti_item_base->getMzId()));
  if(_peak_collection_file.exists())
    {
      throw mcqError(
        QString("error in "
                "AlignedPeakCollectionOnDisk::AlignedPeakCollectionOnDisk "
                ":\ntemporary file %1 already exists")
          .arg(_peak_collection_file.fileName()));
    }
  if(_peak_collection_file.open(QIODevice::WriteOnly))
    {
    }
  else
    {
      throw mcqError(
        QString("error in "
                "AlignedPeakCollectionOnDisk::AlignedPeakCollectionOnDisk "
                ":\nunable to open file %1")
          .arg(_peak_collection_file.fileName()));
    }
  _peak_collection_file.close();
}
AlignedPeakCollectionOnDisk::AlignedPeakCollectionOnDisk(
  const AlignedPeakCollectionOnDisk &other)
  : AlignedPeakCollectionBase(other)
{
  _peak_collection_file.setFileName(other._peak_collection_file.fileName());
}

AlignedPeakCollectionOnDisk::~AlignedPeakCollectionOnDisk()
{
  if(_peak_collection_file.isOpen())
    {
      _peak_collection_file.close();
    }
  _peak_collection_file.remove();
}

void
AlignedPeakCollectionOnDisk::add(const Msrun *p_current_msrun,
                                 AlignedXicPeakSp &xic_peak_sp)
{
  qDebug() << "AlignedPeakCollectionOnDisk::add begin";
  if(_p_msrun_list.size() > 0)
    {
      if(_p_msrun_list.back() == p_current_msrun)
        {
        }
      else
        {
          if(std::find(_p_msrun_list.begin(),
                       _p_msrun_list.end(),
                       p_current_msrun) != _p_msrun_list.end())
            {
              throw mcqError(
                QString("error in AlignedPeakCollectionOnDisk::add "
                        ":\nMS run already exists %1")
                  .arg(p_current_msrun->getMsRunIdCstSPtr()->getXmlId()));
            }
          _p_msrun_list.push_back(p_current_msrun);
        }
    }
  else
    {
      _p_msrun_list.push_back(p_current_msrun);
    }
  _tmp_aligned_peak_list.push_back(xic_peak_sp);

  qDebug() << "AlignedPeakCollectionOnDisk::add end";
}

void
AlignedPeakCollectionOnDisk::clear(const Msrun *p_current_msrun
                                   [[maybe_unused]])
{
  _tmp_aligned_peak_list.clear();
}

std::vector<const Msrun *>
AlignedPeakCollectionOnDisk::getMsRunList()
{
  return _p_msrun_list;
}
std::vector<AlignedXicPeakSp> &
AlignedPeakCollectionOnDisk::getMsRunAlignedPeakList(const Msrun *p_msrun)
{
  qDebug() << "AlignedPeakCollectionOnDisk::getMsRunAlignedPeakList begin";
  _tmp_aligned_peak_list.clear();
  _peak_collection_file.open(QIODevice::ReadOnly);
  QDataStream instream(&_peak_collection_file);
  QString msrun_id;
  quint32 vector_size;

  while(!instream.atEnd())
    {
      instream >> msrun_id;
      instream >> vector_size;

      for(quint32 i = 0; i < vector_size; i++)
        {
          pappso::TracePeak peak;
          AlignedXicPeak mcq_peak(peak, p_msrun);
          if(mcq_peak.unserialize(&instream))
            {
              if(msrun_id == p_msrun->getMsRunIdCstSPtr()->getXmlId())
                {
                  _tmp_aligned_peak_list.push_back(
                    std::make_shared<const AlignedXicPeak>(mcq_peak));
                }
            }
        }
      if(instream.status() != QDataStream::Ok)
        {
          throw mcqError(
            QString(
              "error in "
              "AlignedPeakCollectionOnDisk::getMsRunAlignedPeakList "
              ":\nread datastream failed on %1 with QDataStream status=%2")
              .arg(_peak_collection_file.fileName())
              .arg(instream.status()));
        }
    }

  _peak_collection_file.close();
  qDebug() << "AlignedPeakCollectionOnDisk::getMsRunAlignedPeakList end "
              "_tmp_aligned_peak_list.size()="
           << _tmp_aligned_peak_list.size();
  return _tmp_aligned_peak_list;
}

void
AlignedPeakCollectionOnDisk::endMsrunQuantification(
  const Msrun *p_current_msrun)
{
  qDebug() << "AlignedPeakCollectionOnDisk::endMsrunQuantification begin";
  // flush tmp vector content in file
  if(_tmp_aligned_peak_list.size() > 0)
    {
      if(_p_msrun_list.back() != p_current_msrun)
        {
          throw mcqError(
            QString("error in "
                    "AlignedPeakCollectionOnDisk::endMsrunQuantification "
                    ":\n_p_msrun_list.back() != p_current_msrun %1!=2")
              .arg(_p_msrun_list.back()->getMsRunIdCstSPtr()->getXmlId())
              .arg(p_current_msrun->getMsRunIdCstSPtr()->getXmlId()));
        }
      /*
      if (!_peak_collection_file.exists()) {
          throw mcqError(QString("error in
      AlignedPeakCollectionOnDisk::endMsrunQuantification :\ntemporary file %1
      does not exists").arg(_peak_collection_file.fileName()));
      }*/
      _peak_collection_file.open(QIODevice::Append);
      QDataStream outstream(&_peak_collection_file);
      outstream << p_current_msrun->getMsRunIdCstSPtr()->getXmlId();
      outstream << (quint32)_tmp_aligned_peak_list.size();
      for(AlignedXicPeakSp xic_peak_sp : _tmp_aligned_peak_list)
        {
          xic_peak_sp->serialize(&outstream);
        }
      _peak_collection_file.close();
    }
  _tmp_aligned_peak_list.clear();

  qDebug() << "AlignedPeakCollectionOnDisk::endMsrunQuantification end";
}
