/**
 * \file isotope_label.cpp
 * \date 12 févr. 2010
 * \author Olivier langella
 */

#include "isotope_label.h"

#include "../mcq_error.h"
#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptide.h>

IsotopeLabelModification::IsotopeLabelModification()
{
}

IsotopeLabelModification::~IsotopeLabelModification()
{
}

void
IsotopeLabelModification::setAA(const QString &aa)
{
  //[A,R,N,D,C,E,Q,G,H,I,L,K,M,F,P,S,T,W,Y,V]
  // Nter, Cter
  QRegularExpression match_aa("^[A,R,N,D,C,E,Q,G,H,I,L,K,M,F,P,S,T,W,Y,V]|Nter|Cter$");

  if(!aa.contains(match_aa))
    {
      throw mcqError(
        QObject::tr("ERROR in IsotopeLabelModification::setAA :\n %1 is not a "
                    "valid amino acid or Nter, Cter position")
          .arg(aa));
    }
  _at = aa;
}

void
IsotopeLabelModification::setPappsoModification(
  pappso::AaModificationP modification)
{
  _pappso_modification = modification;

  if(_pappso_modification != nullptr)
    {
      /*
        if (!pappso::MassRange(_mass,
        pappso::Precision::getDaltonInstance(0.001)).contains(_pappso_modification->getMass()))
        {

            throw mcqError(QObject::tr("Error in
        IsotopeLabelModification::setPappsoModification :\n the PSIMOD
        modification mass (%1 %2) differs from the one expected (%3)")
                           .arg(_pappso_modification->getAccession()).arg(QString::number(_pappso_modification->getMass(),
        'g', 8)).arg(QString::number(_mass, 'g', 8)));

        }
      */
      _mass = _pappso_modification->getMass();
    }
}

void
IsotopeLabelModification::addModificationTag(pappso::Peptide &peptide) const
{
  std::vector<unsigned int> position_list;
  if(_at == "Nter")
    {
      position_list.push_back(0);
    }
  else if(_at == "Cter")
    {
      position_list.push_back(peptide.size() - 1);
    }
  else
    {
      position_list = peptide.getAaPositionList(_at[0].toLatin1());
    }

  pappso::AaModificationP modification = _pappso_modification;
  if(modification == nullptr)
    {
      modification = pappso::AaModification::getInstanceCustomizedMod(_mass);
    }

  for(auto &&position : position_list)
    {
      peptide.addAaModification(modification, position);
    }
}

IsotopeLabel::IsotopeLabel()
{
}

IsotopeLabel::~IsotopeLabel()
{
  std::vector<const IsotopeLabelModification *>::const_iterator itmod;
  for(itmod = _v_p_mod.begin(); itmod != _v_p_mod.end(); ++itmod)
    {
      delete(*itmod);
    }
}

void
IsotopeLabel::setXmlId(const QString &id)
{
  _id = id;
}

const QString &
IsotopeLabel::getXmlId() const
{
  return (_id);
}

mcq_double
IsotopeLabel::getMassDelta(const QString &sequence) const
{
  mcq_double mass = 0;
  std::vector<const IsotopeLabelModification *>::const_iterator itmod;
  for(itmod = _v_p_mod.begin(); itmod != _v_p_mod.end(); ++itmod)
    {
      if((*itmod)->getAA() == "Cter")
        {
          mass += (*itmod)->getMass();
        }
      if((*itmod)->getAA() == "Nter")
        {
          mass += (*itmod)->getMass();
        }
      else
        {
          mass += (((*itmod)->getMass()) * sequence.count((*itmod)->getAA()));
        }
    }
  return (mass);
}

void
IsotopeLabel::addIsotopeLabelModification(const IsotopeLabelModification *p_mod)
{
  _v_p_mod.push_back(p_mod);
}

pappso::PeptideSp
IsotopeLabel::getPeptideSpTagged(const pappso::PeptideSp &peptide_sp) const
{
  pappso::Peptide peptide(*peptide_sp.get());
  std::vector<const IsotopeLabelModification *>::const_iterator itmod;
  for(itmod = _v_p_mod.begin(); itmod != _v_p_mod.end(); ++itmod)
    {
      (*itmod)->addModificationTag(peptide);
    }

  return (peptide.makePeptideSp());
}
