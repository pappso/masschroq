/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file isotope_label.h
 * \date 12 févr. 2010
 * \author Olivier langella
 */

#pragma once

#include "../../config.h"
#include "../../exportimportconfig.h"
#include "QString"
#include <pappsomspp/amino_acid/aamodification.h>
#include <vector>

/**
 * \class IsotopeLabelModification
 * \brief Class representing the label modifications of an isotopo

 */

class MCQ_LIB_DECL IsotopeLabelModification
{

  public:
  IsotopeLabelModification();
  virtual ~IsotopeLabelModification();

  void setAA(const QString &aa);

  const QString &
  getAA() const
  {
    return (_at);
  }

  void
  setMass(mcq_double mass)
  {
    _mass = mass;
  }

  void setPappsoModification(pappso::AaModificationP modification);

  pappso::AaModificationP
  getPappsoModification() const
  {
    return _pappso_modification;
  };

  mcq_double
  getMass() const
  {
    return (_mass);
  }

  void addModificationTag(pappso::Peptide &peptide) const;

  private:
  mcq_double _mass;
  QString _at;
  pappso::AaModificationP _pappso_modification = nullptr;
};

/**
 * \class IsotopeLabel
 * \brief Class representing an isotope

 */
class MCQ_LIB_DECL IsotopeLabel
{

  public:
  IsotopeLabel();
  virtual ~IsotopeLabel();

  void setXmlId(const QString &id);
  const QString &getXmlId() const;

  void addIsotopeLabelModification(const IsotopeLabelModification *p_mod);

  mcq_double getMassDelta(const QString &sequence) const;
  pappso::PeptideSp
  getPeptideSpTagged(const pappso::PeptideSp &peptide_sp) const;

  private:
  QString _id;

  std::vector<const IsotopeLabelModification *> _v_p_mod;
};
