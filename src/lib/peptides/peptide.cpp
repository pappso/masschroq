/**
 * \file peptide.cpp
 *
 * \date Created on: 27 oct. 2009
 * \author langella
 */

#include "peptide.h"
#include "../msrun/ms_run_hash_group.h"
#include "../share/utilities.h"
#include "../msrun/msrun.h"


#include <set>

pappso::PrecisionPtr Peptide::_set_mh_precision =
  pappso::PrecisionFactory::getPpmInstance(100);

Peptide::Peptide(const QString &peptide_xml_id)
  : _peptide_xml_id(peptide_xml_id)
{
}

Peptide::~Peptide()
{
  std::vector<PeptideObservedIn *>::iterator it;
  for(it = _observed_in_list.begin(); it != _observed_in_list.end(); it++)
    {
      delete(*it);
    }
  _observed_in_list.clear();
}

const QString
Peptide::getSequence() const
{
  return _peptide_sp.get()->getSequence();
}


PeptideCstSPtr
Peptide::getOriginalPeptidePointer() const
{
  return nullptr;
}

mcq_double
Peptide::getMass() const
{
  return (_peptide_sp.get()->getMass());
}

mcq_double
Peptide::getMz(unsigned int z) const
{
  return (_peptide_sp.get()->getMz(z));
}

const QString &
Peptide::getMods() const
{
  return (_mods);
}

void
Peptide::printInfos(QTextStream &out) const
{
  out << "peptide : " << _peptide_xml_id << Qt::endl;
  out << "_sequence = " << _peptide_sp.get()->getSequence() << Qt::endl;
  out << "_mass = " << _peptide_sp.get()->getMass() << Qt::endl;
}

void
Peptide::addProtein(const Protein *p_protein)
{
  _v_p_protein.push_back(p_protein);
}

void
Peptide::setPappsoPeptideSp(const pappso::PeptideSp &peptide)
{
  _peptide_sp = peptide;
}

void
Peptide::setPeptideRtSp(PeptideRtSp peptide_rt_sp)
{
  _peptide_rt_sp = peptide_rt_sp;
}

void
Peptide::setMods(const QString &mods)
{
  _mods = mods;
}

void
Peptide::setMh(mcq_double mh)
{
  // only check that the given mh is ok with peptide mass
  pappso::pappso_double mass = mh - pappso::MHPLUS;
  if(pappso::MzRange(_peptide_sp.get()->getMass(), _set_mh_precision)
       .contains(mass))
    {
      // OK
    }
  else
    {
      pappso::pappso_double delta = mass - _peptide_sp.get()->getMass();

      pappso::Peptide pappso_peptide(*_peptide_sp.get());

      pappso::AaModificationP modification =
        pappso::AaModification::getInstanceCustomizedMod(delta);

      pappso_peptide.addAaModification(modification, 0);

      _peptide_sp = pappso_peptide.makePeptideSp();
    }
  if(!pappso::MzRange(_peptide_sp.get()->getMass(), _set_mh_precision)
        .contains(mass))
    {

      throw mcqError(
        QObject::tr("Error in Peptide::setMh :\ntheoretical computed mass of "
                    "peptide %3 (%1) is different from given mass %2 (mh=%4)")
          .arg(QString::number(_peptide_sp.get()->getMass(), 'g', 8))
          .arg(QString::number(mass, 'g', 8))
          .arg(_peptide_sp.get()->toAbsoluteString())
          .arg(QString::number(mh, 'g', 8)));
    }
  //_mass = mh - MHPLUS;
}

/// creates a new PeptideObservedIn object ans adds it to the
///_observed_in_list vector
void
Peptide::observed_in(Msrun *p_msrun,
                     std::size_t scan_num,
                     unsigned int z,
                     bool is_spectrum_index)
{
  qDebug();
  PeptideObservedIn *p_obs =
    new PeptideObservedIn(this, p_msrun, scan_num, z, is_spectrum_index);
  _observed_in_list.push_back(p_obs);
  qDebug();
}

/// creates a new vector containing the PeptideObservedIn objects
/// observed in the given group
// Attention : returns a pointer to a new vector. When this function
// is called be careful to delete it after use
std::vector<PeptideObservedIn *> *
Peptide::getObservedInGroup(const msRunHashGroup &group) const
{

  std::vector<PeptideObservedIn *> *v_observed_in_group =
    new std::vector<PeptideObservedIn *>();

  std::vector<PeptideObservedIn *>::const_iterator itobslist;

  for(itobslist = _observed_in_list.begin();
      itobslist != _observed_in_list.end();
      ++itobslist)
    {
      if(group.containsMsRun((*itobslist)->getPmsRun()))
        {
          v_observed_in_group->push_back(*itobslist);
        }
    }
  return v_observed_in_group;
}

/// returns true if this Peptide is observed in the given group
bool
Peptide::isObservedIn(const msRunHashGroup &group) const
{
  auto it = std::find_if(
    _observed_in_list.begin(),
    _observed_in_list.end(),
    [group](const PeptideObservedIn *peptide_observed_in) {
      return (group.containsMsRun(peptide_observed_in->getPmsRun()));
    });
  if(it == _observed_in_list.end())
    return false;
  return true;
}

/// returns true if this Peptide is observed in the given msRun
bool
Peptide::isObservedIn(const Msrun *msrun) const
{

  auto it = std::find_if(_observed_in_list.begin(),
                         _observed_in_list.end(),
                         [msrun](const PeptideObservedIn *peptide_observed_in) {
                           return (peptide_observed_in->getPmsRun() == msrun);
                         });
  if(it == _observed_in_list.end())
    return false;
  return true;
}

/// returns true if this Peptide is observed in the given msRun ID
bool
Peptide::isObservedIn(const QString &msrun_id) const
{

  auto it =
    std::find_if(_observed_in_list.begin(),
                 _observed_in_list.end(),
                 [msrun_id](const PeptideObservedIn *peptide_observed_in) {
                   return (peptide_observed_in->getPmsRun()
                             ->getMsRunIdCstSPtr()
                             .get()
                             ->getXmlId() == msrun_id);
                 });
  if(it == _observed_in_list.end())
    return false;
  return true;
}

/**
   calculates the best rt (the one corresponding to the biggest intensity)
   really observed in the given p_msrun for this peptide. If the peptide
   is not observed in p_msrun, returns -1
*/
mcq_double
Peptide::getObservedBestRtForMsRun(const Msrun *p_msrun,
                                   const msRunHashGroup &group) const
{
  // If peptide not observed at all in msrun, bestRt stays at -1
  mcq_double bestRt(-1);
  mcq_double bestIntensity(-1);
  mcq_double tmpIntensity(-1);

  std::vector<PeptideObservedIn *> *observed_in_group =
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;
  for(itobslist = observed_in_group->begin();
      itobslist != observed_in_group->end();
      ++itobslist)
    {
      if((*itobslist)->getPmsRun() == p_msrun)
        {
          tmpIntensity = (*itobslist)->getIntensity();
          if(tmpIntensity >= bestIntensity)
            {
              bestIntensity = tmpIntensity;
              bestRt        = (*itobslist)->getAlignedRt();
            }
        }
    }
  delete observed_in_group;
  return bestRt;
}

/**
   calculates the mean of the observed best rt-s of this peptide
   overall msruns of the given group.
   If the peptide is not observed in one of the msruns of the group,
   (so bestRt of this msrun is -1) it is not considered.
*/

mcq_double
Peptide::getMeanBestRt(const msRunHashGroup &group) const
{
  // stock all the msrun_ids this peptide is observed in in a set
  std::set<const Msrun *> s_msrun;
  std::vector<PeptideObservedIn *> *observed_in_group =
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;

  for(itobslist = observed_in_group->begin();
      itobslist != observed_in_group->end();
      ++itobslist)
    {
      const Msrun *c_msrun = (*itobslist)->getPmsRun();
      s_msrun.insert(c_msrun);
    }

  // for each msrunId in s_msrunId getObservedBestRtForMsRun(msrunId)
  // and put them in a hash map msrunId -> bestRt
  std::map<QString, mcq_double> map_msrunId_bestRt;
  std::set<const Msrun *>::const_iterator itset;

  for(itset = s_msrun.begin(); itset != s_msrun.end(); ++itset)
    {
      const QString msrunId = (*itset)->getMsRunIdCstSPtr().get()->getXmlId();
      mcq_double bestRt     = getObservedBestRtForMsRun(*itset, group);
      map_msrunId_bestRt[msrunId] = bestRt;
    }

  // calculate the mean of the bestRt-s in this hash map
  mcq_double sumRt(0);
  mcq_double nbRt(0);

  std::map<QString, mcq_double>::iterator itmap;
  for(itmap = map_msrunId_bestRt.begin(); itmap != map_msrunId_bestRt.end();
      ++itmap)
    {
      mcq_double bestRt = itmap->second;
      if(bestRt != -1)
        {
          sumRt += bestRt;
          nbRt++;
        }
    }
  mcq_double meanRt;
  if(nbRt != 0)
    {
      meanRt = sumRt / nbRt;
    }
  else
    {
      meanRt = -1;
    }
  delete observed_in_group;
  return meanRt;
}

/// gets the list of the charges this Peptide has been observed in a group
// Attention : returns a pointer to a new set. When this function is called,
// be careful to delete the set.
std::set<unsigned int> *
Peptide::getCharges(const msRunHashGroup &group) const
{
  std::set<unsigned int> *s_charges = new std::set<unsigned int>;
  std::vector<PeptideObservedIn *> *observed_in_group =
    getObservedInGroup(group);
  std::vector<PeptideObservedIn *>::const_iterator itobslist;

  for(itobslist = observed_in_group->begin();
      itobslist != observed_in_group->end();
      ++itobslist)
    {
      unsigned int z = (*itobslist)->getZ();
      s_charges->insert(z);
    }

  delete(observed_in_group);
  return s_charges;
}

const std::vector<PeptideObservedIn *> &
Peptide::getObservedInList() const
{
  return _observed_in_list;
}

const IsotopeLabel *
Peptide::getIsotopeLabel() const
{
  return nullptr;
}

const std::vector<const Protein *> &
Peptide::getProteinList() const
{
  return (_v_p_protein);
}

const pappso::PeptideSp &
Peptide::getPappsoPeptideSp() const
{
  return (_peptide_sp);
}

const PeptideRtSp &
Peptide::getPeptideRtSp() const
{
  return (_peptide_rt_sp);
}

pappso::XicCoordSPtr
Peptide::getXicCoordToExtractInMsRun(const Msrun *p_msrun) const
{
  qDebug() << "p_msrun=" << p_msrun
           << " _observed_in_list.size()=" << _observed_in_list.size();
  // get XIC coordinates of the most intense precursor
  pappso::XicCoordSPtr xic_coord_max_intensity_in_msrun_sp = nullptr;
  pappso::pappso_double intensity                          = -1;
  for(auto &observed_in : _observed_in_list)
    {
      qDebug() << "observed_in->getPmsRun()=" << observed_in->getPmsRun();
      if(observed_in->getPmsRun() == p_msrun)
        {
          if(intensity < observed_in->getIntensity())
            {
              qDebug() << " p_msrun->getPrecursor(observed_in->getScan())"
                       << observed_in->getScan()
                       << " peptide_id=" << getXmlId();
              Precursor *p_precursor = observed_in->getPrecursorSPtr().get();
              qDebug();
              if(p_precursor == nullptr)
                {

                  throw pappso::PappsoException(
                    QObject::tr("p_precursor == nullptr in msrun=%1 "
                                "index=%2\npeptideid=%3")
                      .arg(p_msrun->getMsRunIdCstSPtr().get()->getXmlId())
                      .arg(observed_in->getScan())
                      .arg(getXmlId()));
                }
              qDebug();
              xic_coord_max_intensity_in_msrun_sp =
                p_precursor->getXicCoordSPtr();
              qDebug();
              intensity = observed_in->getIntensity();
              qDebug();
            }
        }
    }

  return xic_coord_max_intensity_in_msrun_sp;
}

pappso::XicCoordSPtr
Peptide::getXicCoordToExtractInMsRunByCharge(const Msrun *p_msrun,
                                             unsigned int charge) const
{
  qDebug() << "p_msrun=" << p_msrun
           << " _observed_in_list.size()=" << _observed_in_list.size()
           << " charge=" << charge;
  // get XIC coordinates of the most intense precursor
  pappso::XicCoordSPtr xic_coord_max_intensity_in_msrun_sp = nullptr;
  pappso::pappso_double intensity                          = -1;
  for(auto &&observed_in : _observed_in_list)
    {
      qDebug() << "observed_in->getPmsRun()=" << observed_in->getPmsRun();
      if((observed_in->getPmsRun() == p_msrun) &&
         (observed_in->getZ() == charge))
        {
          if(intensity < observed_in->getIntensity())
            {
              qDebug() << " p_msrun->getPrecursor(observed_in->getScan())"
                       << observed_in->getScan();
              xic_coord_max_intensity_in_msrun_sp =
                observed_in->getPrecursorSPtr().get()->getXicCoordSPtr();
              intensity = observed_in->getIntensity();
              qDebug();
            }
        }
    }

  return xic_coord_max_intensity_in_msrun_sp;
}

pappso::XicCoordSPtr
Peptide::getBestIonMobilityXicCoordToExtractOverallMsRun(
  std::vector<MsrunSp> &list_msrun,
  const pappso::IonMobilityGrid *p_ion_mobility_grid,
  const pappso::MsRunId &targeted_msrun,
  unsigned int charge) const
{

  std::set<const Msrun *> msrun_sublist_observed_in_and_charge;
  qDebug() << "_observed_in_list.size()=" << _observed_in_list.size();

  for(auto &&observed_in : _observed_in_list)
    {
      if(observed_in->getZ() == charge)
        {
          const Msrun *p_msrun = observed_in->getPmsRun();
          auto it_found        = find_if(
            list_msrun.begin(), list_msrun.end(), [p_msrun](MsrunSp &msrun_sp) {
              if(msrun_sp.get() == p_msrun)
                return true;
              return false;
            });
          if(it_found != list_msrun.end())
            msrun_sublist_observed_in_and_charge.insert(
              observed_in->getPmsRun());
        }
    }

  if(msrun_sublist_observed_in_and_charge.size() == 0)
    {
      throw pappso::PappsoException(
        QObject::tr(
          "peptide never observed with this charge \npeptideid=%1 %2 %3 %4")
          .arg(getXmlId())
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  qDebug();
  std::vector<pappso::XicCoordSPtr> xic_coord_list;
  qDebug() << "msrun_sublist_observed_in_and_charge.size()="
           << msrun_sublist_observed_in_and_charge.size();
  for(auto p_msrun : msrun_sublist_observed_in_and_charge)
    {
      pappso::XicCoordSPtr xic_coord =
        getXicCoordToExtractInMsRunByCharge(p_msrun, charge);
      if(xic_coord.get() != nullptr)
        {
          if(p_ion_mobility_grid != nullptr)
            {
              xic_coord = p_ion_mobility_grid->translateXicCoordFromTo(
                *xic_coord.get(),
                *(p_msrun->getMsRunIdCstSPtr().get()),
                targeted_msrun);
            }

          if(xic_coord.get() != nullptr)
            {
              xic_coord_list.push_back(xic_coord);
            }
        }
    }
  qDebug() << "xic_coord_list.size()=" << xic_coord_list.size();
  if(xic_coord_list.size() == 0)
    {
      qDebug();
      return pappso::XicCoordSPtr();
    }

  if(xic_coord_list[0].get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("xic_coord_list[0].get() == nullptr\n%1 %2 %3 %4")
          .arg(_peptide_sp.get()->toString())
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  // compute the mean coordinate :
  pappso::XicCoordSPtr xic_coord_mean =
    xic_coord_list[0].get()->initializeAndClone();
  std::size_t i = 0;
  for(auto &xic_coord : xic_coord_list)
    {
      qDebug() << " xic_coord.get()->toString=" << xic_coord.get()->toString();
      if(i > 0)
        {
          xic_coord_mean = xic_coord_mean.get()->addition(xic_coord);
        }
      i++;
    }
  qDebug() << " xic_coord_mean.get()->toString="
           << xic_coord_mean.get()->toString();
  xic_coord_mean = xic_coord_mean.get()->divideBy(xic_coord_list.size());


  qDebug() << " xic_coord_mean.get()->toString="
           << xic_coord_mean.get()->toString();


  return xic_coord_mean;
}


pappso::XicCoordSPtr
Peptide::getBestXicCoordToExtractOverallMsRun(
  std::vector<MsrunSp> &list_msrun) const
{
  std::set<const Msrun *> msrun_sublist_observed_in;
  qDebug() << "_observed_in_list.size()=" << _observed_in_list.size();

  for(auto &&observed_in : _observed_in_list)
    {
      const Msrun *p_msrun = observed_in->getPmsRun();
      auto it_found        = find_if(
        list_msrun.begin(), list_msrun.end(), [p_msrun](MsrunSp &msrun_sp) {
          if(msrun_sp.get() == p_msrun)
            return true;
          return false;
        });
      if(it_found != list_msrun.end())
        msrun_sublist_observed_in.insert(observed_in->getPmsRun());
    }

  if(msrun_sublist_observed_in.size() == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("peptide never observed\npeptideid=%1 %2 %3 %4")
          .arg(getXmlId())
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  qDebug();
  std::vector<pappso::XicCoordSPtr> xic_coord_list;

  qDebug() << "msrun_sublist_observed_in.size()="
           << msrun_sublist_observed_in.size();
  for(auto p_msrun : msrun_sublist_observed_in)
    {
      pappso::XicCoordSPtr xic_coord = getXicCoordToExtractInMsRun(p_msrun);
      if(xic_coord.get() != nullptr)
        {
          xic_coord_list.push_back(xic_coord);
        }
    }
  qDebug() << "xic_coord_list.size()=" << xic_coord_list.size();
  if(xic_coord_list.size() == 0)
    {
      qDebug();
      return pappso::XicCoordSPtr();
    }

  if(xic_coord_list[0].get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("xic_coord_list[0].get() == nullptr\n%1 %2 %3 %4")
          .arg(_peptide_sp.get()->toString())
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  // compute the mean retention time coordinate :
  pappso::XicCoordSPtr xic_coord_mean =
    xic_coord_list[0].get()->initializeAndClone();
  std::size_t i = 0;
  for(auto &xic_coord : xic_coord_list)
    {
      qDebug() << " xic_coord.get()->toString=" << xic_coord.get()->toString();
      if(i > 0)
        {
          xic_coord_mean = xic_coord_mean.get()->addition(xic_coord);
        }
      i++;
    }
  qDebug() << " xic_coord_mean.get()->toString="
           << xic_coord_mean.get()->toString();
  xic_coord_mean = xic_coord_mean.get()->divideBy(xic_coord_list.size());


  qDebug() << " xic_coord_mean.get()->toString="
           << xic_coord_mean.get()->toString();


  return xic_coord_mean;
}
