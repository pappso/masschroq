/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file  peptide.h
 *
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#pragma once

#include <QString>
#include <iostream>
#include <set>
#include <vector>

#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>
#include "peptide_observed_in.h"
#include "peptidert.h"
#include "protein.h"

class msRunHashGroup;
class IsotopeLabel;

class Peptide;
class Msrun;

typedef std::shared_ptr<Peptide> PeptideSPtr;
typedef std::shared_ptr<const Peptide> PeptideCstSPtr;

/**
 * \class Peptide
 * \brief Represents a peptide.
 *
 * Each peptide has :
 * - an unique identifier (_peptide_xml_id)
 * - an unique amino-acids sequence (_sequence)
 * - a list of all the proteins it appears in (_v_p_protein)
 * - a list of all the msruns it has been observed in, of PeptideObservedIn
 * objects (_observed_in_list)
 */

class MCQ_LIB_DECL Peptide
{

  public:
  Peptide(const QString &peptide_xml_id);
  // Peptide(const Peptide &copy);
  virtual ~Peptide();

  const QString &
  getXmlId() const
  {
    return _peptide_xml_id;
  }

  virtual const IsotopeLabel *getIsotopeLabel() const;

  virtual const std::vector<const Protein *> &getProteinList() const;

  virtual void printInfos(QTextStream &out) const;

  virtual const pappso::PeptideSp &getPappsoPeptideSp() const;

  virtual const PeptideRtSp &getPeptideRtSp() const;

  virtual const QString &getMods() const;

  void addProtein(const Protein *p_protein);

  mcq_double getMass() const;

  void setMh(mcq_double mh);

  void setPappsoPeptideSp(const pappso::PeptideSp &peptide);
  void setPeptideRtSp(PeptideRtSp peptide_rt_sp);

  void setMods(const QString &mods);

  mcq_double getMz(unsigned int z) const;

  /**
   * \fn observedIn(const msRun *, const int scan_num,
   * unsigned int z) const
   * \brief create a new PeptideObservedIn(msrun, scan_num, z) object
   * and insert it in a the _observed_in_list member variable.
   *
   * This function is called when when the
   * <observed_in data="samp1" scan="531" z="2"/> tag in the XML
   * file is parsed
   */
  virtual void observed_in(Msrun *p_msrun,
                           std::size_t scan_num,
                           unsigned int z,
                           bool is_spectrum_index);

  /**
   * \fn isObservedIn(const msRunHashGroup &) const
   * \brief tell if the peptide is observed in a msrun group
   */
  virtual bool isObservedIn(const msRunHashGroup &group) const;

  /**
   * \fn isObservedIn(const msRun *) const
   * \brief tell if the peptide is observed in a msrun
   */
  virtual bool isObservedIn(const Msrun *msrun) const;

  /**
   * \fn isObservedIn(QString MsRunID) const
   * \brief tell if the peptide is observed in a msrunID
   */
  virtual bool isObservedIn(const QString &MsRunID) const;

  /**
   * \fn getObservedBestRtForMsRun(const msRun * p_msrun)
   *
   * \brief returns the best rt really observed in p_msrun
   * for this peptide. If the peptide is not observed in p_msrun,
   * returns -1
   */
  virtual mcq_double
  getObservedBestRtForMsRun(const Msrun *p_msrun,
                            const msRunHashGroup &group) const;

  /**
   * \fn getMeanBestRt()
   *
   * \brief returns the mean of the observed best rt-s of this peptide
   * overall msruns of the current group.
   *
   * If the peptide is not observed in one of the msruns of the group,
   * (bestRt of this msrun is -1) it is not considered.
   */
  virtual mcq_double getMeanBestRt(const msRunHashGroup &group) const;

  /**
   * \fn getObservedInGroup(const msRunHashGroup & group)
   *
   * \brief gets the list of the PeptideObservedIn objects
   * associated to this Peptide in group
   */
  virtual std::vector<PeptideObservedIn *> *
  getObservedInGroup(const msRunHashGroup &group) const;

  virtual const std::vector<PeptideObservedIn *> &getObservedInList() const;

  /**
   * \fn getCharges(const msRunHashGroup & group)
   *
   * \brief gets the list of the charges this Peptide has been
   * observed in group
   */
  virtual std::set<unsigned int> *getCharges(const msRunHashGroup &group) const;

  const QString getSequence() const;

  virtual PeptideCstSPtr getOriginalPeptidePointer() const;


  /** @brief get the best xic coordinate observed for this msrun
   *
   * returns nullptr if not observed in msrun
   */
  virtual pappso::XicCoordSPtr
  getXicCoordToExtractInMsRun(const Msrun *p_msrun) const;


  /** @brief get the best xic coordinate observed for this msrun for a given
   * charge
   *
   * returns nullptr if not observed in msrun
   */
  virtual pappso::XicCoordSPtr
  getXicCoordToExtractInMsRunByCharge(const Msrun *p_msrun,
                                      unsigned int charge) const;


  /** @brief get the best xic coordinate overall msrun
   *
   * suitable to compute the best possible retention time overall msruns
   *
   * @param list_msrun the list of msrun to take into account
   */
  virtual pappso::XicCoordSPtr
  getBestXicCoordToExtractOverallMsRun(std::vector<MsrunSp> &list_msrun) const;


  /** @brief get the best xic coordinate IM overall msrun, for the same ion
   * charge
   *
   * @param list_msrun the list of msrun to take into account
   * @param p_ion_mobility_grid ion mobility differences between runs
   * @param targeted_msrun targeted msrun identifier
   * @param charge targeted charge state
   */
  virtual pappso::XicCoordSPtr getBestIonMobilityXicCoordToExtractOverallMsRun(
    std::vector<MsrunSp> &list_msrun,
    const pappso::IonMobilityGrid *p_ion_mobility_grid,
    const pappso::MsRunId &targeted_msrun,
    unsigned int charge) const;

  protected:
  pappso::PeptideSp _peptide_sp;

  QString _mods;

  PeptideRtSp _peptide_rt_sp = nullptr;

  private:
  const QString _peptide_xml_id;

  std::vector<const Protein *> _v_p_protein;

  std::vector<PeptideObservedIn *> _observed_in_list;

  static pappso::PrecisionPtr _set_mh_precision;
};
