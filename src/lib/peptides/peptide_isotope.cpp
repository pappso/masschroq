/**
 * \file peptide_isotope.cpp
 * \date February 12, 2010
 * \author Olivier Langella
 */

#include "peptide_isotope.h"
#include "../mcq_error.h"
#include <QDebug>

PeptideIsotope::PeptideIsotope(PeptideCstSPtr p_peptide_original,
                               const IsotopeLabel *p_label)
  : Peptide(p_peptide_original->getXmlId()),
    msp_peptideOriginal(p_peptide_original),
    _p_label(p_label)
{

  _peptide_sp =
    p_label->getPeptideSpTagged(p_peptide_original->getPappsoPeptideSp());
  _peptide_rt_sp = p_peptide_original->getPeptideRtSp();

  /*
  pappso::Peptide
  pappso_peptide(*p_peptide_original->getPappsoPeptideSp().get());

  pappso::AaModificationP modification =
  pappso::AaModification::getInstanceCustomizedMod(p_label->getMassDelta(p_peptide_original->getPappsoPeptideSp().get()->getSequence()));

  pappso_peptide.addAaModification(modification, 0);

  _peptide_sp = pappso_peptide.makePeptideSp();
  */
}

PeptideIsotope::~PeptideIsotope()
{
}

PeptideCstSPtr
PeptideIsotope::getOriginalPeptidePointer() const
{
  return msp_peptideOriginal;
}

const IsotopeLabel *
PeptideIsotope::getIsotopeLabel() const
{
  return _p_label;
}

void
PeptideIsotope::observed_in(Msrun *p_msrun [[maybe_unused]],
                            std::size_t scan_num [[maybe_unused]],
                            unsigned int z [[maybe_unused]],
                            bool is_spectrum_index [[maybe_unused]])
{
  throw mcqError(
    QObject::tr("ERROR in PeptideIsotope::observed_in :\nobserved_in cannot "
                "be used for isotope peptide"));
}

bool
PeptideIsotope::isObservedIn(const msRunHashGroup &group) const
{
  //	qDebug() << "PeptideIsotope::isObservedIn()"
  //		<< _p_peptide_original->isObservedIn(group);
  return (msp_peptideOriginal->isObservedIn(group));
}

/// returns true if this Peptide is observed in the given msRun ID
bool
PeptideIsotope::isObservedIn(const QString &msRunID) const
{
  return (msp_peptideOriginal->isObservedIn(msRunID));
}

const QString &
PeptideIsotope::getMods() const
{
  return (msp_peptideOriginal->getMods());
}

std::vector<PeptideObservedIn *> *
PeptideIsotope::getObservedInGroup(const msRunHashGroup &group) const
{
  return (msp_peptideOriginal->getObservedInGroup(group));
}

std::set<unsigned int> *
PeptideIsotope::getCharges(const msRunHashGroup &group) const
{
  return (msp_peptideOriginal->getCharges(group));
}

mcq_double
PeptideIsotope::getObservedBestRtForMsRun(const Msrun *p_msrun,
                                          const msRunHashGroup &group) const
{
  return (msp_peptideOriginal->getObservedBestRtForMsRun(p_msrun, group));
}

mcq_double
PeptideIsotope::getMeanBestRt(const msRunHashGroup &group) const
{
  return (msp_peptideOriginal->getMeanBestRt(group));
}

const std::vector<const Protein *> &
PeptideIsotope::getProteinList() const
{
  return (msp_peptideOriginal->getProteinList());
}

void
PeptideIsotope::printInfos(QTextStream &out) const
{
  out << "peptide isotope : " << getXmlId() << Qt::endl;
  out << "_sequence = " << _peptide_sp.get()->getSequence() << Qt::endl;
  out << "_mass = " << _peptide_sp.get()->getMass() << Qt::endl;
}

const std::vector<PeptideObservedIn *> &
PeptideIsotope::getObservedInList() const
{
  return msp_peptideOriginal->getObservedInList();
}

const pappso::PeptideSp &
PeptideIsotope::getPappsoPeptideSp() const
{
  return _peptide_sp;
}

const PeptideRtSp &
PeptideIsotope::getPeptideRtSp() const
{
  return msp_peptideOriginal->getPeptideRtSp();
}

pappso::XicCoordSPtr
PeptideIsotope::getBestXicCoordToExtractOverallMsRun(
  std::vector<MsrunSp> &list_msrun_in_group) const
{
  return msp_peptideOriginal.get()->getBestXicCoordToExtractOverallMsRun(
    list_msrun_in_group);
}

pappso::XicCoordSPtr
PeptideIsotope::getBestIonMobilityXicCoordToExtractOverallMsRun(
  std::vector<MsrunSp> &list_msrun,
  const pappso::IonMobilityGrid *p_ion_mobility_grid,
  const pappso::MsRunId &targeted_msrun,
  unsigned int charge) const
{
  return msp_peptideOriginal.get()
    ->getBestIonMobilityXicCoordToExtractOverallMsRun(
      list_msrun, p_ion_mobility_grid, targeted_msrun, charge);
}


pappso::XicCoordSPtr
PeptideIsotope::getXicCoordToExtractInMsRun(const Msrun *p_msrun) const
{
  return msp_peptideOriginal.get()->getXicCoordToExtractInMsRun(p_msrun);
}
