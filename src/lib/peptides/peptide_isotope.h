/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peptide_isotope.h
 * \date February 12, 2010
 * \author Olivier Langella
 */

#pragma once

#include "isotope_label.h"
#include "peptide.h"

/**
 * \class PeptideIsotope
 * \brief Class representing an isotope of a peptide
 */

class MCQ_LIB_DECL PeptideIsotope : public Peptide
{

  public:
  PeptideIsotope(PeptideCstSPtr p_peptide_original,
                 const IsotopeLabel *p_label);

  virtual ~PeptideIsotope();

  virtual const IsotopeLabel *getIsotopeLabel() const override;

  virtual void observed_in(Msrun *p_msrun,
                           std::size_t scan_num,
                           unsigned int z,
                           bool is_spectrum_index) override;

  virtual bool isObservedIn(const msRunHashGroup &group) const override;
  virtual bool isObservedIn(const QString &msRunID) const override;

  virtual const QString &getMods() const override;

  virtual std::vector<PeptideObservedIn *> *
  getObservedInGroup(const msRunHashGroup &group) const override;

  virtual std::set<unsigned int> *
  getCharges(const msRunHashGroup &group) const override;

  virtual mcq_double
  getObservedBestRtForMsRun(const Msrun *p_msrun,
                            const msRunHashGroup &group) const override;

  virtual mcq_double getMeanBestRt(const msRunHashGroup &group) const override;

  virtual const std::vector<const Protein *> &getProteinList() const override;

  virtual void printInfos(QTextStream &out) const override;

  virtual const std::vector<PeptideObservedIn *> &
  getObservedInList() const override;

  virtual const pappso::PeptideSp &getPappsoPeptideSp() const override;

  virtual const PeptideRtSp &getPeptideRtSp() const override;

  virtual PeptideCstSPtr getOriginalPeptidePointer() const override;

  virtual pappso::XicCoordSPtr
  getXicCoordToExtractInMsRun(const Msrun *p_msrun) const override;

  virtual pappso::XicCoordSPtr getBestXicCoordToExtractOverallMsRun(
    std::vector<MsrunSp> &list_msrun) const override;

  virtual pappso::XicCoordSPtr getBestIonMobilityXicCoordToExtractOverallMsRun(
    std::vector<MsrunSp> &list_msrun,
    const pappso::IonMobilityGrid *p_ion_mobility_grid,
    const pappso::MsRunId &targeted_msrun,
    unsigned int charge) const override;

  private:
  PeptideCstSPtr msp_peptideOriginal;

  const IsotopeLabel *_p_label;
};
