/**
 * \file peptide_list.cpp
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#include "peptide_list.h"
#include "../msrun/ms_run_hash_group.h"
#include "peptide.h"
#include "peptidert.h"

PeptideList::PeptideList()
{
  // TODO Auto-generated constructor stub
}

PeptideList::~PeptideList()
{
}

/// given e peptide id, try to get it from the PeptideList if it is in,
/// return a NULL pointer otherwise
PeptideCstSPtr
PeptideList::getPeptide(const QString &pepid) const
{
  auto it = std::find_if(begin(), end(), [pepid](const PeptideSPtr &peptide) {
    return peptide.get()->getXmlId() == pepid;
  });
  if(it != end())
    {
      return *it;
    }
  return (nullptr);
}

/// tests if the PeptideList contains a given peptide or not
bool
PeptideList::containsPeptide(const Peptide *pep) const
{

  auto it = std::find_if(begin(), end(), [pep](const PeptideSPtr &peptide) {
    return peptide.get() == pep;
  });
  if(it != end())
    {
      return true;
    }
  return false;
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun group
const PeptideList
PeptideList::getPeptideListObservedInMsGroup(const msRunHashGroup &group) const
{
  PeptideList list_pep;
  for(auto peptide : *this)
    {
      if(peptide.get()->isObservedIn(group))
        {
          peptide->setPeptideRtSp(std::make_shared<PeptideRt>());
          list_pep.push_back(peptide);
        }
    }
  return (list_pep);
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun
const PeptideList
PeptideList::getPeptideListObservedInMsrun(const Msrun *msrun) const
{

  PeptideList list_pep;
  for(auto peptide : *this)
    {
      if(peptide.get()->isObservedIn(msrun))
        {
          list_pep.push_back(peptide);
        }
    }
  qDebug() << " list_pep.size()=" << list_pep.size();
  return (list_pep);
}
