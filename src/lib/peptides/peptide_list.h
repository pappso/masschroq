/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file peptide_list.h
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#pragma once

#include <QString>
#include <vector>
#include <memory>
#include "../../exportimportconfig.h"

class msRunHashGroup;
class Peptide;
typedef std::shared_ptr<Peptide> PeptideSPtr;
typedef std::shared_ptr<const Peptide> PeptideCstSPtr;
class Msrun;

/**
 * \class PeptideList
 * \brief This class is a vector of Peptide pointers (inherits from std::vector)
 */

class MCQ_LIB_DECL PeptideList : public std::vector<PeptideSPtr>
{
  public:
  PeptideList();
  virtual ~PeptideList();

  PeptideCstSPtr getPeptide(const QString &idname) const;

  bool containsPeptide(const Peptide *pep) const;

  const PeptideList
  getPeptideListObservedInMsGroup(const msRunHashGroup &group) const;

  const PeptideList getPeptideListObservedInMsrun(const Msrun *msrun) const;
};
