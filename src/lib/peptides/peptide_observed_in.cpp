/**
 * \file peptide_observed_in.c xpp
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#include "peptide_observed_in.h"
#include "../msrun/msrun.h"
#include "../mcq_error.h"
#include "peptide.h"

PeptideObservedIn::PeptideObservedIn(const Peptide *p_peptide,
                                     Msrun *p_msrun,
                                     std::size_t scan_num,
                                     unsigned int z,
                                     bool is_spectrum_index)
  : _p_peptide(p_peptide),
    _p_msrun(p_msrun),
    _scan_num(scan_num),
    _z(z),
    m_isSpectrumIndex(is_spectrum_index)
{
  if(is_spectrum_index)
    {
      msp_precursor = _p_msrun->getPrecursorBySpectrumIndex(scan_num);
    }
  else
    {
      msp_precursor = _p_msrun->getPrecursorByScanNumber(scan_num);
    }
  if(msp_precursor.get()->getXicCoordSPtr().get() == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("msp_precursor.get()->getXicCoordSPtr().get() == "
                    "nullptr\nindex=%1 %2 %3 %4")
          .arg(scan_num)
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  p_msrun->getMsRunRetentionTime().addPeptideAsSeamark(
    p_peptide->getXmlId(),
    msp_precursor.get()->getXicCoordSPtr().get()->rtTarget,
    msp_precursor.get()->getIntensity());
}

PeptideObservedIn::~PeptideObservedIn()
{
}

/// get the intensity of this observation by the scan_number
/// (the Precursor object with this scan number has it)
mcq_double
PeptideObservedIn::getIntensity() const
{
  return (msp_precursor.get()->getIntensity());
}

mcq_double
PeptideObservedIn::getRt()
{
  return (msp_precursor.get()->getXicCoordSPtr().get()->rtTarget);
}

mcq_double
PeptideObservedIn::getAlignedRt()
{
  return (_p_msrun->getAlignedRtByOriginalRt(
    msp_precursor.get()->getXicCoordSPtr().get()->rtTarget));
}

const Msrun *
PeptideObservedIn::getPmsRun() const
{
  return _p_msrun;
}

unsigned int
PeptideObservedIn::getZ() const
{
  return _z;
}

int
PeptideObservedIn::getScan() const
{
  return _scan_num;
}

const PrecursorSPtr &
PeptideObservedIn::getPrecursorSPtr() const
{
  return msp_precursor;
}
