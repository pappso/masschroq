/**
 * \file lib/petpides/peptideisopattern.cpp
 * \date 29/09/2021
 * \author Olivier Langella
 * \brief container to group together quanti items related to the same isotopic
 * pattern same peptide, all charge states, all possible istotopes
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "peptideisopattern.h"

PeptideIsoPattern::PeptideIsoPattern(const PeptideCstSPtr &peptide_sp)
{
  m_peptideCstSptr = peptide_sp;
}

PeptideIsoPattern::~PeptideIsoPattern()
{
}

void
PeptideIsoPattern::add(QuantiItemPeptideNaturalIsotopeSp quanti_item_sp
                       [[maybe_unused]])
{
}
