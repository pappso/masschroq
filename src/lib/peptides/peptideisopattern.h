/**
 * \file lib/petpides/peptideisopattern.h
 * \date 29/09/2021
 * \author Olivier Langella
 * \brief container to group together quanti items related to the same isotopic
 * pattern same peptide, all charge states, all possible istotopes
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <memory>
#include "peptide.h"
#include "../quanti_items/quantiitempeptidenaturalisotope.h"

#pragma once


class PeptideIsoPattern;

typedef std::shared_ptr<PeptideIsoPattern> PeptideIsoPatternSp;
/**
 * @brief container to keep together all isotopes and charge states describing
 * the pattern of a single peptide
 */
class PeptideIsoPattern
{
  public:
  /**
   * Default constructor
   */
  PeptideIsoPattern(const PeptideCstSPtr &peptide_sp);

  /**
   * Destructor
   */
  ~PeptideIsoPattern();


  void add(QuantiItemPeptideNaturalIsotopeSp quanti_item_sp);

  private:
  PeptideCstSPtr m_peptideCstSptr;
  std::vector<QuantiItemPeptideNaturalIsotopeSp> m_quantiItemList;
};
