
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidert.h"
#include "../mcq_error.h"
#include <QDebug>
#include <cmath>
#include <numeric>
#include <pappsomspp/processing/detection/tracepeak.h>
#include "../share/utilities.h"
#include "../msrun/msrun.h"


PeptideRt::PeptideRt()
{
}

PeptideRt::PeptideRt(const PeptideRt &other [[maybe_unused]])
{
}

PeptideRt::~PeptideRt()
{
}

const std::vector<unsigned int> &
PeptideRt::getCharges() const
{
  return _charges;
}

void
PeptideRt::addMsMsRtIntensity(const MsMsObservation &observation)
{
  if(std::isnan(observation.rt))
    {
      qDebug() << " PeptideRt::addMsMsRtIntensity std::isnan(_aligned_rt) "
               << _aligned_rt;
      throw mcqError(QObject::tr(
        "Error in PeptideRt::addMsMsRtIntensity std::isnan(_aligned_rt)"));
    }
  _msms_observation_list.push_back(observation);

  auto it = std::find_if(
    _charges.begin(), _charges.end(), [observation](unsigned int charge) {
      return (observation.z == charge);
    });
  if(it == _charges.end())
    {
      _charges.push_back(observation.z);
    }
}

bool
PeptideRt::isObservedIn(Msrun *_p_msrun) const
{
  auto it = std::find_if(_msms_observation_list.begin(),
                         _msms_observation_list.end(),
                         [_p_msrun](const MsMsObservation &observation) {
                           return (observation._p_msrun == _p_msrun);
                         });
  if(it == _msms_observation_list.end())
    {
      return false;
    }
  return true;
}

const std::vector<MsMsRtIntensity>
PeptideRt::getObservedRtList(const Msrun *_p_msrun) const
{

  std::vector<MsMsRtIntensity> obs_list;

  for(const MsMsObservation &observation : _msms_observation_list)
    {
      if(observation._p_msrun == _p_msrun)
        {
          MsMsRtIntensity ms_intensity;
          ms_intensity.copy(observation);
          obs_list.push_back(ms_intensity);
        }
    }

  return obs_list;
}
pappso::pappso_double
PeptideRt::getRtTarget(const Msrun *p_msrun,
                       McqMatchingMode matching_mode) const
{


  if((matching_mode == McqMatchingMode::real_or_mean) ||
     (matching_mode == McqMatchingMode::post_matching) ||
     (matching_mode == McqMatchingMode::no_matching))
    {
      auto it = std::find_if(_msrun_to_real_rt.begin(),
                             _msrun_to_real_rt.end(),
                             [p_msrun](const MsRunNotAlignedRt &rt) {
                               return (p_msrun == rt.p_msrun);
                             });

      if(it != _msrun_to_real_rt.end())
        {
          qDebug() << "PeptideRt::getRtTarget found " << it->rt;
          return (it->rt);
        }
    }
  else
    {

      throw mcqError(
        QObject::tr("Error in PeptideRt::getRtTarget unable "
                    "to compute retention time reference with mode %1")
          .arg(Utilities::toString(matching_mode)));
    }
  /*
if(matching_mode == McqMatchingMode::no_matching)
  {
    return (_aligned_rt);
  }*/

  if(_aligned_rt == -1)
    {
      return -1;
    }
  return (p_msrun->getOriginalRtByAlignedRt(_aligned_rt));
}

void
PeptideRt::computeRetentionTimeReferences(McqMatchingMode matching_mode)
{
  qDebug() << "begin ";
  // compute peptide retention time references :
  if((matching_mode == McqMatchingMode::real_or_mean) ||
     (matching_mode == McqMatchingMode::post_matching) ||
     (matching_mode == McqMatchingMode::no_matching))
    {
      // sort observations by ms runs, with the most intense last
      qDebug() << " sort " << _msms_observation_list.size();
      std::sort(_msms_observation_list.begin(),
                _msms_observation_list.end(),
                [](const MsMsObservation &a, const MsMsObservation &b) {
                  if(a._p_msrun == b._p_msrun)
                    {
                      return (a.intensity < b.intensity);
                    }
                  else
                    {
                      return (a._p_msrun < b._p_msrun);
                    }
                });

      qDebug() << " best observation mean ";
      // gest best observation for each mz in each msrun, compute the mean of
      // best observations inside an msrun MsMsObservation first(nullptr);
      std::vector<MsMsObservation>::iterator it_last =
        _msms_observation_list.begin();

      for(std::vector<MsMsObservation>::iterator it =
            _msms_observation_list.begin();
          it != _msms_observation_list.end();
          it++)
        {
          // for (auto && observation : _msms_observation_list) {
          // store best retention time in accumulator
          bool msrun_changed = (it_last->_p_msrun != it->_p_msrun);
          if(msrun_changed || (it_last->mz != it->mz))
            {
              // get the best observation for this mz in this msrun
            }
          if(msrun_changed)
            {
              // MS run changed :
              // we have the most intense observation for this msrun at last
              MsRunNotAlignedRt corresp(it_last->_p_msrun, it_last->rt);

              _msrun_to_real_rt.push_back(corresp);
            }
          it_last = it;
        }

      MsRunNotAlignedRt corresp(it_last->_p_msrun, it_last->rt);
      _msrun_to_real_rt.push_back(corresp);
    }
  else
    {
      throw mcqError(
        QObject::tr("Error in PeptideRt::computeRetentionTimeReferences unable "
                    "to compute retention time reference with mode %1")
          .arg(Utilities::toString(matching_mode)));
    }
  _aligned_rt = -1;

  if(matching_mode != McqMatchingMode::no_matching)
    {
      qDebug();
      std::vector<pappso::pappso_double> accumulator;
      for(auto &&corresp : _msrun_to_real_rt)
        {
          accumulator.push_back(
            corresp.p_msrun->getAlignedRtByOriginalRt(corresp.rt));
        }

      _aligned_rt =
        std::accumulate(accumulator.begin(), accumulator.end(), 0.0) /
        accumulator.size();

      qDebug() << "PeptideRt::computeRetentionTimeReferences end "
               << _aligned_rt << " " << accumulator.size() << " "
               << std::accumulate(accumulator.begin(), accumulator.end(), 0.0);
    }
}

void
PeptideRt::computePostMatchingTimeReferences()
{
  _accumulator.clear();
  if(_accumulator_rt_aligned_peak_reference.size() == 0)
    {
      _aligned_rt = -1;
      return;
    }
  std::sort(_accumulator_rt_aligned_peak_reference.begin(),
            _accumulator_rt_aligned_peak_reference.end(),
            [](const AlignedXicPeakSp a, const AlignedXicPeakSp b) {
              return (a.get()->getArea() > b.get()->getArea());
            });

  // keep the 90% most intense peaks
  unsigned int new_size = _accumulator_rt_aligned_peak_reference.size() * 0.9;
  if(new_size == 0)
    {
      new_size = 1;
    }
  // weighted mean : big areas better than little ones
  std::vector<pappso::pappso_double> w_list;
  for(unsigned int i = 0; i < new_size; i++)
    {
      pappso::pappso_double area =
        _accumulator_rt_aligned_peak_reference.at(i).get()->getArea();
      _accumulator.push_back(area * _accumulator_rt_aligned_peak_reference.at(i)
                                      .get()
                                      ->getMaxXicElement()
                                      .x);
      w_list.push_back(area);
    }

  _accumulator_rt_aligned_peak_reference.clear();
  _aligned_rt = std::accumulate(_accumulator.begin(), _accumulator.end(), 0.0) /
                std::accumulate(w_list.begin(), w_list.end(), 0.0);
  _accumulator.clear();
}
void
PeptideRt::postMatchingAlignedPeak(const AlignedXicPeak &aligned_peak)
{
  qDebug() << "PeptideRt::postMatchingAlignedPeak begin";
  QMutexLocker lock(&_mutex);
  //_accumulator.push_back(aligned_peak.getMaxXicElement().rt);

  _accumulator_rt_aligned_peak_reference.push_back(
    std::make_shared<const AlignedXicPeak>(aligned_peak));

  qDebug() << "PeptideRt::postMatchingAlignedPeak end";
}

pappso::pappso_double
PeptideRt::getPostMatchedRtTarget() const
{
  return _aligned_rt;
}

pappso::pappso_double
PeptideRt::getAlignedReferenceRt() const
{
  return _aligned_rt;
}
