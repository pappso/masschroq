
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../xic/alignedxicpeak.h"
#include <QMutex>
#include <memory>
#include <pappsomspp/types.h>
#include <vector>

class QuantiItemRtDeterminator;
class Msrun;

struct MsMsObservation
{
  MsMsObservation(const Msrun *p_msrun) : _p_msrun(p_msrun)
  {
  }
  MsMsObservation(const MsMsObservation &other) : _p_msrun(other._p_msrun)
  {
    rt        = other.rt;
    intensity = other.intensity;
    z         = other.z;
    mz        = other.mz;
  }
  pappso::pappso_double rt        = 0;
  pappso::pappso_double intensity = 0;
  unsigned int z                  = 0;
  pappso::pappso_double mz        = 0;
  const Msrun *_p_msrun           = nullptr;
};

struct MsMsRtIntensity
{
  pappso::pappso_double rt        = 0;
  pappso::pappso_double intensity = 0;
  unsigned int z                  = 0;
  void
  copy(const MsMsObservation &other)
  {
    rt        = other.rt;
    intensity = other.intensity;
    z         = other.z;
  };
};

class PeptideRt;
typedef std::shared_ptr<PeptideRt> PeptideRtSp;

class PeptideRt
{
  public:
  PeptideRt();
  PeptideRt(const PeptideRt &other);
  ~PeptideRt();
  void addMsMsRtIntensity(const MsMsObservation &observation);

  void computeRetentionTimeReferences(McqMatchingMode matching_mode);

  /** @brief return original retention time target (not aligned)
   *
   * @return rt_target gets a real observed retention time if possible, or a
   * composite retention time based on the mean of observed msrun
   */
  pappso::pappso_double getRtTarget(const Msrun *p_msrun,
                                    McqMatchingMode matching_mode) const;

  /** @brief get observed charge states for this peptide
   */
  const std::vector<unsigned int> &getCharges() const;
  
  const std::vector<MsMsRtIntensity>
  getObservedRtList(const Msrun *_p_msrun) const;
  bool isObservedIn(Msrun *_p_msrun) const;

  /** @brief keep trace of matched peak
   * required in post matching mode to compute the new retention time of
   * reference
   */
  void postMatchingAlignedPeak(const AlignedXicPeak &aligned_peak);
  void computePostMatchingTimeReferences();
  pappso::pappso_double getPostMatchedRtTarget() const;

  /** \brief get the aligned reference retention time for this Peptide
   * This gives the computed target reference time used by MassChroQ to find
   * peaks
   * This reference depends on the alignment method and the method used to
   * compute time references
   */
  pappso::pappso_double getAlignedReferenceRt() const;

  private:
  struct MsRunNotAlignedRt
  {
    MsRunNotAlignedRt(const Msrun *p_msrun, double rtin) : p_msrun(p_msrun), rt(rtin)
    {
    }
    const Msrun *p_msrun;
    pappso::pappso_double rt;
  };

  std::vector<MsMsObservation> _msms_observation_list;
  std::vector<MsRunNotAlignedRt> _msrun_to_real_rt;
  std::vector<pappso::pappso_double> _accumulator;
  std::vector<AlignedXicPeakSp> _accumulator_rt_aligned_peak_reference;
  std::vector<unsigned int> _charges;

  pappso::pappso_double _aligned_rt = -1;
  QMutex _mutex;
  pappso::pappso_double _temp_intensity = 0;
};
