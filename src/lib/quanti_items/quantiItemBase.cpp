/**
 * \file quantiItemBase.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemBase.h"
#include <QStringList>
#include <odsstream/calcwriterinterface.h>
#include <odsstream/tsvdirectorywriter.h>
#include "../quantificator.h"

#include "../../output/detectionresult.h"

QuantiItemBase::QuantiItemBase(bool trace_peptide_on,
                               pappso::XicCoordSPtr xic_coord)
{
  // this->setType();
  msp_xicCoord = xic_coord;

  _trace_peptide_on = trace_peptide_on;
}

QuantiItemBase::~QuantiItemBase()
{
}

bool
QuantiItemBase::isTracePeptideOn() const
{
  return _trace_peptide_on;
}

const PeptideCstSPtr
QuantiItemBase::getPeptide() const
{
  return nullptr;
}

PeptideRtSp
QuantiItemBase::getPeptideRtSp() const
{
  PeptideRtSp rtSp;
  return rtSp;
}
bool
QuantiItemBase::isTraceOn() const
{
  return (_trace_peptide_on);
}

double
QuantiItemBase::getMz() const
{
  if(msp_xicCoord == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in %1, %2, %3 :\n msp_xicCoord == nullptr")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  return (msp_xicCoord.get()->mzRange.getMz());
}

bool
QuantiItemBase::operator<(const QuantiItemBase &other) const
{
  const mcq_double mz       = this->getMz();
  const mcq_double mz_other = other.getMz();
  return (mz < mz_other);
}

void
QuantiItemBase::printInfos(QTextStream &out) const
{
  out << "Mz quantification item :" << Qt::endl;
  out << "_mz = " << this->getMz() << Qt::endl;
}

const QString
QuantiItemBase::getQuantiItemId() const
{
  return QString("");
}

const QString
QuantiItemBase::getMzId() const
{
  return (QString::number(getMz(), 'f', 5));
}

void
QuantiItemBase::writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const
{
  _output_stream->writeAttribute("mz", QString::number(getMz(), 'f', 0));
}

void
QuantiItemBase::fillPeptideInfoLine(PeptideInfoLine &peptide_line
                                    [[maybe_unused]]) const
{
}

void
QuantiItemBase::writeOdsPeptideLine(CalcWriterInterface &writer
                                    [[maybe_unused]]) const
{
}

void
QuantiItemBase::writeOdsComparHeaderLine(CalcWriterInterface &writer) const
{

  //_p_writer->writeCell("peptide");
  writer.writeEmptyCell();
  //_p_writer->writeCell("m/z");
  writer.writeCell(getMz());
  //_p_writer->writeCell("rt reference");
  writer.writeEmptyCell();
  //_p_writer->writeCell("z");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope number");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope rank");
  writer.writeEmptyCell();
  // isotope ratio
  writer.writeEmptyCell();
  //_p_writer->writeCell("sequence");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope");
  writer.writeEmptyCell();
  //_p_writer->writeCell("mods");
  writer.writeEmptyCell();
  //_p_writer->writeCell("proteins");
  writer.writeEmptyCell();
}

pappso::PeptideNaturalIsotopeAverageSp
QuantiItemBase::getPeptideNaturalIsotopeAverageSp() const
{
  pappso::PeptideNaturalIsotopeAverageSp emptySp;
  return emptySp;
}

XicTraceBase *
QuantiItemBase::newXicTrace(CalcWriterInterface *p_writer [[maybe_unused]],
                            const DetectionResult &detection_result
                            [[maybe_unused]]) const
{
  return nullptr;
}

QString
QuantiItemBase::getTraceBaseName(const QString &prefix [[maybe_unused]],
                                 const QString &quantificator_xmlid
                                 [[maybe_unused]]) const
{
  return QString();
}


pappso::XicCoordSPtr &
QuantiItemBase::getXicCoordSPtr()
{
  return msp_xicCoord;
}

pappso::XicCoordSPtr &
QuantiItemBase::newXicCoordToExtractInMsRun(
  const Msrun *p_msrun [[maybe_unused]],
  std::vector<MsrunSp> &list_msrun_in_group [[maybe_unused]],
                const pappso::IonMobilityGrid *p_ion_mobility_grid [[maybe_unused]])
{
  // for basic objects, xic coordinates are only mz + rt
  msp_xicCoordToExtractInMsRun = msp_xicCoord.get()->initializeAndClone();
  return msp_xicCoordToExtractInMsRun;
}

pappso::XicCoordSPtr &
QuantiItemBase::getXicCoordSPtrExtractedInMsRun()
{
  return msp_xicCoordToExtractInMsRun;
}
