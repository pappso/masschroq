/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantiItemBase.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#pragma once

#include "../../output/mcq_qxmlstreamwriter.h"
#include "../../output/monitors/peptideinfoline.h"
#include "../peptides/peptide.h"
#include <iostream>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/peptide/peptidenaturalisotopeaverage.h>
#include <pappsomspp/msrun/xiccoord/xiccoord.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>

class xicPeak;
class Msrun;
class Peptide;
class XicTraceBase;
class Quantificator;
struct DetectionResult;


class MCQ_LIB_DECL QuantiItemBase;

typedef std::shared_ptr<QuantiItemBase> QuantiItemBaseSp;

/**
 * \class QuantiItemBase
 * \brief Base class representing a given mz value to be quantified.
 *
 * The items to be quantified can be:
 * - an mz value,
 * - a pair of mz, rt values,
 * - a peptide.
 */

class QuantiItemBase
{

  protected:
  // QuantiItemBase(bool trace_peptide_on);

  public:
  QuantiItemBase(bool trace_peptide_on, pappso::XicCoordSPtr xic_coord);

  virtual ~QuantiItemBase();

  bool operator<(const QuantiItemBase &other) const;

  virtual const unsigned int *
  getZ() const
  {
    return (NULL);
  }

  virtual const PeptideCstSPtr getPeptide() const;

  virtual pappso::PeptideNaturalIsotopeAverageSp
  getPeptideNaturalIsotopeAverageSp() const;

  virtual mcq_double
  getRt() const
  {
    return -1;
  }

  // virtual QString getSimpleName(const Msrun * msrun, McqMatchingMode
  // matching_mode) const;

  // virtual QStringList getTracesTitle(const Msrun * msrun, McqMatchingMode
  // matching_mode) const;

  virtual const QString getQuantiItemId() const;

  /** @brief get a unique ID for this particular mass to quantify
   * get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber
   * or even the same peptide with different labels
   */
  virtual const QString getMzId() const;

  virtual void printInfos(QTextStream &out) const;

  // virtual void concatList(QStringList & list, std::map< QString,
  // std::map<QString, QString> > & map_peptide_protein) const;
  // virtual void concatComparList(QStringList & list) const;
  virtual void writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const;
  // virtual void getPepHeaders(QStringList & tmp_list) const ;
  // virtual mcq_double getMatchingRt(const Msrun * msrun, McqMatchingMode
  // match_mode) const;
  virtual void writeOdsPeptideLine(CalcWriterInterface &writer) const;
  virtual void fillPeptideInfoLine(PeptideInfoLine &peptide_line) const;
  virtual void writeOdsComparHeaderLine(CalcWriterInterface &writer) const;
  bool isTraceOn() const;

  virtual QString getTraceBaseName(const QString &prefix,
                                   const QString &quantificator_xmlid) const;
  virtual XicTraceBase *
  newXicTrace(CalcWriterInterface *p_writer,
              const DetectionResult &detection_result) const;

  virtual PeptideRtSp getPeptideRtSp() const;
  virtual void storeAlignedXicPeakForPostMatching(const Msrun *p_current_msrun
                                                  [[maybe_unused]],
                                                  AlignedXicPeakSp &xic_peak_sp
                                                  [[maybe_unused]]){};
  virtual void clearAlignedXicPeakForPostMatching(const Msrun *p_current_msrun
                                                  [[maybe_unused]]){};
  virtual void doPostMatching(DetectionResult &detection_result
                              [[maybe_unused]],
                              MonitorSpeedInterface &_p_monitor_speed
                              [[maybe_unused]]){};

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun
                                      [[maybe_unused]]){};

  virtual pappso::XicCoordSPtr &getXicCoordSPtr();

  virtual pappso::XicCoordSPtr &getXicCoordSPtrExtractedInMsRun();

  double getMz() const;

  /** @brief intialize and clone a clean xic coord object for a specific MSrun
   * @param p_msrun the current MSrun beeing extracted
   * @param list_msrun_in_group list of all msrun in the current group to be
   * quantified
   * @param p_ion_mobility_grid map ion mobility differences between runs
   */
  virtual pappso::XicCoordSPtr &newXicCoordToExtractInMsRun(
    const Msrun *p_msrun,
    std::vector<MsrunSp> &list_msrun_in_group,
    const pappso::IonMobilityGrid *p_ion_mobility_grid);

  bool isTracePeptideOn() const;


  protected:
  // general xic coord (mz, rt)
  pappso::XicCoordSPtr msp_xicCoord;

  // specific xic coord for an msrun (may be containing mobility coordinates)
  pappso::XicCoordSPtr msp_xicCoordToExtractInMsRun;

  // McqQuantiItemType _type;
  bool _trace_peptide_on = false;
};
