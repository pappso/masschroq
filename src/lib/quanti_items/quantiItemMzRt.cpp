/**
 * \file quantiItemMzRt.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemMzRt.h"

#include <QStringList>
#include <pappsomspp/pappsoexception.h>

QuantiItemMzRt::QuantiItemMzRt(bool trace_peptide_on,
                               pappso::XicCoordSPtr xic_coord)
  : QuantiItemBase(trace_peptide_on, xic_coord)
{

  if(xic_coord == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in %1, %2, %3 :\n msp_xicCoord == nullptr")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
}

QuantiItemMzRt::~QuantiItemMzRt()
{
}

mcq_double
QuantiItemMzRt::getRt() const
{
  if(msp_xicCoord == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in %1, %2, %3 :\n msp_xicCoord == nullptr")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  return (msp_xicCoord.get()->rtTarget);
}

void
QuantiItemMzRt::printInfos(QTextStream &out) const
{
  out << " Mz-Rt quantification item :" << Qt::endl;
  out << "_mz = " << this->getMz() << Qt::endl;
  out << "_rt = " << getRt() << Qt::endl;
}

void
QuantiItemMzRt::writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const
{

  QuantiItemBase::writeCurrentSearchItem(_output_stream);

  _output_stream->writeAttribute("rt", QString::number(getRt(), 'g', 8));
}

const QString
QuantiItemMzRt::getMzId() const
{
  return (QString("%1-%2")
            .arg(QString::number(getMz(), 'f', 5))
            .arg(QString::number(getRt(), 'f', 5)));
}
