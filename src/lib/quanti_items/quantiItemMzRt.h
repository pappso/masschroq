/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantiItemMzRt.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#pragma once

#include "quantiItemBase.h"

/**
 * \class QuantiItemMzRt
 * \brief Class representing a given pair of mz, retention time values to be
 * quantified.
 */

class QuantiItemMzRt : public QuantiItemBase
{

  public:
  QuantiItemMzRt(bool trace_peptide_on, pappso::XicCoordSPtr xic_coord);

  virtual ~QuantiItemMzRt();

  virtual void printInfos(QTextStream &out) const;

  virtual void writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const;

  /** get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber**/
  virtual const QString getMzId() const override;

  protected:
  virtual mcq_double getRt() const override;
};
