/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#include "quantiItemPeptide.h"
#include "../mcq_error.h"
#include "../peptides/isotope_label.h"
#include "../quantificator.h"
#include "../share/utilities.h"
#include "../xic/xictracepeptide.h"
#include <odsstream/calcwriterinterface.h>
#include <odsstream/tsvdirectorywriter.h>
#include "../../output/monitors/monitorspeedinterface.h"

#include <QStringList>

PeptideRtSp
QuantiItemPeptide::getPeptideRtSp() const
{
  return msp_peptide->getPeptideRtSp();
}
QuantiItemPeptide::QuantiItemPeptide(bool trace_peptide_on,
                                     PeptideCstSPtr p_peptide,
                                     unsigned int z,
                                     pappso::XicCoordSPtr xic_coord)
  : QuantiItemBase(trace_peptide_on, xic_coord), msp_peptide(p_peptide), _z(z)
{
  _p_post_matched_aligned_peaks = AlignedPeakCollectionBase::newInstance(this);
}

QuantiItemPeptide::~QuantiItemPeptide()
{
  // delete _p_post_matched_aligned_peaks;
}

const PeptideCstSPtr
QuantiItemPeptide::getPeptide() const
{
  return (msp_peptide);
}

const unsigned int *
QuantiItemPeptide::getZ() const
{
  return (&_z);
}

void
QuantiItemPeptide::printInfos(QTextStream &out) const
{
  out << "peptide quantification item :" << Qt::endl;
  msp_peptide->printInfos(out);
  out << "_z = " << _z << Qt::endl;
  out << "_mz = " << this->getMz() << Qt::endl;
}

const QString
QuantiItemPeptide::getQuantiItemId() const
{
  return (msp_peptide->getXmlId());
}

const QString
QuantiItemPeptide::getMzId() const
{
  if(msp_peptide->getIsotopeLabel() != nullptr)
    {
      return (QString("%1_%2+%3")
                .arg(msp_peptide->getXmlId())
                .arg(msp_peptide->getIsotopeLabel()->getXmlId())
                .arg(_z));
    }
  return (QString("%1+%2").arg(msp_peptide->getXmlId()).arg(_z));
}

void
QuantiItemPeptide::writeCurrentSearchItem(
  MCQXmlStreamWriter *_output_stream) const
{

  _output_stream->writeAttribute("item_id_ref", msp_peptide->getXmlId());

  _output_stream->writeAttribute("z", QString::number(_z, 'f', 0));
  const IsotopeLabel *p_isotopeLabel = msp_peptide->getIsotopeLabel();
  if(p_isotopeLabel != nullptr)
    {
      _output_stream->writeAttribute("isotope", p_isotopeLabel->getXmlId());
    }
}

void
QuantiItemPeptide::writeOdsPeptideLine(CalcWriterInterface &writer) const
{

  writer.writeCell(msp_peptide->getXmlId());

  const IsotopeLabel *isotope = msp_peptide->getIsotopeLabel();
  if(isotope != NULL)
    {
      writer.writeCell(isotope->getXmlId());
    }
  else
    {
      writer.writeEmptyCell();
    }
  // writer.writeCell(_p_peptide->getXmlId());
  writer.writeCell(msp_peptide->getSequence());

  // writer.writeCell(_p_peptide->getSequence());

  writer.writeCell((std::size_t)_z);

  // writer.writeCell(msp_peptide->getMods());
  writer.writeCell(
    QString("[%1] %2")
      .arg(msp_peptide.get()->getPappsoPeptideSp().get()->getFormula(_z))
      .arg(msp_peptide->getMods()));
}

void
QuantiItemPeptide::fillPeptideInfoLine(PeptideInfoLine &peptide_line) const
{
  peptide_line.m_peptide = msp_peptide->getXmlId();

  if(msp_peptide->getIsotopeLabel() != nullptr)
    {
      peptide_line.m_label = msp_peptide->getIsotopeLabel()->getXmlId();
    }
  peptide_line.m_sequence = msp_peptide->getPappsoPeptideSp().get()->toString();
  peptide_line.m_z        = (std::size_t)_z;
  peptide_line.m_mods =
    QString("[%1] %2")
      .arg(msp_peptide.get()->getPappsoPeptideSp().get()->getFormula(_z))
      .arg(msp_peptide->getMods());
}

void
QuantiItemPeptide::writeOdsComparHeaderLine(CalcWriterInterface &writer) const
{

  //_p_writer->writeCell("peptide");
  writer.writeCell(msp_peptide->getXmlId());
  //_p_writer->writeCell("m/z");
  writer.writeCell(getMz());
  //_p_writer->writeCell("rt reference");
  writer.writeCell(
    this->msp_peptide->getPeptideRtSp().get()->getAlignedReferenceRt());
  //_p_writer->writeCell("z");
  writer.writeCell((std::size_t)_z);
  //_p_writer->writeCell("isotope number");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope rank");
  writer.writeEmptyCell();
  writer.writeEmptyCell();
  //_p_writer->writeCell("sequence");
  writer.writeCell(msp_peptide->getPappsoPeptideSp().get()->toString());
  //_p_writer->writeCell("isotope");
  if(msp_peptide->getIsotopeLabel() == nullptr)
    {
      writer.writeEmptyCell();
    }
  else
    {
      writer.writeCell(msp_peptide->getIsotopeLabel()->getXmlId());
    }
  //_p_writer->writeCell("mods");
  writer.writeCell(
    QString("[%1] %2")
      .arg(msp_peptide.get()->getPappsoPeptideSp().get()->getFormula(_z))
      .arg(msp_peptide->getMods()));
  //_p_writer->writeCell("proteins");
  QStringList protein_list;
  for(const Protein *p_protein : msp_peptide->getProteinList())
    {
      protein_list << p_protein->getXmlId();
    }
  writer.writeCell(protein_list.join(" "));
}

XicTraceBase *
QuantiItemPeptide::newXicTrace(CalcWriterInterface *p_writer,
                               const DetectionResult &detection_result) const
{
  return new XicTracePeptide(p_writer, this, detection_result.p_msrun);
}


QString
QuantiItemPeptide::getTraceBaseName(const QString &prefix,
                                    const QString &quantificator_xmlid) const
{
  qDebug();
  const IsotopeLabel *p_label = this->getPeptide()->getIsotopeLabel();
  QString begin_path;
  if(p_label == nullptr)
    {
      begin_path = QString("%1_%2-%3-z%4")
                     .arg(prefix)
                     .arg(quantificator_xmlid)
                     .arg(this->getPeptide()->getXmlId())
                     .arg(this->_z);
    }
  else
    {
      begin_path = QString("%1_%2-%3-%4-z%5")
                     .arg(prefix)
                     .arg(quantificator_xmlid)
                     .arg(this->getPeptide()->getXmlId())
                     .arg(p_label->getXmlId())
                     .arg(this->_z);
    }
  qDebug();
  return begin_path;
}

void
QuantiItemPeptide::storeAlignedXicPeakForPostMatching(
  const Msrun *p_current_msrun, AlignedXicPeakSp &xic_peak_sp)
{
  qDebug() << "begin";
  // std::vector<AlignedXicPeakSp> vec_peak;
  // vec_peak.push_back(xic_peak_sp);
  _p_post_matched_aligned_peaks->add(p_current_msrun, xic_peak_sp);
  // std::pair<std::map<const Msrun*, std::vector<AlignedXicPeakSp>>::iterator,
  // bool> ret = _map_post_matched_aligned_peaks.insert(std::pair<const Msrun*,
  // std::vector<AlignedXicPeakSp>> (p_current_msrun,vec_peak));
  // if (ret.second == false) {
  //    ret.first->second.push_back(xic_peak_sp);
  //}

  qDebug() << "end";
}
void
QuantiItemPeptide::clearAlignedXicPeakForPostMatching(
  const Msrun *p_current_msrun)
{
  qDebug() << "egin";
  _p_post_matched_aligned_peaks->clear(p_current_msrun);
  // std::map<const Msrun*, std::vector<AlignedXicPeakSp>>::iterator it =
  // _map_post_matched_aligned_peaks.find(p_current_msrun);
  // if (it != _map_post_matched_aligned_peaks.end()) {
  //    _map_post_matched_aligned_peaks.erase (it);
  //}

  qDebug() << "end";
}

void
QuantiItemPeptide::doPostMatching(DetectionResult &detection_result,
                                  MonitorSpeedInterface &monitor_speed)
{
  detection_result.p_msrun = nullptr;
  qDebug(); // _p_monitor_speed.writeMatchedPeak();
  pappso::pappso_double rt_target =
    msp_peptide->getPeptideRtSp().get()->getPostMatchedRtTarget();
  std::vector<const Msrun *> msrun_list =
    _p_post_matched_aligned_peaks->getMsRunList();
  for(const Msrun *p_msrun : msrun_list)
    {
      detection_result.p_msrun = p_msrun;
      bool matched             = false;
      std::vector<AlignedXicPeakSp> &peak_list =
        _p_post_matched_aligned_peaks->getMsRunAlignedPeakList(p_msrun);
      for(AlignedXicPeakSp &peak : peak_list)
        {
          if(peak.get()->containsRt(rt_target))
            {
              matched                         = true;
              detection_result.sp_alignedPeak = peak;
              detection_result.mbr_category =
                PeakQualityCategory::post_matching;
              monitor_speed.writeDetectionResult(detection_result);
              break;
            }
        }
      if(matched == false)
        {

          detection_result.sp_alignedPeak = nullptr;

          detection_result.mbr_category = PeakQualityCategory::missed;
          monitor_speed.writeDetectionResult(detection_result);
        }

      _p_post_matched_aligned_peaks->clear(p_msrun);
    }

  qDebug();
}

void
QuantiItemPeptide::endMsrunQuantification(const Msrun *p_current_msrun)
{

  _p_post_matched_aligned_peaks->endMsrunQuantification(p_current_msrun);
}

pappso::XicCoordSPtr &
QuantiItemPeptide::newXicCoordToExtractInMsRun(
  const Msrun *p_msrun,
  std::vector<MsrunSp> &list_msrun_in_group,
  const pappso::IonMobilityGrid *p_ion_mobility_grid)
{

  qDebug();
  pappso::XicCoordSPtr xic_coord =
    msp_peptide.get()->getXicCoordToExtractInMsRun(p_msrun);
  qDebug();
  if(xic_coord.get() == nullptr)
    {
      // no direct observation
      qDebug();
      xic_coord = msp_peptide.get()->getBestXicCoordToExtractOverallMsRun(
        list_msrun_in_group);
    }
  qDebug();
  if(xic_coord.get() == nullptr)
    { // error
      qDebug() << "peptide id=" << msp_peptide.get()->getXmlId();
      throw pappso::PappsoException(QObject::tr("xic coord is nullptr"));
    }
  qDebug();
  xic_coord.get()->mzRange = msp_xicCoord.get()->mzRange;

  // rt and mzrange OK
  if(!xic_coord.get()
        ->getParam(pappso::XicCoordParam::TimsTofIonMobilityScanNumberStart)
        .isNull())
    {
      // Ion mobility TimsTOF coordinates

      // direct observation in this msrun :
      pappso::XicCoordSPtr xic_coord_im =
        msp_peptide.get()->getXicCoordToExtractInMsRunByCharge(p_msrun,
                                                               *getZ());


      if(xic_coord_im.get() == nullptr)
        {
          // no direct observation for IM
          qDebug();
          xic_coord_im =
            msp_peptide.get()->getBestIonMobilityXicCoordToExtractOverallMsRun(
              list_msrun_in_group,
              p_ion_mobility_grid,
              *p_msrun->getMsRunIdCstSPtr().get(),
              *getZ());
        }
      qDebug();
      if(xic_coord_im.get() == nullptr)
        { // error
          qDebug() << "peptide id=" << msp_peptide.get()->getXmlId();
          throw pappso::PappsoException(QObject::tr("xic coord IM is nullptr"));
        }

      xic_coord_im.get()->rtTarget = xic_coord.get()->rtTarget;
      xic_coord_im.get()->mzRange  = msp_xicCoord.get()->mzRange;

      xic_coord = xic_coord_im;
    }

  msp_xicCoordToExtractInMsRun = xic_coord.get()->initializeAndClone();


  qDebug() << " msp_xicCoordToExtractInMsRun.get()->rtTarget="
           << msp_xicCoordToExtractInMsRun.get()->rtTarget;

  return msp_xicCoordToExtractInMsRun;
}
