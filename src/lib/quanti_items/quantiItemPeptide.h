/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#pragma once

#include "../peak_collections/alignedpeakcollectionbase.h"
#include "../peptides/peptide.h"
#include "quantiItemBase.h"

class xicPeak;

/**
 * \class QuantiItemPeptide
 * \brief Class representing a peptide item to be quantified.
 */

class QuantiItemPeptide : public QuantiItemBase
{


  public:
  QuantiItemPeptide(bool trace_peptide_on,
                    PeptideCstSPtr p_peptide,
                    unsigned int z,
                    pappso::XicCoordSPtr xic_coord);

  virtual ~QuantiItemPeptide();

  virtual const std::shared_ptr<const Peptide> getPeptide() const override;

  virtual const unsigned int *getZ() const;

  virtual const QString getQuantiItemId() const final;

  virtual void printInfos(QTextStream &out) const;

  virtual void
  writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const override;

  /** get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber**/
  virtual const QString getMzId() const override;
  virtual void writeOdsPeptideLine(CalcWriterInterface &writer) const override;
  virtual void
  fillPeptideInfoLine(PeptideInfoLine &peptide_line) const override;
  virtual void
  writeOdsComparHeaderLine(CalcWriterInterface &writer) const override;

  virtual QString
  getTraceBaseName(const QString &prefix,
                   const QString &quantificator_xmlid) const override;

  virtual XicTraceBase *
  newXicTrace(CalcWriterInterface *p_writer,
              const DetectionResult &detection_result) const override;


  virtual PeptideRtSp getPeptideRtSp() const override;
  virtual void
  storeAlignedXicPeakForPostMatching(const Msrun *p_current_msrun,
                                     AlignedXicPeakSp &xic_peak_sp) override;
  virtual void
  clearAlignedXicPeakForPostMatching(const Msrun *p_current_msrun) override;

  virtual void doPostMatching(DetectionResult &detection_result,
                              MonitorSpeedInterface &monitor_speed) override;

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun) override;

  protected:
  /** @brief intialize and clone a clean xic coord object for a specific MSrun
   */
  virtual pappso::XicCoordSPtr &newXicCoordToExtractInMsRun(
    const Msrun *p_msrun,
    std::vector<MsrunSp> &list_msrun_in_group,
    const pappso::IonMobilityGrid *p_ion_mobility_grid) override;

  protected:
  PeptideCstSPtr msp_peptide;
  unsigned int _z;
  // std::map< const Msrun*, std::vector<AlignedXicPeakSp>>
  // _map_post_matched_aligned_peaks;
  AlignedPeakCollectionBaseSp _p_post_matched_aligned_peaks = nullptr;
};
