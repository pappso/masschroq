
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "quantiItemPeptide.h"


class QuantiItemPeptideNaturalIsotope;

typedef std::shared_ptr<QuantiItemPeptideNaturalIsotope>
  QuantiItemPeptideNaturalIsotopeSp;

class QuantiItemPeptideNaturalIsotope : public QuantiItemPeptide
{
  public:
  QuantiItemPeptideNaturalIsotope(bool trace_peptide_on,
                                  PeptideCstSPtr p_peptide,
                                  const pappso::PeptideNaturalIsotopeAverageSp
                                    &peptide_natural_isotope_average_sp,
                                  pappso::XicCoordSPtr xic_coord);
  virtual ~QuantiItemPeptideNaturalIsotope();
  virtual void
  writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const override;
  /** get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber**/
  virtual const QString getMzId() const override;
  virtual void writeOdsPeptideLine(CalcWriterInterface &writer) const override;
  virtual void
  fillPeptideInfoLine(PeptideInfoLine &peptide_line) const override;
  virtual void
  writeOdsComparHeaderLine(CalcWriterInterface &writer) const override;

  virtual pappso::PeptideNaturalIsotopeAverageSp
  getPeptideNaturalIsotopeAverageSp() const override;

  virtual QString
  getTraceBaseName(const QString &prefix,
                   const QString &quantificator_xmlid) const override;


  virtual XicTraceBase *
  newXicTrace(CalcWriterInterface *p_writer,
              const DetectionResult &detection_result) const override;

  private:
  pappso::PeptideNaturalIsotopeAverageSp _peptide_natural_isotope_average_sp;
};
