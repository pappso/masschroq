
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "mapquanti.h"
#include "../consoleout.h"
#include "../quanti_items/quantiItemBase.h"
#include <cmath>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/processing/detection/tracepeaklist.h>

MapQuanti::MapQuanti(Msrun *current_msrun,
                     Quantificator *quantificator,
                     pappso::TraceDetectionInterfaceCstSPtr p_detection)

{
  _p_msrun       = current_msrun;
  _p_detection   = p_detection;
  _matching_mode = quantificator->getMatchingMode();
  _quantificator = quantificator;
}

DetectionResult
MapQuanti::operator()(QuantiItemBaseSp p_currentSearchItem)
{
  qDebug();
  DetectionResult detection_result;
  detection_result.p_msrun       = _p_msrun;
  detection_result.p_quanti_item = p_currentSearchItem;
  detection_result.mbr_category  = PeakQualityCategory::missed;

  detection_result.sp_traceResult = std::make_shared<TraceResult>();
  detection_result.sp_traceResult.get()->sp_xicCoord =
    p_currentSearchItem->getXicCoordSPtrExtractedInMsRun();
  try
    {

      pappso::pappso_double rt_target = -1;
      // std::vector<MsMsRtIntensity> rt_list;

      std::vector<MsMsRtIntensity> &rt_list =
        detection_result.sp_traceResult.get()->rtList;

      PeptideRt *p_peptide_rt = p_currentSearchItem->getPeptideRtSp().get();

      if(p_peptide_rt != nullptr)
        {

          rt_list = p_peptide_rt->getObservedRtList(_p_msrun);
          if(rt_list.size() == 0)
            {
              // not observed
              rt_target = p_peptide_rt->getRtTarget(_p_msrun, _matching_mode);
            }


          if(std::isnan(rt_target))
            {
              qDebug() << " isnan(rt_target) "
                       << p_currentSearchItem->getQuantiItemId() << " "
                       << rt_target;
              throw mcqError(
                QObject::tr(
                  "Error in MapQuanti::operator() isnan(rt_target) %1")
                  .arg(p_currentSearchItem->getQuantiItemId()));
            }
        }

      if(rt_target == -1)
        {
          if(_matching_mode == McqMatchingMode::post_matching)
            {
              // it's Ok, just detect and keep peaks
            }
          else
            {
              qDebug() << " rt_target == -1 exit";
              return DetectionResult();
            }
        }
      qDebug() << " rt_target=" << rt_target;

      // only take an rt reference to match peak

      XicFilterDetectMatchRealRt filter_detect_match(_quantificator,
                                                     _matching_mode);


      //_p_msrun->extractXicCoordSPtr(p_currentSearchItem->getXicCoordSPtr());

      pappso::XicCstSPtr msrun_xic_sp =
        detection_result.sp_traceResult.get()->sp_xicCoord.get()->xicSptr;

      qDebug();
      if(msrun_xic_sp != nullptr)
        {
          qDebug();

          pappso::TracePeakList &peak_list =
            detection_result.sp_traceResult.get()->peakList;
          _p_detection->detect(
            *msrun_xic_sp.get(), peak_list, m_removePeakBase);

          filter_detect_match.filterDetectQuantify(*msrun_xic_sp.get(),
                                                   peak_list,
                                                   rt_target,
                                                   rt_list,
                                                   p_currentSearchItem.get(),
                                                   _p_msrun);
          qDebug();


          for(std::size_t i = 0; i < peak_list.size(); i++)
            {
              if(filter_detect_match.getMatchedPeakPtr() == &peak_list[i])
                {
                  detection_result.sp_traceResult.get()->matchedPeakPosition =
                    i;
                  break;
                }
            }
        }
      else
        {
          qDebug();
        }
      p_currentSearchItem->endMsrunQuantification(_p_msrun);


      detection_result.mbr_category =
        filter_detect_match.getPeakQualityCategory();
      detection_result.sp_alignedPeak =
        filter_detect_match.getAlignedXicPeakSp();
      return detection_result;

      qDebug();
    }

  catch(pappso::PappsoException &errorp)
    {
      ConsoleOut::mcq_cerr() << " MapQuanti::operator " << errorp.qwhat();
      throw mcqError(
        QObject::tr(
          "Error in MapQuanti::operator() pappso::PappsoException:\n %1")
          .arg(errorp.qwhat()));
    }

  catch(std::exception &e)
    {
      ConsoleOut::mcq_cerr() << " MapQuanti::operator " << e.what();
      throw mcqError(
        QObject::tr("Error in MapQuanti::operator() std::exception :\n %1")
          .arg(e.what()));
    }
  return detection_result;
}
