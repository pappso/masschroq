#include "quantificationMethod.h"
#include "../share/utilities.h"
#include <QDebug>

QuantificationMethod::QuantificationMethod(const QString &xml_id)
  : _xml_id(xml_id)
{
  if(_precision_unit == pappso::PrecisionUnit::ppm)
    {
      _lower_precision_p =
        pappso::PrecisionFactory::getPpmInstance(_lower_precision);
      _upper_precision_p =
        pappso::PrecisionFactory::getPpmInstance(_upper_precision);
    }
  else
    {
      _lower_precision_p =
        pappso::PrecisionFactory::getDaltonInstance(_lower_precision);
      _upper_precision_p =
        pappso::PrecisionFactory::getDaltonInstance(_upper_precision);
    }
  m_secondChance = false;
}

QuantificationMethod::QuantificationMethod(const QuantificationMethod &other)
  : _xml_id(other._xml_id)
{

  m_filter_list         = other.m_filter_list;
  mcsp_detection_method = other.mcsp_detection_method;
  _m_xic_extract_method = other._m_xic_extract_method;
  _precision_unit       = other._precision_unit;
  _upper_precision      = other._upper_precision;
  _lower_precision      = other._lower_precision;

  _lower_precision_p = other._lower_precision_p;
  _upper_precision_p = other._upper_precision_p;
  m_secondChance     = other.m_secondChance;
}

QuantificationMethod::~QuantificationMethod()
{
}

const QString &
QuantificationMethod::getXmlId() const
{
  return _xml_id;
}

void
QuantificationMethod::printInfos(QTextStream &out) const
{
  out << "\t Quantification method '" << _xml_id
      << "' parameters : " << Qt::endl;
  out << "\t xic type : '" << Utilities::toString(_m_xic_extract_method) << "' "
      << "second chance:" << m_secondChance << Qt::endl;
  /*
  std::vector<const FilterBase *>::const_iterator it_f;
  for (it_f = _v_p_xic_filters.begin();
          it_f != _v_p_xic_filters.end();
          ++it_f) {
      out << ( (*it_f)->printInfos() );
  }
  */
  // out << (_p_detection_method->printInfos());
}

void
QuantificationMethod::setDetectionMethod(
  pappso::TraceDetectionInterfaceCstSPtr detection_method)
{
  mcsp_detection_method = detection_method;
}

const pappso::TraceDetectionInterfaceCstSPtr &
QuantificationMethod::getDetectionMethod() const
{
  return mcsp_detection_method;
}

void
QuantificationMethod::setXicExtractMethod(
  pappso::XicExtractMethod xic_extraction_method)
{
  _m_xic_extract_method = xic_extraction_method;
}

pappso::XicExtractMethod
QuantificationMethod::getXicExtractMethod() const
{
  return _m_xic_extract_method;
}

void
QuantificationMethod::addFilter(pappso::FilterInterfaceSPtr filter_sp)
{

  qDebug();
  m_filter_list.push_back(filter_sp);

  qDebug();
}

const pappso::FilterSuite &
QuantificationMethod::getXicFilters() const
{
  return m_filter_list;
}

void
QuantificationMethod::setXicExtractPrecisionUnit(
  pappso::PrecisionUnit precision_unit)
{
  _precision_unit = precision_unit;
}
void
QuantificationMethod::setXicExtractPrecision(pappso::PrecisionPtr precision)
{
  _lower_precision_p = precision;
  _upper_precision_p = precision;
  _precision_unit    = precision->unit();

  _lower_precision = precision->getNominal();
  _upper_precision = precision->getNominal();
}

void
QuantificationMethod::setXicExtractLowerPrecision(pappso::pappso_double min)
{
  _lower_precision = min;

  if(_precision_unit == pappso::PrecisionUnit::ppm)
    {
      _lower_precision_p =
        pappso::PrecisionFactory::getPpmInstance(_lower_precision);
    }
  else
    {
      _lower_precision_p =
        pappso::PrecisionFactory::getDaltonInstance(_lower_precision);
    }
}
void
QuantificationMethod::setXicExtractUpperPrecision(pappso::pappso_double max)
{
  _upper_precision = max;

  if(_precision_unit == pappso::PrecisionUnit::ppm)
    {
      _upper_precision_p =
        pappso::PrecisionFactory::getPpmInstance(_upper_precision);
    }
  else
    {
      _upper_precision_p =
        pappso::PrecisionFactory::getDaltonInstance(_upper_precision);
    }
}
pappso::PrecisionPtr
QuantificationMethod::getLowerPrecision() const
{
  return _lower_precision_p;
}
pappso::PrecisionPtr
QuantificationMethod::getUpperPrecision() const
{
  return _upper_precision_p;
}

pappso::PrecisionPtr
QuantificationMethod::getMeanPrecision() const
{

  if(_precision_unit == pappso::PrecisionUnit::ppm)
    {
      return pappso::PrecisionFactory::getPpmInstance(
        (_upper_precision + _lower_precision) / 2);
    }
  return pappso::PrecisionFactory::getDaltonInstance(
    (_upper_precision + _lower_precision) / 2);
}

const pappso::MzRange
QuantificationMethod::getMzRange(pappso::pappso_double mz) const
{
  return pappso::MzRange(mz, _lower_precision_p, _upper_precision_p);
}

bool
QuantificationMethod::getSecondChance() const
{
  return m_secondChance;
}
