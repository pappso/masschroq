/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantificationMethod.h
 * \date October 26, 2010
 * \author Edlira Nano
 */

#pragma once

#include <QTextStream>
#include <pappsomspp/types.h>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filtersuite.h>
#include <odsstream/calcwriterinterface.h>
#include "../../exportimportconfig.h"

/**
   \class QuantificationMethod
   \brief Represents a method of quantification
*/
class MCQ_LIB_DECL QuantificationMethod
{

  public:
  QuantificationMethod(const QString &xml_id);
  QuantificationMethod(const QuantificationMethod &other);
  virtual ~QuantificationMethod();

  const QString &getXmlId() const;

  virtual void printInfos(QTextStream &out) const;

  void
  setDetectionMethod(pappso::TraceDetectionInterfaceCstSPtr detection_method);

  const pappso::TraceDetectionInterfaceCstSPtr &getDetectionMethod() const;

  void setXicExtractMethod(pappso::XicExtractMethod xic_type);

  pappso::XicExtractMethod getXicExtractMethod() const;

  void addFilter(pappso::FilterInterfaceSPtr filter_sp);

  const pappso::FilterSuite &getXicFilters() const;

  pappso::PrecisionPtr getLowerPrecision() const;
  pappso::PrecisionPtr getUpperPrecision() const;
  pappso::PrecisionPtr getMeanPrecision() const;

  const pappso::MzRange getMzRange(pappso::pappso_double mz) const;

  void setXicExtractLowerPrecision(pappso::pappso_double min);
  void setXicExtractUpperPrecision(pappso::pappso_double max);
  void setXicExtractPrecisionUnit(pappso::PrecisionUnit precision_unit);

  void setXicExtractPrecision(pappso::PrecisionPtr precision);

  /** @brief tells if we want a second chance to detect small peaks, very near
   * the targeted retention time if enabled, this looks for small peaks with
   * boundary retention time smaller than the MS1 minimum range
   */
  bool getSecondChance() const;

  private:
  /// unique xml id for this method
  const QString _xml_id;

  /// the xic filters
  pappso::FilterSuite m_filter_list;
  // pappso::XicFilterInterface *_p_xic_filter = nullptr;

  /// the peak detection method for this quantification
  pappso::TraceDetectionInterfaceCstSPtr mcsp_detection_method;

  pappso::XicExtractMethod _m_xic_extract_method =
    pappso::XicExtractMethod::max;

  pappso::PrecisionUnit _precision_unit = pappso::PrecisionUnit::ppm;

  pappso::pappso_double _upper_precision = 10;
  pappso::pappso_double _lower_precision = 10;

  pappso::PrecisionPtr _lower_precision_p = nullptr;
  pappso::PrecisionPtr _upper_precision_p = nullptr;

  bool m_secondChance = false;
};
