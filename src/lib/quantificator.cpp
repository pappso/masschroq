/**
 * \file quantificator.cpp
 * \date August 03, 2011
 * \author Edlira Nano
 */

#include "quantificator.h"
#include "./consoleout.h"
//#include "xic/xic_base.h"
#include "peptides/peptide_list.h"
#include "quanti_items/quantiItemMzRt.h"
#include "quanti_items/quantiItemPeptide.h"
#include "quanti_items/quantiitempeptidenaturalisotope.h"
#include "quantifications/quantificationMethod.h"
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include "share/utilities.h"

#include "peptides/peptidert.h"
#include <QDir>
#include <QStringList>
#include <QtConcurrentMap>
#include <algorithm>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>
#include "../mcqsession.h"
#include "peptides/peptideisopattern.h"

uint Quantificator::_cpu_number = 1;

void
Quantificator::setCpuNumber(uint cpu_number)
{

  qDebug() << " Quantificator::setCpuNumber" << cpu_number;
  uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
  if(cpu_number > ideal_number_of_thread)
    {
      cpu_number = ideal_number_of_thread;
    }
  else
    {
      QThreadPool::globalInstance()->setMaxThreadCount(cpu_number);
    }
  Quantificator::_cpu_number = cpu_number;
  ConsoleOut::mcq_cout() << QObject::tr("%1 CPUs used").arg(cpu_number) << Qt::endl;
}

Quantificator::Quantificator(const QString &xml_id,
                             const msRunHashGroup *quanti_group,
                             QuantificationMethod *quanti_method)
  : _xml_id(xml_id)
{
  set_quanti_group(quanti_group);
  set_quanti_method(quanti_method);
}

Quantificator::~Quantificator()
{
  clear();
}

void
Quantificator::setTracesOutput(QDir traces_directory,
                               McqTsvFormat traces_format)
{
  m_xicTraceWriterSp = std::make_shared<XicTraceWriter>(this);
  m_xicTraceWriterSp.get()->setTracesDirectory(traces_directory);
  m_xicTraceWriterSp.get()->setTracesFormat(traces_format);
}

const XicTraceWriterSp &
Quantificator::getXicTraceWriterSp() const
{
  return m_xicTraceWriterSp;
}


void
Quantificator::set_quanti_group(const msRunHashGroup *quanti_group)
{
  _group_to_quantify = quanti_group;
}

void
Quantificator::set_quanti_method(QuantificationMethod *quanti_method)
{
  _quanti_method = quanti_method;
}

void
Quantificator::setMatchingMode(McqMatchingMode match_mode)
{
  _matching_mode = match_mode;
}

const msRunHashGroup *
Quantificator::getMsRunGroup() const
{
  return (_group_to_quantify);
}

McqMatchingMode
Quantificator::getMatchingMode() const
{
  return _matching_mode;
};


const QString &
Quantificator::getXmlId() const
{
  return _xml_id;
}

void
Quantificator::setNiMinimumAbundance(mcq_double ni_minimum_abundance)
{
  _minimum_isotope_abundance = ni_minimum_abundance;
}

mcq_double
Quantificator::getNiMinimumAbundance() const
{
  return _minimum_isotope_abundance;
}

const std::vector<PeptideCstSPtr> &
Quantificator::getPeptideList() const
{
  return _peptide_list;
}

const QuantificationMethod *
Quantificator::getQuantificationMethod() const
{
  return _quanti_method;
}
void
Quantificator::computeRetentionTimeReferences()
{
  //_map_peptides_2_retention_times.clear();
  //_accumulator.clear();
  for(PeptideCstSPtr p_peptide : _peptide_list)
    {

      PeptideRt *_p_peptide_rt = p_peptide->getPeptideRtSp().get();
      auto find_it             = std::find(
        _peptide_rt_list.begin(), _peptide_rt_list.end(), _p_peptide_rt);
      if(find_it == _peptide_rt_list.end())
        {
          _peptide_rt_list.push_back(_p_peptide_rt);
          // auto it_new =
          // _map_peptides_2_retention_times.insert(std::pair<const Peptide *,
          // std::map< const Msrun * , std::vector< MsMsRtIntensity > >
          // >(p_peptide,std::map< const Msrun * , std::vector< MsMsRtIntensity
          // >
          // >())).first;
          // auto it_charge_new = map_peptides_charge_set.insert(std::pair<const
          // Peptide *, std::set<unsigned int>>(p_peptide, std::set<unsigned
          // int>())).first;

          for(auto &pair : *_group_to_quantify)
            {
              const Msrun *p_msrun = pair.second.get();
              // auto it_rt_list = it_new->second.insert(std::pair<const Msrun *
              // , std::vector< MsMsRtIntensity >>
              //                                        (p_msrun, std::vector<
              //                                        MsMsRtIntensity
              //                                        >())).first;

              // qDebug() << "Quantificator::computeRetentionTimeReferences
              // getObservedInList size" <<
              // p_peptide->getObservedInList().size();
              for(PeptideObservedIn *obs_in : p_peptide->getObservedInList())
                {
                  if(obs_in->getPmsRun() == p_msrun)
                    {
                      MsMsObservation observation(p_msrun);
                      observation.intensity = obs_in->getIntensity();
                      observation.rt        = obs_in->getRt();
                      observation.z         = obs_in->getZ();
                      observation.mz        = p_peptide->getMz(observation.z);
                      // it_rt_list->second.push_back(observation);

                      // it_charge_new->second.insert(observation.z);

                      _p_peptide_rt->addMsMsRtIntensity(observation);
                    }
                }
            }
        }
    }

  if((_matching_mode == McqMatchingMode::real_or_mean) ||
     (_matching_mode == McqMatchingMode::post_matching))
    {
      // accumulatorToRetentionTimeReferences();
      for(auto &&p_peptide_rt : _peptide_rt_list)
        {
          p_peptide_rt->computeRetentionTimeReferences(_matching_mode);
        }
    }
}

void
Quantificator::add_peptide_quanti_items(
  pappso::UiMonitorInterface &ui_monitor,
  std::vector<PeptideCstSPtr> &peptide_list,
  std::vector<PeptideCstSPtr> &trace_peptide_list)
{

  qDebug() << peptide_list.size();

  _peptide_list = peptide_list;

  /** \brief peptides charge to survey
   * */
  // std::map<const Peptide *, std::set<unsigned int>> map_peptides_charge_set;
  ui_monitor.setTitle(
    QObject::tr("preparing peptide quantification (%1 peptides)")
      .arg(_peptide_list.size()));

  ui_monitor.setTitle(QObject::tr("computing retention time references"));
  computeRetentionTimeReferences();

  // const XicExtractionMethodBase * extraction_method =
  //   _quanti_method->getXicExtractionMethod();

  pappso::PrecisionPtr extraction_precision_mean =
    _quanti_method->getMeanPrecision();

  qDebug() << extraction_precision_mean->toString();
  QuantiItemBaseSp quanti_item;
  /// for each peptide (or isotope in the vector), we will create a peptide
  /// quanti item
  ui_monitor.setTitle(QObject::tr("computing XIC coordinates"));
  ui_monitor.setTotalSteps(peptide_list.size());
  for(auto p_peptide : peptide_list)
    {
      ui_monitor.count();
      pappso::XicCoord xic_coord;
      xic_coord.rtTarget = 0;
      // mcqout() << "Quantificator::add_peptide_quanti_items for " << debug_i
      // << " " << isotope_peptides.size() << endl;
      bool trace_peptide = false;
      PeptideCstSPtr original_peptide_p =
        p_peptide->getOriginalPeptidePointer();
      if(original_peptide_p == nullptr)
        {
          original_peptide_p = p_peptide;
        }
      if(std::find(trace_peptide_list.begin(),
                   trace_peptide_list.end(),
                   original_peptide_p) != trace_peptide_list.end())
        {
          trace_peptide = true;
        }

      pappso::PeptideSp peptide_sp = p_peptide->getPappsoPeptideSp();
      // mcqout() << (*itpep)->getXmlId() << " " <<
      // peptide_sp.get()->toAbsoluteString() << endl;
      // mcqout().flush();

      /// get the set of charges z the peptide is observed in the current group,
      /// and construct a quantification item for each peptide/z

      // std::set<unsigned int> * s_charges = (*itpep)->getCharges(
      //      *_group_to_quantify);
      std::vector<unsigned int> v_charges =
        p_peptide->getPeptideRtSp()->getCharges();

      // mcqout() << "charge OK " << endl;
      // mcqout().flush();

      // if minimum natural isotope is set :
      // compute with natural isotope
      pappso::PeptideNaturalIsotopeList *p_isotope_list = nullptr;
      PeptideIsoPatternSp peptide_iso_pattern;
      if(_minimum_isotope_abundance > 0)
        {

          qDebug() << "if (_minimum_isotope_abundance > 0)";
          p_isotope_list = new pappso::PeptideNaturalIsotopeList(
            peptide_sp, m_minimumRatioToCompute);
          // mcqout() << "PeptideNaturalIsotopeList OK " << endl;
          // mcqout().flush();
          peptide_iso_pattern = std::make_shared<PeptideIsoPattern>(p_peptide);
        }

      // for each z do
      for(unsigned int charge : v_charges)
        {
          if(p_isotope_list != nullptr)
            {

              qDebug()
                << "Quantificator::add_peptide_quanti_items (p_isotope_list "
                   "!= nullptr) charge="
                << charge
                << " p_isotope_list->size()=" << p_isotope_list->size()
                << " pep xml id=" << p_peptide->getXmlId()
                << " peptide=" << peptide_sp.get()->toAbsoluteString();
              std::vector<pappso::PeptideNaturalIsotopeAverageSp>
                natural_isotope_average_list =
                  p_isotope_list->getByIntensityRatio(
                    charge,
                    extraction_precision_mean,
                    _minimum_isotope_abundance);
              // mcqout() << "natural_isotope_average_list ok" << endl;
              // mcqout().flush();


              qDebug() << " natural_isotope_average_list";
              for(pappso::PeptideNaturalIsotopeAverageSp isotope_average_sp :
                  natural_isotope_average_list)
                {
                  qDebug() << " average isotope "
                           << isotope_average_sp.get()->getIntensityRatio()
                           << " level="
                           << isotope_average_sp.get()->getIsotopeNumber()
                           << " rank="
                           << isotope_average_sp.get()->getIsotopeRank();

                  xic_coord.mzRange =
                    this->getQuantificationMethod()->getMzRange(
                      isotope_average_sp.get()->getMz());

                  QuantiItemPeptideNaturalIsotopeSp quanti_item_isotope_sp =
                    std::make_shared<QuantiItemPeptideNaturalIsotope>(
                      trace_peptide,
                      p_peptide,
                      isotope_average_sp,
                      xic_coord.initializeAndClone());
                  _quanti_item_list.push_back(quanti_item_isotope_sp);

                  peptide_iso_pattern.get()->add(quanti_item_isotope_sp);
                }
            }
          else
            {
              xic_coord.mzRange = this->getQuantificationMethod()->getMzRange(
                p_peptide.get()->getMz(charge));
              quanti_item = std::make_shared<QuantiItemPeptide>(
                trace_peptide,
                p_peptide,
                charge,
                xic_coord.initializeAndClone());
              _quanti_item_list.push_back(quanti_item);
            }
        }

      if(p_isotope_list != nullptr)
        {
          delete p_isotope_list;
          p_isotope_list = nullptr;
        }
    }

  ui_monitor.setTotalSteps(0);

  ui_monitor.setTitle(
    QObject::tr("peptide quantification prepared (%1 peptides)")
      .arg(_peptide_list.size()));
  qDebug() << " Quantificator::add_peptide_quanti_items end";
}

void
Quantificator::add_mz_quanti_items(const QStringList &mz_list)
{
  QuantiItemBaseSp quanti_item;

  QStringList::const_iterator it;

  for(it = mz_list.begin(); it != mz_list.end(); ++it)
    {
      pappso::XicCoord xic_coord;
      xic_coord.mzRange =
        this->getQuantificationMethod()->getMzRange(it->toDouble());
      xic_coord.rtTarget = 0;
      quanti_item =
        std::make_shared<QuantiItemBase>(false, xic_coord.initializeAndClone());
      _quanti_item_list.push_back(quanti_item);
    }
}

void
Quantificator::add_mzrt_quanti_item(const QString &mz, const QString &rt)
{

  pappso::XicCoord xic_coord;
  xic_coord.mzRange =
    this->getQuantificationMethod()->getMzRange(mz.toDouble());
  xic_coord.rtTarget = rt.toDouble();
  QuantiItemBaseSp quanti_item =
    std::make_shared<QuantiItemMzRt>(false, xic_coord.initializeAndClone());
  _quanti_item_list.push_back(quanti_item);
}

void
Quantificator::setMonitor(MonitorSpeedInterface *monitor)
{
  _p_monitor_speed = monitor;
}

void
Quantificator::sortQuantiItems()
{
  sort(_quanti_item_list.begin(),
       _quanti_item_list.end(),
       [](const QuantiItemBaseSp &l1, const QuantiItemBaseSp &l2) {
         const mcq_double mz1 = l1.get()->getMz();
         const mcq_double mz2 = l2.get()->getMz();
         return (mz1 < mz2);
       });
}

void
Quantificator::quantify(pappso::UiMonitorInterface &ui_monitor,
                        const pappso::IonMobilityGrid *p_ion_mobility_grid)
{


  qDebug();
  sortQuantiItems();

  QString group_id = _group_to_quantify->getXmlId();

  std::vector<MsrunSp> list_msrun_in_group =
    _group_to_quantify->getMsRunSpList();
  QString quanti_method_id = _quanti_method->getXmlId();
  //_p_xic_filter
  pappso::FilterInterfaceCstSPtr csp_filter_suite =
    std::make_shared<const pappso::FilterSuite>(
      _quanti_method->getXicFilters());

  pappso::TraceDetectionInterfaceCstSPtr detection_method =
    _quanti_method->getDetectionMethod();

  ui_monitor.setStatus(
    QObject::tr(
      "MS run group '%1': quantification method '%2': quantification begin")
      .arg(group_id)
      .arg(quanti_method_id));

  _quanti_method->printInfos(ConsoleOut::mcq_cout());

  qDebug();
  _p_monitor_speed->writeQuantifyBegin(this);
  //_p_monitor_speed->setCurrentGroup(_group_to_quantify);


  qDebug();
  ui_monitor.setStatus(QObject::tr("Starting quantification on %1 MS runs")
                         .arg(_group_to_quantify->size()));
  // launch quantification in group
  Msrun *current_msrun;
  msRunHashGroup::const_iterator itmsrun;
  unsigned int count_msruns(1);
  std::vector<QuantiItemBase *>::const_iterator itmz;
  count_msruns = 1;
  qDebug();
  for(itmsrun = _group_to_quantify->begin();
      itmsrun != _group_to_quantify->end();
      ++itmsrun, ++count_msruns)
    {
      current_msrun = itmsrun->second.get();
      ui_monitor.setStatus(
        QObject::tr("Quantification '%1' : preparing MS run '%2' to "
                    "extract and quantify %3 XICs")
          .arg(_xml_id)
          .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
          .arg(_quanti_item_list.size()));

      ui_monitor.setStatus(
        QObject::tr("quantifying MS run '%1' : %2/%3 in group '%4'")
          .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
          .arg(count_msruns)
          .arg(_group_to_quantify->size())
          .arg(group_id));


      qDebug();
      /**********************************
       * XIC extraction begin *
       **********************************/
      std::vector<pappso::XicCoordSPtr> xic_coord_list;
      for(auto &&quanti_item : _quanti_item_list)
        {
          pappso::XicCoordSPtr xic_coord =
            quanti_item->newXicCoordToExtractInMsRun(
              current_msrun, list_msrun_in_group, p_ion_mobility_grid);

          if(xic_coord.get() != nullptr)
            {
              xic_coord_list.push_back(xic_coord);
            }
          else
            {
              // error
              throw pappso::PappsoException(
                QObject::tr("xic coord is nullptr"));
            }
        }
      // pappso::UiMonitorTextPercent xic_extraction_monitor(mcqout());

      _p_monitor_speed->writeQuantificationItemListInMsRun(
        this->getXmlId(),
        current_msrun->getMsRunIdCstSPtr()->getXmlId(),
        _quanti_item_list);

      current_msrun->quantify(
        ui_monitor, xic_coord_list, _p_monitor_speed, _quanti_method, this);
    }

  if(_matching_mode == McqMatchingMode::post_matching)
    {
      qDebug();
      // compute new rt references
      ui_monitor.setStatus(
        QObject::tr("post matching : computing retention time references "));
      QFuture<void> res =
        QtConcurrent::map(_peptide_rt_list.begin(),
                          _peptide_rt_list.end(),
                          [](PeptideRt *p_peptide_rt) {
                            p_peptide_rt->computePostMatchingTimeReferences();
                          });
      res.waitForFinished();

      qDebug();
      // rematch peaks stored in quanti items
      ui_monitor.setStatus(
        QObject::tr("post matching : trying to match peaks using new "
                    "retention time references \n"));
      MonitorSpeedInterface *p_monitor_speed = _p_monitor_speed;
      res =
        QtConcurrent::map(_quanti_item_list.begin(),
                          _quanti_item_list.end(),
                          [p_monitor_speed](QuantiItemBaseSp &p_quanti_item) {
                            DetectionResult detection_result;
                            detection_result.p_quanti_item = p_quanti_item;
                            p_quanti_item.get()->doPostMatching(
                              detection_result, *p_monitor_speed);
                            /*
                                      qDebug() << "detection_result.p_msrun=" <<
                               detection_result.p_msrun;
                                      if(detection_result.p_msrun != nullptr)
                                        {
                                          //
                               p_quantificator->getXicTraceWriterSp().get()->writeTrace(
                                          //  detection_result);
                                        }
                                        */
                          });
      res.waitForFinished();
      qDebug();
    }

  qDebug();
  _p_monitor_speed->writeQuantifyEnd();

  qDebug() << "end";
}

void
Quantificator::addMissedQuantiItem(const Msrun *_p_current_msrun,
                                   const QuantiItemBase *_p_current_search_item)
{
  QMutexLocker locker(&_mutex);
  auto it_missed = _missed_quanti_item.insert(
    std::pair<const Msrun *, std::vector<const QuantiItemBase *>>(
      _p_current_msrun, std::vector<const QuantiItemBase *>()));
  it_missed.first->second.push_back(_p_current_search_item);
}

bool
Quantificator::isInMissedQuantiItem(
  const Msrun *_p_msrun, const QuantiItemBase *p_currentSearchItem) const
{
  auto it = _missed_quanti_item.find(_p_msrun);
  if(it == _missed_quanti_item.end())
    {

      qDebug() << "Quantificator::isInMissedQuantiItem false no msrun end ";
      return false;
    }
  else
    {
      auto itquanti =
        std::find(it->second.begin(), it->second.end(), p_currentSearchItem);
      if(itquanti == it->second.end())
        {

          qDebug()
            << "Quantificator::isInMissedQuantiItem false no quanti item end ";
          return false;
        }
    }
  return true;
}

void
Quantificator::clear()
{
  _quanti_item_list.clear();
}

std::vector<QuantiItemBaseSp> &
Quantificator::getQuantiItemList()
{
  return _quanti_item_list;
}

void
Quantificator::setPeptideNaturalIsotopeListMinimumRatioToCompute(
  double minimum_ratio)
{
  m_minimumRatioToCompute = minimum_ratio;
}
