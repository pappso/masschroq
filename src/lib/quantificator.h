/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file quantificator.h
 * \date August 03, 2011
 * \author Edlira Nano
 */

#pragma once

#include "../output/monitors/monitorspeedinterface.h"
#include "../output/xictracewriter.h"
#include "msrun/ms_run_hash_group.h"
#include "quantifications/quantificationMethod.h"
#include <QDir>
#include <odsstream/calcwriterinterface.h>
#include "quanti_items/quantiItemBase.h"
#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>

class QuantificationMethod;
class MonitorBase;
class Peptide;
class MonitorBridge;

/**
   \class Quantificator
   \brief The class that launches the quantification of a group.
 */

class MCQ_LIB_DECL Quantificator
{

  public:
  Quantificator(const QString &xml_id,
                const msRunHashGroup *quanti_group,
                QuantificationMethod *quanti_method);

  virtual ~Quantificator();

  static void setCpuNumber(uint cpu_number);

  const QString &getXmlId() const;

  void setMatchingMode(McqMatchingMode match_mode);
  McqMatchingMode getMatchingMode() const;

  void setMonitor(MonitorSpeedInterface *monitor);

  const msRunHashGroup *getMsRunGroup() const;

  void
  add_peptide_quanti_items(pappso::UiMonitorInterface &ui_monitor,
                           std::vector<PeptideCstSPtr> &peptide_list,
                           std::vector<PeptideCstSPtr> &trace_peptide_list);

  void add_mz_quanti_items(const QStringList &mz_list);

  void add_mzrt_quanti_item(const QString &mz, const QString &rt);

  void quantify(pappso::UiMonitorInterface &ui_monitor,
                const pappso::IonMobilityGrid *p_ion_mobility_grid);

  void setNiMinimumAbundance(mcq_double ni_minimum_abundance);
  mcq_double getNiMinimumAbundance() const;

  const std::vector<PeptideCstSPtr> &getPeptideList() const;
  // void addWeightedRealXicRt(const Peptide * p_peptide, const AlignedXicPeak &
  // aligned_peak, unsigned int weight);
  // pappso::pappso_double getPeptideRetentionTimeReference(const Peptide*
  // p_peptide) const;
  // void addMissedQuantiItem(const Msrun* _p_current_msrun, const
  // QuantiItemBase* _p_current_search_item);
  //
  void setTracesOutput(QDir traces_directory, McqTsvFormat traces_format);
  const XicTraceWriterSp &getXicTraceWriterSp() const;

  const QuantificationMethod *getQuantificationMethod() const;

  void addMissedQuantiItem(const Msrun *_p_current_msrun,
                           const QuantiItemBase *_p_current_search_item);
  bool isInMissedQuantiItem(const Msrun *_p_msrun,
                            const QuantiItemBase *p_currentSearchItem) const;

  void clear();

  std::vector<QuantiItemBaseSp> &getQuantiItemList();
  const std::vector<QuantiItemBaseSp> &getQuantiItemList() const;

  /** @brief quantificator argument to have a better isotope ratio computation
   * @param minimum_ratio default is 0.001, set with caution. more precise
   * number is lesser but computation time is bigger
   */
  void setPeptideNaturalIsotopeListMinimumRatioToCompute(
    double minimum_ratio = 0.001);

  private:
  void set_quanti_group(const msRunHashGroup *quanti_group);

  void set_quanti_method(QuantificationMethod *quanti_method);

  void sortQuantiItems();
  void computeRetentionTimeReferences();

  private:
  const QString _xml_id;

  const msRunHashGroup *_group_to_quantify;

  QuantificationMethod *_quanti_method;

  /** \brief keep the peptide list used in this quantification (for report
   * purpose)
   * */
  std::vector<PeptideCstSPtr> _peptide_list;

  /** \brief keep the peptide retention time list used in this quantification
   * (for report purpose)
   * */
  std::vector<PeptideRt *> _peptide_rt_list;

  /** \brief list of experimental retention times for this group (observed
   * peptides by MS/MS)
   */
  // std::map<const Peptide *, std::map< const Msrun * , std::vector<
  // MsMsRtIntensity > > > _map_peptides_2_retention_times;

  /** \brief peptides retention time references (depending on _matching_mode)
   * */
  // std::map<const Peptide *, pappso::pappso_double>
  // _map_peptides_retention_time_references;

  // std::map<const Msrun*, std::vector<const QuantiItemBase*>>
  // _missed_quanti_item;

  std::vector<QuantiItemBaseSp> _quanti_item_list;

  static uint _cpu_number;

  mcq_double _minimum_isotope_abundance = 0;

  McqMatchingMode _matching_mode          = McqMatchingMode::no_matching;
  MonitorSpeedInterface *_p_monitor_speed = nullptr;
  // std::map<const Peptide *, std::vector<pappso::pappso_double>> _accumulator;
  QMutex _mutex;

  private:
  XicTraceWriterSp m_xicTraceWriterSp;

  std::map<const Msrun *, std::vector<const QuantiItemBase *>>
    _missed_quanti_item;

  /** @brief pappso::PeptideNaturalIsotopeList minimum ratio to compute
   */
  double m_minimumRatioToCompute = 0.001;
};
