/**
 * \file utilities.cpp
 * \date November 29, 2010
 * \author Edlira Nano
 */

#include "utilities.h"
#include <QDebug>
#include <algorithm>
#include <functional>
#include <numeric>
#include <time.h>
#include <QRegularExpression>

mcq_double
get_average(constvdoubleit beginit, constvdoubleit endit)
{
  mcq_double nb_element = (endit - beginit);
  mcq_double sum(0), result(0);
  if(nb_element != 0)
    {
      const mcq_double init_accumul(0);
      sum    = std::accumulate(beginit, endit, init_accumul);
      result = sum / nb_element;
    }
  return result;
}

mcq_double
get_median(constvdoubleit beginit, constvdoubleit endit)
{
  unsigned int nb_element = (endit - beginit);
  std::vector<mcq_double> vcopy;
  vcopy.resize(nb_element);
  std::copy(beginit, endit, vcopy.begin());
  std::sort(vcopy.begin(), vcopy.end());

  mcq_double result(0);
  if(nb_element % 2 == 0)
    {
      constvdoubleit centerit1(vcopy.begin() + (nb_element / 2) - 1);
      constvdoubleit centerit2(centerit1 + 1);
      result = (*centerit1 + *centerit2) / 2;
    }
  else
    {
      constvdoubleit centerit(vcopy.begin() + (nb_element / 2));
      result = *centerit;
    }
  return result;
}

mcq_double
get_spike_to_zero(constvdoubleit beginit, constvdoubleit endit)
{
  // check if other values are 0
  int nb_element = (endit - beginit);
  constvdoubleit centerit;
  if(nb_element % 2 == 0)
    {
      centerit = beginit + (nb_element / 2) - 1;
    }
  else
    {
      centerit = beginit + (nb_element / 2);
    }
  if((*centerit) > 0)
    {
      int num_items = count(beginit, endit, 0);
      if(num_items == (nb_element - 1))
        {
          return (0);
        }
    }
  return (*centerit);
}

Utilities::Utilities()
{
}

Utilities::~Utilities()
{
}

const QString
Utilities::toString(pappso::XicExtractMethod xic_type)
{
  switch(xic_type)
    {
      case pappso::XicExtractMethod::max:
        return QString("max");
        break;
      case pappso::XicExtractMethod::sum:
        return QString("sum");
        break;
    }
  throw mcqError(QObject::tr(
    "Utilities::toString(pappso::XicExtractMethod xic_type) ERROR\n"));
}

const QString
Utilities::toString(const McqMatchingMode mode)
{
  switch(mode)
    {
      case(McqMatchingMode::no_matching):
        return QString("no_matching");
        break;
      case(McqMatchingMode::post_matching):
        return QString("post_matching");
        break;
      case(McqMatchingMode::real_or_mean):
        return QString("real_or_mean");
        break;
    }
  throw mcqError(
    QObject::tr("Utilities::toString(McqMatchingMode mode) ERROR\n"));
}

std::vector<mcq_double> *
Utilities::newVectorByApplyingWindowsOperation(
  const std::vector<mcq_double> *old_vector,
  unsigned int half_edge_size,
  mcq_double (*p_sub_function)(constvdoubleit begin, constvdoubleit end))
{

  // check if the size of the treatment window is greater than
  // the vector's size
  unsigned int window_size(1 + 2 * half_edge_size);
  unsigned int vector_size = old_vector->size();

  if(window_size > vector_size)
    {

      // char err_vector[50], err_window[50];
      // std::sprintf(err_vector, "%d", vector_size);
      // std::sprintf(err_window, "%d", window_size);

      throw mcqError(
        QObject::tr("error in Utilities::newVectorByApplyingWindowsOperation "
                    ":\n trying to apply a treatment : %1\n greater than the "
                    "starting vector size : %2\n")
          .arg(window_size, vector_size));
    }

  // create new vector where to put the result, of same size than the starting
  // one
  std::vector<mcq_double> *new_vector = new std::vector<mcq_double>;
  new_vector->reserve(old_vector->size());

  /****** BEGIN : applying function p_sub_function*****************************
   * For each element elmt of our given vector, we insert in our result vector
   the element = p_sub_function(elmt-half_edge_size, elmt+half_edge_size)

   For the first and the last half_edge_size elements this is impossible,
   so we behave differentely
  ******/
  constvdoubleit it_oldv;
  mcq_double sun_fct_result;

  /* For the first half_edge_size elements elmt, we calculate p_sub_function
     on a window going from as much as we can elements before elmt
     (depending on the position of elmt) to the half_edge_size elements beyond
     elmt. Therefore the result is p_sub_function(elmt-position,
     elmt+half_edge_size)
     where position is the position of elmt in the vector.
   */

  for(unsigned int position = 0; position < half_edge_size; ++position)
    {
      sun_fct_result =
        (*p_sub_function)(old_vector->begin(),
                          old_vector->begin() + half_edge_size + position + 1);
      new_vector->push_back(sun_fct_result);
    }

  /* applying function to the middle elements :
     window = elmt - half_edge_size, elmt + half_edge_size
  */

  for(it_oldv = old_vector->begin() + half_edge_size;
      it_oldv < old_vector->end() - half_edge_size;
      ++it_oldv)
    {

      new_vector->push_back((*p_sub_function)(it_oldv - half_edge_size,
                                              it_oldv + half_edge_size + 1));
    }

  /* applying function to the last half_edge_size elements
     the same way we did for the first elements
  */

  for(it_oldv = old_vector->end() - half_edge_size;
      it_oldv != old_vector->end();
      ++it_oldv)
    {
      new_vector->push_back(
        (*p_sub_function)(it_oldv - half_edge_size, old_vector->end()));
    }
  /*********END : applying function p_sub_function****************/

  return (new_vector);
}

bool
Utilities::isValidSequence(const QString &sequence,
                           QRegularExpression::PatternOptions cs)
{
  QRegularExpression match_aaseq("^[A,R,N,D,C,E,Q,G,H,I,L,K,M,F,P,S,T,W,Y,V]*$",
                                 cs);
  return (sequence.contains(match_aaseq));
}

Duration
Utilities::getDurationFromDates(const QDateTime &date_begin,
                                const QDateTime &date_end)
{
  Duration ret_vector(4, 0);
  const int secs_elapsed    = date_begin.secsTo(date_end);
  const int secs_per_day    = 3600 * 24;
  const int secs_per_hour   = 3600;
  const int secs_per_minute = 60;
  const int days            = secs_elapsed / secs_per_day;
  ret_vector.at(0)          = days;
  int days_in_secs          = days * secs_per_day;
  const int hours           = (secs_elapsed - days_in_secs) / secs_per_hour;
  ret_vector.at(1)          = hours;
  int hours_in_secs         = hours * secs_per_hour;
  const int minutes =
    (secs_elapsed - days_in_secs - hours_in_secs) / secs_per_minute;
  ret_vector.at(2)    = minutes;
  int minutes_in_secs = minutes * secs_per_minute;
  const int seconds =
    secs_elapsed - days_in_secs - hours_in_secs - minutes_in_secs;
  ret_vector.at(3) = seconds;
  return ret_vector;
}

int
Utilities::getDaysFromDuration(const Duration &d)
{
  return d.at(0);
}

int
Utilities::getHoursFromDuration(const Duration &d)
{
  return d.at(1);
}

int
Utilities::getMinutesFromDuration(const Duration &d)
{
  return d.at(2);
}

int
Utilities::getSecondsFromDuration(const Duration &d)
{
  return d.at(3);
}

bool
Utilities::isSorted(const std::vector<mcq_double> &v)
{
  return (adjacent_find(v.begin(), v.end(), std::greater<mcq_double>()) ==
          v.end());
}
