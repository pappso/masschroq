/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file utilities.h
 * \date November 29, 2010
 * \author Edlira Nano
 */

#pragma once

#include "../../config.h"
#include "../../exportimportconfig.h"
#include "../mcq_error.h"
#include <QDateTime>
#include <map>
#include <pappsomspp/types.h>
#include <vector>
#include <QRegularExpression>

class spectrum;

typedef std::vector<mcq_double>::const_iterator constvdoubleit;

typedef std::vector<int> Duration;

mcq_double get_average(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_median(constvdoubleit beginit, constvdoubleit endit);

mcq_double get_spike_to_zero(constvdoubleit beginit, constvdoubleit endit);

/**
   \class Utilities
   \brief This class implements some calculation algorithms needed by
   MassChroQ
*/

class MCQ_LIB_DECL Utilities
{
  private:
  Utilities();

  virtual ~Utilities();

  public:
  /**
   * \brief Creates a new vector by applying a given function to a given vector.
   *
   * The given function calculates the value of an element of the vector by
   * applying operations (like mean, median) to the elements located in a given
   * window on the vector.
   */

  static std::vector<mcq_double> *newVectorByApplyingWindowsOperation(
    const std::vector<mcq_double> *p_v_intensity,
    unsigned int half_edge_size,
    mcq_double (*p_sub_function)(constvdoubleit begin, constvdoubleit end));

  /// verify if a given amino-acid sequence is valid

  static bool isValidSequence(const QString &seq,
                              QRegularExpression::PatternOptions cs);

  static Duration getDurationFromDates(const QDateTime &date_begin,
                                       const QDateTime &date_end);

  static int getDaysFromDuration(const Duration &d);

  static int getHoursFromDuration(const Duration &d);

  static int getMinutesFromDuration(const Duration &d);

  static int getSecondsFromDuration(const Duration &d);

  static bool isSorted(const std::vector<mcq_double> &v);

  static const QString toString(const McqMatchingMode raw_data_format);
  static const QString toString(pappso::XicExtractMethod raw_data_format);
};
