
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "alignedxicpeak.h"
#include "../mcq_error.h"
#include <QDataStream>
#include <QDebug>

AlignedXicPeak::AlignedXicPeak(const pappso::TracePeak &peak,
                               const Msrun *p_msrun)
  : pappso::TracePeak(peak)
{
  m_left.x         = p_msrun->getAlignedRtByOriginalRt(m_left.x);
  m_right.x        = p_msrun->getAlignedRtByOriginalRt(m_right.x);
  m_max.x          = p_msrun->getAlignedRtByOriginalRt(m_max.x);
  m_nonAlignedPeak = peak;
}

AlignedXicPeak::AlignedXicPeak(const AlignedXicPeak &other)
  : pappso::TracePeak(other)
{
  m_nonAlignedPeak = other.m_nonAlignedPeak;
}

AlignedXicPeak::~AlignedXicPeak()
{
}

void
AlignedXicPeak::serialize(QDataStream *p_outputstream) const
{
  qDebug() << "AlignedXicPeak::serialize begin";
  try
    {
      (*p_outputstream) << m_area << m_left.x << m_left.y << m_max.x << m_max.y
                        << m_right.x << m_right.y;
    }
  catch(const std::exception &ex)
    {
      qDebug() << QObject::tr("error in AlignedXicPeak::serialize :\n%1")
                    .arg(ex.what());
      throw mcqError(
        QObject::tr("error in AlignedXicPeak::serialize :\n%1").arg(ex.what()));
    }
  qDebug() << "AlignedXicPeak::serialize end";
}

bool
AlignedXicPeak::unserialize(QDataStream *instream)
{
  *instream >> m_area;
  *instream >> m_left.x;
  *instream >> m_left.y;
  *instream >> m_max.x;
  *instream >> m_max.y;
  *instream >> m_right.x;
  *instream >> m_right.y;

  if(instream->status() != QDataStream::Ok)
    {
      throw mcqError(
        QString("error in AlignedXicPeak::unserialize :\nread AlignedXicPeak "
                "from datastream failed. QDataStream status=%1")
          .arg(instream->status()));
    }
  return true;
}

const pappso::TracePeak
AlignedXicPeak::getNonAlignedPeak() const
{
  return m_nonAlignedPeak;
}
