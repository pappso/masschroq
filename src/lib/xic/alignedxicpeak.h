
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include "../msrun/msrun.h"
#include <pappsomspp/processing/detection/tracepeak.h>

class AlignedXicPeak;
typedef std::shared_ptr<const AlignedXicPeak> AlignedXicPeakSp;

class AlignedXicPeak : public pappso::TracePeak
{
  public:
  AlignedXicPeak(const TracePeak &peak, const Msrun *p_msrun);
  AlignedXicPeak(const AlignedXicPeak &other);
  ~AlignedXicPeak();

  void serialize(QDataStream *p_outputstream) const;
  bool unserialize(QDataStream *instream);

  const pappso::TracePeak getNonAlignedPeak() const;

  private:
  pappso::TracePeak m_nonAlignedPeak;
};
