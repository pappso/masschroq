/**
 * \file xic_base.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include <algorithm>

#include "../quanti_items/quantiItemBase.h"
#include "xic_base.h"

xicBase::xicBase(const Msrun *p_ms_run,
                 const pappso::MzRange &mz_range,
                 pappso::XicExtractMethod xic_type)
  : _p_ms_run(p_ms_run), _mz_range(mz_range), _xic_type(xic_type)
{
}

xicBase::xicBase(const xicBase &xic_base)
  : pappso::Xic(xic_base),
    _p_ms_run(xic_base._p_ms_run),
    _mz_range(xic_base._mz_range),
    _xic_type(xic_base._xic_type)
{
}

xicBase::~xicBase()
{
}

const Msrun *
xicBase::getMsRun() const
{
  return (_p_ms_run);
}

std::vector<mcq_double>
xicBase::getAlignedRetentionTimes() const
{
  std::vector<mcq_double> p_v_aligned_rt;
  auto it    = begin();
  auto itend = end();
  for(; it != itend; ++it)
    {
      p_v_aligned_rt.push_back(_p_ms_run->getAlignedRtByOriginalRt(it->x));
    }
  return (p_v_aligned_rt);
}

const std::vector<mcq_double>
xicBase::getIntensities() const
{
  return (this->yValues());
}

void
xicBase::fillDataArray(mcq_double *xdata,
                       mcq_double *ydata,
                       unsigned int plotsize [[maybe_unused]]) const
{
  auto it    = begin();
  auto itend = end();
  for(size_t i = 0; it != itend; ++it, ++i)
    {
      xdata[i] = it->x;
      ydata[i] = it->y;
    }
}

void
xicBase::applyFilter(const pappso::FilterInterface &filter)
{
  // Xic newxic;
  // filter.filter(*this, &newxic);
  // m_dataPoints.resize(newxic.size());
  // copy(newxic.begin(), newxic.end(), m_dataPoints.begin());

  filter.filter(*this);
}

void
xicBase::toStream(QTextStream &to_stream) const
{

  auto it    = begin();
  auto itend = end();
  for(; it != itend; ++it)
    {
      to_stream << it->x << " ";
    }
  to_stream << Qt::endl;

  it    = begin();
  itend = end();
  for(; it != itend; ++it)
    {
      to_stream << it->y << " ";
    }
  to_stream << Qt::endl;
}
