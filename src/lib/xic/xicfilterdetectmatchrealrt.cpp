/** @file src/lib/xic/xicfilterdetectmatchrealrt.cpp
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "xicfilterdetectmatchrealrt.h"
#include "../quanti_items/quantiItemBase.h"

XicFilterDetectMatchRealRt::XicFilterDetectMatchRealRt(
  Quantificator *p_quantificator, McqMatchingMode matching_mode)
{
  qDebug();

  _p_quantificator = p_quantificator;
  _matching_mode   = matching_mode;

  qDebug();
}

XicFilterDetectMatchRealRt::~XicFilterDetectMatchRealRt()
{
  qDebug() << this;
}

void
XicFilterDetectMatchRealRt::filterDetectQuantify(
  const pappso::Xic &xic,
  pappso::TracePeakList &peak_list,
  pappso::pappso_double rt_target,
  const std::vector<MsMsRtIntensity> &p_real_rt,
  QuantiItemBase *p_currentSearchItem,
  const Msrun *p_msrun)
{

  mp_matchedPeak = nullptr;


  _p_current_search_item = p_currentSearchItem;
  _p_current_msrun       = p_msrun;
  //_p_real_rt             = p_real_rt;


  qDebug();
  std::size_t nb_match;
  bool match_between_run = false;
  std::vector<double> rt_list;
  for(auto &&msms : p_real_rt)
    {
      rt_list.push_back(msms.rt);
    }
  if(rt_list.size() == 0)
    {
      rt_list.push_back(rt_target);
      match_between_run = true;
    }

  qDebug();
  auto it_best_matched_peak = findBestTracePeakGivenRtList(
    peak_list.begin(), peak_list.end(), rt_list, nb_match);

  qDebug();
  if(nb_match != 0)
    {
      if(nb_match == 1)
        {
          qDebug() << " best CHOICE "
                      " nb_match="
                   << nb_match << " " << this;
          // const pappso::XicPeak * p_peak =
          // _peak_counter_map.begin()->first.get();
          PeakQualityCategory category = PeakQualityCategory::a;
          if(match_between_run)
            {
              category = PeakQualityCategory::b;
            }
          else
            { // peak match from direct MSMS observation

              if(_matching_mode == McqMatchingMode::post_matching)
                {
                  // store aligned rt of this matched peak
                  AlignedXicPeak aligned_peak(*it_best_matched_peak,
                                              _p_current_msrun);
                  _p_current_search_item->getPeptideRtSp()
                    .get()
                    ->postMatchingAlignedPeak(aligned_peak);
                }
              if(rt_list.size() > 1)
                {
                  category = PeakQualityCategory::aa;
                }
            }

          writeDetectionResult(*it_best_matched_peak, category);
        }
      else
        {
          // this is a real observation, but a fragmented peak :/
          PeakQualityCategory category = PeakQualityCategory::ab;
          qDebug() << " nb_match=" << nb_match;


          writeDetectionResult(*it_best_matched_peak, category);

          if(_matching_mode == McqMatchingMode::post_matching)
            { // do not store it, because reliability is poor
            }
        }
    }
  else
    {

      /** @comments look for small little peaks nearby (second chance)
       */
      if((!match_between_run) &&
         _p_quantificator->getQuantificationMethod()->getSecondChance())
        {
          qDebug() << " no match with retention time" << this;
          //_monitor.writeMatchedPeak(
          //  _p_current_msrun, _p_current_search_item, nullptr);

          if(peak_list.size() > 0)
            {
              unsigned int nbpeaks_near = 0;
              for(unsigned int i = 0; i < rt_list.size(); i++)
                {
                  pappso::pappso_double rt = rt_list[i];
                  for(unsigned int j = 0; j < peak_list.size(); j++)
                    {
                      pappso::TracePeak &current_peak = peak_list[j];
                      if(current_peak.getMaxXicElement().x < rt)
                        {
                          if((rt - current_peak.getRightBoundary().x) < 10)
                            {
                              unsigned int distance_point =
                                xic.getMsPointDistance(
                                  rt, current_peak.getRightBoundary().x);
                              if(distance_point < 10)
                                {
                                  nbpeaks_near++;
                                }
                            }
                        }
                      else
                        {
                          if((current_peak.getLeftBoundary().x - rt) < 10)
                            {
                              unsigned int distance_point =
                                xic.getMsPointDistance(
                                  rt, current_peak.getLeftBoundary().x);
                              if(distance_point == 0)
                                {
                                  writeDetectionResult(current_peak,
                                                       PeakQualityCategory::d);

                                  if(_matching_mode ==
                                     McqMatchingMode::post_matching)
                                    {
                                      // store aligned rt of this matched
                                      // peak warning : this could be risky
                                      //_p_current_search_item->getPeptideRtSp()
                                      //  .get()
                                      // ->postMatchingAlignedPeak(aligned_peak);
                                    }

                                  return;
                                }
                              nbpeaks_near++;
                            }
                        }
                    }
                }
              pappso::PeptideNaturalIsotopeAverageSp peptide_natural_sp =
                _p_current_search_item->getPeptideNaturalIsotopeAverageSp();
              if(nbpeaks_near > 0)
                {
                  if((peptide_natural_sp == nullptr) ||
                     (peptide_natural_sp.get()->getIsotopeNumber() == 0))
                    {
                      //                 _monitor.logMissedObservedRealXic(_p_current_search_item,
                      //                 p_origxic, p_real_rt,_p_log_peaks);
                    }

                  // it seems to be better to forget quanti items that was
                  // not matched here : leave it in comments
                  //_p_quantificator->addMissedQuantiItem(_p_current_msrun,
                  //_p_current_search_item);
                }
            }
          else
            {
              // no peaks : nothing to do
            }
        }


      if(match_between_run &&
         (_matching_mode == McqMatchingMode::post_matching))
        {
          for(auto &&peak : peak_list)
            {
              // AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
              AlignedXicPeakSp aligned_peak_sp =
                std::make_shared<const AlignedXicPeak>(peak, _p_current_msrun);
              _p_current_search_item->storeAlignedXicPeakForPostMatching(
                _p_current_msrun, aligned_peak_sp);
            }
        }
    }

  qDebug() << " " << this;
}

void
XicFilterDetectMatchRealRt::writeDetectionResult(
  const pappso::TracePeak &peak, PeakQualityCategory mbr_category)
{

  mp_matchedPeak = &peak;
  msp_alignedPeak =
    std::make_shared<const AlignedXicPeak>(peak, _p_current_msrun);
  m_mbrCategory = mbr_category;
}


const pappso::TracePeak *
XicFilterDetectMatchRealRt::getMatchedPeakPtr() const
{
  return mp_matchedPeak;
}

PeakQualityCategory
XicFilterDetectMatchRealRt::getPeakQualityCategory() const
{
  return m_mbrCategory;
}

const AlignedXicPeakSp &
XicFilterDetectMatchRealRt::getAlignedXicPeakSp() const
{
  return msp_alignedPeak;
}
