/** @file src/lib/xic/xicfilterdetectmatchrealrt.h
 */


/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include "../quantificator.h"
#include "xictracebase.h"
#include <QMutex>
#include <pappsomspp/processing/detection/tracepeaklist.h>

class XicFilterDetectMatchRealRt
{
  public:
  XicFilterDetectMatchRealRt(Quantificator *p_quantificator,
                             McqMatchingMode matching_mode);
  ~XicFilterDetectMatchRealRt();

  void filterDetectQuantify(const pappso::Xic &xic,
                            pappso::TracePeakList &peak_list,
                            pappso::pappso_double rt_target,
                            const std::vector<MsMsRtIntensity> &p_real_rt,
                            QuantiItemBase *p_currentSearchItem,
                            const Msrun *p_msrun);

  const pappso::TracePeak *getMatchedPeakPtr() const;

  const AlignedXicPeakSp &getAlignedXicPeakSp() const;

  PeakQualityCategory getPeakQualityCategory() const;

  private:
  void writeDetectionResult(const pappso::TracePeak &peak,
                            PeakQualityCategory mbr_category);

  private:
  Quantificator *_p_quantificator;

  // const std::vector<MsMsRtIntensity> &_p_real_rt;
  QuantiItemBase *_p_current_search_item = nullptr;
  const Msrun *_p_current_msrun;

  McqMatchingMode _matching_mode;

  const pappso::TracePeak *mp_matchedPeak = nullptr;

  AlignedXicPeakSp msp_alignedPeak  = nullptr;
  PeakQualityCategory m_mbrCategory = PeakQualityCategory::missed;
  // QMutex _mutex;
};
