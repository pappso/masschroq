
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "xictracebase.h"
#include "xic_base.h"
#include <pappsomspp/pappsoexception.h>

XicTraceBase::XicTraceBase(CalcWriterInterface *p_writer,
                           const QuantiItemBase *p_quanti_item_base,
                           const Msrun *p_msrun)
  : _p_writer(p_writer), _p_quanti_item(p_quanti_item_base), _p_msrun(p_msrun)
{
}

void
XicTraceBase::setXic(pappso::XicCoordSPtr xic_coord)
{
  msp_xicCoord = xic_coord;
}
void
XicTraceBase::setMsMsRtList(const std::vector<MsMsRtIntensity> &rt_list)
{
  _p_rt_list = new std::vector<MsMsRtIntensity>(rt_list);
}

void
XicTraceBase::setMatchedPeak(const pappso::TracePeak &xic_peak)
{
  _p_matched_xic_peak = xic_peak.makeTracePeakCstSPtr();
}

void
XicTraceBase::setAlignedMatchedPeak(const AlignedXicPeak &aligned_peak)
{
  _p_aligned_matched_xic_peak = aligned_peak.makeTracePeakCstSPtr();
}

void
XicTraceBase::addXicPeak(pappso::TracePeak &xic_peak)
{
  _peak_list.push_back(xic_peak);
}


void
XicTraceBase::setAlignedRtTarget(pappso::pappso_double aligned_rt_target)
{
  _aligned_rt_target = aligned_rt_target;
}
void
XicTraceBase::setRtTarget(pappso::pappso_double rt_target)
{
  _rt_target = rt_target;
}
void
XicTraceBase::close()
{
  writeQuantiItemDescription();
  writeXicRtPeaks();

  if(_p_matched_xic_peak != nullptr)
    {
      _p_writer->writeSheet("match");
      _p_writer->writeCell("area");
      _p_writer->writeCell("start rt");
      _p_writer->writeCell("max rt");
      _p_writer->writeCell("end rt");
      _p_writer->writeCell("start intensity");
      _p_writer->writeCell("max intensity");
      _p_writer->writeCell("end intensity");
      _p_writer->writeLine();
      _p_writer->writeCell(_p_matched_xic_peak->getArea());
      _p_writer->writeCell(_p_matched_xic_peak->getLeftBoundary().x);
      _p_writer->writeCell(_p_matched_xic_peak->getMaxXicElement().x);
      _p_writer->writeCell(_p_matched_xic_peak->getRightBoundary().x);
      _p_writer->writeCell(_p_matched_xic_peak->getLeftBoundary().y);
      _p_writer->writeCell(_p_matched_xic_peak->getMaxXicElement().y);
      _p_writer->writeCell(_p_matched_xic_peak->getRightBoundary().y);

      if(_p_aligned_matched_xic_peak != nullptr)
        {
          _p_writer->writeLine();
          _p_writer->writeCell("aligned rt");
          _p_writer->writeCell(
            _p_aligned_matched_xic_peak->getLeftBoundary().x);
          _p_writer->writeCell(
            _p_aligned_matched_xic_peak->getMaxXicElement().x);
          _p_writer->writeCell(
            _p_aligned_matched_xic_peak->getRightBoundary().x);
        }
    }
}

XicTraceBase::~XicTraceBase()
{
  delete _p_writer;
  if(_p_rt_list != nullptr)
    {
      delete _p_rt_list;
    }
}

void
XicTraceBase::writeQuantiItemDescription()
{

  if(msp_xicCoord == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in %1, %2, %3 :\n msp_xicCoord == nullptr")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  _p_writer->writeSheet("description");
  _p_writer->writeCell("mz");
  _p_writer->writeCell("min mz");
  _p_writer->writeCell("max mz");

  _p_writer->writeCell("charge");

  _p_writer->writeLine();
  _p_writer->writeCell(msp_xicCoord.get()->mzRange.getMz());

  _p_writer->writeCell(msp_xicCoord.get()->mzRange.lower());

  _p_writer->writeCell(msp_xicCoord.get()->mzRange.upper());

  _p_writer->writeCell((std::size_t) * (_p_quanti_item->getZ()));
  _p_writer->writeLine();
}

void
XicTraceBase::writeXicRtPeaks()
{
  _p_writer->writeSheet("xic");


  pappso::Xic *p_xic = msp_xicCoord.get()->xicSptr.get();

  _p_quanti_item->writeOdsPeptideLine(*_p_writer);

  if(_p_rt_list != nullptr)
    {
      _p_writer->writeLine();
      _p_writer->writeCell("xic size");
      _p_writer->writeCell("observed MSMS count");
      _p_writer->writeCell("peak count");
      _p_writer->writeLine();
      _p_writer->writeCell((int)p_xic->size());
      _p_writer->writeCell((int)_p_rt_list->size());
      _p_writer->writeCell((int)_peak_list.size());
    }
  else
    {
      _p_writer->writeLine();
      _p_writer->writeCell("xic size");
      _p_writer->writeCell("aligned target RT");
      _p_writer->writeCell("target RT");
      _p_writer->writeLine();
      _p_writer->writeCell((int)p_xic->size());
      _p_writer->writeCell(_aligned_rt_target);
      _p_writer->writeCell(_rt_target);

      _p_rt_list = new std::vector<MsMsRtIntensity>();
      MsMsRtIntensity ms;
      ms.rt = _rt_target;
      _p_rt_list->push_back(ms);
    }

  _p_writer->writeLine();
  _p_writer->writeLine();
  _p_writer->writeCell("xic rt");
  _p_writer->writeCell("xic intensity");
  _p_writer->writeCell("MSMS intensity");
  _p_writer->writeCell("peak boundings");

  auto it     = p_xic->begin();
  auto itnext = p_xic->begin()++;
  auto itend  = p_xic->end();

  std::vector<MsMsRtIntensity> real_rt(*_p_rt_list);
  std::sort(real_rt.begin(),
            real_rt.end(),
            [](const MsMsRtIntensity &A, const MsMsRtIntensity &B) {
              return (A.rt < B.rt);
            });
  std::vector<MsMsRtIntensity>::const_iterator it_real_rt    = real_rt.begin();
  std::vector<MsMsRtIntensity>::const_iterator itend_real_rt = real_rt.end();

  std::sort(_peak_list.begin(),
            _peak_list.end(),
            [](const pappso::TracePeak &A, const pappso::TracePeak &B) {
              return (A.getMaxXicElement().x < B.getMaxXicElement().x);
            });
  std::vector<pappso::TracePeak>::const_iterator it_peak = _peak_list.begin();
  std::vector<pappso::TracePeak>::const_iterator itend_peak = _peak_list.end();

  while(it != itend)
    {
      _p_writer->writeLine();
      pappso::pappso_double rt     = it->x;
      pappso::pappso_double nextrt = 10000000000000000000.0;
      if(itnext != itend)
        {
          nextrt = itnext->x;
        }

      _p_writer->writeCell(rt);
      _p_writer->writeCell(it->y);

      bool write_msms = false;
      if(it_real_rt != itend_real_rt)
        {
          while(((it_real_rt + 1) != itend_real_rt) && (it_real_rt->rt < rt))
            {
              it_real_rt++;
            }
          if(it_real_rt->rt == rt)
            {
              _p_writer->writeCell(it_real_rt->intensity);
              it_real_rt++;
            }
          else if(it_real_rt->rt < nextrt)
            {
              write_msms = true;
              _p_writer->writeEmptyCell();
            }
          else
            {
              _p_writer->writeEmptyCell();
            }
        }
      else
        {
          _p_writer->writeEmptyCell();
        }

      if(it_peak != itend_peak)
        {
          if(rt == it_peak->getLeftBoundary().x)
            {
              _p_writer->writeCell(it_peak->getLeftBoundary().y);
              _p_writer->writeCell("begin");
            }

          if(rt == it_peak->getMaxXicElement().x)
            {
              _p_writer->writeCell(it_peak->getMaxXicElement().y);
              _p_writer->writeCell("max");
            }

          if(rt == it_peak->getRightBoundary().x)
            {
              _p_writer->writeCell(it_peak->getRightBoundary().y);
              _p_writer->writeCell("end");
              it_peak++;
            }
        }

      if(write_msms)
        {
          _p_writer->writeLine();
          _p_writer->writeCell(it_real_rt->rt);
          _p_writer->writeEmptyCell();
          _p_writer->writeCell(it_real_rt->intensity);
          it_real_rt++;
        }
      it++;
      itnext++;
    }
}
