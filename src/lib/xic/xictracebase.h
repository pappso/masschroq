
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include "../quanti_items/quantiItemBase.h"
#include <pappsomspp/xic/xic.h>
#include "../../output/detectionresult.h"

class XicTraceBase
{
  public:
  XicTraceBase(CalcWriterInterface *p_writer,
               const QuantiItemBase *p_quanti_item_base,
               const Msrun *p_msrun);
  virtual ~XicTraceBase();
  virtual void setXic(pappso::XicCoordSPtr xic_coord);
  virtual void setMsMsRtList(const std::vector<MsMsRtIntensity> &p_rt_list);
  void addXicPeak(pappso::TracePeak &xic_peak);
  void setAlignedRtTarget(pappso::pappso_double aligned_rt_target);
  void setRtTarget(pappso::pappso_double rt_target);
  virtual void close();
  void setMatchedPeak(const pappso::TracePeak &xic_peak);
  void setAlignedMatchedPeak(const AlignedXicPeak &aligned_peak);

  protected:
  virtual void writeQuantiItemDescription();

  protected:
  CalcWriterInterface *_p_writer;
  const QuantiItemBase *_p_quanti_item;
  const Msrun *_p_msrun;
  pappso::TracePeakCstSPtr _p_aligned_matched_xic_peak = nullptr;
  pappso::TracePeakCstSPtr _p_matched_xic_peak         = nullptr;

  std::vector<MsMsRtIntensity> *_p_rt_list = nullptr;
  std::vector<pappso::TracePeak> _peak_list;
  pappso::pappso_double _aligned_rt_target = -1;
  pappso::pappso_double _rt_target         = -1;

  pappso::XicCoordSPtr msp_xicCoord;

  private:
  void writeXicRtPeaks();
};
