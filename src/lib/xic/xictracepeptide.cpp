
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "xictracepeptide.h"

XicTracePeptide::XicTracePeptide(CalcWriterInterface *p_writer,
                                 const QuantiItemPeptide *p_quanti_item_peptide,
                                 const Msrun *p_msrun)
  : XicTraceBase(p_writer, p_quanti_item_peptide, p_msrun)
{
}

XicTracePeptide::~XicTracePeptide()
{
}

void
XicTracePeptide::writeQuantiItemDescription()
{
  qDebug();
  XicTraceBase::writeQuantiItemDescription();
  PeptideCstSPtr peptide = _p_quanti_item->getPeptide();
  pappso::PeptideSp peptide_sp =
    _p_quanti_item->getPeptide()->getPappsoPeptideSp();

  _p_writer->writeLine();
  _p_writer->writeCell("peptide id");
  _p_writer->writeCell("sequence");
  _p_writer->writeCell("sequence_mod");
  _p_writer->writeCell("mass");

  _p_writer->writeLine();

  _p_writer->writeCell(peptide->getXmlId());
  _p_writer->writeCell(peptide_sp->getSequence());
  _p_writer->writeCell(peptide_sp->toString());
  _p_writer->writeCell(peptide_sp->getMass());
  _p_writer->writeLine();
  qDebug();
}
