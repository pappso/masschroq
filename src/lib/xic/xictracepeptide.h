
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include "xictracebase.h"

#include "../quanti_items/quantiItemPeptide.h"

class XicTracePeptide : public XicTraceBase
{
  public:
  XicTracePeptide(CalcWriterInterface *p_writer,
                  const QuantiItemPeptide *p_quanti_item_peptide,
                  const Msrun *p_msrun);
  virtual ~XicTracePeptide();

  protected:
  virtual void writeQuantiItemDescription() override;
};
