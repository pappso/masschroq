
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xictracepeptidenaturalisotope.h"

XicTracePeptideNaturalIsotope::XicTracePeptideNaturalIsotope(
  CalcWriterInterface *p_writer,
  const QuantiItemPeptide *p_quanti_item_peptide,
  const Msrun *p_msrun)
  : XicTracePeptide(p_writer, p_quanti_item_peptide, p_msrun)
{
}

XicTracePeptideNaturalIsotope::~XicTracePeptideNaturalIsotope()
{
}

void
XicTracePeptideNaturalIsotope::writeQuantiItemDescription()
{
  qDebug()
    << "XicTracePeptideNaturalIsotope::writeQuantiItemDescription() begin";
  XicTracePeptide::writeQuantiItemDescription();
  pappso::PeptideNaturalIsotopeAverageSp isotope =
    _p_quanti_item->getPeptideNaturalIsotopeAverageSp();

  _p_writer->writeLine();
  _p_writer->writeCell("isotope m/z");
  _p_writer->writeCell("number");
  _p_writer->writeCell("rank");
  _p_writer->writeCell("intensity ratio");

  _p_writer->writeLine();

  _p_writer->writeCell(isotope->getMz());
  _p_writer->writeCell((std::size_t)isotope->getIsotopeNumber());
  _p_writer->writeCell((std::size_t)isotope->getIsotopeRank());
  _p_writer->writeCell(isotope->getIntensityRatio());
  _p_writer->writeLine();
}
