#include <QCoreApplication>
#include <QTimer>
//#include <iostream>
#include "lib/mcq_error.h"
#include "masschroq_cli.h"
#include <pappsomspp/pappsoexception.h>
#include <sys/stat.h>

int
main(int argc, char **argv)
{
  
  umask(0);
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

  QCoreApplication app(argc, argv);
  QCoreApplication::setApplicationName("MassChroQ");
  QCoreApplication::setApplicationVersion(MASSCHROQ_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  masschroqCli myMain;
  // connect up the signals
  
  // Qt5 code
  QObject::connect(&myMain, &masschroqCli::finished, &app, &QCoreApplication::quit);
  QObject::connect(
    &app, &QCoreApplication::aboutToQuit, &myMain, &masschroqCli::aboutToQuitApp);

  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
