/*
    MassChroQ : Mass Chromatogram Quantification
    Copyright (C) 2014  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lib/mass_chroq.h"
#include "lib/share/utilities.h"
#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFont>
#include <QLocale>
#include <QString>

#include "lib/consoleout.h"
#include <QCoreApplication>
#include <QObject>
#include "exportimportconfig.h"

class MCQ_LIB_DECL masschroqCli : public QObject
{
  Q_OBJECT

  private:
  QCoreApplication *app;

  public:
  void windaube_exit();

  explicit masschroqCli(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();
};
