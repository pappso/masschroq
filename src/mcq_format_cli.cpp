
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "mcq_format.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QTimer>
#include <iostream>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>
#include <vector>

void
writeMcqTable(QFile &mcqDataFile, CalcWriterInterface &writer)
{

  McqMlToTable parser(writer);
  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(&parser);
  simplereader.setErrorHandler(&parser);
  QXmlInputSource xmlInputSource(mcqDataFile);
  if(simplereader.parse(xmlInputSource))
    {
    }
  else
    {
      throw GpError(QObject::tr("error reading input file :\n")
                      .append(parser.errorString()));
    }
}

McqFormatCli::McqFormatCli(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
McqFormatCli::run()
{

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "McqFormatCli::run() begin";
      QCommandLineParser parser;

      parser.setApplicationDescription(
        QString(SOFTWARE_NAME)
          .append(" ")
          .append(MASSCHROQ_VERSION)
          .append(" format tool.\n reads MassChroqML results and export it "
                  "in other formats."));
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument(
        "peptide", QCoreApplication::translate("main", "peptide sequence"));
      QCommandLineOption mcqData(
        QStringList() << "i"
                      << "MassChroqML",
        QCoreApplication::translate("main", "MassChroqML data file <input>."),
        QCoreApplication::translate("main", "input"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"),
        QString());
      parser.addOption(odsOption);
      parser.addOption(tsvOption);
      parser.addOption(mcqData);

      qDebug() << "McqFormatCli::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "McqFormatCli.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroq_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();

      // QTextStream outputStream(stdout, QIODevice::WriteOnly);
      // outputStream.setRealNumberPrecision(10);
      // TsvOutputStream writer(outputStream);

      QString mcqDataFileStr = parser.value(mcqData);
      QFile mcqDataFile;
      if(fastaFileStr.isEmpty())
        {
          mcqDataFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          mcqDataFile.setFileName(mcqDataFileStr);
          mcqDataFile.open(QIODevice::ReadOnly);
        }

      QString odsFileStr = parser.value(odsOption);
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);
          // quantificator.report(writer);
          writeMcqTable(mcqDataFile, writer);
          writer.close();
          file.close();
        }
      QString tsvFileStr = parser.value(tsvOption);
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          // file.open(QIODevice::WriteOnly);
          TsvDirectoryWriter writer(directory);
          writeMcqTable(mcqDataFile, writer);
          // quantificator.report(writer);
          writer.close();
        }

      // readMzXml();
      qDebug() << "MasschroqPrmCli::run() end";
    }
  catch(pappso::PappsoException &error)
    {
      errorStream
        << "MassChroQ format tool encountered an error:"
        << endl;
      errorStream << error.qwhat() << endl;
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream
        << "MassChroQ format tool encountered an error:"
        << endl;
      errorStream << error.what() << endl;
      app->exit(1);
    }

  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
McqFormatCli::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
McqFormatCli::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}

int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QApplication app(argc, argv);

  qDebug() << "main 1";
  QApplication::setApplicationName("MassChroQ format");
  QApplication::setApplicationVersion(MASSCHROQ_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  McqFormatCli myMain;
  // connect up the signals
  qDebug() << "main 2";

#if QT_VERSION >= 0x050000
  // Qt5 code
  QObject::connect(&myMain, &McqFormatCli::finished, &app, &QApplication::quit);
  QObject::connect(
    &app, &QApplication::aboutToQuit, &myMain, &McqFormatCli::aboutToQuitApp);
#else
  // Qt4 code
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
#endif
  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
