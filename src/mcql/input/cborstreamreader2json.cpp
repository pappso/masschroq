/**
 * \file mcql/input/cborstreamreader2json.cpp
 * \date 08/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to produce JSON readable document
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "cborstreamreader2json.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <QJsonArray>

mcql::CborStreamReader2Json::CborStreamReader2Json()
{
}

mcql::CborStreamReader2Json::~CborStreamReader2Json()
{
}

bool
mcql::CborStreamReader2Json::getExpectedString()
{

  bool is_ok       = false;
  m_expectedString = "";
  if(m_cborReader.type() == QCborStreamReader::String)
    {
      is_ok  = true;
      auto r = m_cborReader.readString();
      while(r.status == QCborStreamReader::Ok)
        {
          m_expectedString = r.data;

          r = m_cborReader.readString();
        }
    }

  return is_ok;
}


void
mcql::CborStreamReader2Json::readCbor(QFile *cborp,
                                      pappso::UiMonitorInterface &monitor)
{
  // try to mmap the file, this is faster
  char *ptr = reinterpret_cast<char *>(
    cborp->map(0, cborp->size(), QFile::MapPrivateOption));
  if(ptr)
    {
      // worked
      m_data = QByteArray::fromRawData(ptr, cborp->size());
      m_cborReader.addData(m_data);
    }
  else if(cborp->isSequential())
    {
      // details requires full contents, so allocate memory
      m_data = cborp->readAll();
      m_cborReader.addData(m_data);
    }
  else
    {
      // just use the QIODevice
      m_cborReader.setDevice(cborp);
    }

  if(m_cborReader.isMap())
    {
      readRoot(monitor);
    }
}

void
mcql::CborStreamReader2Json::readRoot(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer();

  while(getExpectedString())
    {
      if(m_expectedString == "informations")
        {
          writeCborObject("informations", QCborValue::fromCbor(m_cborReader));
        }
      else if(m_expectedString == "project_parameters")
        {
          writeCborObject("project_parameters",
                          QCborValue::fromCbor(m_cborReader));
        }
      else if(m_expectedString == "masschroq_methods")
        {
          writeCborObject("masschroq_methods",
                          QCborValue::fromCbor(m_cborReader));
        }
      else if(m_expectedString == "identification_data")
        {
          writeCborObject("identification_data",
                          QCborValue::fromCbor(m_cborReader));
        }
      else if(m_expectedString == "actions")
        {
          writeCborObject("actions", QCborValue::fromCbor(m_cborReader));
        }
      else if(m_expectedString == "alignment_data")
        {
          // qWarning();
          m_cborReader.next();
          // writeCborObject("alignment_data",
          // QCborValue::fromCbor(m_cborReader));
          //  writeCborObject("alignment_data",
          //  QCborValue::fromCbor(m_cborReader));
          //  qWarning();
        }
      else if(m_expectedString == "quantification_data")
        {
          m_cborReader.enterContainer(); // array
          qWarning();
          QJsonArray qdata_array;
          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              m_cborReader.enterContainer(); // map
              QJsonObject qdata;

              while(getExpectedString())
                {
                  qWarning() << m_expectedString;
                  if(m_expectedString == "first_pass" ||
                     m_expectedString == "second_pass")
                    {
                      m_cborReader.next();
                    }
                  else if(!m_expectedString.isEmpty())
                    {
                      QJsonValue js_value =
                        QCborValue::fromCbor(m_cborReader).toJsonValue();
                      qdata.insert(m_expectedString, js_value);
                      qWarning() << js_value.toString();
                    }
                  // m_cborReader.next();
                }
              qdata_array.append(qdata);
              m_cborReader.leaveContainer();
            }

          m_cborReader.leaveContainer();
          m_jsonRoot.insert("quantification_data", qdata_array);
        }
      else if(m_expectedString == "end")
        {
          writeCborObject("end", QCborValue::fromCbor(m_cborReader));
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1").arg(m_expectedString));
        }
    }
}


void
mcql::CborStreamReader2Json::writeCborObject(const QString &name,
                                             const QCborValue &cbor_value)
{
  QJsonValue json_value;
  m_jsonRoot.insert(name, cbor_value.toJsonValue());
}

const QJsonObject &
mcql::CborStreamReader2Json::getJsonObject() const
{
  return m_jsonRoot;
}
