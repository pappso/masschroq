/**
 * \file mcql/input/cborstreamreader2json.h
 * \date 08/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to produce JSON readable document
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QCborStreamReader>
#include <QJsonObject>
#include <QStack>
#include <QFile>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>

namespace mcql
{
/**
 * @todo write docs
 */
class CborStreamReader2Json
{
  public:
  /**
   * Default constructor
   */
  CborStreamReader2Json();

  /**
   * Destructor
   */
  virtual ~CborStreamReader2Json();


  virtual void readCbor(QFile *cborp, pappso::UiMonitorInterface &monitor);

  const QJsonObject &getJsonObject() const;


  private:
  void readRoot(pappso::UiMonitorInterface &monitor);
  bool getExpectedString();

  void writeCborObject(const QString &name, const QCborValue &cbor_value);

  private:
  QCborStreamReader m_cborReader;
  QByteArray m_data;
  QString m_expectedString;

  QJsonObject m_jsonRoot;
};

} // namespace mcql
