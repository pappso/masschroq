
/**
 * \file mcql/input/cborstreamreaderdebug.h
 * \date 03/01/2025
 * \author Olivier Langella
 * \brief read cbor stream for debug
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <QCborStreamReader>
#include <QStack>
#include <QFile>

namespace mcql
{
/**
 * @todo write docs
 * 
 * https://code.qt.io/cgit/qt/qtbase.git/tree/examples/corelib/serialization/cbordump/main.cpp?h=6.8
 */
struct CborStreamReaderDebug
{
    enum DumpOption { ShowCompact = 0x01, ShowWidthIndicators = 0x02, ShowAnnotated = 0x04 };
    Q_DECLARE_FLAGS(DumpOptions, DumpOption)

    CborStreamReaderDebug(QFile *f, DumpOptions opts_);
    QCborError dump();

private:
    void dumpOne(int nestingLevel);
    void dumpOneDetailed(int nestingLevel);

    void printByteArray(const QByteArray &ba);
    void printWidthIndicator(quint64 value, char space = '\0');
    void printStringWidthIndicator(quint64 value);

    QCborStreamReader reader;
    QByteArray data;
    QStack<quint8> byteArrayEncoding;
    qint64 offset = 0;
    DumpOptions opts;
};
}
