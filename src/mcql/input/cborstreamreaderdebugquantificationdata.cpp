/**
 * \file mcql/input/cborstreamreaderdebugquantificationdata.cpp
 * \date 17/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to extract given quantified petpides
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "cborstreamreaderdebugquantificationdata.h"

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>

mcql::CborStreamReaderDebugQuantificationData::
  CborStreamReaderDebugQuantificationData()
{
}

mcql::CborStreamReaderDebugQuantificationData::
  ~CborStreamReaderDebugQuantificationData()
{
}

void
mcql::CborStreamReaderDebugQuantificationData::setFindPeptideIdList(
  const QStringList &peptide_id)
{
  m_findPeptideIdList = peptide_id;
}


bool
mcql::CborStreamReaderDebugQuantificationData::getExpectedString()
{

  bool is_ok       = false;
  m_expectedString = "";
  if(m_cborReader.type() == QCborStreamReader::String)
    {
      is_ok  = true;
      auto r = m_cborReader.readString();
      while(r.status == QCborStreamReader::Ok)
        {
          m_expectedString = r.data;

          r = m_cborReader.readString();
        }
    }

  return is_ok;
}


void
mcql::CborStreamReaderDebugQuantificationData::readCbor(
  QFile *cborp, pappso::UiMonitorInterface &monitor)
{
  // try to mmap the file, this is faster
  char *ptr = reinterpret_cast<char *>(
    cborp->map(0, cborp->size(), QFile::MapPrivateOption));
  if(ptr)
    {
      // worked
      m_data = QByteArray::fromRawData(ptr, cborp->size());
      m_cborReader.addData(m_data);
    }
  else if(cborp->isSequential())
    {
      // details requires full contents, so allocate memory
      m_data = cborp->readAll();
      m_cborReader.addData(m_data);
    }
  else
    {
      // just use the QIODevice
      m_cborReader.setDevice(cborp);
    }

  if(m_cborReader.isMap())
    {
      readRoot(monitor);
    }
}

const QJsonObject &
mcql::CborStreamReaderDebugQuantificationData::getJsonObject()
{
  return m_jsonRoot;
}
void
mcql::CborStreamReaderDebugQuantificationData::readRoot(
  pappso::UiMonitorInterface &monitor)
{
  m_jsonRoot.insert("find_peptide",
                    QJsonArray::fromStringList(m_findPeptideIdList));


  m_cborReader.enterContainer();

  while(getExpectedString())
    {
      if(m_expectedString == "informations")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "project_parameters")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "masschroq_methods")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "identification_data")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "actions")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "alignment_data")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "quantification_data")
        {
          readQuantificationData(monitor);
        }
      else if(m_expectedString == "end")
        {
          m_cborReader.next();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1").arg(m_expectedString));
        }
    }
}

void
mcql::CborStreamReaderDebugQuantificationData::readQuantificationData(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();


  while(!m_cborReader.lastError() && m_cborReader.hasNext())
    {
      readQuantificationDataObject(monitor);
      // m_cborReader.next();
      //  dumpOne(monitor, 0);
      //  skipCurrentElement();
    }
  qDebug();
  m_cborReader.leaveContainer();
}


void
mcql::CborStreamReaderDebugQuantificationData::readQuantificationDataObject(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  if(m_expectedString == "quantify_id")
    {
      getExpectedString();
      m_quantificationId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting quantify_id element not %1")
          .arg(m_expectedString));
    }


  getExpectedString();
  if(m_expectedString == "group_id")
    {
      getExpectedString();
      m_groupId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting group_id element not %1")
          .arg(m_expectedString));
    }
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  if(m_expectedString == "first_pass")
    {
      readGroupQuantificationPass(monitor);

      m_jsonRoot.insert("found_list_first_pass", m_jsonPeptideArray);
      m_jsonPeptideArray = QJsonArray();
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting first_pass element not %1")
          .arg(m_expectedString));
    }

  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  if(m_expectedString == "second_pass")
    {
      readGroupQuantificationPass(monitor);
      m_jsonRoot.insert("found_list_second_pass", m_jsonPeptideArray);
    }
  else if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
    }
  qDebug();
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      if(m_expectedString.startsWith("timestamp"))
        {
          m_cborReader.next();
          // getExpectedString();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: element %1 not expected")
              .arg(m_expectedString));
        }
    }
  // skipCurrentElement();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
}

void
mcql::CborStreamReaderDebugQuantificationData::readGroupQuantificationPass(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();

  std::size_t size = m_cborReader.length();
  std::size_t i    = 0;
  m_cborReader.enterContainer();
  while(getExpectedString())
    {

      qDebug() << m_expectedString;
      m_msrunId = m_expectedString;
      readQrDataBlock(monitor);
      i++;
      // m_cborReader.next();
    }

  if(i < size)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 msruns out of %2 expected, "
                    "groupid %3")
          .arg(i)
          .arg(size)
          .arg(m_groupId));
    }
  qDebug();
  // skipCurrentElement();
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
}


void
mcql::CborStreamReaderDebugQuantificationData::readQrDataBlock(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString == "msrun")
    {
      m_cborReader.next();
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting msrun element not %1")
          .arg(m_expectedString));
    }
  // retention_time_correction
  getExpectedString();
  if(m_expectedString == "peptide_measurements")
    {
      readPeptideMeasurements(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting peptide_measurements element not %1")
          .arg(m_expectedString));
    }
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
}

void
mcql::CborStreamReaderDebugQuantificationData::readPeptideMeasurements(
  pappso::UiMonitorInterface &monitor)
{
  qDebug() << m_cborReader.length();
  monitor.setTotalSteps(m_cborReader.length());
  m_cborReader.enterContainer(); // map

  std::size_t size = m_cborReader.length();
  std::size_t i    = 0;
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      m_peptideId = m_expectedString;
      if(m_findPeptideIdList.contains(m_peptideId))
        {
          QCborValue cbor_value   = QCborValue::fromCbor(m_cborReader);
          QJsonObject json_object = cbor_value.toJsonValue().toObject();
          json_object.insert("msrun", m_msrunId);
          json_object.insert("group", m_groupId);
          m_jsonPeptideArray.append(json_object);
        }
      else
        {
          m_cborReader.next();
        }
      i++;
      // m_cborReader.next();
    }

  /*
if(i < size)
  {
    throw pappso::PappsoException(
      QObject::tr("ERROR: only %1 peptide_measurements out of %2 expected, "
                  "peptideid %3")
        .arg(i)
        .arg(size)
        .arg(m_peptideId));
  }
  */
  m_cborReader.leaveContainer();
}
