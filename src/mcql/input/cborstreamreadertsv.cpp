/**
 * \file mcql/input/cborstreamreadertsv.cpp
 * \date 04/01/2025
 * \author Olivier Langella
 * \brief read cbor stream for tsv output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "cborstreamreadertsv.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <QCborValue>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>


mcql::CborStreamReaderTsvBase::CborStreamReaderTsvBase()
{
}

mcql::CborStreamReaderTsvBase::~CborStreamReaderTsvBase()
{
  mpa_calcWriterInterface->close();

  delete mpa_calcWriterInterface;
}

void
mcql::CborStreamReaderTsvBase::readCbor(QFile *cborp,
                                        pappso::UiMonitorInterface &monitor)
{
  // try to mmap the file, this is faster
  char *ptr = reinterpret_cast<char *>(
    cborp->map(0, cborp->size(), QFile::MapPrivateOption));
  if(ptr)
    {
      // worked
      m_data = QByteArray::fromRawData(ptr, cborp->size());
      m_cborReader.addData(m_data);
    }
  else if(cborp->isSequential())
    {
      // details requires full contents, so allocate memory
      m_data = cborp->readAll();
      m_cborReader.addData(m_data);
    }
  else
    {
      // just use the QIODevice
      m_cborReader.setDevice(cborp);
    }

  if(m_cborReader.isMap())
    {
      readRoot(monitor);
    }
}


mcql::CborStreamReaderOds::CborStreamReaderOds(const QString &ods_file)
{

  try
    {
      OdsDocWriter *ods_p     = new OdsDocWriter(ods_file);
      mpa_calcWriterInterface = ods_p;
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error writing ODS file %1 :\n%2")
          .arg(ods_file)
          .arg(error.qwhat()));
    }
}
mcql::CborStreamReaderOds::~CborStreamReaderOds()
{
}


mcql::CborStreamReaderTsv::CborStreamReaderTsv(const QString &output_directory)
{

  try
    {
      TsvDirectoryWriter *tsv_dir_p =
        new TsvDirectoryWriter(QDir(output_directory));
      tsv_dir_p->setFlushLines(true);
      mpa_calcWriterInterface = tsv_dir_p;
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error writing TSV ouput in directory %1 :\n%2")
          .arg(output_directory)
          .arg(error.qwhat()));
    }
}

mcql::CborStreamReaderTsv::~CborStreamReaderTsv()
{
}


void
mcql::CborStreamReaderTsvBase::setNoMissedPeak(bool no_missed_peak)
{
  m_isMissedPeaks = !no_missed_peak;
}


void
mcql::CborStreamReaderTsvBase::dumpOne(pappso::UiMonitorInterface &monitor,
                                       int nestingLevel)
{
  qDebug() << m_cborReader.type();
  switch(m_cborReader.type())
    {
      case QCborStreamReader::UnsignedInteger:
        {
          quint64 u = m_cborReader.toUnsignedInteger();
          monitor.setStatus(QString("quint64 %1").arg(u));
          m_cborReader.next();
          return;
        }

      case QCborStreamReader::NegativeInteger:
        {
          quint64 n = quint64(m_cborReader.toNegativeInteger());
          if(n == 0) // -2^64 (wrapped around)
            printf("-18446744073709551616");
          else
            monitor.setStatus(QString("quint64 negative -%1").arg(n));
          m_cborReader.next();
          return;
        }

      case QCborStreamReader::ByteArray:
      case QCborStreamReader::String:
        {
          bool isLengthKnown = m_cborReader.isLengthKnown();
          if(!isLengthKnown)
            {
              printf("(_ ");
              ++m_offset;
            }

          QString comma;
          if(m_cborReader.isByteArray())
            {
              auto r = m_cborReader.readByteArray();
              while(r.status == QCborStreamReader::Ok)
                {
                  monitor.setStatus(QString("ByteArray %1").arg(r.data));

                  r = m_cborReader.readByteArray();
                }
            }
          else
            {
              auto r = m_cborReader.readString();
              while(r.status == QCborStreamReader::Ok)
                {

                  monitor.setStatus(QString("string %1").arg(r.data));

                  r = m_cborReader.readString();
                }
            }

          if(!isLengthKnown && !m_cborReader.lastError())
            printf(")");
          break;
        }

      case QCborStreamReader::Array:
      case QCborStreamReader::Map:
        {
          const char *delimiters = (m_cborReader.isArray() ? "[]" : "{}");
          printf("%c", delimiters[0]);

          if(m_cborReader.isLengthKnown())
            {
              quint64 len = m_cborReader.length();
              m_cborReader.enterContainer();
            }
          else
            {
              m_cborReader.enterContainer();
              m_offset = m_cborReader.currentOffset();
              printf("_ ");
            }

          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              dumpOne(monitor, nestingLevel + 1);

              if(m_cborReader.parentContainerType() != QCborStreamReader::Map)
                continue;
              if(m_cborReader.lastError())
                break;
              printf(": ");
              dumpOne(monitor, nestingLevel + 1);
            }

          if(!m_cborReader.lastError())
            {
              m_cborReader.leaveContainer();
            }
          break;
        }

      case QCborStreamReader::Tag:
        {
          QCborTag tag = m_cborReader.toTag();
          printf("%llu", quint64(tag));

          if(tag == QCborKnownTags::ExpectedBase16 ||
             tag == QCborKnownTags::ExpectedBase64 ||
             tag == QCborKnownTags::ExpectedBase64url)
            m_byteArrayEncoding.push(quint8(tag));

          if(m_cborReader.next())
            {
              // printWidthIndicator(quint64(tag));
              printf("(");
              dumpOne(monitor, nestingLevel); // same level!
              printf(")");
            }

          if(tag == QCborKnownTags::ExpectedBase16 ||
             tag == QCborKnownTags::ExpectedBase64 ||
             tag == QCborKnownTags::ExpectedBase64url)
            m_byteArrayEncoding.pop();
          break;
        }

      case QCborStreamReader::SimpleType:
        switch(m_cborReader.toSimpleType())
          {
            case QCborSimpleType::False:
              printf("false");
              break;
            case QCborSimpleType::True:
              printf("true");
              break;
            case QCborSimpleType::Null:
              printf("null");
              break;
            case QCborSimpleType::Undefined:
              printf("undefined");
              break;
            default:

              monitor.setStatus(
                QString("simple %1").arg(quint8(m_cborReader.toSimpleType())));
              break;
          }
        m_cborReader.next();
        break;

      case QCborStreamReader::Float16:
        monitor.setStatus(QString("float16 %1").arg(m_cborReader.toFloat16()));
        m_cborReader.next();
        break;
      case QCborStreamReader::Float:

        monitor.setStatus(QString("float %1").arg(m_cborReader.toFloat()));
        m_cborReader.next();
        break;
      case QCborStreamReader::Double:
        monitor.setStatus(QString("double %1").arg(m_cborReader.toDouble()));
        m_cborReader.next();
        break;
      case QCborStreamReader::Invalid:
        return;
    }

  m_offset = m_cborReader.currentOffset();
}

bool
mcql::CborStreamReaderTsvBase::getExpectedString()
{
  bool is_ok       = false;
  m_expectedString = "";
  if(m_cborReader.type() == QCborStreamReader::String)
    {
      is_ok  = true;
      auto r = m_cborReader.readString();
      while(r.status == QCborStreamReader::Ok)
        {
          m_expectedString = r.data;

          r = m_cborReader.readString();
        }
    }

  return is_ok;
}

void
mcql::CborStreamReaderTsvBase::skipCurrentElement()
{
  m_cborReader.next();
  /*
  if(m_cborReader.isContainer())
    {
      m_cborReader.enterContainer();

      while(m_cborReader.hasNext())
        {
          m_cborReader.next();
        }
      m_cborReader.leaveContainer();
    }
  else
    {

      m_cborReader.next();
    }*/
}


void
mcql::CborStreamReaderTsvBase::readRoot(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer();

  getExpectedString();
  if(m_expectedString == "informations")
    {
      writeInformations();
    }
  else
    {
      throw pappso::PappsoException("ERROR: expecting informations element");
    }


  getExpectedString();
  if(m_expectedString == "project_parameters")
    {
      readProjectParameters();
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting project_parameters element");
    }


  pappso::masschroq::AlignmentMethodSp msp_alignmentMethod;
  pappso::masschroq::QuantificationMethodSp msp_quantificationMethod;
  getExpectedString();
  if(m_expectedString == "masschroq_methods")
    {
      QJsonValue methods = QCborValue::fromCbor(m_cborReader).toJsonValue();
      msp_alignmentMethod =
        std::make_shared<pappso::masschroq::AlignmentMethod>("a1");
      msp_alignmentMethod.get()->setJsonObject(
        methods.toObject().value("alignment_method").toObject());
      msp_quantificationMethod =
        std::make_shared<pappso::masschroq::QuantificationMethod>("q1");
      msp_quantificationMethod.get()->setJsonObject(
        methods.toObject().value("quantification_method").toObject());
      writeAlignmentMethod(msp_alignmentMethod.get());
      writeQuantificationMethod(msp_quantificationMethod.get());

      m_niMinimumAbundance =
        msp_quantificationMethod.get()->getIsotopeMinimumRatio();
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting masschroq_methods element");
    }


  mpa_calcWriterInterface->writeSheet("Project parameters");
  m_projectParameters.writeParameters(*mpa_calcWriterInterface);

  getExpectedString();
  if(m_expectedString == "identification_data")
    {
      readIdentificationData(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting identification_data element");
    }

  getExpectedString();
  if(m_expectedString == "actions")
    {
      readActions(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting actions element not %1")
          .arg(m_expectedString));
    }

  getExpectedString();
  if(m_expectedString == "alignment_data")
    {
      skipCurrentElement();
      getExpectedString();
    }

  if(m_expectedString == "quantification_data")
    {
      readQuantificationData(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting quantification_data element not %1")
          .arg(m_expectedString));
    }

  /*
     dumpOne(monitor, 0);

    dumpOne(monitor, 0);
    dumpOne(monitor, 0);
    */
}


void
mcql::CborStreamReaderTsvBase::writeInformations()
{
  m_cborReader.enterContainer();
  mpa_calcWriterInterface->writeSheet("Informations");
  bool is_ok = getExpectedString();
  while(is_ok)
    {
      mpa_calcWriterInterface->writeCell(m_expectedString);
      if(m_expectedString == "cpu_used")
        {
          mpa_calcWriterInterface->writeCell((int)m_cborReader.toInteger());
          m_cborReader.next();
        }
      else
        {
          is_ok = getExpectedString();
          if(is_ok)
            {

              mpa_calcWriterInterface->writeCell(m_expectedString);
            }
        }

      mpa_calcWriterInterface->writeLine();

      if(is_ok == false)
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR writing Informations sheet"));
        }
      is_ok = getExpectedString();
    }

  m_cborReader.leaveContainer();
}


void
mcql::CborStreamReaderTsvBase::readIdentificationData(
  pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer();

  getExpectedString();
  if(m_expectedString == "msrun_list")
    {
      skipCurrentElement();
    }
  else
    {
      throw pappso::PappsoException("ERROR: expecting msrun_list element");
    }
  getExpectedString();
  if(m_expectedString == "protein_list")
    {
      qDebug() << m_cborReader.length();
      // throw pappso::PappsoException("ERROR: expecting msrun_list element");
      m_cborReader.enterContainer();
      while(getExpectedString())
        {
          QString protein_id = m_expectedString;
          qDebug() << protein_id;

          m_cborReader.enterContainer();
          while(getExpectedString())
            {
              qDebug() << m_expectedString;
              if(m_expectedString == "description")
                {
                  // qDebug() << m_expectedString;
                  if(getExpectedString())
                    {

                      qDebug() << m_expectedString;
                      m_proteinMap.insert({protein_id, m_expectedString});
                    }
                }
              else
                {
                  skipCurrentElement();
                }
            }
          m_cborReader.leaveContainer();
        }
      m_cborReader.leaveContainer();
    }
  else
    {
      throw pappso::PappsoException("ERROR: expecting protein_list element");
    }

  getExpectedString();
  if(m_expectedString == "peptide_list")
    {
      writePeptideProteinList();
    }
  else
    {
      throw pappso::PappsoException("ERROR: expecting peptide_list element");
    }
  // peptide_list
  // protein_list

  getExpectedString();
  if(m_expectedString == "msrunpeptide_list")
    {
      skipCurrentElement();
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting msrunpeptide_list element");
    }
  // m_cborReader.enterContainer();
  // dumpOne(monitor, 0);

  m_cborReader.leaveContainer();
}


void
mcql::CborStreamReaderTsvBase::writePeptideProteinList()
{
  qDebug();
  mpa_calcWriterInterface->writeSheet("proteins");
  // mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("peptide");
  mpa_calcWriterInterface->writeCell("ProForma");
  mpa_calcWriterInterface->writeCell("protein");
  mpa_calcWriterInterface->writeCell("protein_description");

  OdsTableSettings settings;
  settings.setVerticalSplit(1);
  mpa_calcWriterInterface->setCurrentOdsTableSettings(settings);


  if(m_expectedString == "peptide_list")
    {
      m_cborReader.enterContainer();
      while(getExpectedString())
        {
          QString peptide_id = m_expectedString;
          qDebug() << peptide_id;

          //"proforma" : "SLTNDWEDHLAVK",
          //             "proteins" : ["prota1a1"],

          m_cborReader.enterContainer();
          QString proforma = "";
          QStringList protein_list_str;
          while(getExpectedString())
            {
              qDebug() << m_expectedString;
              if(m_expectedString == "proforma")
                {
                  // qDebug() << m_expectedString;
                  if(getExpectedString())
                    {
                      proforma = m_expectedString;
                    }
                }
              else if(m_expectedString == "proteins")
                {
                  m_cborReader.enterContainer();
                  while(getExpectedString())
                    {
                      protein_list_str << m_expectedString;
                    }
                  m_cborReader.leaveContainer();
                }
              else
                {
                  skipCurrentElement();
                }
            }

          m_cborReader.leaveContainer();

          for(auto protein_id : protein_list_str)
            {
              mpa_calcWriterInterface->writeLine();
              mpa_calcWriterInterface->writeCell(peptide_id);
              mpa_calcWriterInterface->writeCell(proforma);
              mpa_calcWriterInterface->writeCell(protein_id);
              auto it = m_proteinMap.find(protein_id);
              if(it == m_proteinMap.end())
                {
                  throw pappso::ExceptionNotFound(
                    QObject::tr("protein %1 not found").arg(protein_id));
                }
              mpa_calcWriterInterface->writeCell(it->second);
            }
        }
      m_cborReader.leaveContainer();
    }

  qDebug();
}

void
mcql::CborStreamReaderTsvBase::writePeakLine(
  const mcql::CborStreamReaderTsvBase::PeakStruct &peak)
{
  if(!m_isMissedPeaks)
    {
      if(m_quality == "missed")
        return;
    }
  bool is_aligned = true;
  if(peak.aligned_rt[1] == 0)
    is_aligned = false;
  mpa_calcWriterInterface->writeCell(m_quantificationId);
  mpa_calcWriterInterface->writeCell(m_groupId);
  mpa_calcWriterInterface->writeCell(m_msrunId);
  // mpa_calcWriterInterface->writeCell(m_msrunFileName);
  mpa_calcWriterInterface->writeCell(m_msrunSampleName);
  mpa_calcWriterInterface->writeCell(m_mz);
  if(is_aligned)
    mpa_calcWriterInterface->writeCell(peak.aligned_rt[1]);
  else
    mpa_calcWriterInterface->writeCell(peak.rt[1]);
  mpa_calcWriterInterface->writeCell(peak.max_intensity);
  mpa_calcWriterInterface->writeCell(peak.area);
  mpa_calcWriterInterface->writeCell(m_quality);

  if(is_aligned)
    {
      mpa_calcWriterInterface->writeCell(peak.aligned_rt[0]);
      mpa_calcWriterInterface->writeCell(peak.aligned_rt[2]);
    }
  else
    {
      mpa_calcWriterInterface->writeCell(peak.rt[0]);
      mpa_calcWriterInterface->writeCell(peak.rt[2]);
    }
  mpa_calcWriterInterface->writeCell(peak.rt[0]);
  mpa_calcWriterInterface->writeCell(peak.rt[1]);
  mpa_calcWriterInterface->writeCell(peak.rt[2]);
  mpa_calcWriterInterface->writeCell(m_peptideId);
  mpa_calcWriterInterface->writeCell(m_label);
  mpa_calcWriterInterface->writeCell(m_proForma);
  mpa_calcWriterInterface->writeCell(m_charge);
  mpa_calcWriterInterface->writeCell(m_mods);

  if(m_niMinimumAbundance != 0)
    {
      mpa_calcWriterInterface->writeCell(m_isotope);
      mpa_calcWriterInterface->writeCell(m_rank);
      mpa_calcWriterInterface->writeCell(m_thRatio);
    }
  mpa_calcWriterInterface->writeLine();
}

void
mcql::CborStreamReaderTsvBase::writeQuantificationSheet()
{

  OdsTableSettings settings;
  settings.setVerticalSplit(1);
  mpa_calcWriterInterface->setCurrentOdsTableSettings(settings);

  mpa_calcWriterInterface->setCellAnnotation("quantification XML id");
  mpa_calcWriterInterface->writeCell("quantification");
  mpa_calcWriterInterface->setCellAnnotation("group XML id (fraction name)");
  mpa_calcWriterInterface->writeCell("group");
  mpa_calcWriterInterface->setCellAnnotation("MS run XML id (sample id)");
  mpa_calcWriterInterface->writeCell("msrun");
  mpa_calcWriterInterface->setCellAnnotation("MS run file path");
  mpa_calcWriterInterface->writeCell("msrunfile");
  mpa_calcWriterInterface->setCellAnnotation("XIC m/z");
  mpa_calcWriterInterface->writeCell("mz");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak maximum intensity retention time aligned");
  mpa_calcWriterInterface->writeCell("rt");
  mpa_calcWriterInterface->setCellAnnotation("peak maximum intensity");
  mpa_calcWriterInterface->writeCell("maxintensity");
  mpa_calcWriterInterface->setCellAnnotation("peak area");
  mpa_calcWriterInterface->writeCell("area");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak quality : how the peak was measured");
  mpa_calcWriterInterface->writeCell("peak quality");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak start retention time aligned");
  mpa_calcWriterInterface->writeCell("rtbegin");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak stop retention time aligned");
  mpa_calcWriterInterface->writeCell("rtend");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak start retention time not aligned");
  mpa_calcWriterInterface->writeCell("realrtbegin");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak apex (maximum intensity) retention time not aligned");
  mpa_calcWriterInterface->writeCell("realrtapex");
  mpa_calcWriterInterface->setCellAnnotation(
    "peak stop retention time not aligned");
  mpa_calcWriterInterface->writeCell("realrtend");
  mpa_calcWriterInterface->setCellAnnotation("peptide id");
  mpa_calcWriterInterface->writeCell("peptide");
  mpa_calcWriterInterface->setCellAnnotation("isotope label");
  mpa_calcWriterInterface->writeCell("label");
  mpa_calcWriterInterface->setCellAnnotation("peptide sequence");
  mpa_calcWriterInterface->writeCell("sequence");
  mpa_calcWriterInterface->setCellAnnotation("peptide charge");
  mpa_calcWriterInterface->writeCell("z");
  mpa_calcWriterInterface->setCellAnnotation(
    "peptide modifications (free text)");
  mpa_calcWriterInterface->writeCell("mods");
  if(m_niMinimumAbundance != 0)
    {
      mpa_calcWriterInterface->setCellAnnotation("natural isotope number");
      mpa_calcWriterInterface->writeCell("ninumber");
      mpa_calcWriterInterface->setCellAnnotation("natural isotope rank");
      mpa_calcWriterInterface->writeCell("nirank");
      mpa_calcWriterInterface->setCellAnnotation(
        "natural isotope theoretical ratio");
      mpa_calcWriterInterface->writeCell("niratio");
    }


  mpa_calcWriterInterface->writeLine();
}

void
mcql::CborStreamReaderTsvBase::readActions(pappso::UiMonitorInterface &monitor)
{

  m_cborReader.enterContainer();


  while(getExpectedString())
    {
      if(m_expectedString == "group_list")
        {
          skipCurrentElement();
        }
      else if(m_expectedString == "align_group")
        {
          skipCurrentElement();
        }
      else if(m_expectedString == "quantify_all")
        {
          skipCurrentElement();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: element %1 not expected in actions")
              .arg(m_expectedString));
        }
    }
  m_cborReader.leaveContainer();
}

void
mcql::CborStreamReaderTsvBase::readQuantificationData(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();


  while(!m_cborReader.lastError() && m_cborReader.hasNext())
    {
      readQuantificationDataObject(monitor);
      // m_cborReader.next();
      //  dumpOne(monitor, 0);
      //  skipCurrentElement();
    }
  qDebug();
  m_cborReader.leaveContainer();
}


void
mcql::CborStreamReaderTsvBase::readQuantificationDataObject(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }
  if(m_expectedString == "quantify_id")
    {
      getExpectedString();
      m_quantificationId = m_expectedString;
    }
  else
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting quantify_id element not %1")
          .arg(m_expectedString));
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }

  getExpectedString();
  if(m_expectedString == "group_id")
    {
      getExpectedString();
      m_groupId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting group_id element not %1")
          .arg(m_expectedString));
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }
  mpa_calcWriterInterface->writeSheet(QString("peptides_%1").arg(m_groupId));
  writeQuantificationSheet();
  getExpectedString();

  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }
  if(m_expectedString == "first_pass")
    {
      readGroupQuantificationPass(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting first_pass element not %1")
          .arg(m_expectedString));
    }

  getExpectedString();

  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }

  if(m_expectedString == "second_pass")
    {
      readGroupQuantificationPass(monitor);
    }
  else
    {
      skipCurrentElement();
    }

  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      skipCurrentElement();
      getExpectedString();
    }
  qDebug();
  // skipCurrentElement();
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
}

void
mcql::CborStreamReaderTsvBase::readGroupQuantificationPass(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();

  std::size_t size = m_cborReader.length();
  std::size_t i    = 0;
  m_cborReader.enterContainer();
  while(getExpectedString())
    {
      m_msrunId = m_expectedString;
      readQrDataBlock(monitor);
      i++;
      // m_cborReader.next();
    }

  if(i < size)
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 msruns out of %2 expected, "
                    "groupid %3")
          .arg(i)
          .arg(size)
          .arg(m_groupId));
    }
  qDebug();
  // skipCurrentElement();
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
}

void
mcql::CborStreamReaderTsvBase::readQrDataBlock(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString == "msrun")
    {
      readMsrun(monitor);
    }
  else
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting msrun element not %1")
          .arg(m_expectedString));
    }
  // retention_time_correction
  getExpectedString();
  if(m_expectedString == "retention_time_correction")
    {
      skipCurrentElement();
      getExpectedString();
    }
  if(m_expectedString == "peptide_measurements")
    {
      readPeptideMeasurements(monitor);
    }
  else
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting peptide_measurements element not %1")
          .arg(m_expectedString));
    }
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
}

void
mcql::CborStreamReaderTsvBase::readMsrun(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer();
  m_msrunId = "";
  while(getExpectedString())
    {
      if(m_expectedString == "id")
        {
          getExpectedString();
          m_msrunId = m_expectedString;
        }

      else if(m_expectedString == "filename")
        {
          getExpectedString();
          m_msrunFileName = m_expectedString;
        }
      else if(m_expectedString == "sample")
        {
          getExpectedString();
          m_msrunSampleName = m_expectedString;
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in msrun")
              .arg(m_expectedString));
        }
    }
  m_cborReader.leaveContainer();
}

void
mcql::CborStreamReaderTsvBase::readPeptideMeasurements(
  pappso::UiMonitorInterface &monitor)
{
  qDebug() << m_cborReader.length();
  monitor.setTotalSteps(m_cborReader.length());


  if(!m_cborReader.isLengthKnown())
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR: peptide_measurements map size not known in peptideid %3")
          .arg(m_peptideId));
    }
  std::size_t size = m_cborReader.length();
  m_cborReader.enterContainer(); // map
  std::size_t i = 0;
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      m_peptideId = m_expectedString;
      readPeptideMeasurement(monitor);
      i++;
      // m_cborReader.next();
    }

  qDebug() << size << " " << i;
  if(i < size)
    {
      dumpOne(monitor, 0);
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 peptide_measurements out of %2 expected, "
                    "peptideid %3")
          .arg(i)
          .arg(size)
          .arg(m_peptideId));
    }
  m_cborReader.leaveContainer();
  qDebug() << size;
}

void
mcql::CborStreamReaderTsvBase::readPeptideMeasurement(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();

  m_mods = "";
  while(getExpectedString())
    {
      if(m_expectedString == "proforma")
        {
          getExpectedString();
          m_proForma = m_expectedString;
        }
      else if(m_expectedString == "mods")
        {
          getExpectedString();
          m_mods = m_expectedString;
        }
      else if(m_expectedString == "xics")
        {
          if(!m_cborReader.isLengthKnown())
            {
              dumpOne(monitor, 0);
              throw pappso::PappsoException(
                QObject::tr("ERROR: xics array size not known in peptideid %3")
                  .arg(m_peptideId));
            }
          std::size_t size = m_cborReader.length();
          std::size_t i    = 0;
          m_cborReader.enterContainer(); // array


          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug();
              readXic(monitor);
              i++;
              // m_cborReader.next();

              qDebug();
            }
          if(i < size)
            {
              dumpOne(monitor, 0);
              throw pappso::PappsoException(
                QObject::tr(
                  "ERROR: only %1 xics out of %2 expected, peptideid %3")
                  .arg(i)
                  .arg(size)
                  .arg(m_peptideId));
            }
          qDebug();
          m_cborReader.leaveContainer();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in PeptideMeasurement")
              .arg(m_expectedString));
        }
    }
  qDebug();
  m_cborReader.leaveContainer();
}


void
mcql::CborStreamReaderTsvBase::readXic(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  // dumpOne(monitor, 0);
  if(!m_cborReader.enterContainer())
    {
      checkError("xic");
    }
  m_charge  = 0;
  m_mz      = 0;
  m_isotope = 0;
  m_rank    = 0;
  m_thRatio = 0;
  m_quality = "";
  m_label   = "";
  PeakStruct peak;
  while(getExpectedString())
    {
      if(m_expectedString == "charge")
        {
          if(m_cborReader.isInteger())
            {
              m_charge = m_cborReader.toInteger();
              qDebug() << "m_charge=" << m_charge;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for charge"));
            }
          skipCurrentElement();
        }
      else if(m_expectedString == "mz")
        {

          if(m_cborReader.isDouble())
            {
              m_mz = m_cborReader.toDouble();
              qDebug() << "m_mz=" << m_mz;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for mz"));
            }
          skipCurrentElement();
        }
      else if(m_expectedString == "xic_coord")
        {
          skipCurrentElement();
        }
      else if(m_expectedString == "isotope")
        {
          if(m_cborReader.isInteger())
            {
              m_isotope = m_cborReader.toInteger();
              qDebug() << "m_isotope=" << m_isotope;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for isotope"));
            }
          skipCurrentElement();
        }

      else if(m_expectedString == "rank")
        {
          if(m_cborReader.isInteger())
            {
              m_rank = m_cborReader.toInteger();
              qDebug() << "m_rank=" << m_rank;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for rank"));
            }
          skipCurrentElement();
        }
      else if(m_expectedString == "th_ratio")
        {
          if(m_cborReader.isDouble())
            {
              m_thRatio = m_cborReader.toDouble();
              qDebug() << "th_ratio=" << m_thRatio;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for m_thRatio"));
            }
          skipCurrentElement();
        }
      else if(m_expectedString == "quality")
        {

          getExpectedString();
          m_quality = m_expectedString;
        }
      else if(m_expectedString == "label")
        {

          getExpectedString();
          m_label = m_expectedString;
        }


      else if(m_expectedString == "peak")
        {
          peak = readPeak(monitor);
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in Xic")
              .arg(m_expectedString));
        }
    }
  writePeakLine(peak);
  m_cborReader.leaveContainer();
}

mcql::CborStreamReaderTsvBase::PeakStruct
mcql::CborStreamReaderTsvBase::readPeak(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  // dumpOne(monitor, 0);
  PeakStruct peak;
  m_cborReader.enterContainer();
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      if(m_expectedString == "area")
        {
          if(m_cborReader.isDouble())
            {
              peak.area = m_cborReader.toDouble();
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for area"));
            }
          skipCurrentElement();
        }
      else if(m_expectedString == "max_intensity")
        {
          // dumpOne(monitor, 0);
          if(m_cborReader.isDouble())
            {
              peak.max_intensity = m_cborReader.toDouble();
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for max_intensity"));
            }
          skipCurrentElement();

          checkError("peak max_intensity");
        }

      else if(m_expectedString == "rt")
        {
          // dumpOne(monitor, 0);
          m_cborReader.enterContainer();
          std::size_t i = 0;

          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug() << i;
              // if(m_cborReader.isDouble())
              //{
              peak.rt[i] = m_cborReader.toDouble();
              qDebug() << peak.rt[i];
              m_cborReader.next();
              i++;
              //}
            }
          m_cborReader.leaveContainer();
        }
      else if(m_expectedString == "aligned_rt")
        {
          // dumpOne(monitor, 0);
          m_cborReader.enterContainer();
          std::size_t i = 0;

          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug() << i;
              // if(m_cborReader.isDouble())
              //{
              peak.aligned_rt[i] = m_cborReader.toDouble();
              qDebug() << peak.rt[i];
              m_cborReader.next();
              i++;
              //}
            }
          m_cborReader.leaveContainer();
        }
      else
        {
          dumpOne(monitor, 0);
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in peak")
              .arg(m_expectedString));
        }
    }
  m_cborReader.leaveContainer();
  qDebug();
  return peak;
}

void
mcql::CborStreamReaderTsvBase::checkError(const QString &element_name)
{
  if(m_cborReader.type() == QCborStreamReader::Invalid)
    {
      throw pappso::PappsoException(QObject::tr("ERROR: %1 in %2")
                                      .arg(m_cborReader.lastError().toString())
                                      .arg(element_name));
    }
}

void
mcql::CborStreamReaderTsvBase::readProjectParameters()
{

  QCborValue parameters;
  parameters = parameters.fromCbor(m_cborReader);
  pappso::ProjectParameters all_params(parameters.toJsonValue().toObject());
  m_projectParameters.merge(all_params);
}


void
mcql::CborStreamReaderTsvBase::writeQuantificationMethod(
  pappso::masschroq::QuantificationMethod *p_quantification_method)
{

  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("XIC parameters");
  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("integration");
  if(p_quantification_method->getXicExtractMethod() ==
     pappso::XicExtractMethod::max)
    mpa_calcWriterInterface->writeCell("max");
  if(p_quantification_method->getXicExtractMethod() ==
     pappso::XicExtractMethod::sum)
    mpa_calcWriterInterface->writeCell("sum");
  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("extraction range lower limit");
  mpa_calcWriterInterface->writeCell(
    p_quantification_method->getXicExtractionLowerPrecisionPtr()->toString());
  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("extraction range upper limit");
  mpa_calcWriterInterface->writeCell(
    p_quantification_method->getXicExtractionUppersPrecisionPtr()->toString());
  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeLine();


  mpa_calcWriterInterface->writeCell("Filters");
  mpa_calcWriterInterface->writeCell(
    p_quantification_method->getXicFilter().get()->toString());
  mpa_calcWriterInterface->writeLine();


  mpa_calcWriterInterface->writeCell("Detection method");
  mpa_calcWriterInterface->writeLine();
  const pappso::TraceDetectionInterface *p_detection =
    p_quantification_method->getTraceDetectionInterfaceCstSPtr().get();
  if(p_detection != nullptr)
    {
      const pappso::TraceDetectionZivy *p_detectionZivy =
        dynamic_cast<const pappso::TraceDetectionZivy *>(p_detection);
      if(p_detectionZivy != nullptr)
        {
          mpa_calcWriterInterface->writeCell("detection Zivy");
          mpa_calcWriterInterface->writeLine();
          mpa_calcWriterInterface->writeCell("smoothing half edge window");
          mpa_calcWriterInterface->writeCell(
            (std::size_t)p_detectionZivy->getSmoothingHalfEdgeWindows());
          mpa_calcWriterInterface->writeLine();
          mpa_calcWriterInterface->writeCell("maxmin half edge window");
          mpa_calcWriterInterface->writeCell(
            (std::size_t)p_detectionZivy->getMaxMinHalfEdgeWindows());
          mpa_calcWriterInterface->writeLine();
          mpa_calcWriterInterface->writeCell("minmax half edge window");
          mpa_calcWriterInterface->writeCell(
            (std::size_t)p_detectionZivy->getMinMaxHalfEdgeWindows());
          mpa_calcWriterInterface->writeLine();
          mpa_calcWriterInterface->writeCell("detection threshold on maxmin");
          mpa_calcWriterInterface->writeCell(
            (std::size_t)p_detectionZivy->getDetectionThresholdOnMaxmin());
          mpa_calcWriterInterface->writeLine();
          mpa_calcWriterInterface->writeCell("detection threshold on minmax");
          mpa_calcWriterInterface->writeCell(
            (std::size_t)p_detectionZivy->getDetectionThresholdOnMinmax());
          mpa_calcWriterInterface->writeLine();
        }
    }


  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeLine();
}

void
mcql::CborStreamReaderTsvBase::writeAlignmentMethod(
  pappso::masschroq::AlignmentMethod *p_alignment_method)
{

  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeLine();
  mpa_calcWriterInterface->writeCell("Alignment method");
  mpa_calcWriterInterface->writeLine();
  if(p_alignment_method != nullptr)
    {
      mpa_calcWriterInterface->writeCell("MS1 smoothing half window");
      mpa_calcWriterInterface->writeCell(
        p_alignment_method->getMs1SmoothingWindow());
      mpa_calcWriterInterface->writeLine();
      mpa_calcWriterInterface->writeCell("MS2 smoothing half window");
      mpa_calcWriterInterface->writeCell(
        p_alignment_method->getMs2SmoothingWindow());
      mpa_calcWriterInterface->writeLine();
      mpa_calcWriterInterface->writeCell("MS2 tendency half window");
      mpa_calcWriterInterface->writeCell(
        p_alignment_method->getMs2TendencyWindow());
      mpa_calcWriterInterface->writeLine();
    }
}
