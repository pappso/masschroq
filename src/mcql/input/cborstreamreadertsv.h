/**
 * \file mcql/input/cborstreamreadertsv.h
 * \date 04/01/2025
 * \author Olivier Langella
 * \brief read cbor stream for tsv output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>
#include <odsstream/tsvdirectorywriter.h>
#include <QStringList>
#include <QCborStreamReader>
#include <QStack>
#include <QFile>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <pappsomspp/processing/project/projectparameters.h>
#include <pappsomspp/masschroq/quantificationmethod.h>
#include <pappsomspp/masschroq/alignmentmethod.h>


namespace mcql
{

class CborStreamReaderTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderTsvBase();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderTsvBase();


  virtual void readCbor(QFile *cborp, pappso::UiMonitorInterface &monitor);

  /** @brief do not report missed peaks in output files
   * mimic the MassChroQ leagacy behaviour that does not report missed peaks
   * (not quantified peaks)
   */
  virtual void setNoMissedPeak(bool no_missed_peak);

  protected:
  struct PeakStruct
  {
    double area          = 0;
    double max_intensity = 0;
    double rt[3]         = {0};
    double aligned_rt[3] = {0};
  };

  void readRoot(pappso::UiMonitorInterface &monitor);
  void dumpOne(pappso::UiMonitorInterface &monitor, int nestingLevel);
  bool getExpectedString();
  void writeInformations();
  void writeQuantificationMethod(
    pappso::masschroq::QuantificationMethod *p_quantification_method);
  void
  writeAlignmentMethod(pappso::masschroq::AlignmentMethod *p_alignment_method);
  void readIdentificationData(pappso::UiMonitorInterface &monitor);
  void writePeptideProteinList();
  void skipCurrentElement();

  void writeQuantificationSheet();
  void writePeakLine(const PeakStruct &peak);
  void readActions(pappso::UiMonitorInterface &monitor);
  void readQuantificationData(pappso::UiMonitorInterface &monitor);
  void readQuantificationDataObject(pappso::UiMonitorInterface &monitor);
  void readGroupQuantificationPass(pappso::UiMonitorInterface &monitor);
  void readQrDataBlock(pappso::UiMonitorInterface &monitor);
  void readMsrun(pappso::UiMonitorInterface &monitor);
  void readPeptideMeasurements(pappso::UiMonitorInterface &monitor);
  void readPeptideMeasurement(pappso::UiMonitorInterface &monitor);
  void readXic(pappso::UiMonitorInterface &monitor);
  void readProjectParameters();

  PeakStruct readPeak(pappso::UiMonitorInterface &monitor);

  void checkError(const QString &element_name);


  protected:
  CalcWriterInterface *mpa_calcWriterInterface = nullptr;
  QString m_currentSheetName;


  private:
  QCborStreamReader m_cborReader;
  QByteArray m_data;
  QStack<quint8> m_byteArrayEncoding;
  qint64 m_offset = 0;
  QString m_expectedString;

  std::map<QString, QString> m_proteinMap;
  double m_niMinimumAbundance = 0;
  QString m_quantificationId;
  QString m_groupId;
  QString m_msrunId;
  QString m_msrunFileName;
  QString m_msrunSampleName;
  QString m_peptideId;
  QString m_proForma;
  QString m_label;
  QString m_mods;
  std::size_t m_charge;
  std::size_t m_isotope;
  std::size_t m_rank;
  double m_mz;
  double m_thRatio;
  QString m_quality;
  pappso::ProjectParameters m_projectParameters;

  bool m_isMissedPeaks = true;
};

/**
 * @todo write docs
 */
class CborStreamReaderOds : public CborStreamReaderTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderOds(const QString &ods_file);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderOds();
};

/**
 * @todo write docs
 */
class CborStreamReaderTsv : public CborStreamReaderTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderTsv(const QString &output_directory);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderTsv();
};
} // namespace mcql
