/**
 * \file mcql/masschroqlightcborcli.h
 * \date 03/01/2025
 * \author Olivier Langella
 * \brief simple MassChroQ CBOR reader
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../lib/consoleout.h"
#include <QCoreApplication>
#include <QObject>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>

/**
 * @todo write docs
 */
class MassChroqLightCborCli : public QObject
{
  Q_OBJECT

  public:
  explicit MassChroqLightCborCli(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();

  private:
  void debugCbor(pappso::UiMonitorInterface &monitor, const QStringList &files);
  void tsvOutputDirectory(pappso::UiMonitorInterface &monitor,
                          const QStringList &files,
                          const QString &directory,
                          bool no_missed_in_output);

  void odsOutputDirectory(pappso::UiMonitorInterface &monitor,
                          const QStringList &files,
                          const QString &filename,
                          bool no_missed_in_output);
  void produceJson(pappso::UiMonitorInterface &monitor,
                   const QStringList &files,
                   const QString &filename);

  void extractPeptideIdQuantificationData(pappso::UiMonitorInterface &monitor,
                                          const QStringList &inputfiles,
                                          const QStringList &peptide_id_list,
                                          const QString &json_output_file);

  private:
  QCoreApplication *m_mcqLightCborApp;
};
