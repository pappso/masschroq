/**
 * \file mcql/masschroqlightcli.h
 * \date 23/09/2024
 * \author Olivier Langella
 * \brief simple MassChroQ XIC extraction
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../lib/consoleout.h"
#include <QCoreApplication>
#include <QObject>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>

/**
 * @todo write docs
 */
class MassChroqLightCli : public QObject
{
  Q_OBJECT

  public:
  explicit MassChroqLightCli(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();

  private:
  void readMassChroqMlFile(pappso::UiMonitorInterface &monitor,
                           const QString &filename);

  void readMassChroqMl2JsonFile(pappso::UiMonitorInterface &monitor,
                                const QString &xml_filename,
                                const QString &json_file);

  void setCpuNumber(pappso::UiMonitorInterface &ui_monitor, uint cpu_number);

  void readJsonStdinOrFile(QJsonDocument &json_document, QString &filename);

  void readJsonDocument(pappso::UiMonitorInterface &monitor,
                        const QJsonDocument &json_document,
                        QString &cbor_filename);

  private:
  QCoreApplication *m_mcqLightApp;
  QString m_tmpDirName;
  uint m_cpuNumber         = 1;
  bool m_isTraceOutput     = false;
  bool m_isPeakShapeOutput = false;
  bool m_isXicCoordOutput  = true;

  double m_peakShapeMarginInSeconds = 20;
};
