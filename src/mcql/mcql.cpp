#include <QCoreApplication>
#include <QTimer>
#include <QIODevice>
#include <QLocale>
#include "masschroqlightcli.h"
#include "../lib/consoleout.h"
#include "../config.h"
#include <pappsomspp/pappsoexception.h>
#include <sys/stat.h>

//./build/src/mcql/mcql -m
///gorgone/pappso/versions_logiciels_pappso/masschroq/donnees/mcql/minimum_eme.masschroqml
int
main(int argc, char **argv)
{
  umask(0);
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

  QCoreApplication app(argc, argv);
  QCoreApplication::setApplicationName("MassChroQlite");
  QCoreApplication::setApplicationVersion(MASSCHROQ_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  MassChroqLightCli myMain;
  // connect up the signals

  // Qt5 code
  QObject::connect(
    &myMain, &MassChroqLightCli::finished, &app, &QCoreApplication::quit);
  QObject::connect(&app,
                   &QCoreApplication::aboutToQuit,
                   &myMain,
                   &MassChroqLightCli::aboutToQuitApp);

  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
