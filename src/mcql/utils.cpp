
/**
 * \file mcql/utils.cpp
 * \date 03/01/2025
 * \author Olivier Langella
 * \brief MassChroQ Lite utilities
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "utils.h"
#include <QObject>
#include <pappsomspp/pappsoexception.h>

QString
mcql::Utils::enumToString(PeakQualityCategory peak_category)
{

  switch(peak_category)
    {
      case PeakQualityCategory::a:
        return "a";
        break;
      case PeakQualityCategory::aa:
        return "aa";
        break;
      case PeakQualityCategory::ab:
        return "ab";
        break;
      case PeakQualityCategory::za:
        return "za";
        break;
      case PeakQualityCategory::zaa:
        return "zaa";
        break;
      case PeakQualityCategory::zab:
        return "zab";
        break;
      case PeakQualityCategory::b:
        return "b";
        break;
      case PeakQualityCategory::c:
        return "c";
        break;
      case PeakQualityCategory::d:
        return "d";
        break;
      case PeakQualityCategory::missed:
        return "missed";
        break;
      case PeakQualityCategory::nomatch:
        return "nomatch";
        break;
      case PeakQualityCategory::post_matching:
        return "pm";
        break;
      case PeakQualityCategory::last:
        throw pappso::PappsoException(
          QObject::tr("no QString for PeakQualityCategory::last"));
        break;
    }

  return "";
}


QString
mcql::Utils::enumToString(pappso::XicExtractMethod extract_method)
{

  switch(extract_method)
    {
      case pappso::XicExtractMethod::sum:
        return "sum";
        break;
      case pappso::XicExtractMethod::max:
        return "max";
        break;
      default:
        break;
    }
  return "";
}
