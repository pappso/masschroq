
/**
 * \file mcql/utils.h
 * \date 03/01/2025
 * \author Olivier Langella
 * \brief MassChroQ Lite utilities
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include <QString>
#include "../mcq_types.h"
#include <pappsomspp/types.h>

namespace mcql
{
/**
 * @todo write docs
 */
class Utils
{
  public:
  
  
  /** @brief Convenience function to return a string describing the specglob
  alingment type
   * @return QString
   */
  
  static QString enumToString(PeakQualityCategory peak_category);
  static QString enumToString(pappso::XicExtractMethod extract_method);
};

}
