
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file mcqsession.h
 * \date April 28, 2018
 * \author Olivier Langella
 * \brief singleton to store general informations on the current MassChroQ
 * session
 */

#pragma once

#include <QTemporaryDir>
#include <memory>
#include <pappsomspp/types.h>
#include "exportimportconfig.h"

class MCQ_LIB_DECL McqSession
{
  public:
  static McqSession &getInstance();
  void setCpuNumber(uint cpu_number);

  /// set the temporary working directory
  void setTmpDir(const QString &dir_name);

  /** @brief get a unique temporary directory inside the tmp dir with a unique
   * name depending on the mcq session
   * it allows to launch several masschroq exe on the same computer
   */
  const QString getSessionTmpDirName() const;

  /// get the temporary working directory's absolute path name
  const QString getTmpDirName() const;

  void setUnassignedPeaksOnDisk(bool unassigned_peaks_on_disk);
  bool getUnassignedPeaksOnDisk() const;

  // rocksdb::DB* getRocksDb();

  pappso::pappso_double getRetentionTimeRange() const;

  private:
  McqSession();
  ~McqSession();

  private:
  static McqSession *mpa_instance;
  uint m_cpuNumber = 1;

  /** @brief store unassigned peaks on disk (--ondisk flag)
   */
  bool _unassigned_peaks_on_disk = false;

  /** @brief Temporary working directory static variable. it has to be set to
   * use MassChroQ
   */
  QDir _tmp_dir;

  /** @brief session temporary directory
   */
  std::shared_ptr<QTemporaryDir> _sp_session_tmp_dir = nullptr;

  // rocksdb::DB* m_p_db = nullptr;

  /** @brief maximum retention time range in seconds to look for peaks on XIC
   * XIC is extracted from (retention time target - range) to (retention time
   * target + range)
   */
  pappso::pappso_double _m_retention_time_range = 300;
};

