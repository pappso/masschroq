#include "detectionresult.h"
#include <pappsomspp/processing/filters/filterresample.h>

pappso::TraceSPtr
TraceResult::getMatchedPeakTraceSPtr() const
{
  pappso::TraceSPtr trace_sp;

  if(matchedPeakPosition < peakList.size())
    {
      pappso::FilterResampleKeepXRange cut_xic(
        peakList.at(matchedPeakPosition).getLeftBoundary().x - 20,
        peakList.at(matchedPeakPosition).getRightBoundary().x + 20);

      if(sp_xicCoord.get()->xicSptr.get() != nullptr)
        {
          trace_sp = sp_xicCoord.get()->xicSptr.get()->makeTraceSPtr();
          trace_sp.get()->filter(cut_xic);
        }
    }

  return trace_sp;
}
