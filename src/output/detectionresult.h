#pragma once

#include <pappsomspp/processing/detection/tracepeaklist.h>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include "../lib/quanti_items/quantiItemBase.h"

struct TraceResult
{
  std::vector<MsMsRtIntensity> rtList;
  pappso::TracePeakList peakList;
  std::size_t matchedPeakPosition  = 0;
  pappso::XicCoordSPtr sp_xicCoord = nullptr;

  pappso::TraceSPtr getMatchedPeakTraceSPtr() const;
};

struct DetectionResult
{
  const Msrun *p_msrun                        = nullptr;
  QuantiItemBaseSp p_quanti_item              = nullptr;
  AlignedXicPeakSp sp_alignedPeak             = nullptr;
  PeakQualityCategory mbr_category            = PeakQualityCategory::missed;
  std::shared_ptr<TraceResult> sp_traceResult = nullptr;
};
