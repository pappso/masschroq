/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file masschroqWriter.h
 * \date January 03, 2011
 * \author Edlira Nano
 */

#pragma once

#include "mcq_qxmlstreamwriter.h"
#include "monitors/monitorspeedinterface.h"

/**
 * \class MasschroqWriter
 * \brief masschroqML type of output containing quantification results

 * This type of result produces a masschroqML result file that contains the XML
 input file
 * used to generate these results plus the quantification results and the
 * xic traces produced if any. The output masschroqML file follows the same
 schema as
 * the input masschroqML file.
 */

class MasschroqWriter : public MonitorSpeedInterface
{

  public:
  MasschroqWriter(const QString &in_filename,
                  const QString &out_filename,
                  const bool with_traces);
  virtual ~MasschroqWriter();

  virtual void
  writeQuantifyBegin(const Quantificator *p_quantificator) override;
  virtual void writeQuantifyEnd() override;

  protected:
  virtual void privWriteMatchedPeak(const DetectionResult &detection_result) override;

  private:
  virtual void setCurrentMsrun(const Msrun *msrun);
  virtual void setEndCurrentMsrun();

  void debriefing();
  void writeInputParameters();

  void initialiseOutput();

  // void printQuantificationData(const std::vector<xicPeak *> * p_v_peak_list);

  void printAlignmentResults(const Msrun *msrun);

  /// counter for the search items, used to generate unique search xml ids
  unsigned int _quanti_item_counter;

  QString _input_filename;

  QFile *_output_file = nullptr;

  MCQXmlStreamWriter *_output_stream = nullptr;

  QString _current_group_id;
  QString _current_quantify_id;

  const Quantificator *_p_quantificator = nullptr;
  const Msrun *_p_msrun                 = nullptr;
  std::vector<const Msrun *> _written_align_results;
};
