/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file mcq_qxmlstreamwriter.h
 * \date January 19, 2011
 * \author Edlira Nano
 */

#pragma once

#include <QDateTime>
#include <QXmlStreamWriter>

#include "../config.h"

/**
 * \class MCQXmlStreamWriter
 * \brief Class deriving from QXmlStreamWriter that overloads
 * writeAttribute method and makes it suitable for other attribute values
 * than QString
 */

class MCQXmlStreamWriter : public QXmlStreamWriter
{

  public:
  MCQXmlStreamWriter();

  MCQXmlStreamWriter(const char format, const int precision);

  virtual ~MCQXmlStreamWriter();

  void writeAttribute(const QString &qualifiedName, const QString &value);

  void writeAttribute(const QString &namespaceUri,
                      const QString &name,
                      const QString &value);

  void writeAttribute(const QString &qualifiedName, const mcq_double value);

  void writeAttribute(const QString &qualifiedName, const int value);

  void writeAttribute(const QString &qualifiedName, const unsigned int value);

  void writeAttribute(const QString &qualifiedName, const QDate &value);

  void writeAttribute(const QString &qualifiedName, const QTime &value);

  void writeAttribute(const QString &qualifiedName, const QDateTime &value);

  void setFormat(const char f);

  void setPrecision(const int p);

  private:
  char _format;
  int _precision;
};
