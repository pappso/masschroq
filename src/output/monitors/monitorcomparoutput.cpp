
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorcomparoutput.h"
#include "../../lib/peptides/peptide.h"
#include "../../lib/quantificator.h"
#include <QDir>
#include <odsstream/odsexception.h>

MonitorComparTsvOutput::MonitorComparTsvOutput(const QString &output_directory)
  : _tsv_directory_writer(QDir(output_directory))
{
  _p_writer = &_tsv_directory_writer;
}

MonitorComparTsvOutput::~MonitorComparTsvOutput()
{

  qDebug() << "begin";

  _p_writer->close();
}

MonitorComparOdsOutput::MonitorComparOdsOutput(const QString &ods_file)
  : _ods_writer(ods_file)
{
  _p_writer = &_ods_writer;
}

MonitorComparOdsOutput::~MonitorComparOdsOutput()
{
  qDebug() << "begin";
  _p_writer->close();
}

MonitorComparInterfaceOutput::MonitorComparInterfaceOutput()
{
}

MonitorComparInterfaceOutput::~MonitorComparInterfaceOutput()
{
  qDebug() << "begin";
}

void
MonitorComparInterfaceOutput::writeQuantifyBegin(
  const Quantificator *p_quantificator)
{
  qDebug() << "begin";

  try
    {
      _p_writer->writeSheet(
        QString("compar_%1").arg(p_quantificator->getXmlId()));
      OdsTableSettings settings;
      settings.setVerticalSplit(1);
      _p_writer->setCurrentOdsTableSettings(settings);
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error writing peptide list for group %1 :\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }
  qDebug() << "end";
}

void
MonitorComparInterfaceOutput::writeQuantifyEnd()
{
  qDebug() << "begin _qitem_list.size()=" << _qitem_list.size();
  try
    {
      // sort quanti items :
      sort(_qitem_list.begin(),
           _qitem_list.end(),
           [](const QuantiItemBase *l1, const QuantiItemBase *l2) {
             return (l1->getMzId() < l2->getMzId());
           });
      // sort samples :
      sort(_msrun_list.begin(),
           _msrun_list.end(),
           [](const Msrun *l1, const Msrun *l2) {
             return (l1->getMsRunIdCstSPtr()->getXmlId() <
                     l2->getMsRunIdCstSPtr()->getXmlId());
           });

      // write headers
      _p_writer->writeLine();
      // mzid	peptide	m/z	z	sequence	mods	proteins
      _p_writer->writeEmptyCell();
      _p_writer->setCellAnnotation("peptide id");
      _p_writer->writeCell("peptide");
      _p_writer->writeCell("m/z");
      _p_writer->setCellAnnotation(
        "retention time in seconds, adjusted by MassChroQ");
      _p_writer->writeCell("rt reference (seconds)");
      _p_writer->setCellAnnotation("peptide charge");
      _p_writer->writeCell("z");
      _p_writer->setCellAnnotation("natural isotope number");
      _p_writer->writeCell("isotope number");
      _p_writer->setCellAnnotation("natural isotope rank");
      _p_writer->writeCell("isotope rank");
      _p_writer->setCellAnnotation("natural isotope theoretical ratio");
      _p_writer->writeCell("isotope ratio");
      _p_writer->setCellAnnotation(
        "peptide sequence (containing OBO PSIMOD modifications)");
      _p_writer->writeCell("sequence");
      _p_writer->setCellAnnotation("isotope label (tag)");
      _p_writer->writeCell("isotope");
      _p_writer->setCellAnnotation("peptide modifications (free text)");
      _p_writer->writeCell("mods");
      _p_writer->setCellAnnotation("protein ids");
      _p_writer->writeCell("proteins");
      for(const Msrun *p_msrun : _msrun_list)
        {
          _p_writer->writeCell(p_msrun->getMsRunIdCstSPtr()->getXmlId());
        }
      qDebug();
      QString start_position;
      // write content
      for(const QuantiItemBase *qitem : _qitem_list)
        {
          _p_writer->writeLine();
          _p_writer->writeCell(qitem->getMzId());
          qitem->writeOdsComparHeaderLine(*_p_writer);
          std::map<const QuantiItemBase *,
                   std::map<const Msrun *, pappso::pappso_double>>::
            const_iterator it_qitem = _compar_table.find(qitem);
          if(it_qitem != _compar_table.end())
            {
              for(const Msrun *p_msrun : _msrun_list)
                {
                  std::map<const Msrun *, pappso::pappso_double>::const_iterator
                    it_msrun = it_qitem->second.find(p_msrun);
                  if(it_msrun == it_qitem->second.end())
                    {
                      _p_writer->writeEmptyCell();
                    }
                  else
                    {
                      _p_writer->writeCell(it_msrun->second);
                    }
                  if(start_position.isEmpty())
                    {
                      start_position = _p_writer->getOdsCellCoordinate();
                    }
                }
            }
        }

      if(!start_position.isEmpty())
        {
          OdsColorScale color_scale(start_position,
                                    _p_writer->getOdsCellCoordinate());
          _p_writer->addColorScale(color_scale);
        }

      _compar_table.clear();
      _msrun_list.clear();
      _qitem_list.clear();
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error in MonitorComparInterfaceOutput::writeQuantifyEnd "
                    "group %1:\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }
  catch(std::exception &error)
    {
      throw mcqError(
        QObject::tr("error in MonitorComparInterfaceOutput::writeQuantifyEnd "
                    "group %1 :\n%2")
          .arg(_current_group_id)
          .arg(error.what()));
    }
  qDebug() << "end";
}

// print results
void
MonitorComparInterfaceOutput::privWriteMatchedPeak(
  const DetectionResult &detection_result)
{
  try
    {
      qDebug() << "begin";
      if(detection_result.sp_alignedPeak.get() != nullptr)
        {
          // store aligned peak in a table
          // rows are quanti items
          // columns are MSruns
          //_compar_table[p_quanti_item][p_msrun] = peak->getArea();
          std::map<const Msrun *, pappso::pappso_double> *p_msrun_value =
            nullptr;
          std::pair<
            std::map<const QuantiItemBase *,
                     std::map<const Msrun *, pappso::pappso_double>>::iterator,
            bool>
            pair_insert_qitem = _compar_table.insert(
              std::pair<const QuantiItemBase *,
                        std::map<const Msrun *, pappso::pappso_double>>(
                detection_result.p_quanti_item.get(),
                std::map<const Msrun *, pappso::pappso_double>()));
          if(pair_insert_qitem.second == true)
            {
              // new qitem
              _qitem_list.push_back(detection_result.p_quanti_item.get());
            }
          else
            {
              // existing qitem
            }

          p_msrun_value = &pair_insert_qitem.first->second;
          std::pair<std::map<const Msrun *, pappso::pappso_double>::iterator,
                    bool>
            pair_insert_msrun = p_msrun_value->insert(
              std::pair<const Msrun *, pappso::pappso_double>(
                detection_result.p_msrun,
                detection_result.sp_alignedPeak.get()->getArea()));
          if(pair_insert_msrun.second == true)
            {
              // new msrun
              if(std::find(_msrun_list.begin(),
                           _msrun_list.end(),
                           detection_result.p_msrun) == _msrun_list.end())
                {
                  _msrun_list.push_back(detection_result.p_msrun);
                }
            }
          else
            {
              // existing msrun
              // this means that we have more than one area value for a single
              // mcq item : potential problem we decide to store only the last
              // value
              qDebug()
                << "MonitorComparInterfaceOutput::privWriteMatchedPeak "
                   "multiple "
                   "value for qitem:"
                << detection_result.p_quanti_item->getQuantiItemId()
                << " msrun:"
                << detection_result.p_msrun->getMsRunIdCstSPtr()->getXmlId();
              pair_insert_msrun.first->second =
                detection_result.sp_alignedPeak.get()->getArea();
            }
        }
      qDebug() << "end";
    }
  catch(OdsException &error)
    {
      throw mcqError(
        QObject::tr(
          "Error in MonitorComparInterfaceOutput::privWriteMatchedPeak :\n%1")
          .arg(error.qwhat()));
    }
}
