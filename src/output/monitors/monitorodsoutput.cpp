
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorodsoutput.h"
#include "../../lib/peptides/peptide.h"
#include "../../lib/quantificator.h"
#include "../../lib/share/utilities.h"
#include <QDir>
#include <odsstream/writer/options/odstablesettings.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>

MonitorTsvOutput::MonitorTsvOutput(const QString &output_directory)
{
  try
    {
      _p_tsv_directory_writer = new TsvDirectoryWriter(QDir(output_directory));
      _p_tsv_directory_writer->setFlushLines(true);
    }
  catch(OdsException &error)
    {
      throw mcqError(
        QObject::tr("Error writing TSV ouput in directory %1 :\n%2")
          .arg(output_directory)
          .arg(error.qwhat()));
    }
  _p_writer = _p_tsv_directory_writer;
}

MonitorTsvOutput::~MonitorTsvOutput()
{

  qDebug();

  _p_writer->close();

  delete _p_tsv_directory_writer;
}

MonitorOdsOutput::MonitorOdsOutput(const QString &ods_file)
{
  try
    {
      _p_ods_writer = new OdsDocWriter(ods_file);
    }
  catch(OdsException &error)
    {
      throw mcqError(QObject::tr("Error writing ODS file %1 :\n%2")
                       .arg(ods_file)
                       .arg(error.qwhat()));
    }
  _p_writer = _p_ods_writer;
}

MonitorOdsOutput::~MonitorOdsOutput()
{
  qDebug();

  _p_writer->close();
  delete _p_ods_writer;
}

MonitorOdsInterfaceOutput::MonitorOdsInterfaceOutput()
{
}

MonitorOdsInterfaceOutput::~MonitorOdsInterfaceOutput()
{
  qDebug();
}

void
MonitorOdsInterfaceOutput::writeQuantifyBegin(
  const Quantificator *p_quantificator)
{
  qDebug();
  _current_group_id          = p_quantificator->getMsRunGroup()->getXmlId();
  _current_quantification_id = p_quantificator->getXmlId();
  qDebug();
  try
    {

      _p_writer->writeSheet(
        QString("MassChroQ informations - %1").arg(_current_quantification_id));

      _p_writer->writeCell("MassChroQ version");
      _p_writer->writeCell(QString("%1").arg(MASSCHROQ_VERSION));
      _p_writer->writeLine();
      const AlignmentMs2 *p_alignment_ms2 =
        p_quantificator->getMsRunGroup()->getAlignmentMs2CstPtr();
      _p_writer->writeLine();
      _p_writer->writeCell("Alignment parameters");
      _p_writer->writeLine();
      _p_writer->writeCell("alignment group id");
      qDebug();

      _p_writer->writeCell(p_quantificator->getMsRunGroup()->getXmlId());
      _p_writer->writeLine();
      qDebug();

      _p_writer->writeCell("alignment group reference XML id");
      if(p_alignment_ms2 == nullptr)
        {
          _p_writer->writeCell("no alignment");
        }
      else
        {
          _p_writer->writeCell(p_quantificator->getMsRunGroup()
                                 ->getReferenceMsrun()
                                 ->getMsRunIdCstSPtr()
                                 .get()
                                 ->getRunId());
        }
      _p_writer->writeLine();
      qDebug();

      _p_writer->writeCell("alignment group reference msrun filename");
      if(p_alignment_ms2 == nullptr)
        {
          _p_writer->writeCell("no alignment");
        }
      else
        {
          _p_writer->writeCell(p_quantificator->getMsRunGroup()
                                 ->getReferenceMsrun()
                                 ->getMsRunIdCstSPtr()
                                 .get()
                                 ->getFileName());
        }
      _p_writer->writeLine();

      qDebug();
      if(p_alignment_ms2 != nullptr)
        {
          _p_writer->writeCell("MS1 smoothing half window");
          _p_writer->writeCell(p_alignment_ms2->getMs1SmoothingWindow());
          _p_writer->writeLine();
          _p_writer->writeCell("MS2 smoothing half window");
          _p_writer->writeCell(p_alignment_ms2->getMs2SmoothingWindow());
          _p_writer->writeLine();
          _p_writer->writeCell("MS2 tendency half window");
          _p_writer->writeCell(p_alignment_ms2->getMs2TendencyWindow());
          _p_writer->writeLine();
        }
      qDebug();
      // alignment parameters
      // ms2_tendency_halfwindow \t 10
      // ms2_smoothing_halfwindow \t 15
      // ms1_smoothing_halfwindow \t 0

      // extraction parameters

      _p_writer->writeLine();
      _p_writer->writeCell("XIC parameters");
      _p_writer->writeLine();
      _p_writer->writeCell("extraction range lower limit");
      _p_writer->writeCell(p_quantificator->getQuantificationMethod()
                             ->getLowerPrecision()
                             ->toString());
      _p_writer->writeLine();
      _p_writer->writeCell("extraction range upper limit");
      _p_writer->writeCell(p_quantificator->getQuantificationMethod()
                             ->getUpperPrecision()
                             ->toString());
      _p_writer->writeLine();
      _p_writer->writeCell("natural isotope minimum abundance");
      _p_writer->writeCell(p_quantificator->getNiMinimumAbundance());
      _p_writer->writeLine();
      _p_writer->writeCell("matching mode");
      _p_writer->writeCell(
        Utilities::toString(p_quantificator->getMatchingMode()));
      _p_writer->writeLine();
      _p_writer->writeLine();


      _p_writer->writeCell("Filters");
      _p_writer->writeLine();

      const pappso::FilterSuite &filters =
        p_quantificator->getQuantificationMethod()->getXicFilters();

      for(auto &&one_filter : filters)
        {
          const pappso::FilterNameInterface *p_filter =
            dynamic_cast<const pappso::FilterNameInterface *>(one_filter.get());
          if(p_filter != nullptr)
            {
              _p_writer->writeCell(p_filter->toString());
              _p_writer->writeLine();
            }
        }


      _p_writer->writeCell("Detection method");
      _p_writer->writeLine();
      const pappso::TraceDetectionInterface *p_detection =
        p_quantificator->getQuantificationMethod()->getDetectionMethod().get();
      if(p_detection != nullptr)
        {
          const pappso::TraceDetectionZivy *p_detectionZivy =
            dynamic_cast<const pappso::TraceDetectionZivy *>(p_detection);
          if(p_detectionZivy != nullptr)
            {
              _p_writer->writeCell("detection Zivy");
              _p_writer->writeLine();
              _p_writer->writeCell("smoothing half edge window");
              _p_writer->writeCell(
                (std::size_t)p_detectionZivy->getSmoothingHalfEdgeWindows());
              _p_writer->writeLine();
              _p_writer->writeCell("maxmin half edge window");
              _p_writer->writeCell(
                (std::size_t)p_detectionZivy->getMaxMinHalfEdgeWindows());
              _p_writer->writeLine();
              _p_writer->writeCell("minmax half edge window");
              _p_writer->writeCell(
                (std::size_t)p_detectionZivy->getMinMaxHalfEdgeWindows());
              _p_writer->writeLine();
              _p_writer->writeCell("detection threshold on maxmin");
              _p_writer->writeCell(
                (std::size_t)p_detectionZivy->getDetectionThresholdOnMaxmin());
              _p_writer->writeLine();
              _p_writer->writeCell("detection threshold on minmax");
              _p_writer->writeCell(
                (std::size_t)p_detectionZivy->getDetectionThresholdOnMinmax());
              _p_writer->writeLine();
            }
        }


      _p_writer->writeLine();
      _p_writer->writeLine();


      const std::vector<PeptideCstSPtr> &peptide_list =
        p_quantificator->getPeptideList();
      if(!_peptide_list_written.contains(_current_group_id))
        {
          this->writePeptideList(peptide_list);
        }


      //_p_writer->writeLine();
      _p_writer->writeSheet(QString("peptides_%1_%2")
                              .arg(p_quantificator->getXmlId())
                              .arg(_current_group_id));

      OdsTableSettings settings;
      settings.setVerticalSplit(1);
      _p_writer->setCurrentOdsTableSettings(settings);

      _p_writer->setCellAnnotation("quantification XML id");
      _p_writer->writeCell("quantification");
      _p_writer->setCellAnnotation("group XML id (fraction name)");
      _p_writer->writeCell("group");
      _p_writer->setCellAnnotation("MS run XML id (sample id)");
      _p_writer->writeCell("msrun");
      _p_writer->setCellAnnotation("MS run file path");
      _p_writer->writeCell("msrunfile");
      _p_writer->setCellAnnotation("XIC m/z");
      _p_writer->writeCell("mz");
      _p_writer->setCellAnnotation(
        "peak maximum intensity retention time aligned");
      _p_writer->writeCell("rt");
      _p_writer->setCellAnnotation("peak maximum intensity");
      _p_writer->writeCell("maxintensity");
      _p_writer->setCellAnnotation("peak area");
      _p_writer->writeCell("area");
      _p_writer->setCellAnnotation("peak quality : how the peak was measured");
      _p_writer->writeCell("peak quality");
      _p_writer->setCellAnnotation("peak start retention time aligned");
      _p_writer->writeCell("rtbegin");
      _p_writer->setCellAnnotation("peak stop retention time aligned");
      _p_writer->writeCell("rtend");
      _p_writer->setCellAnnotation("peak start retention time not aligned");
      _p_writer->writeCell("realrtbegin");
      _p_writer->setCellAnnotation(
        "peak apex (maximum intensity) retention time not aligned");
      _p_writer->writeCell("realrtapex");
      _p_writer->setCellAnnotation("peak stop retention time not aligned");
      _p_writer->writeCell("realrtend");
      _p_writer->setCellAnnotation("peptide id");
      _p_writer->writeCell("peptide");
      _p_writer->setCellAnnotation("isotope label");
      _p_writer->writeCell("label");
      _p_writer->setCellAnnotation("peptide sequence");
      _p_writer->writeCell("sequence");
      _p_writer->setCellAnnotation("peptide charge");
      _p_writer->writeCell("z");
      _p_writer->setCellAnnotation("peptide modifications (free text)");
      _p_writer->writeCell("mods");
      if(p_quantificator->getNiMinimumAbundance() != 0)
        {
          _p_writer->setCellAnnotation("natural isotope number");
          _p_writer->writeCell("ninumber");
          _p_writer->setCellAnnotation("natural isotope rank");
          _p_writer->writeCell("nirank");
          _p_writer->setCellAnnotation("natural isotope theoretical ratio");
          _p_writer->writeCell("niratio");
        }


      _p_writer->writeLine();
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error writing quantification for group %1 :\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }
  qDebug();
}

void
MonitorOdsInterfaceOutput::writePeptideList(
  const std::vector<PeptideCstSPtr> &isotope_peptides)
{
  qDebug();
  try
    {
      _peptide_list_written.append(_current_group_id);
      _p_writer->writeSheet(QString("%1_proteins").arg(_current_group_id));
      //_p_writer->writeLine();
      _p_writer->writeCell("peptide");
      _p_writer->writeCell("ProForma");
      _p_writer->writeCell("protein");
      _p_writer->writeCell("protein_description");

      OdsTableSettings settings;
      settings.setVerticalSplit(1);
      _p_writer->setCurrentOdsTableSettings(settings);

      std::vector<std::pair<QString, QString>> prot_pep_ids;

      auto itpep    = isotope_peptides.begin();
      auto itpepend = isotope_peptides.end();
      for(; itpep != itpepend; ++itpep)
        {
          auto itprot    = (*itpep)->getProteinList().begin();
          auto itprotend = (*itpep)->getProteinList().end();
          for(; itprot != itprotend; ++itprot)
            {
              std::pair<QString, QString> prot_pep = {(*itpep)->getXmlId(),
                                                      (*itprot)->getXmlId()};
              if(std::find_if(
                   prot_pep_ids.begin(),
                   prot_pep_ids.end(),
                   [prot_pep](std::pair<QString, QString> &pair_pep_prot) {
                     return ((pair_pep_prot.first == prot_pep.first) &&
                             (pair_pep_prot.second == prot_pep.second));
                   }

                   ) == prot_pep_ids.end())
                {
                  _p_writer->writeLine();
                  _p_writer->writeCell((*itpep)->getXmlId());
                  if((*itpep)->getOriginalPeptidePointer().get() == nullptr)
                    {
                      _p_writer->writeCell(
                        (*itpep)->getPappsoPeptideSp().get()->toProForma());
                    }
                  else
                    {
                      _p_writer->writeCell((*itpep)
                                             ->getOriginalPeptidePointer()
                                             .get()
                                             ->getPappsoPeptideSp()
                                             .get()
                                             ->toProForma());
                    }

                  _p_writer->writeCell((*itprot)->getXmlId());
                  _p_writer->writeCell((*itprot)->getDescription());
                  prot_pep_ids.push_back(prot_pep);
                }
            }
        }
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error writing peptide list for group %1 :\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }

  qDebug();
}

void
MonitorOdsInterfaceOutput::writeQuantifyEnd()
{
}

// print results
void
MonitorOdsInterfaceOutput::privWriteMatchedPeak(
  const DetectionResult &detection_result)
{
  try
    {
      qDebug();
      if(detection_result.sp_alignedPeak.get() != nullptr)
        {
          _p_writer->writeCell(_current_quantification_id);
          _p_writer->writeCell(_current_group_id);
          _p_writer->writeCell(
            detection_result.p_msrun->getMsRunIdCstSPtr()->getXmlId());
          _p_writer->writeCell(
            QFileInfo(
              detection_result.p_msrun->getMsRunIdCstSPtr()->getFileName())
              .baseName());
          _p_writer->writeCell(detection_result.p_quanti_item->getMz());
          _p_writer->writeCell(
            detection_result.sp_alignedPeak.get()->getMaxXicElement().x);
          _p_writer->writeCell(
            detection_result.sp_alignedPeak.get()->getMaxXicElement().y);

          _p_writer->writeCell(
            detection_result.sp_alignedPeak.get()->getArea());
          _p_writer->writeCell(enumToString(detection_result.mbr_category));
          _p_writer->writeCell(
            detection_result.sp_alignedPeak.get()->getLeftBoundary().x);
          _p_writer->writeCell(
            detection_result.sp_alignedPeak.get()->getRightBoundary().x);
          _p_writer->writeCell(detection_result.sp_alignedPeak.get()
                                 ->getNonAlignedPeak()
                                 .getLeftBoundary()
                                 .x);
          _p_writer->writeCell(detection_result.sp_alignedPeak.get()
                                 ->getNonAlignedPeak()
                                 .getMaxXicElement()
                                 .x);
          _p_writer->writeCell(detection_result.sp_alignedPeak.get()
                                 ->getNonAlignedPeak()
                                 .getRightBoundary()
                                 .x);

          detection_result.p_quanti_item->writeOdsPeptideLine(*_p_writer);

          _p_writer->writeLine();
        }
      qDebug();
    }
  catch(OdsException &error)
    {
      throw mcqError(
        QObject::tr(
          "Error in MonitorOdsInterfaceOutput::privWriteMatchedPeak :\n%1")
          .arg(error.qwhat()));
    }
}
