
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../lib/quanti_items/quantiItemBase.h"
#include "monitorspeedinterface.h"
#include <odsstream/calcwriterinterface.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>
#include <odsstream/tsvdirectorywriter.h>

class MonitorOdsInterfaceOutput : public MonitorSpeedInterface
{
  public:
  void writeQuantifyBegin(const Quantificator *p_quantificator) override;
  void writeQuantifyEnd() override;


  protected:
  MonitorOdsInterfaceOutput();
  virtual ~MonitorOdsInterfaceOutput();
  void privWriteMatchedPeak(const DetectionResult &detection_result) override;

  void writePeptideList(const std::vector<PeptideCstSPtr> &isotope_peptides);


  protected:
  CalcWriterInterface *_p_writer = nullptr;
  QString _current_group_id;
  QString _current_quantification_id;

  private:
  QStringList _peptide_list_written;
};

class MonitorTsvOutput : public MonitorOdsInterfaceOutput
{
  public:
  MonitorTsvOutput(const QString &output_directory);
  ~MonitorTsvOutput();

  protected:
  TsvDirectoryWriter *_p_tsv_directory_writer;
};

class MonitorOdsOutput : public MonitorOdsInterfaceOutput
{
  public:
  MonitorOdsOutput(const QString &ods_file);
  ~MonitorOdsOutput();

  protected:
  OdsDocWriter *_p_ods_writer;
};
