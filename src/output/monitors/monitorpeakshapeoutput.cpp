/**
 * \file src/output/monitors/monitorpeakshapeoutput.cpp
 * \date 7/7/2022
 * \author Olivier Langella
 * \brief store peak shapes
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "monitorpeakshapeoutput.h"
#include "../../lib/quantificator.h"
#include <QDebug>

MonitorPeakShapeOutput::MonitorPeakShapeOutput(const QDir &output_directory)

{
  qDebug();
  m_outputDir = output_directory;

  mpa_peptideInfoOut =
    new MonitorTsvOutput(QString("%1/infos").arg(m_outputDir.absolutePath()));
  qDebug();
}

MonitorPeakShapeOutput::~MonitorPeakShapeOutput()
{
  delete mpa_peptideInfoOut;
  if(mpa_shapeTableOut != nullptr)
    {
      mpa_shapeTableOut->close();
      delete mpa_shapeTableOut;
    }
}
void
MonitorPeakShapeOutput::writeQuantifyBegin(const Quantificator *p_quantificator)
{
  qDebug();
  if(mpa_shapeTableOut != nullptr)
    {
      qDebug();
      mpa_shapeTableOut->close();
      qDebug();
      delete mpa_shapeTableOut;
      qDebug();
      mpa_shapeTableOut = nullptr;
      qDebug();
    }
  qDebug();
  mpa_shapeTableOut =
    new TsvDirectoryWriter(QDir(QString("%1/%2_shape")
                                  .arg(m_outputDir.absolutePath())
                                  .arg(p_quantificator->getXmlId())));
  m_lineCount = 0;

  mpa_shapeTableOut->writeSheet("peaks");
  mpa_shapeTableOut->writeCell("peak_index");
  mpa_shapeTableOut->writeCell("rtx");
  mpa_shapeTableOut->writeCell("rtxalign");
  mpa_shapeTableOut->writeCell("y");
  mpa_shapeTableOut->writeLine();

  mpa_peptideInfoOut->writeQuantifyBegin(p_quantificator);
  qDebug();
}

void
MonitorPeakShapeOutput::writeQuantifyEnd()
{
  mpa_peptideInfoOut->writeQuantifyEnd();

  mpa_shapeTableOut->close();
  delete mpa_shapeTableOut;
  mpa_shapeTableOut = nullptr;
}

void
MonitorPeakShapeOutput::privWriteMatchedPeak(
  const DetectionResult &detection_result)
{
  qDebug();
  if(detection_result.sp_alignedPeak.get() != nullptr)
    {
      if(detection_result.sp_traceResult.get() != nullptr)
        {
          pappso::TraceSPtr trace_sp =
            detection_result.sp_traceResult.get()->getMatchedPeakTraceSPtr();
          if(trace_sp != nullptr)
            {
              for(const pappso::DataPoint &point : *trace_sp.get())
                {
                  mpa_shapeTableOut->writeCell(m_lineCount);
                  mpa_shapeTableOut->writeCell(point.x);
                  mpa_shapeTableOut->writeCell(
                    detection_result.p_msrun->getAlignedRtByOriginalRt(
                      point.x));
                  mpa_shapeTableOut->writeCell(point.y);
                  mpa_shapeTableOut->writeLine();
                }

              mpa_peptideInfoOut->writeDetectionResult(detection_result);
            }
        }

      m_lineCount++;
    }
  qDebug();
}
