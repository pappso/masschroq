/**
 * \file src/output/monitors/monitorpeakshapeoutput.h
 * \date 7/7/2022
 * \author Olivier Langella
 * \brief store peak shapes
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include <QDir>
#include "monitorspeedinterface.h"
#include <odsstream/tsvdirectorywriter.h>
#include "monitorodsoutput.h"

#pragma once
/**
 * @todo write docs
 */
class MonitorPeakShapeOutput : public MonitorSpeedInterface
{
  public:
  /**
   * Default constructor
   */
  MonitorPeakShapeOutput(const QDir &output_directory);

  /**
   * Destructor
   */
  virtual ~MonitorPeakShapeOutput();

  void writeQuantifyBegin(const Quantificator *p_quantificator) override;
  void writeQuantifyEnd() override;


  protected:
  void privWriteMatchedPeak(const DetectionResult &detection_result) override;


  private:
  QDir m_outputDir;
  MonitorTsvOutput *mpa_peptideInfoOut = nullptr;
  TsvDirectoryWriter *mpa_shapeTableOut = nullptr;

  std::size_t m_lineCount = 0;
};
