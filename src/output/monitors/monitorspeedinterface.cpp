
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorspeedinterface.h"
#include "../../lib/quanti_items/quantiItemBase.h"
#include "../../lib/quantificator.h"
#include <QFile>
#include <QDebug>
#include "../../lib/msrun/msrun.h"
#include "../../lib/quanti_items/quantiItemBase.h"

MonitorSpeedInterface::MonitorSpeedInterface()
{
}

MonitorSpeedInterface::~MonitorSpeedInterface()
{
  if(_p_log_writer != nullptr)
    {
      _p_log_writer->close();
      _p_log_file->close();
    }
}

void
MonitorSpeedInterface::writeDetectionResult(
  const DetectionResult &detection_result)
{
  QMutexLocker locker(&_mutex);
  qDebug();
  privWriteMatchedPeak(detection_result);
  qDebug();
}

void
MonitorSpeedInterface::logMissedObservedRealXic(
  const QuantiItemBase *p_quanti_item,
  const pappso::Xic *p_xic,
  const std::vector<MsMsRtIntensity> *p_real_rt,
  const std::vector<pappso::TracePeakCstSPtr> *p_log_peaks)
{
  QMutexLocker locker(&_mutex);
  if(_p_log_writer == nullptr)
    {
      _p_log_file = new QFile("log_missed_real_xic.ods");
      // file.open(QIODevice::WriteOnly);
      _p_log_writer = new OdsDocWriter(_p_log_file);
    }
  _p_log_writer->writeSheet("missed");

  p_quanti_item->writeOdsPeptideLine(*_p_log_writer);

  _p_log_writer->writeLine();
  _p_log_writer->writeCell("xic size");
  _p_log_writer->writeCell("observed MSMS count");
  _p_log_writer->writeCell("peak count");
  _p_log_writer->writeLine();
  _p_log_writer->writeCell((int)p_xic->size());
  _p_log_writer->writeCell((int)p_real_rt->size());
  _p_log_writer->writeCell((int)p_log_peaks->size());

  _p_log_writer->writeLine();
  _p_log_writer->writeLine();
  _p_log_writer->writeCell("xic rt");
  _p_log_writer->writeCell("xic intensity");
  _p_log_writer->writeCell("MSMS intensity");
  _p_log_writer->writeCell("peak boundings");

  auto it     = p_xic->begin();
  auto itnext = p_xic->begin()++;
  auto itend  = p_xic->end();

  std::vector<MsMsRtIntensity> real_rt(*p_real_rt);
  std::sort(real_rt.begin(),
            real_rt.end(),
            [](const MsMsRtIntensity &A, const MsMsRtIntensity &B) {
              return (A.rt < B.rt);
            });
  std::vector<MsMsRtIntensity>::const_iterator it_real_rt    = real_rt.begin();
  std::vector<MsMsRtIntensity>::const_iterator itend_real_rt = real_rt.end();

  std::vector<pappso::TracePeakCstSPtr> v_peaks(*p_log_peaks);
  std::sort(
    v_peaks.begin(),
    v_peaks.end(),
    [](const pappso::TracePeakCstSPtr &A, const pappso::TracePeakCstSPtr &B) {
      return (A.get()->getMaxXicElement().x < B.get()->getMaxXicElement().x);
    });
  std::vector<pappso::TracePeakCstSPtr>::const_iterator it_peak =
    v_peaks.begin();
  std::vector<pappso::TracePeakCstSPtr>::const_iterator itend_peak =
    v_peaks.end();

  while(it != itend)
    {
      _p_log_writer->writeLine();
      pappso::pappso_double rt     = it->x;
      pappso::pappso_double nextrt = 10000000000000000000.0;
      if(itnext != itend)
        {
          nextrt = itnext->x;
        }

      _p_log_writer->writeCell(rt);
      _p_log_writer->writeCell(it->y);

      bool write_msms = false;
      if(it_real_rt != itend_real_rt)
        {
          while(((it_real_rt + 1) != itend_real_rt) && (it_real_rt->rt < rt))
            {
              it_real_rt++;
            }
          if(it_real_rt->rt == rt)
            {
              _p_log_writer->writeCell(it_real_rt->intensity);
              it_real_rt++;
            }
          else if(it_real_rt->rt < nextrt)
            {
              write_msms = true;
              _p_log_writer->writeEmptyCell();
            }
          else
            {
              _p_log_writer->writeEmptyCell();
            }
        }
      else
        {
          _p_log_writer->writeEmptyCell();
        }

      if(it_peak != itend_peak)
        {
          if(rt == it_peak->get()->getLeftBoundary().x)
            {
              _p_log_writer->writeCell(it_peak->get()->getLeftBoundary().y);
              _p_log_writer->writeCell("begin");
            }

          if(rt == it_peak->get()->getMaxXicElement().x)
            {
              _p_log_writer->writeCell(it_peak->get()->getMaxXicElement().y);
              _p_log_writer->writeCell("max");
            }

          if(rt == it_peak->get()->getRightBoundary().x)
            {
              _p_log_writer->writeCell(it_peak->get()->getRightBoundary().y);
              _p_log_writer->writeCell("end");
              it_peak++;
            }
        }

      if(write_msms)
        {
          _p_log_writer->writeLine();
          _p_log_writer->writeCell(it_real_rt->rt);
          _p_log_writer->writeEmptyCell();
          _p_log_writer->writeCell(it_real_rt->intensity);
          it_real_rt++;
        }
      it++;
      itnext++;
    }
}

QString
MonitorSpeedInterface::enumToString(PeakQualityCategory peak_category)
{
  switch(peak_category)
    {
      case PeakQualityCategory::a:
        return "a";
        break;
      case PeakQualityCategory::aa:
        return "aa";
        break;
      case PeakQualityCategory::za:
        return "za";
        break;
      case PeakQualityCategory::zaa:
        return "zaa";
        break;
      case PeakQualityCategory::ab:
        return "ab";
        break;
      case PeakQualityCategory::zab:
        return "zab";
        break;
      case PeakQualityCategory::b:
        return "b";
        break;
      case PeakQualityCategory::c:
        return "c";
        break;
      case PeakQualityCategory::d:
        return "d";
        break;
      case PeakQualityCategory::missed:
        return "missed";
        break;
      case PeakQualityCategory::nomatch:
        return "nomatch";
        break;
      case PeakQualityCategory::post_matching:
        return "pm";
        break;
      case PeakQualityCategory::last:
        throw mcqError(QObject::tr("no QString for PeakQualityCategory::last"));
        break;
    }

  return "";
}


void
MonitorSpeedInterface::writeQuantificationItemListInMsRun(
  const QString &quanti_xml_id,
  const QString &msrun_xml_id,
  const std::vector<QuantiItemBaseSp> &quanti_item_list)
{
}
