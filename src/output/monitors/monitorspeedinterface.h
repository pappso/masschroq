
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../lib/msrun/ms_run_hash_group.h"
#include "../../lib/peptides/peptide.h"
#include "../../lib/xic/alignedxicpeak.h"
#include <QMutex>
#include <odsstream/odsdocwriter.h>
#include <pappsomspp/processing/detection/tracepeak.h>
#include "../../output/detectionresult.h"

class Quantificator;
struct MsMsRtIntensity;
class MonitorSpeedList;
class Msrun;
class QuantiItemBase;


class MonitorSpeedInterface;
typedef std::shared_ptr<MonitorSpeedInterface> MonitorSpeedInterfaceSp;


class MonitorSpeedInterface
{
  friend MonitorSpeedList;

  public:
  MonitorSpeedInterface();
  virtual ~MonitorSpeedInterface();

  virtual void
  writeDetectionResult(const DetectionResult &detection_result) final;

  virtual void writeQuantifyBegin(const Quantificator *p_quantificator) = 0;
  virtual void writeQuantifyEnd()                                       = 0;
  virtual void logMissedObservedRealXic(
    const QuantiItemBase *p_quanti_item,
    const pappso::Xic *p_xic,
    const std::vector<MsMsRtIntensity> *p_real_rt,
    const std::vector<pappso::TracePeakCstSPtr> *p_log_peaks);
  
  
  
  virtual void writeQuantificationItemListInMsRun(
    const QString &quanti_xml_id,
    const QString &msrun_xml_id,
    const std::vector<QuantiItemBaseSp> &quanti_item_list);


  static QString enumToString(PeakQualityCategory peak_category);

  protected:
  virtual void
  privWriteMatchedPeak(const DetectionResult &detection_result) = 0;

  private:
  OdsDocWriter *_p_log_writer = nullptr;
  QFile *_p_log_file;

  QMutex _mutex;
};
