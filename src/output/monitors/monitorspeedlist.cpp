
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorspeedlist.h"

MonitorSpeedList::MonitorSpeedList()
{
}

MonitorSpeedList::~MonitorSpeedList()
{
  qDebug();
}

void
MonitorSpeedList::writeQuantifyBegin(const Quantificator *p_quantificator)
{

  qDebug();
  for(auto &&p_monitor : m_monitorSpList)
    {
      qDebug() << p_monitor.get();
      p_monitor->writeQuantifyBegin(p_quantificator);
      qDebug();
    }
  qDebug();
}

void
MonitorSpeedList::writeQuantifyEnd()
{

  qDebug();
  for(auto &&p_monitor : m_monitorSpList)
    {
      qDebug() << p_monitor.get();
      p_monitor->writeQuantifyEnd();
      qDebug();
    }
  qDebug();
}

void
MonitorSpeedList::writeQuantificationItemListInMsRun(
  const QString &quanti_xml_id,
  const QString &msrun_xml_id,
  const std::vector<QuantiItemBaseSp> &quanti_item_list)
{
  for(auto &&p_monitor : m_monitorSpList)
    {
      qDebug() << p_monitor.get();
      p_monitor->writeQuantificationItemListInMsRun(
        quanti_xml_id, msrun_xml_id, quanti_item_list);
      qDebug();
    }
}


void
MonitorSpeedList::addMonitor(MonitorSpeedInterfaceSp sp_monitor_speed)
{
  qDebug();
  m_monitorSpList.push_back(sp_monitor_speed);
  qDebug();
}

void
MonitorSpeedList::privWriteMatchedPeak(const DetectionResult &detection_result)
{

  qDebug();
  for(auto &&p_monitor : m_monitorSpList)
    {
      qDebug() << p_monitor.get();
      p_monitor->privWriteMatchedPeak(detection_result);
      qDebug();
    }
  qDebug();
}
