
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "monitorspeedinterface.h"

class MonitorSpeedList : public MonitorSpeedInterface
{
  public:
  MonitorSpeedList();
  virtual ~MonitorSpeedList();
  void addMonitor(MonitorSpeedInterfaceSp sp_monitor_speed);

  void writeQuantifyBegin(const Quantificator *p_quantificator) override;
  void writeQuantifyEnd() override;

  virtual void writeQuantificationItemListInMsRun(
    const QString &quanti_xml_id,
    const QString &msrun_xml_id,
    const std::vector<QuantiItemBaseSp> &quanti_item_list) override;

  protected:
  void privWriteMatchedPeak(const DetectionResult &detection_result) override;

  protected:
  std::vector<MonitorSpeedInterfaceSp> m_monitorSpList;
};
