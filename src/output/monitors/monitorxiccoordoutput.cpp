/**
 * \file src/output/monitors/monitorxiccoordoutput.cpp
 * \date 13/1/2023
 * \author Olivier Langella
 * \brief write exact XIC coordinates on each msrun
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "monitorxiccoordoutput.h"

MonitorXicCoordOutput::MonitorXicCoordOutput(const QDir &output_directory)
{
  qDebug();
  m_outputDir = output_directory;

  mpa_xicCoordTableOut =
    new TsvDirectoryWriter(QDir(QString("%1").arg(m_outputDir.absolutePath())));
  qDebug();
}

MonitorXicCoordOutput::~MonitorXicCoordOutput()
{
  if(mpa_xicCoordTableOut != nullptr)
    {
      mpa_xicCoordTableOut->close();
      delete mpa_xicCoordTableOut;
    }
}

void
MonitorXicCoordOutput::writeQuantifyBegin(const Quantificator *p_quantificator
                                          [[maybe_unused]])
{
}

void
MonitorXicCoordOutput::writeQuantifyEnd()
{
}

void
MonitorXicCoordOutput::writeQuantificationItemListInMsRun(
  const QString &quanti_xml_id,
  const QString &msrun_xml_id,
  const std::vector<QuantiItemBaseSp> &quanti_item_list)
{
  //_p_writer->writeLine();
  mpa_xicCoordTableOut->writeSheet(
    QString("xic_coord_%1_%2").arg(quanti_xml_id).arg(msrun_xml_id));

  for(auto &quanti_item : quanti_item_list)
    {

      auto xic_coord = quanti_item->getXicCoordSPtrExtractedInMsRun();
      mpa_xicCoordTableOut->writeCell(quanti_item->getQuantiItemId());
      mpa_xicCoordTableOut->writeCell(
        xic_coord.get()
          ->getParam(pappso::XicCoordParam::TimsTofIonMobilityScanNumberStart)
          .toInt());
      mpa_xicCoordTableOut->writeCell(
        xic_coord.get()
          ->getParam(pappso::XicCoordParam::TimsTofIonMobilityScanNumberStop)
          .toInt());
      mpa_xicCoordTableOut->writeCell(xic_coord.get()->toString());
      mpa_xicCoordTableOut->writeLine();
    }
}

void
MonitorXicCoordOutput::privWriteMatchedPeak(
  const DetectionResult &detection_result [[maybe_unused]])
{
}
