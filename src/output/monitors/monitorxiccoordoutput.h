/**
 * \file src/output/monitors/monitorxiccoordoutput.h
 * \date 13/1/2023
 * \author Olivier Langella
 * \brief write exact XIC coordinates on each msrun
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include <QDir>
#include "monitorspeedinterface.h"
#include <odsstream/tsvdirectorywriter.h>

#pragma once
/**
 * @todo write docs
 */
class MonitorXicCoordOutput : public MonitorSpeedInterface
{
  public:
  /**
   * Default constructor
   */
  MonitorXicCoordOutput(const QDir &output_directory);

  /**
   * Destructor
   */
  ~MonitorXicCoordOutput();


  void writeQuantifyBegin(const Quantificator *p_quantificator) override;
  void writeQuantifyEnd() override;

  virtual void writeQuantificationItemListInMsRun(
    const QString &quanti_xml_id,
    const QString &msrun_xml_id,
    const std::vector<QuantiItemBaseSp> &quanti_item_list) override;

  protected:
  virtual void
  privWriteMatchedPeak(const DetectionResult &detection_result);

  private:
  QDir m_outputDir;
  TsvDirectoryWriter *mpa_xicCoordTableOut = nullptr;
};
