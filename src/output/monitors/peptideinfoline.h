/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QString>
#include <pappsomspp/types.h>

class PeptideInfoLine
{
  public:
  QString m_quantification;
  QString m_group;
  QString m_msrun;
  QString m_msrunFile;
  pappso::pappso_double m_mz;
  pappso::pappso_double m_rt;
  pappso::pappso_double m_maxIntensity;
  pappso::pappso_double m_area;
  PeakQualityCategory m_peakQuality;
  pappso::pappso_double m_rtBegin;
  pappso::pappso_double m_rtEnd;
  QString m_peptide;
  QString m_label;
  QString m_sequence;
  int m_z;
  QString m_mods;
  int m_niNumber;
  int m_niRank;
  pappso::pappso_double m_niRatio;
};
