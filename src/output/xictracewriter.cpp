/**
 * \file src/output/xictracewriter.cpp
 * \date 1/7/2022
 * \author Olivier Langella
 * \brief one object to rule traces output
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "xictracewriter.h"
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/odsdocwriter.h>
#include "../lib/xic/xictracepeptide.h"
#include "../lib/share/utilities.h"
#include "monitors/monitorspeedinterface.h"
#include "../lib/quantificator.h"

XicTraceWriter::XicTraceWriter(const Quantificator *p_quantificator)
{
  mp_quantificator = p_quantificator;
}

XicTraceWriter::XicTraceWriter(const XicTraceWriter &other)
{
  m_tracesDirectory = other.m_tracesDirectory;
  m_tracesFormat    = other.m_tracesFormat;
}

XicTraceWriter::~XicTraceWriter()
{
}

const QDir &
XicTraceWriter::getTracesDirectory() const
{
  return m_tracesDirectory;
}
McqTsvFormat
XicTraceWriter::getTracesFormat() const
{
  return m_tracesFormat;
}

void
XicTraceWriter::setTracesDirectory(const QDir &directory)
{
  m_tracesDirectory = directory;
}

void
XicTraceWriter::setTracesFormat(McqTsvFormat format)
{
  m_tracesFormat = format;
}

const QDir
XicTraceWriter::makeTraceDirectory(const DetectionResult &result) const
{
  qDebug();
  QDir trace_dir;
  if(result.p_msrun == nullptr)
    {
      throw mcqError(QObject::tr("result.p_msrun == nullptr"));
    }
  if(result.p_msrun->getMsRunIdCstSPtr().get() == nullptr)
    {
      throw mcqError(
        QObject::tr("result.p_msrun->getMsRunIdCstSPtr().get() == nullptr"));
    }
  if(result.p_quanti_item.get()->getPeptide() != nullptr)
    {
      qDebug();
      trace_dir.setPath(
        QString("%1/%2/%3")
          .arg(m_tracesDirectory.absolutePath())
          .arg(result.p_quanti_item.get()->getPeptide().get()->getXmlId())
          .arg(result.p_msrun->getMsRunIdCstSPtr().get()->getXmlId()));
      if(!trace_dir.mkpath(trace_dir.absolutePath()))
        {
          throw mcqError(
            QObject::tr("mkpath %1 failed ").arg(trace_dir.absolutePath()));
        }
    }
  else
    {
      qDebug();
      trace_dir.setPath(
        QString("%1/%2")
          .arg(m_tracesDirectory.absolutePath())
          .arg(result.p_msrun->getMsRunIdCstSPtr().get()->getXmlId()));
      if(!trace_dir.mkpath(trace_dir.absolutePath()))
        {
          throw mcqError(
            QObject::tr("mkpath %1 failed ").arg(trace_dir.absolutePath()));
        }
    }
  qDebug();
  return trace_dir;
}

void
XicTraceWriter::writeTrace(const DetectionResult &detection_result)
{
  qDebug() << "detection_result.p_msrun=" << detection_result.p_msrun;
  if(detection_result.p_quanti_item->isTracePeptideOn())
    {
      qDebug();
      QDir trace_dir = makeTraceDirectory(detection_result);
      qDebug();
      CalcWriterInterface *p_writer = nullptr;
      QString prefix =
        MonitorSpeedInterface::enumToString(detection_result.mbr_category);
      if(m_tracesFormat == McqTsvFormat::tsv)
        {
          p_writer = new TsvDirectoryWriter(
            QString("%1/%2.d")
              .arg(trace_dir.absolutePath())
              .arg(detection_result.p_quanti_item.get()->getTraceBaseName(
                prefix, mp_quantificator->getXmlId())));
        }
      else
        {
          p_writer = new OdsDocWriter(
            QString("%1/%2.ods")
              .arg(trace_dir.absolutePath())
              .arg(detection_result.p_quanti_item.get()->getTraceBaseName(
                prefix, mp_quantificator->getXmlId())));
        }

      qDebug();
      XicTraceBase *p_xic_trace =
        detection_result.p_quanti_item->newXicTrace(p_writer, detection_result);
      if(p_xic_trace != nullptr)
        {
          qDebug();
          for(auto &&peak : detection_result.sp_traceResult.get()->peakList)
            {
              p_xic_trace->addXicPeak(peak);
            }

          qDebug();

          if(detection_result.mbr_category != PeakQualityCategory::missed)
            {
              if(detection_result.sp_traceResult.get()->matchedPeakPosition <
                 detection_result.sp_traceResult.get()->peakList.size())
                {
                  p_xic_trace->setMatchedPeak(
                    detection_result.sp_traceResult.get()
                      ->peakList[detection_result.sp_traceResult.get()
                                   ->matchedPeakPosition]);
                }
              qDebug();
              p_xic_trace->setAlignedMatchedPeak(
                *(detection_result.sp_alignedPeak.get()));
              qDebug();
            }

          p_xic_trace->setMsMsRtList(
            detection_result.sp_traceResult.get()->rtList);
          qDebug();
          p_xic_trace->setXic(
            detection_result.sp_traceResult.get()->sp_xicCoord);
          qDebug();
          p_xic_trace->close();
          delete p_xic_trace;
        }
    }

  qDebug();
}
