/**
 * \file src/output/xictracewriter.h
 * \date 1/7/2022
 * \author Olivier Langella
 * \brief one object to rule traces output
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDir>
#include "../mcq_types.h"
#include <memory>
#include "../lib/peptides/peptide.h"
#include "detectionresult.h"

class XicTraceWriter;

typedef std::shared_ptr<XicTraceWriter> XicTraceWriterSp;

/**
 * @todo write docs
 */
class XicTraceWriter
{
  public:
  /**
   * Default constructor
   */
  XicTraceWriter(const Quantificator *p_quantificator);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  XicTraceWriter(const XicTraceWriter &other);

  /**
   * Destructor
   */
  ~XicTraceWriter();

  void setTracesDirectory(const QDir &directory);
  void setTracesFormat(McqTsvFormat format);
  const QDir &getTracesDirectory() const;
  McqTsvFormat getTracesFormat() const;

  void writeTrace(const DetectionResult &result);

  private:
  const QDir makeTraceDirectory(const DetectionResult &result) const;

  private:
  QDir m_tracesDirectory;
  McqTsvFormat m_tracesFormat;
  const Quantificator *mp_quantificator = nullptr;
};
