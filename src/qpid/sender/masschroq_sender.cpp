#include <qpid/messaging/Address.h>
#include <qpid/messaging/Connection.h>
#include <qpid/messaging/Message.h>
#include <qpid/messaging/Receiver.h>
#include <qpid/messaging/Sender.h>
#include <qpid/messaging/Session.h>

#include <cstdlib>
#include <iostream>

#include <sstream>

#include <QCoreApplication>
#include <QFile>
#include <QLocale>
#include <QString>
#include <QStringList>
#include <QTextStream>

using namespace qpid::messaging;
using namespace std;

int
main(int argc, char **argv)
{

  QCoreApplication app(argc, argv);
  QLocale::setDefault(QLocale::system());
  QStringList arguments_total(app.arguments());
  QStringList arguments;

  QString inputFile = "";
  QStringList::const_iterator iter_args;
  for(iter_args = arguments_total.begin(); iter_args != arguments_total.end();
      ++iter_args)
    {
      if(*iter_args == "-i")
        {
          ++iter_args;
          inputFile = *iter_args;
        }
      else
        {
          arguments.append(*iter_args);
        }
    }
  if(inputFile == "")
    {
      std::cout << "please specify input file with -i option" << std::endl;
      return 1;
    }

  QString broker = "localhost:5672";
  if(arguments.size() > 1)
    {
      broker = arguments.at(1);
    }

  // si addresse=topic les messages sant envoyés à l'exchange topic
  // donc si pas de receveur au bout le message est perdu
  // faut mettre une file en adresse si on veut que le message reste
  // QString address = "amq.topic";
  // if (arguments.size() > 2) {
  //	address = arguments.at(2);
  //}

  QString connectionOptions = "";
  if(arguments.size() > 4)
    {
      connectionOptions = arguments.at(4);
    }

  qpid::messaging::Connection connection(broker.toStdString(),
                                         connectionOptions.toStdString());
  try
    {
      QFile file(inputFile);
      if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
          std::cout << "unable to read input file" << std::endl;
          return 1;
        }

      QTextStream in(&file);
      QString mc_content = in.readAll();
      file.close();

      connection.open();
      qpid::messaging::Session session = connection.createSession();

      Address request_address("masschroq_request_queue; {create:always}");

      // In the C++ client, if the address starts with the
      // character #, it is given a unique name.
      Address response_address(
        "#response-queue; {create:always, delete:always}");
      qpid::messaging::Sender sender = session.createSender(request_address);

      qpid::messaging::Receiver receiver =
        session.createReceiver(response_address);

      qpid::messaging::Message message;
      message.setReplyTo(response_address);
      message.setContent(mc_content.toStdString());
      sender.send(message);
      Message response = receiver.fetch();
      std::cout << response.getContent() << std::endl;
      connection.close();
      return 0;
    }
  catch(const std::exception &error)
    {
      std::cout << error.what() << std::endl;
      connection.close();
    }
  return 1;
}
