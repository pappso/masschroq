/**
 * \file xmlSimpleParser.cpp
 * \date November 23, 2010
 * \author Edlira Nano
 */

#include "xmlSimpleParser.h"
#include <QDebug>
#include <pappsomspp/utils.h>
#include <pappsomspp/pappsoexception.h>
#include <QThread>

XmlSimpleParserHandlerInterface::XmlSimpleParserHandlerInterface(
  pappso::MsRunReaderCstSPtr msrun_reader, Msrun *p_msrun)
{
  _p_msrun        = p_msrun;
  msp_msrunReader = msrun_reader;
}
bool
XmlSimpleParserHandlerInterface::needPeakList() const
{
  return false;
}
void
XmlSimpleParserHandlerInterface::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{

  if(qspectrum.getMsLevel() > 1)
    {

      PrecursorSPtr precursor = std::make_shared<Precursor>(
        qspectrum.getPrecursorSpectrumIndex(),
        qspectrum.getPrecursorIntensity(),
        msp_msrunReader.get()->newXicCoordSPtrFromQualifiedMassSpectrum(
          qspectrum, pappso::PrecisionFactory::getPpmInstance(10)));


      qDebug() << QThread::currentThreadId()
               << " native_id=" << qspectrum.getMassSpectrumId().getNativeId()
               << " scan idx="
               << qspectrum.getMassSpectrumId().getSpectrumIndex()
               << " precursor_idx=" << qspectrum.getPrecursorSpectrumIndex()
               << " precursor.get()->getXicCoordSPtr().get()->toString="
               << precursor.get()->getXicCoordSPtr().get()->toString();
      if(precursor.get()->getXicCoordSPtr().get() == nullptr)
        {
          throw pappso::PappsoException(
            QObject::tr("precursor.get()->getXicCoordSPtr().get() == "
                        "nullptr\nindex=%1 %2 %3 %4")
              .arg(qspectrum.getMassSpectrumId().getNativeId())
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      /// adding this new precursor to the msrun's hash map of scan_num ->
      /// precursor
      qDebug() << QThread::currentThreadId();
      if(qspectrum.getMassSpectrumId().getNativeId().contains("scan="))
        {
          qDebug() << QThread::currentThreadId();
          _p_msrun->mapPrecursor(
            pappso::Utils::extractScanNumberFromMzmlNativeId(
              qspectrum.getMassSpectrumId().getNativeId()),
            precursor);
          _p_msrun->mapSpectrumIndexToPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);
          qDebug() << QThread::currentThreadId();
        }
      else
        {
          qDebug() << QThread::currentThreadId();
          _p_msrun->mapPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);

          _p_msrun->mapSpectrumIndexToPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);
          qDebug() << QThread::currentThreadId();
        }
      qDebug() << QThread::currentThreadId();
    }
}

bool
XmlSimpleParserHandlerInterface::isReadAhead() const
{
  return true;
}
