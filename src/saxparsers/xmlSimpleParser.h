/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file xmlSimpleParser.h
 * \date November 23, 2010
 * \author Edlira Nano
 */

#pragma once

#include "../lib/msrun/msrun.h"
#include <pappsomspp/msrun/msrunreader.h>

class XmlSimpleParserHandlerInterface
  : public pappso::SpectrumCollectionHandlerInterface
{

  public:
  XmlSimpleParserHandlerInterface(pappso::MsRunReaderCstSPtr msrun_reader,
                                  Msrun *p_msrun);

  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &qspectrum) override;
  virtual bool needPeakList() const override;
  virtual bool isReadAhead() const override;

  private:
  Msrun *_p_msrun;
  pappso::MsRunReaderCstSPtr msp_msrunReader;
};
