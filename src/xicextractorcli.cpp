/**
 * \file xicextractorcli.cpp
 * \date 19/02/2023
 * \author Olivier Langella
 * \brief Extract XIC from msrun
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./src/xicextractor --mz 800 --precision "10 ppm" --im-begin 300 --im-end 350
// --rt 30 --filters "passQuantileBasedRemoveY|0.6"
// /gorgone/pappso/versions_logiciels_pappso/masschroq/donnees/PXD014777_maxquant_timstof/20180809_120min_200ms_WEHI25_brute20k_timsON_100ng_HYE124A_Slot1-7_1_890.d

#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFileInfo>
#include <QElapsedTimer>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/xicextractor/msrunxicextractorfactory.h>
#include <pappsomspp/processing/uimonitor/uimonitortext.h>
#include <pappsomspp/msrun/xiccoord/xiccoordtims.h>
#include <odsstream/tsvoutputstream.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include "xicextractorcli.h"

XicExtractorCli::XicExtractorCli(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
XicExtractorCli::run()
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  /* ./src/pt-mzxmlconverter -i
     /gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
     -o
     /gorgone/pappso/versions_logiciels_pappso/i2masschroq/bruker/200ngHeLaPASEF_2min.mzXML
     */

  //./src/pt-mzxmlconverter -i
  /// gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
  //-o /tmp/test.xml


  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QTextStream outputStream(stdout, QIODevice::WriteOnly);

  try
    {
      // qDebug();
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(
        QString("%1 ")
          .arg(QCoreApplication::applicationName())
          .append("xicextractor")
          .append(" (")
          .append(MASSCHROQ_VERSION)
          .append(")"));

      parser.addHelpOption();
      parser.addVersionOption();

      QCommandLineOption mzStr(QStringList() << "mz",
                               QCoreApplication::translate("mz", "m/z"),
                               QCoreApplication::translate("mz", "m/z"));

      QCommandLineOption precisionStr(
        QStringList() << "precision",
        QCoreApplication::translate(
          "precision",
          "mass precision is of the form : \"nominal_value unit\", example : "
          "\"0.02 dalton\""),
        QCoreApplication::translate("precision", "m/z"));

      QCommandLineOption imBegin(
        QStringList() << "im-begin",
        QCoreApplication::translate("im-begin", "im scan number start"),
        QCoreApplication::translate("im-begin", "im-begin"));


      QCommandLineOption imEnd(
        QStringList() << "im-end",
        QCoreApplication::translate("im-end", "im scan number stop"),
        QCoreApplication::translate("im-end", "im-end"));

      QCommandLineOption rtTarget(
        QStringList() << "rt",
        QCoreApplication::translate("rt", "retention time"),
        QCoreApplication::translate("rt", "retention time"));


      QCommandLineOption rtRange(
        QStringList() << "rt-range",
        QCoreApplication::translate(
          "rt-range", "retention time range around target (default is 300)"),
        QCoreApplication::translate(
          "rt-range", "retention time range around target (default is 300)"));
      // time_range = 300;


      QCommandLineOption xicFilters(
        QStringList() << "filters",
        QCoreApplication::translate("filters",
                                    "filter suite string to apply on XIC. "
                                    "example \"passQuantileBasedRemoveY|0.6\""),
        QCoreApplication::translate(
          "filters",
          "filter suite string to apply on XIC. example "
          "\"passQuantileBasedRemoveY|0.6\""));


      parser.addOption(rtRange);
      parser.addOption(xicFilters);
      parser.addOption(rtTarget);
      parser.addOption(mzStr);
      parser.addOption(precisionStr);
      parser.addOption(imBegin);
      parser.addOption(imEnd);


      parser.addPositionalArgument(
        "<msrun file name>",
        QCoreApplication::translate(
          "main", "Full path to the peak list data file <msrun>."));
      // Process the actual command line arguments given by the user
      parser.process(*app);


      const QStringList positional_args = parser.positionalArguments();
      // qDebug();


      if(positional_args.size() > 1)
        {
          qFatal("\n\n\tOnly one MS run file needed.\n");
        }


      if(positional_args.size() < 1)
        {
          qFatal("\n\n\tMS run file needed.\n");
        }

      QFileInfo file_info(positional_args[0]);
      if(!file_info.exists())
        {
          QString msg = QString("\n\n\tFile %1 does not exist on disk.\n")
                          .arg(file_info.fileName());

          qFatal("\n\n\tFile '%s' not found.\n",
                 file_info.fileName().toLatin1().constData());
        }


      pappso::MsFileAccessor file_access(file_info.absoluteFilePath(), "file");

      file_access.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                             pappso::FileReaderType::tims_ms2);
      pappso::MsRunReaderSPtr msrun =
        file_access.getMsRunReaderSPtrByRunId("", "runa1");

      pappso::MsRunXicExtractorFactory::getInstance().setTmpDir("/tmp");


      std::shared_ptr<pappso::FilterSuiteString> xic_filter;
      if(!parser.value(xicFilters).isEmpty())
        {
          // filters_str = "passQuantileBasedRemoveY|0.6";
          xic_filter = std::make_shared<pappso::FilterSuiteString>(
            parser.value(xicFilters));
        }


      pappso::MzRange mass_range(
        parser.value(mzStr).toDouble(),
        pappso::PrecisionFactory::fromString(parser.value(precisionStr)));

      pappso::XicCoordSPtr xic_coord;

      if(msrun.get()->getMsRunId().get()->getMsDataFormat() ==
         pappso::MsDataFormat::brukerTims)
        {
          pappso::XicCoordTims xic_coord_tims;

          xic_coord_tims.scanNumBegin = parser.value(imBegin).toULong();
          xic_coord_tims.scanNumEnd   = parser.value(imEnd).toULong();
          xic_coord                   = xic_coord_tims.initializeAndClone();
        }
      else
        {
          pappso::XicCoord xic_coord_notims;
          xic_coord = xic_coord_notims.initializeAndClone();
        }
      xic_coord.get()->rtTarget = parser.value(rtTarget).toDouble();
      xic_coord.get()->mzRange  = mass_range;

      std::vector<pappso::XicCoordSPtr> xic_coord_list;
      xic_coord_list.push_back(xic_coord.get()->initializeAndClone());
      // MassRange mass_range_b(600.2, Precision::getPpmInstance(10));

      pappso::MsRunXicExtractorInterfaceSp xic_extractor =
        pappso::MsRunXicExtractorFactory::getInstance()
          .buildMsRunXicExtractorSp(msrun);

      xic_extractor->setXicExtractMethod(pappso::XicExtractMethod::max);

      if(xic_filter.get() != nullptr)
        {
          pappso::FilterInterfaceCstSPtr filters = xic_filter;
          xic_extractor.get()->setPostExtractionTraceFilterCstSPtr(filters);
        }

      xic_extractor.get()->setRetentionTimeAroundTarget(300);
      if(!parser.value(rtRange).isEmpty())
        {
          xic_extractor.get()->setRetentionTimeAroundTarget(
            parser.value(rtRange).toDouble());
        }


      QElapsedTimer timer;
      timer.start();
      QTextStream outputStream(stdout, QIODevice::WriteOnly);
      pappso::UiMonitorText monitor(outputStream);
      xic_extractor.get()->extractXicCoordSPtrList(monitor, xic_coord_list);
      pappso::XicSPtr xic_pwiz = xic_coord_list[0].get()->xicSptr;

      TsvOutputStream tsv_output(outputStream);
      tsv_output.writeSheet("XIC");
      tsv_output.writeCell("rt");
      tsv_output.writeCell("intensity");
      tsv_output.writeLine();
      for(std::size_t i = 0; i < xic_pwiz->size(); i++)
        {
          tsv_output.writeCell((*xic_pwiz)[i].x);
          tsv_output.writeCell((*xic_pwiz)[i].y);
          tsv_output.writeLine();
        }

      qDebug();
    }
  catch(pappso::PappsoException &error)
    {

      errorStream << QString("Oops! an error occurred in %1. Don't Panic :\n%2")
                       .arg(QCoreApplication::applicationName())
                       .arg(error.qwhat());

      errorStream << Qt::endl << Qt::endl;

      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {

      errorStream << QString("Oops! an error occurred in %1. Don't Panic :\n%2")
                       .arg(QCoreApplication::applicationName())
                       .arg(error.what());

      errorStream << Qt::endl << Qt::endl;

      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
XicExtractorCli::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
XicExtractorCli::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug();
  QCoreApplication app(argc, argv);
  qDebug();
  QCoreApplication::setApplicationName("xicextractor");
  QCoreApplication::setApplicationVersion(MASSCHROQ_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  XicExtractorCli myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug();


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
