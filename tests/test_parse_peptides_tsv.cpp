/**
 * \file tests/test_parse_peptides_tsv.cpp
 * \date 6/12/2021
 * \author Olivier Langella
 * \brief test text tabulated petpide file parsing
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [parse_peptide] -s

#include <QDebug>
#include <QString>

#include <catch2/catch_test_macros.hpp>
#include "config.h"

#include "../src/input/masschroqmlpeptideparser.h"
#include <QFileInfo>
#include <pappsomspp/pappsoexception.h>

TEST_CASE("parsing peptide tsv test suite", "[parse_peptide]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: peptide parser init ::..", "[parse_peptide]")
  {
    MasschroqmlPeptideParser translate_peptides(
      QFileInfo("peptides.masschroqml").absoluteFilePath());

    if(translate_peptides.readFile(
         QString("%1/doc/examples/example_orbitrap.masschroqML")
           .arg(CMAKE_SOURCE_DIR)))
      {
      }
    else
      {
        throw pappso::PappsoException(translate_peptides.errorString());
      }
  }
}
