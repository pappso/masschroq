
# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/Toolchain-mingw64.cmake ..

SET (NOGUI 0)
set (MINGW 1)

set(CMAKE_CXX_COMPILER "/mingw64/bin/g++.exe")


set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "/home/polipo/devel/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "/home/polipo/devel/libodsstream/build/src/libodsstream-qt5.so")


# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "/home/polipo/devel/quazip-0.7.3")
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "/home/polipo/devel/quazip-0.7.3/quazip/release/quazip.dll")
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers

set (QCustomPlot_FOUND 1)
set (QCustomPlot_INCLUDES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1")
set (QCustomPlot_LIBRARIES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1/build/libqcustomplot.so")

set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/polipo/devel/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "/home/polipo/devel/pappsomspp/build/src/libpappsomspp-qt5.so")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/polipo/devel/pappsomspp/build/src/pappsomspp/widget/libpappsomspp-widget-qt5.so")
