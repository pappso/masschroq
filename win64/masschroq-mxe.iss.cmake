[Setup]
AppName=MassChroQ

#define public winerootdir "z:"
; Set version number below
#define public version "${MASSCHROQ_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/masschroq/"
#define cmake_build_dir "z:/home/langella/developpement/git/masschroq/wbuild"

; Set version number below
AppVerName=MassChroQ version {#version}
DefaultDirName={pf}\masschroq
DefaultGroupName=masschroq
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=masschroq-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=masschroq-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2009-2020 Benoit Valot, Edlira Nano, Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="MassChroQ, mass chromatogram quantifier"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "z:/win64/mxeqt6_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/qt6/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs
Source: "z:/home/langella/developpement/git/cutelee/wbuild/templates/lib/libCuteleeQt6Templates.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/cutelee/wbuild/cutelee-qt6/6.1/*"; DestDir: {app}/cutelee-qt6/6.1; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: mcqComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: mcqComp
Source: "{#sourceDir}\win64\masschroq_icon.ico"; DestDir: {app}; Components: mcqComp

Source: "{#cmake_build_dir}\src\masschroq.exe"; DestDir: {app}; Components: mcqComp 
Source: "{#cmake_build_dir}\src\mcql\mcql.exe"; DestDir: {app}; Components: mcqComp 
Source: "{#cmake_build_dir}\src\mcql\mcqlcbor.exe"; DestDir: {app}; Components: mcqComp 
Source: "{#cmake_build_dir}\src\masschroq_gui.exe"; DestDir: {app}; Components: mcqComp
Source: "{#cmake_build_dir}\src\masschroq_studio.exe"; DestDir: {app}; Components: mcqComp
Source: "{#cmake_build_dir}\MassChroQ_user_manual.pdf"; DestDir: {app};

[Icons]
Name: "{group}\masschroq"; Filename: "{app}\masschroq_gui.exe"; WorkingDir: "{app}";IconFilename: "{app}\masschroq_icon.ico"
Name: "{group}\masschroq_studio"; Filename: "{app}\masschroq_studio.exe"; WorkingDir: "{app}";IconFilename: "{app}\masschroq_icon.ico"
Name: "{group}\Uninstall masschroq"; Filename: "{uninstallexe}"


[Types]
Name: "mcqType"; Description: "Full installation"

[Components]
Name: "mcqComp"; Description: "MassChroQ files and related documentation"; Types: mcqType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\masschroq_gui.exe"; Description: "Launch MassChroQ GUI"; Flags: postinstall nowait unchecked
